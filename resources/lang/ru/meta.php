<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы постраничного вывода
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки используются :meta эндпоинтами.
    |
    */

    'created_at' => 'Дата создания',
    'updated_at' => 'Дата обновления',

    'name' => 'Наименование',
    'title' => 'Название',
    'activity' => 'Активность',

    'status' => 'Статус',
    'code' => 'Код',
    'image' => 'Изображение',
    'sorting' => 'Сортировка',
    'type' => 'Тип',

    'temp_file_path' => 'Путь до временного файла',

    'price' => 'Цена',
    'allow_publish' => 'Публикация разрешена',

    'start_date' => 'Дата начала',
    'end_date' => 'Дата окончания',

    'is_migrated' => 'Сохранено/создано при миграции записей из мастер-сервисов',

    'pages' => [
        'link' => 'Ссылка',
        'beginning_of_publication' => 'Начало публикации',
        'end_of_publication' => 'Окончание публикации',
    ],

    'templates' => [
        'header' => 'Заголовок',
    ],

    'products' => [
        'product_id' => 'ID продукта',
        'product_name' => 'Название продукта',
        'article_number' => 'Артикул',
        'barcode_ean' => 'Штрихкод (EAN)',
        'allow_publish' => 'Публикация разрешена',
        'main_image_file' => 'Основной файл изображения',
        'category_id' => 'Идентификатор категории из Pim',
        'brand_id' => 'Идентификатор бренда из Pim',
        'code' => 'Код ЧПУ',
        'description' => 'Описание продукта',
        'type' => 'Тип продукта (вес, штука, ...)',
        'vendor_code' => 'Артикул',
        'weight' => 'Вес нетто (кг)',
        'weight_gross' => 'Вес брутто (кг)',
        'width' => 'Ширина (мм)',
        'height' => 'Высота (мм)',
        'length' => 'Длина (мм)',
        'is_adult' => 'Продукт 18+',
        'uom' => 'Единица измерения',
        'tariffing_volume' => 'Платежная единица',
        'order_step' => 'Шаг заказа',
        'order_minvol' => 'Минимальное значение для заказа',
        'image_id' => 'ID картинки из Pim',
        'file' => 'Файл картинки',
        'image_name' => 'Описание картинки',
        'sort' => 'Порядок сортировки',
        'product_property_value_id' => 'Идентификатор значения свойства продукта из Pim',
        'property_id' => 'Идентификатор свойства из Pim',
        'directory_value_id' => 'Идентификатор значения справочника из Pim',
        'value_type' => 'Тип значения',
        'value_name' => 'Название значения',
        'value' => 'Значение',
    ],

    'points' => [
        'delivery_service' => 'Сервис доставки',
        'title' => 'Название',
        'external_id' => 'Внешний идентификатор',
        'delivery_of_only_fully_paid_parcels' => 'Выдача только полностью оплаченных посылок',
        'possibility_of_payment_by_bank_card' => 'Возможность оплаты банковской картой',
        'courier_delivery' => 'Курьерская доставка',
        'is_postamat' => 'Является постаматом',
        'maximum_volume' => 'Максимальный объем',
        'maximum_weight' => 'Максимальный вес',
        'from' => 'От',
        'to' => 'До',
        'phone_number' => 'Номер телефона',
        'time_zone' => 'Часовой пояс',
        'metro_station' => 'Станция метро',
        'width' => 'Широта',
        'longitude' => 'Долгота',
        'city_id' => 'Идентификатор города',
    ],

    'delivery_prices' => [
        'federal_district' => 'Федеральный округ',
        'region' => 'Регион',
        'fias_of_the_region' => 'ФИАС региона',
        'delivery_service' => 'Служба доставки',
        'shipping_method' => 'Метод доставки',
    ],

    'delivery_kpi' => [
        'rtg' => 'Среднее время подтверждения заказа',
        'cc' => 'Среднее время ожидания начала сборки заказа',
        'ppt' => 'Среднее время сборки заказа',
    ],

    'orders' => [
        'order_number' => '№ заказа',
        'source' => 'Источник',
        'customer' => 'Покупатель',
        'customer_id' => 'ID покупателя',
        'basket_id' => 'ID корзины',
        'product_id' => 'ID товара',
        'qty' => 'Количество товара',
        'customer_email' => 'Почта покупателя',
        'price_before_discount' => 'Сумма до скидок',
        'price' => 'Сумма',
        'promo_code' => 'Промокод',
        'delivery_method' => 'Метод доставки',
        'delivery_address' => 'Адрес доставки',
        'receiver_name' => 'ФИО получателя',
        'receiver_phone' => 'Телефон получателя',
        'receiver_email' => 'Почта получателя',
        'status_at' => 'Дата изменения статуса',
        'payment_status' => 'Статус оплаты',
        'payment_status_at' => 'Дата изменения статуса оплаты',
        'payed_at' => 'Дата оплаты',
        'payment_expires_at' => 'Дата просрочки оплаты',
        'payment_method' => 'Метод оплаты',
        'payment_system' => 'Система оплаты',
        'payment_external_id' => 'ID оплаты во внешней системе',
        'is_changed' => 'Заказ был изменён',
        'is_editable' => 'Заказ может быть изменён',
        'is_expired' => 'Заказ просрочен',
        'is_expired_at' => 'Дата, когда заказ был просрочен',
        'is_return' => 'Заказ возвращен',
        'is_return_at' => 'Дата изменения признака "Заказ возвращен"',
        'is_partial_return' => 'Заказ возвращен частично',
        'is_partial_return_at' => 'Дата изменения признака "Заказ возвращен частично"',
        'is_problem' => 'Заказ проблемный',
        'is_problem_at' => 'Дата изменения признака "Заказ проблемный"',
        'problem_comment' => 'Комментарий о проблеме',
        'delivery_price' => 'Цена доставки',
        'delivery_comment' => 'Комментарий к доставке',
        'client_comment' => 'Комментарий клиента',
    ],

    'settings' => [
        'code' => 'Символьный код',
        'value' => 'Значение',
    ],

    'refunds' => [
        'order_id' => 'Номер заказа',
        'channel' => 'Канал',
    ],

    'brands' => [
        'brand_id' => 'ID бренда из Pim',
        'code' => 'Код шаблона',
        'description' => 'Описание',
    ],

    'categories' => [
        'parent_id' => 'ID родителя',
        'is_active' => 'Активна',
        'is_real_active' => 'Фактически активна',
        'is_inherits_properties' => 'Наследует атрибуты',
        'has_is_gluing' => 'Есть атрибут для склейки',
    ],

    'actual_category_properties' => [
        'category_id' => 'ID категории',
        'property_id' => 'ID атрибута',
        'is_required' => 'Этот атрибут должен быть заполнен',
        'is_gluing' => 'Это свойство используется для склеивания изделий',
        'is_inherited' => 'Свойство унаследовано от родительской категории',
        'is_common' => 'Это свойство является общим для всех категорий',
    ],

    'properties' => [
        'name' => 'Рабочее название',
        'display_name' => 'Публичное название',
        'type' => 'Тип данных',
        'property_id' => 'ID атрибута из Pim',
        'is_active' => 'Активный',
        'is_public' => 'Выводить на витрине',
        'is_required' => 'Обязательность',
        'is_gluing' => 'Параметр склеивания',
        'is_filterable' => 'Фильтр на витрине',
        'is_multiple' => 'Множественный',
        'has_directory' => 'Справочник',
    ],

    'property_directory_values' => [
        'directory_value_id' => 'ID значения справочника из Pim',
        'property_id' => 'ID свойства из Pim',
        'value' => 'Значение',
        'type' => 'Тип хранимого значения',
    ],

    'drafts' => [
        'barcode' => 'Штрихкод',
        'code' => 'Код товара',
        'main_image_file' => 'Изображение',
        'vendor_code' => 'Артикул',
        'type' => 'Тип товара',
        'status_id' => 'Статус товара',
        'is_adult' => 'Товар 18+',
        'category_id' => 'Категория',
        'brand_id' => 'Бренд',
        'external_id' => 'Внешний код товара',
        'weight' => 'Масса нетто, кг',
        'weight_gross' => 'Масса брутто, кг',
        'length' => 'Длина, мм',
        'width' => 'Ширина, мм',
        'height' => 'Высота, мм',
        'has_no_filled_required_attributes' => 'Есть ли у товара хотя бы один незаполненный обязательный атрибут',
    ],

    'stores' => [
        'store_id' => 'Название магазина',
        'qty' => 'Остаток',
        'seller_id' => 'Продавец',
        'address' => 'Полный адрес',
    ],

    'product_groups' => [
        'products_count' => 'Количество товаров',
        'main_product_image' => 'Изображение главного товара',
        'category_id' => 'Название категории',
        'main_product_id' => 'Название главного товара',
        'product_barcode' => 'Штрихкод входящего в склейку товара',
        'product_vendor_code' => 'Артикул входящего в склейку товара',

        'product_group_product_id' => 'ID связи товарной склейки и товара из Pim',
        'product_group_id' => 'ID товарной склейки из Pim',
        'product_id' => 'ID товара из Pim',
        'is_active' => 'Признак активности из Pim',
        'db_main_product_id' => 'ID главного товара из Pim',
        'db_category_id' => 'ID категории из Pim',
    ],

    'product_events' => [
        'event_id' => 'ID события',
        'product_id' => 'ID товара',
    ],

    'operations' => [
        'action' => 'Действие',
        'entity' => 'Сущность',
        'ids_string' => 'Обрабатываемые сущности',
    ],

    'product_imports' => [
        'file_name' => 'Название файла',
        'status' => 'Статус импорта',
    ],

    'imports' => [
        'import_type' => 'Тип импорта',
        'vendor_code' => 'Артикул товара',
        'message' => 'Сообщение',
    ],

    'product_statuses' => [
        'color' => 'Цвет',
        'is_active' => 'Активна',
        'previous_status_ids' => 'Переход со статусов',
        'next_status_ids' => 'Переход в статусы',
    ],

    'reviews' => [
        'grade' => 'Оценка (от 1 до 6)',
        'status_id' => 'Статус публикации отзыва',
        'comment' => 'Комментарий',
    ],

    'customers' => [
        'last_name' => 'Фамилия',
        'first_name' => 'Имя',
        'middle_name' => 'Отчество',
        'full_name' => 'Полное ФИО',
        'user_id' => 'Пользователь',
        'manager_id' => 'Менеджер',
        'yandex_metric_id' => 'ID пользователя в YandexMetric',
        'google_analytics_id' => 'ID пользователя в GoogleAnalytics',
        'is_deleted' => 'Пользователь удален',
        'email' => 'Почта',
        'phone' => 'Телефон',
        'gender' => 'Гендер',
        'create_by_admin' => 'Пользователь создан администратором',
        'city' => 'Город',
        'birthday' => 'Дата рождения',
        'last_visit_date' => 'Дата последней авторизации',
        'comment_status' => 'Комментарий к статусу',
        'timezone' => 'Временная зона',
    ],

    'feeds' => [
        'file_url' => 'Файл фида',
        'planned_delete_at' => 'Плановая дата удаления',
    ],

    'feed_settings' => [
        'name' => 'Наименование шаблона',
        'platform' => 'Платформа',
    ],

    'promo_codes' => [
        'counter' => 'Доступное количество применений',
        'current_counter' => 'Текущее количество применений',
    ],

    'discounts' => [
        'type' => 'Тип скидки',
        'value_type' => 'Тип значения',
        'value' => 'Размер скидки',
        'promo_code_only' => 'Только по промокоду',
        'product_id' => 'Название продукта',
    ],

    'offers' => [
        'offer_id' => 'ID оффера из Pim',
        'product_id' => 'ID товара из Pim',
        'is_active' => 'Последний статус активности оффера',
        'price' => 'Окончательная цена со скидкой',
        'base_price' => 'Базовая цена',
    ],

    'notifications' => [
        'event' => 'Событие',
        'channels' => 'Канал',
        'theme' => 'Тема',
    ],

    'sellers' => [
        'legal_name' => 'Юридическое наименование',
        'legal_address' => 'Юридический адрес',
    ],

    'banner_types' => [
        'active' => 'Активность',
    ],

    'failed_jobs' => [
        'uuid' => 'Идентификатор задания',
        'connection' => 'Соединение',
        'queue' => 'Очередь',
        'payload' => 'Тело сообщения',
        'exception' => 'Текст исключения',
        'failed_at' => 'Время сбоя задачи',
    ],

    'nameplates' => [
        'nameplate_id' => 'ID тега из CMS',
        'nameplate_product_id' => 'Связки с тегов и продуктов из CMS',
        'product_id' => 'ID товара из Pim',
        'background_color' => 'Цвет фона',
        'text_color' => 'Цвет текста',
        'is_active' => 'Активен',
    ],

    'indexer_timestamps' => [
        'index' => 'Базовое название индекса (товары, категории)',
        'stage' => 'Ветка, в которой происходит индексация',
        'index_hash' => 'Хэш индекса',
        'last_schedule' => 'Последнее индексирование',
        'is_current_index' => 'Этот параметр предназначен для текущего хэша',
        'is_current_stage' => 'Этот параметр предназначен для текущего APP_STAGE',
        'is_current' => 'Эта настройка предназначена для текущих настроек',
    ],
];
