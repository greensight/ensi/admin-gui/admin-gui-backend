<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Meta Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by :meta endpoints.
    |
    */

    'created_at' => 'Date of creation',
    'updated_at' => 'Date of update',

    'name' => 'Name',
    'title' => 'Title',
    'activity' => 'Activity',

    'status' => 'Status',
    'code' => 'Code',
    'image' => 'Image',
    'sorting' => 'Sorting',
    'type' => 'Type',

    'temp_file_path' => 'Path to temp file',

    'price' => 'Price',
    'allow_publish' => 'Publication is allowed',

    'start_date' => 'Start date',
    'end_date' => 'End date',

    'is_migrated' => 'Saved/created during migration of records from master services',

    'pages' => [
        'link' => 'Link',
        'beginning_of_publication' => 'The beginning of publication',
        'end_of_publication' => 'End of publication',
    ],

    'templates' => [
        'header' => 'Header',
    ],

    'products' => [
        'product_id' => 'Product ID',
        'product_name' => 'Product Name',
        'article_number' => 'Article number',
        'barcode_ean' => 'Barcode (EAN)',
        'allow_publish' => 'Publication allowed',
        'main_image_file' => 'Main image file',
        'category_id' => 'Category ID from Pim',
        'brand_id' => 'Brand ID from Pim',
        'code' => 'CNC code',
        'description' => 'Product description',
        'type' => 'Product type (weight, piece, ...)',
        'vendor_code' => 'Article number',
        'weight' => 'Net weight (kg)',
        'weight_gross' => 'Gross weight (kg)',
        'width' => 'Width (mm)',
        'height' => 'Height (mm)',
        'length' => 'Length (mm)',
        'is_adult' => 'Product 18+',
        'uom' => 'The unit of measurement',
        'tariffing_volume' => 'The billing unit',
        'order_step' => 'Order step',
        'order_minvol' => 'Minimum value for order',
        'image_id' => 'Image ID from Pim',
        'file' => 'Image file',
        'image_name' => 'Image description',
        'sort' => 'Sorting order',
        'product_property_value_id' => 'Product property value ID from Pim',
        'property_id' => 'Property ID from Pim',
        'directory_value_id' => 'Directory value ID from Pim',
        'value_type' => 'Value type',
        'value_name' => 'Value name',
        'value' => 'Value',
    ],

    'points' => [
        'delivery_service' => 'Delivery service',
        'title' => 'Title',
        'external_id' => 'External ID',
        'delivery_of_only_fully_paid_parcels' => 'Delivery of only fully paid parcels',
        'possibility_of_payment_by_bank_card' => 'Possibility of payment by bank card',
        'courier_delivery' => 'Courier delivery',
        'is_postamat' => 'Is a pickup point',
        'maximum_volume' => 'Maximum volume',
        'maximum_weight' => 'Maximum weight',
        'from' => 'From',
        'to' => 'To',
        'phone_number' => 'Phone number',
        'time_zone' => 'Time zone',
        'metro_station' => 'Metro station',
        'width' => 'Width',
        'longitude' => 'Longitude',
        'city_id' => 'City ID',
    ],

    'delivery_prices' => [
        'federal_district' => 'Federal District',
        'region' => 'Region',
        'fias_of_the_region' => 'FIAS of the region',
        'delivery_service' => 'Delivery Service',
        'shipping_method' => 'Shipping Method',
    ],

    'delivery_kpi' => [
        'rtg' => 'Average order confirmation time',
        'cc' => 'The average start time of the assembly since the creation of orders',
        'ppt' => 'Average order assembly time',
    ],

    'orders' => [
        'order_number' => 'Order No.',
        'source' => 'A source',
        'customer' => 'Buyer',
        'customer_id' => 'Customer ID',
        'basket_id' => 'Basket ID',
        'product_id' => 'Product ID',
        'qty' => 'Product quantity',
        'customer_email' => 'Customer email',
        'price_before_discount' => 'Amount before discounts',
        'price' => 'Price',
        'promo_code' => 'Promo code',
        'delivery_method' => 'Delivery method',
        'delivery_address' => 'Delivery address',
        'receiver_name' => "Recipient's full name",
        'receiver_phone' => "Recipient's phone number",
        'receiver_email' => "Recipient's email",
        'status_at' => 'Date of status change',
        'payment_status' => 'Payment status',
        'payment_status_at' => 'Date of payment status change',
        'payed_at' => 'Date of payment',
        'payment_expires_at' => 'Date of payment expires',
        'payment_method' => 'Payment method',
        'payment_system' => 'Payment system',
        'payment_external_id' => 'Payment ID in the external system',
        'is_changed' => 'The order has been changed',
        'is_editable' => 'The order can be changed',
        'is_expired' => 'The order is overdue',
        'is_expired_at' => 'The date when the order was overdue',
        'is_return' => 'The order has been returned',
        'is_return_at' => 'Date of change of the "Order returned" sign',
        'is_partial_return' => 'The order has been partially refunded',
        'is_partial_return_at' => 'Date of change of the "Partially returned order" sign',
        'is_problem' => 'The order is problematic',
        'is_problem_at' => 'Date of change of the "Problematic order" attribute',
        'problem_comment' => 'Comment about the problem',
        'delivery_price' => 'Shipping cost',
        'delivery_comment' => 'Delivery comment',
        'client_comment' => "Customer's comment",
    ],

    'settings' => [
        'code' => 'Character code',
        'value' => 'Value',
    ],

    'refunds' => [
        'order_id' => 'Order number',
        'channel' => 'Channel',
    ],

    'brands' => [
        'brand_id' => 'Brand ID from Pim',
        'code' => 'Template code',
        'description' => 'Description',
    ],

    'categories' => [
        'parent_id' => "Parent's ID",
        'is_active' => 'Active',
        'is_real_active' => 'Actually active',
        'is_inherits_properties' => 'Inherits attributes',
        'has_is_gluing' => 'There is an attribute for gluing',
    ],

    'actual_category_properties' => [
        'category_id' => 'Category ID',
        'property_id' => 'Property ID',
        'is_required' => 'The attribute must be filled in',
        'is_gluing' => 'The property is used for gluing products',
        'is_inherited' => 'The property is inherited from parent category',
        'is_common' => 'The property is common to all categories',
    ],

    'properties' => [
        'name' => 'Working title',
        'display_name' => 'Public name',
        'type' => 'Data type',
        'property_id' => 'Property ID from Pim',
        'is_active' => 'Active',
        'is_public' => 'Display on the showcase',
        'is_required' => 'Obligatory',
        'is_gluing' => 'Gluing parameter',
        'is_filterable' => 'The filter is on display',
        'is_multiple' => 'Multiple',
        'has_directory' => 'Guide',
    ],

    'property_directory_values' => [
        'directory_value_id' => 'Directory value id from Pim',
        'property_id' => 'Property id from Pim',
        'value' => 'Value',
        'type' => 'Type of stored value',
    ],

    'drafts' => [
        'barcode' => 'Barcode',
        'code' => 'Product code',
        'main_image_file' => 'Image',
        'vendor_code' => 'Article number',
        'type' => 'Product type',
        'status_id' => 'Product status',
        'is_adult' => 'Product 18+',
        'category_id' => 'Category',
        'brand_id' => 'Brand',
        'external_id' => 'External product code',
        'weight' => 'Net weight, kg',
        'weight_gross' => 'Gross weight, kg',
        'length' => 'Length, mm',
        'width' => 'Width, mm',
        'height' => 'Height, mm',
        'has_no_filled_required_attributes' => 'Does the product have at least one blank required attribute',
    ],

    'stores' => [
        'store_id' => 'Store name',
        'qty' => 'Remains',
        'seller_id' => 'Seller',
        'address' => 'Full address',
    ],

    'product_groups' => [
        'products_count' => 'Number of products',
        'main_product_image' => 'The image of the main product',
        'category_id' => 'Category name',
        'main_product_id' => 'The name of the main product',
        'product_barcode' => 'Barcode of the product included in the gluing',
        'product_vendor_code' => 'The article of the product included in the gluing',

        'product_group_product_id' => 'ID of the product link and the product from Pim',
        'product_group_id' => 'Product gluing ID from Pim',
        'product_id' => 'Product ID from Pim',
        'is_active' => 'Activity from Pim',
        'db_main_product_id' => 'Main product ID from Pim',
        'db_category_id' => 'Category ID from Pim',
    ],

    'product_events' => [
        'event_id' => 'Event ID',
        'product_id' => 'Product ID',
    ],

    'operations' => [
        'action' => 'Action',
        'entity' => 'Entity',
        'ids_string' => 'Processed entities',
    ],

    'product_imports' => [
        'file_name' => 'File name',
        'status' => 'Import status',
    ],

    'imports' => [
        'import_type' => 'Type of import',
        'vendor_code' => 'Article number',
        'message' => 'Message',
    ],

    'product_statuses' => [
        'color' => 'Colour',
        'is_active' => 'Active',
        'previous_status_ids' => 'Switching from statuses',
        'next_status_ids' => 'Switching to statuses',
    ],

    'reviews' => [
        'grade' => 'Rating (from 1 to 6)',
        'status_id' => 'Review publication status',
        'comment' => 'Comment',
    ],

    'customers' => [
        'last_name' => 'Surname',
        'first_name' => 'Name',
        'middle_name' => 'Middle name',
        'full_name' => 'Full name',
        'user_id' => 'User',
        'manager_id' => 'Manager',
        'yandex_metric_id' => 'User ID in YandexMetric',
        'google_analytics_id' => 'User ID in GoogleAnalytics',
        'is_deleted' => 'The user has been deleted',
        'email' => 'Mail',
        'phone' => 'Phone',
        'gender' => 'Gender',
        'create_by_admin' => 'The user was created by the administrator',
        'city' => 'City',
        'birthday' => 'Birthday',
        'last_visit_date' => 'Date of last authorization',
        'comment_status' => 'Comment on the status',
        'timezone' => 'Time zone',
    ],

    'feeds' => [
        'file_url' => 'Feed file',
        'planned_delete_at' => 'Planned date of deletion',
    ],

    'feed_settings' => [
        'name' => 'Name of the template',
        'platform' => 'Platform',
    ],

    'promo_codes' => [
        'counter' => 'Available number of applications',
        'current_counter' => 'Current number of applications',
    ],

    'discounts' => [
        'type' => 'Type of discount',
        'value_type' => 'Type of value',
        'value' => 'The discount amount',
        'promo_code_only' => 'Only by promo code',
        'product_id' => 'Product name',
    ],

    'offers' => [
        'offer_id' => 'Offer ID from Pim',
        'product_id' => 'Product ID from Pim',
        'is_active' => 'The final activity of the offer',
        'price' => 'The final discounted price',
        'base_price' => 'Base price',
    ],

    'notifications' => [
        'event' => 'Event',
        'channels' => 'Channel',
        'theme' => 'Topic',
    ],

    'sellers' => [
        'legal_name' => 'Legal name',
        'legal_address' => 'Legal address',
    ],

    'banner_types' => [
        'active' => 'Active',
    ],

    'failed_jobs' => [
        'uuid' => 'Task ID',
        'connection' => 'Connection',
        'queue' => 'Queue',
        'payload' => 'Message body',
        'exception' => 'Exception test',
        'failed_at' => 'Task failure time',
    ],

    'nameplates' => [
        'nameplate_id' => 'Nameplate ID from CMS',
        'nameplate_product_id' => 'Bundles of nameplate and product from CMS',
        'product_id' => 'Product ID from Pim',
        'background_color' => 'Background color',
        'text_color' => 'Text color',
        'is_active' => 'Active',
    ],

    'indexer_timestamps' => [
        'index' => 'The base name of the index (products, categories)',
        'stage' => 'The branch where indexing takes place',
        'index_hash' => 'Index hash',
        'last_schedule' => 'Last schedule',
        'is_current_index' => 'This setting is for the current hash',
        'is_current_stage' => 'This setting is for the current APP_STAGE',
        'is_current' => 'This setting is for current settings',
    ],
];
