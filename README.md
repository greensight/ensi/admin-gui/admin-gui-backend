# Admin GUI Backend

## Main info

Name: Admin GUI Backend  
Domain: Admin GUI  
Purpose: Backend for administration panel  

## Development

Instructions describing how to deploy, launch and test the service on a local machine can be found in a separate document at [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

The regulations for working on tasks are also in [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

### Authorization settings

To configure user authorization, you need to add the variables *UNITS_ADMIN_AUTH_SERVICE_CLIENT_ID* and *UNITS_ADMIN_AUTH_SERVICE_CLIENT_SECRET* to the .env file. The values of these variables can be obtained:
1. From the console when creating a client in the corresponding *-auth service (for more information on creating a client, see the readme of the corresponding *-auth service)

2. If the client has already been generated, then in the *oauth_clients* table in the corresponding *-auth service

## Service structure

You can read about the service structure [here](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Dependencies

| Name              | Description                                                  | Environment variables                                                                                                                                                                                    |
|-------------------|--------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Ensi services** | **Ensi services with which this service communicates**       |
| Catalog           | Ensi Offers<br/>Ensi PIM<br/>Ensi Feed<br/>Catalog Cache     | CATALOG_OFFERS_SERVICE_HOST<br/>CATALOG_PIM_SERVICE_HOST<br/>CATALOG_FEED_SERVICE_HOST<br/>CATALOG_CATALOG_CACHE_SERVICE_HOST                                                                                                              |
| Communication     | Ensi Communication Manager<br/>Ensi Internal Messenger       | COMMUNICATION_COMMUNICATION_SERVICE_HOST<br/>COMMUNICATION_INTERMAL_MESSAGES_SERVICE_HOST                                                                                                                |
| Customers         | Ensi Customers<br/>Ensi Customer Auth<br/>Ensi CRM           | CUSTOMERS_CUSTOMERS_SERVICE_HOST<br/>CUSTOMERS_CUSTOMER_AUTH_SERVICE_HOST<br/>CUSTOMERS_CUSTOMER_AUTH_SERVICE_CLIENT_ID<br/>CUSTOMERS_CUSTOMER_AUTH_SERVICE_CLIENT_SECRET<br/>CUSTOMERS_CRM_SERVICE_HOST |
| CMS               | Ensi Cms                                                     | CMS_CMS_SERVICE_HOST                                                                                                                                                                                     |
| Logistic          | Ensi Logistic                                                | LOGISTIC_LOGISTIC_SERVICE_HOST                                                                                                                                                                           |
| Marketing         | Ensi Marketing                                               | MARKETING_MARKETING_SERVICE_HOST                                                                                                                                                                         |
| Orders            | Ensi OMS<br/>Ensi Baskets                                    | ORDERS_OMS_SERVICE_HOST<br/>ORDERS_BASKETS_SERVICE_HOST                                                                                                                                                  |
| Units             | Ensi Admin Auth<br/>Ensi Business Units<br/>Ensi Seller Auth | UNITS_ADMIN_AUTH_SERVICE_HOST<br/>UNITS_ADMIN_AUTH_SERVICE_CLIENT_ID<br/>UNITS_ADMIN_AUTH_SERVICE_CLIENT_SECRET<br/>UNITS_BU_SERVICE_HOST<br/>UNITS_SELLER_AUTH_SERVICE_HOST                             |
| Reviews           | Ensi Reviews                                                 | REVIEWS_REVIEWS_SERVICE_HOST                                                                                                                                                                             |


## Environments

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/admin-gui/job/admin-gui-backend/  
URL: https://admin-gui-backend-master-dev.ensi.tech/docs/swagger  

### Preprod

N/A

### Prod

N/A

## Contacts

The team supporting this service: https://gitlab.com/groups/greensight/ensi/-/group_members

Email: mail@greensight.ru

## License

[Open license for the right to use the Greensight Ecom Platform (GEP) computer program](LICENSE.md).
