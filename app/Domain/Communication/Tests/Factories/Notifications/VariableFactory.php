<?php

namespace App\Domain\Communication\Tests\Factories\Notifications;

use Ensi\CommunicationManagerClient\Dto\Variable;
use Ensi\LaravelTestFactories\BaseApiFactory;

class VariableFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->sentence(1),
            'title' => $this->faker->sentence(3),
            'items' => $this->faker->boolean(10) ? $this->faker->randomList(fn () => VariableFactory::new()->make()) : [],
        ];
    }

    public function make(array $extra = []): Variable
    {
        return new Variable($this->makeArray($extra));
    }
}
