<?php

namespace App\Domain\Communication\Tests\Factories\Notifications;

use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationSetting;
use Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NotificationSettingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(3),
            'event' => $this->faker->unique()->randomElement(NotificationEventEnum::getAllowableEnumValues()),
            'channels' => $this->faker->randomList(fn () => $this->faker->randomElement(NotificationChannelEnum::getAllowableEnumValues()), min: 1),
            'theme' => $this->faker->nullable()->sentence(3),
            'text' => $this->faker->text(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): NotificationSetting
    {
        return new NotificationSetting($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): NotificationSettingResponse
    {
        return new NotificationSettingResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchNotificationSettingsResponse
    {
        return $this->generateResponseSearch(SearchNotificationSettingsResponse::class, $extra, $count);
    }
}
