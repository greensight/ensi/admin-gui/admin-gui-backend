<?php

namespace App\Domain\Communication\Tests\Factories\Notifications;

use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Ensi\CommunicationManagerClient\Dto\VariablesDictionary;
use Ensi\CommunicationManagerClient\Dto\VariablesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class VariableDictionaryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'event' => $this->faker->unique()->randomElement(NotificationEventEnum::getAllowableEnumValues()),
            'variables' => $this->faker->randomList(fn () => VariableFactory::new()->make(), 1, 2),
        ];
    }

    public function make(array $extra = []): VariablesDictionary
    {
        return new VariablesDictionary($this->makeArray($extra));
    }

    public function makeResponse(array $extras = [], int $count = 1): VariablesResponse
    {
        $data = [];
        $count = $extras ? count($extras) : $count;
        for ($i = 0; $i < $count; $i++) {
            $extra = $extras[$i] ?? [];
            $data[] = is_object($extra) ? $extra : $this->make($extra);
        }

        return new VariablesResponse([
            'data' => $data,
        ]);
    }
}
