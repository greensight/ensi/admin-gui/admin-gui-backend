<?php

namespace App\Domain\Communication\Actions;

use App\Domain\Communication\Data\ChatData;
use Ensi\InternalMessenger\Api\ChatsApi;
use Ensi\InternalMessenger\Dto\ChatDirectionEnum;
use Ensi\InternalMessenger\Dto\CreateChatRequest;

class CreateChatAction
{
    public function __construct(protected readonly ChatsApi $chatsApi)
    {
    }

    public function execute(array $fields): ChatData
    {
        $chatForCreate = new CreateChatRequest($fields);
        $chatForCreate->setDirection(ChatDirectionEnum::FROM_ADMIN_TO_USER);
        $chatForCreate->setMuted(false);

        $chat = $this->chatsApi->createChat($chatForCreate)->getData();

        return new ChatData($chat);
    }
}
