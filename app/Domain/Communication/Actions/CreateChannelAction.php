<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ChannelsApi;
use Ensi\CommunicationManagerClient\Dto\Channel;
use Ensi\CommunicationManagerClient\Dto\CreateChannelRequest;

class CreateChannelAction
{
    public function __construct(protected readonly ChannelsApi $channelsApi)
    {
    }

    public function execute(array $fields): Channel
    {
        $channel = new CreateChannelRequest($fields);

        return $this->channelsApi->createChannel($channel)->getData();
    }
}
