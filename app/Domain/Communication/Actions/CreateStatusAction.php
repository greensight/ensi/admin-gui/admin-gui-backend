<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\CreateStatusRequest;
use Ensi\CommunicationManagerClient\Dto\Status;

class CreateStatusAction
{
    public function __construct(protected readonly StatusesApi $statusesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Status
    {
        $status = new CreateStatusRequest($fields);

        return $this->statusesApi->createStatus($status)->getData();
    }
}
