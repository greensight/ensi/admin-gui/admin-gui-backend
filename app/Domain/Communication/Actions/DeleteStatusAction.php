<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;

class DeleteStatusAction
{
    public function __construct(protected readonly StatusesApi $statusesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->statusesApi->deleteStatus($id);
    }
}
