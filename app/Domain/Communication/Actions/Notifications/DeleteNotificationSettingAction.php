<?php

namespace App\Domain\Communication\Actions\Notifications;

use Ensi\CommunicationManagerClient\Api\NotificationsApi;

class DeleteNotificationSettingAction
{
    public function __construct(protected readonly NotificationsApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteNotificationSetting($id);
    }
}
