<?php

namespace App\Domain\Communication\Actions\Notifications;

use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CommunicationManagerClient\Dto\CreateNotificationSettingRequest;
use Ensi\CommunicationManagerClient\Dto\NotificationSetting;

class CreateNotificationSettingAction
{
    public function __construct(protected readonly NotificationsApi $api)
    {
    }

    public function execute(array $fields): NotificationSetting
    {
        $request = new CreateNotificationSettingRequest($fields);

        return $this->api->createNotificationSetting($request)->getData();
    }
}
