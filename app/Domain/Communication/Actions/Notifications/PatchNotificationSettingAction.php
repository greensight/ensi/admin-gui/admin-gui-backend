<?php

namespace App\Domain\Communication\Actions\Notifications;

use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CommunicationManagerClient\Dto\NotificationSetting;
use Ensi\CommunicationManagerClient\Dto\PatchNotificationSettingRequest;

class PatchNotificationSettingAction
{
    public function __construct(protected readonly NotificationsApi $api)
    {
    }

    public function execute(int $id, array $fields): NotificationSetting
    {
        $request = new PatchNotificationSettingRequest($fields);

        return $this->api->patchNotificationSetting($id, $request)->getData();
    }
}
