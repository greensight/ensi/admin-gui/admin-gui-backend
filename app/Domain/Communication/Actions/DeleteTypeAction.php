<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;

class DeleteTypeAction
{
    public function __construct(protected readonly TypesApi $typesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->typesApi->deleteType($id);
    }
}
