<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;

class DeleteThemeAction
{
    public function __construct(protected readonly ThemesApi $themesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $themeId): void
    {
        $this->themesApi->deleteTheme($themeId);
    }
}
