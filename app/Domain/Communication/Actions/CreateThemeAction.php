<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\CreateThemeRequest;
use Ensi\CommunicationManagerClient\Dto\Theme;

class CreateThemeAction
{
    public function __construct(protected readonly ThemesApi $themesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Theme
    {
        $theme = new CreateThemeRequest($fields);

        return $this->themesApi->createTheme($theme)->getData();
    }
}
