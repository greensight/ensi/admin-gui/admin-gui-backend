<?php

namespace App\Domain\Communication\Actions;

use Ensi\InternalMessenger\Api\AttachmentsApi;
use Ensi\InternalMessenger\Dto\MassDeleteAttachmentsRequest;

class DeleteAttachmentsAction
{
    public function __construct(protected readonly AttachmentsApi $attachmentsApi)
    {
    }

    public function execute($fileIds): void
    {
        $request = new MassDeleteAttachmentsRequest();
        $request->setIds($fileIds);

        $this->attachmentsApi->massDeleteAttachments($request);
    }
}
