<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\CreateTypeRequest;
use Ensi\CommunicationManagerClient\Dto\Type;

class CreateTypeAction
{
    public function __construct(protected readonly TypesApi $typesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Type
    {
        $type = new CreateTypeRequest($fields);

        return $this->typesApi->createType($type)->getData();
    }
}
