<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ThemesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\PatchThemeRequest;
use Ensi\CommunicationManagerClient\Dto\Theme;

class PatchThemeAction
{
    public function __construct(protected readonly ThemesApi $themesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Theme
    {
        $theme = new PatchThemeRequest($fields);

        return $this->themesApi->patchTheme($id, $theme)->getData();
    }
}
