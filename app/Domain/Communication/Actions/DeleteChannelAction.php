<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ChannelsApi;

class DeleteChannelAction
{
    public function __construct(protected readonly ChannelsApi $channelsApi)
    {
    }

    public function execute(int $id): void
    {
        $this->channelsApi->deleteChannel($id);
    }
}
