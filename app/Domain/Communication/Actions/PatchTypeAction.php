<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\TypesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\PatchTypeRequest;
use Ensi\CommunicationManagerClient\Dto\Type;

class PatchTypeAction
{
    public function __construct(protected readonly TypesApi $typesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Type
    {
        $type = new PatchTypeRequest($fields);

        return $this->typesApi->patchType($id, $type)->getData();
    }
}
