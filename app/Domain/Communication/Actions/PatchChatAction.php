<?php

namespace App\Domain\Communication\Actions;

use App\Domain\Communication\Data\ChatData;
use Ensi\InternalMessenger\Api\ChatsApi;
use Ensi\InternalMessenger\Dto\PatchChatRequest;

class PatchChatAction
{
    public function __construct(protected readonly ChatsApi $chatsApi)
    {
    }

    public function execute(int $id, array $fields): ChatData
    {
        $chatForPatch = new PatchChatRequest($fields);

        return new ChatData($this->chatsApi->patchChat($id, $chatForPatch)->getData());
    }
}
