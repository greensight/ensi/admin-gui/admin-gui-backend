<?php

namespace App\Domain\Communication\Actions;

use Ensi\InternalMessenger\Api\AttachmentsApi;
use Ensi\InternalMessenger\Dto\Attachment;
use Illuminate\Http\UploadedFile;

class CreateAttachmentAction
{
    public function __construct(protected readonly AttachmentsApi $attachmentsApi)
    {
    }

    public function execute(UploadedFile $file): Attachment
    {
        return $this->attachmentsApi->createAttachment($file, $file->getClientOriginalName())->getData();
    }
}
