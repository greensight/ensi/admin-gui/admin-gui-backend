<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\ChannelsApi;
use Ensi\CommunicationManagerClient\Dto\Channel;
use Ensi\CommunicationManagerClient\Dto\PatchChannelRequest;

class PatchChannelAction
{
    public function __construct(protected readonly ChannelsApi $channelsApi)
    {
    }

    public function execute(int $id, array $fields): Channel
    {
        $channel = new PatchChannelRequest($fields);

        return $this->channelsApi->patchChannel($id, $channel)->getData();
    }
}
