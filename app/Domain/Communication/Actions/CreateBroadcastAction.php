<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\EventsApi;
use Ensi\CommunicationManagerClient\Dto\SendInternalMessageEvent;
use Ensi\CommunicationManagerClient\Dto\SendInternalMessageEventData;
use Ensi\InternalMessenger\Api\ChatsApi;
use Ensi\InternalMessenger\Dto\ChatDirectionEnum;
use Ensi\InternalMessenger\Dto\CreateChatRequest;

class CreateBroadcastAction
{
    public function __construct(protected readonly ChatsApi $chatsApi, protected readonly EventsApi $eventsApi)
    {
    }

    public function execute(array $fields): void
    {
        $users = $fields['user_ids'];
        foreach ($users as $user) {
            $chat = new CreateChatRequest();
            $chat->setTheme($fields['theme']);
            $chat->setDirection(ChatDirectionEnum::FROM_ADMIN_TO_USER);
            $chat->setMuted(true);
            $chat->setTypeId($fields['type_id']);
            $chat->setUnreadAdmin(false);
            $chat->setUnreadUser(true);
            $chat->setUserId($user);
            $chat->setUserType($fields['user_type']);
            $createdChatId = $this->chatsApi->createChat($chat)->getData()->getId();
            $data = new SendInternalMessageEventData(
                [
                'text' => $fields['message'],
                'user_id' => 1,
                'user_type' => $fields['user_type'],
                'chat_id' => $createdChatId,
                'files' => $fields['files'] ?? null, ]
            );
            $event = (new SendInternalMessageEvent())
                ->setData($data)
                ->setType('send-internal-message');

            /** @phpstan-ignore-next-line  */
            $this->eventsApi->parseEvent($event);
        }
    }
}
