<?php

namespace App\Domain\Communication\Actions;

use Ensi\CommunicationManagerClient\Api\StatusesApi;
use Ensi\CommunicationManagerClient\ApiException;
use Ensi\CommunicationManagerClient\Dto\PatchStatusRequest;
use Ensi\CommunicationManagerClient\Dto\Status;

class PatchStatusAction
{
    public function __construct(protected readonly StatusesApi $statusesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Status
    {
        $status = new PatchStatusRequest($fields);

        return $this->statusesApi->patchStatus($id, $status)->getData();
    }
}
