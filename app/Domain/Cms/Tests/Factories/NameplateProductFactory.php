<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\NameplateProduct;
use Ensi\CmsClient\Dto\NameplateProductResponse;
use Ensi\CmsClient\Dto\SearchNameplateProductsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'nameplate_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): NameplateProduct
    {
        return new NameplateProduct($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): NameplateProductResponse
    {
        return new NameplateProductResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchNameplateProductsResponse
    {
        return $this->generateResponseSearch(SearchNameplateProductsResponse::class, $extras, $count, $pagination);
    }
}
