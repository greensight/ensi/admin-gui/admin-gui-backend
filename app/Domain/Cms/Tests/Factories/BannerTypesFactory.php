<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\BannerType;
use Ensi\CmsClient\Dto\BannerTypeEnum;
use Ensi\CmsClient\Dto\SearchBannerTypesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerTypesFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->words(3, true),
            'code' => $this->faker->randomElement(BannerTypeEnum::getAllowableEnumValues()),
            'active' => $this->faker->boolean,

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): BannerType
    {
        return new BannerType($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchBannerTypesResponse
    {
        return $this->generateResponseSearch(SearchBannerTypesResponse::class, $extra, $count);
    }
}
