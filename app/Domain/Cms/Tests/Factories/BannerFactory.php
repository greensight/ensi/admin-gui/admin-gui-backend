<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\Banner;
use Ensi\CmsClient\Dto\BannerButton;
use Ensi\CmsClient\Dto\BannerResponse;
use Ensi\CmsClient\Dto\FileOrNull;
use Ensi\CmsClient\Dto\SearchBannersResponse;
use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Support\Str;

class BannerFactory extends BaseApiFactory
{
    protected ?BannerButton $button = null;

    protected function definition(): array
    {
        $name = $this->faker->words(3, true);

        $data = [
            'id' => $this->faker->modelId(),
            'name' => $name,
            'code' => Str::slug($name),
            'is_active' => $this->faker->boolean,
            'desktop_image' => $this->faker->boolean ? new FileOrNull(EnsiFileFactory::new()->make()) : null,
            'mobile_image' => $this->faker->boolean ? new FileOrNull(EnsiFileFactory::new()->make()) : null,
            'url' => $this->faker->optional()->url,
            'sort' => $this->faker->numberBetween(),
            'type_id' => $this->faker->modelId(),
            'button_id' => $this->faker->modelId(),
            'updated_at' => $this->faker->dateTime(),
            'created_at' => $this->faker->dateTime(),
        ];

        if ($this->button) {
            $data['button'] = $this->button;
        }

        return $data;
    }

    public function withButton(BannerButton $button): static
    {
        $this->button = $button;

        return $this;
    }

    public function make(array $extra = []): Banner
    {
        return new Banner($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): BannerResponse
    {
        return new BannerResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchBannersResponse
    {
        return $this->generateResponseSearch(SearchBannersResponse::class, $extra, $count);
    }
}
