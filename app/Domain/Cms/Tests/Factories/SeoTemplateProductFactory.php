<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\SearchSeoTemplateProductsResponse;
use Ensi\CmsClient\Dto\SeoTemplateProduct;
use Ensi\CmsClient\Dto\SeoTemplateProductResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SeoTemplateProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'template_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): SeoTemplateProduct
    {
        return new SeoTemplateProduct($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): SeoTemplateProductResponse
    {
        return new SeoTemplateProductResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchSeoTemplateProductsResponse
    {
        return $this->generateResponseSearch(SearchSeoTemplateProductsResponse::class, $extras, $count, $pagination);
    }
}
