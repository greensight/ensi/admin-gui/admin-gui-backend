<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\BannerButton;
use Ensi\CmsClient\Dto\BannerButtonLocationEnum;
use Ensi\CmsClient\Dto\BannerButtonTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerButtonFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'url' => $this->faker->url,
            'text' => $this->faker->sentence,
            'location' => $this->faker->randomElement(BannerButtonLocationEnum::getAllowableEnumValues()),
            'type' => $this->faker->randomElement(BannerButtonTypeEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): BannerButton
    {
        return new BannerButton($this->makeArray($extra));
    }
}
