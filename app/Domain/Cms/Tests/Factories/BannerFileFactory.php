<?php

namespace App\Domain\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\BannerFile;
use Ensi\CmsClient\Dto\BannerFileResponse;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return EnsiFile::factory()->make();
    }

    public function make(array $extra = []): BannerFileResponse
    {
        $data = $this->makeArray($extra);

        return new BannerFileResponse(['data' => new BannerFile($data)]);
    }
}
