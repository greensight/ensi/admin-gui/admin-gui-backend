<?php

namespace App\Domain\Cms\Data\Nameplates;

use Ensi\CmsClient\Dto\NameplateProduct;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Collection;

class NameplateProductData
{
    public function __construct(public NameplateProduct $data, public ?Product $product = null)
    {
    }

    /**
     * @param Collection $nameplateProducts
     * @param ?Collection<Product> $products
     * @return Collection<NameplateProductData>
     */
    public static function mapCollect(Collection $nameplateProducts, ?Collection $products = null): Collection
    {
        $nameplateProductsData = collect();

        foreach ($nameplateProducts as $nameplateProduct) {
            $product = $products?->get($nameplateProduct->getProductId());
            $productItemData = new NameplateProductData($nameplateProduct, $product);

            $nameplateProductsData->put($productItemData->data->getId(), $productItemData);
        }

        return $nameplateProductsData;
    }
}
