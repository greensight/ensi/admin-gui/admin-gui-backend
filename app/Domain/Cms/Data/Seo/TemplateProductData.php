<?php

namespace App\Domain\Cms\Data\Seo;

use Ensi\CmsClient\Dto\SeoTemplateProduct;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Collection;

class TemplateProductData
{
    public function __construct(public SeoTemplateProduct $data, public ?Product $product = null)
    {
    }

    /**
     * @param Collection<SeoTemplateProduct> $templateProducts
     * @param ?Collection<Product> $products
     * @return Collection<static>
     */
    public static function mapCollect(Collection $templateProducts, ?Collection $products = null): Collection
    {
        $nameplateProductsData = collect();

        foreach ($templateProducts as $templateProduct) {
            $product = $products?->get($templateProduct->getProductId());
            $productItemData = new static($templateProduct, $product);

            $nameplateProductsData->put($productItemData->data->getId(), $productItemData);
        }

        return $nameplateProductsData;
    }
}
