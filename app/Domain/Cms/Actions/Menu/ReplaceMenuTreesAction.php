<?php

namespace App\Domain\Cms\Actions\Menu;

use Ensi\CmsClient\Api\MenusApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\UpdateMenuTreesRequest;

class ReplaceMenuTreesAction
{
    public function __construct(protected readonly MenusApi $menusApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $data): array
    {
        $request = new UpdateMenuTreesRequest($data);

        return $this->menusApi->updateMenuTrees($id, $request)->getData();
    }
}
