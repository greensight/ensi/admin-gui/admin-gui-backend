<?php

namespace App\Domain\Cms\Actions\Pages;

use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\CreatePageRequest;
use Ensi\CmsClient\Dto\Page;

class CreatePageAction
{
    public function __construct(protected readonly PagesApi $pagesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Page
    {
        $request = new CreatePageRequest($fields);

        return $this->pagesApi->createPage($request)->getData();
    }
}
