<?php

namespace App\Domain\Cms\Actions\Pages;

use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\Page;
use Ensi\CmsClient\Dto\PatchPageRequest;

class PatchPageAction
{
    public function __construct(protected readonly PagesApi $pagesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Page
    {
        $request = new PatchPageRequest($fields);

        return $this->pagesApi->patchPage($id, $request)->getData();
    }
}
