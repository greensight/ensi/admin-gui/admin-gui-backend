<?php

namespace App\Domain\Cms\Actions\Seo;

use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CmsClient\Dto\DeleteSeoTemplateProductsRequest;

class DeleteTemplateProductsAction
{
    public function __construct(protected SeoTemplatesApi $api)
    {
    }

    public function execute(int $templateId, array $productIds): void
    {
        $request = new DeleteSeoTemplateProductsRequest(['ids' => $productIds]);

        $this->api->deleteSeoTemplateProducts($templateId, $request);
    }
}
