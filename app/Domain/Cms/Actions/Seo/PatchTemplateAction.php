<?php

namespace App\Domain\Cms\Actions\Seo;

use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CmsClient\Dto\SeoTemplate;
use Ensi\CmsClient\Dto\SeoTemplateForPatch;

class PatchTemplateAction
{
    public function __construct(protected SeoTemplatesApi $api)
    {
    }

    public function execute(int $id, array $fields): SeoTemplate
    {
        $request = new SeoTemplateForPatch($fields);

        return $this->api->patchSeoTemplate($id, $request)->getData();
    }
}
