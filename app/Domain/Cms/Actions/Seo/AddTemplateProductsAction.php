<?php

namespace App\Domain\Cms\Actions\Seo;

use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CmsClient\Dto\AddSeoTemplateProductsRequest;

class AddTemplateProductsAction
{
    public function __construct(protected SeoTemplatesApi $api)
    {
    }

    public function execute(int $templateId, array $productIds): void
    {
        $request = new AddSeoTemplateProductsRequest(['ids' => $productIds]);

        $this->api->addSeoTemplateProducts($templateId, $request);
    }
}
