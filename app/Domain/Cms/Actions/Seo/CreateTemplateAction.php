<?php

namespace App\Domain\Cms\Actions\Seo;

use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CmsClient\Dto\SeoTemplate;
use Ensi\CmsClient\Dto\SeoTemplateForCreate;

class CreateTemplateAction
{
    public function __construct(protected SeoTemplatesApi $api)
    {
    }

    public function execute(array $fields): SeoTemplate
    {
        $request = new SeoTemplateForCreate($fields);

        return $this->api->createSeoTemplate($request)->getData();
    }
}
