<?php

namespace App\Domain\Cms\Actions\Seo;

use Ensi\CmsClient\Api\SeoTemplatesApi;

class DeleteTemplateAction
{
    public function __construct(protected SeoTemplatesApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteSeoTemplate($id);
    }
}
