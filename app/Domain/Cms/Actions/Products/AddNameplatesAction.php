<?php

namespace App\Domain\Cms\Actions\Products;

use Ensi\CmsClient\Api\ProductsApi;
use Ensi\CmsClient\Dto\AddNameplatesRequest;

class AddNameplatesAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $productId, array $nameplateIds): void
    {
        $request = new AddNameplatesRequest(['ids' => $nameplateIds]);

        $this->api->addProductNameplates($productId, $request);
    }
}
