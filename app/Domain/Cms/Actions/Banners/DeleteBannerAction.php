<?php

namespace App\Domain\Cms\Actions\Banners;

use Ensi\CmsClient\Api\BannersApi;

class DeleteBannerAction
{
    public function __construct(protected readonly BannersApi $bannersApi)
    {
    }

    public function execute(int $id): void
    {
        $this->bannersApi->deleteBanner($id);
    }
}
