<?php

namespace App\Domain\Cms\Actions\Banners;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\BannerFile;
use Illuminate\Http\UploadedFile;

class UploadBannerFileAction
{
    public function __construct(protected readonly BannersApi $bannersApi)
    {
    }

    public function execute(int $id, string $type, UploadedFile $file): BannerFile
    {
        return $this->bannersApi->uploadBannerFile($id, $type, $file)->getData();
    }
}
