<?php

namespace App\Domain\Cms\Actions\Banners;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\Banner;
use Ensi\CmsClient\Dto\ReplaceBannerRequest;

class ReplaceBannerAction
{
    public function __construct(protected readonly BannersApi $bannersApi)
    {
    }

    public function execute(int $id, $data): Banner
    {
        $request = new ReplaceBannerRequest($data);

        return $this->bannersApi->replaceBanner($id, $request)->getData();
    }
}
