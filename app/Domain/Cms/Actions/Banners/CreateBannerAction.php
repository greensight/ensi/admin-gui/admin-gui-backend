<?php

namespace App\Domain\Cms\Actions\Banners;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\Banner;
use Ensi\CmsClient\Dto\CreateBannerRequest;

class CreateBannerAction
{
    public function __construct(protected readonly BannersApi $bannersApi)
    {
    }

    public function execute(array $data): Banner
    {
        $request = new CreateBannerRequest($data);

        return $this->bannersApi->createBanner($request)->getData();
    }
}
