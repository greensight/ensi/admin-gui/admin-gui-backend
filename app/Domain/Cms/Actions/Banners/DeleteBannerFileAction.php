<?php

namespace App\Domain\Cms\Actions\Banners;

use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\DeleteBannerFileRequest;

class DeleteBannerFileAction
{
    public function __construct(protected readonly BannersApi $bannersApi)
    {
    }

    public function execute(int $id, string $type): void
    {
        $deleteBannerFileRequest = new DeleteBannerFileRequest();
        $deleteBannerFileRequest->setType($type);

        $this->bannersApi->deleteBannerFile($id, $deleteBannerFileRequest);
    }
}
