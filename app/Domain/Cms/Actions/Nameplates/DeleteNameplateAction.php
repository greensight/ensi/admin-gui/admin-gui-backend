<?php

namespace App\Domain\Cms\Actions\Nameplates;

use Ensi\CmsClient\Api\NameplatesApi;

class DeleteNameplateAction
{
    public function __construct(protected readonly NameplatesApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteNameplate($id);
    }
}
