<?php

namespace App\Domain\Cms\Actions\Nameplates;

use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Dto\AddProductsRequest;

class AddProductsAction
{
    public function __construct(protected readonly NameplatesApi $api)
    {
    }

    public function execute(int $nameplateId, array $productIds): void
    {
        $request = new AddProductsRequest(['ids' => $productIds]);

        $this->api->addNameplateProducts($nameplateId, $request);
    }
}
