<?php

namespace App\Domain\Cms\Actions\Nameplates;

use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Dto\CreateNameplateRequest;
use Ensi\CmsClient\Dto\Nameplate;

class CreateNameplateAction
{
    public function __construct(protected readonly NameplatesApi $api)
    {
    }

    public function execute(array $fields): Nameplate
    {
        $request = new CreateNameplateRequest($fields);

        return $this->api->createNameplate($request)->getData();
    }
}
