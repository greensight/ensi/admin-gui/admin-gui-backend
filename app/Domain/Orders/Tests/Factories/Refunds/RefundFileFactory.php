<?php

namespace App\Domain\Orders\Tests\Factories\Refunds;

use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\File;
use Ensi\OmsClient\Dto\RefundFile;
use Ensi\OmsClient\Dto\RefundFileResponse;

class RefundFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'refund_id' => $this->faker->modelId(),
            'file' => new File(EnsiFileFactory::new()->make()),
            'original_name' => $this->faker->word() . '.' . $this->faker->fileExtension(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): RefundFile
    {
        return new RefundFile($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): RefundFileResponse
    {
        return new RefundFileResponse(['data' => $this->make($extra)]);
    }
}
