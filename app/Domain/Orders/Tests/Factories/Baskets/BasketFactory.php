<?php

namespace App\Domain\Orders\Tests\Factories\Baskets;

use Ensi\BasketsClient\Dto\Basket;
use Ensi\BasketsClient\Dto\BasketItem;
use Ensi\BasketsClient\Dto\SearchBasketsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BasketFactory extends BaseApiFactory
{
    protected array $items = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'promo_code' => $this->faker->nullable()->word(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->items) {
            $definition['items'] = $this->items;
        }

        return $definition;
    }

    public function make(array $extra = []): Basket
    {
        return new Basket($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchBasketsResponse
    {
        return $this->generateResponseSearch(SearchBasketsResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param BasketItem[]|null $items
     */
    public function withItems(?array $items = null): static
    {
        return $this->state(['items' => $items ?? BasketItemFactory::new()->makeSeveral(1)->all()]);
    }
}
