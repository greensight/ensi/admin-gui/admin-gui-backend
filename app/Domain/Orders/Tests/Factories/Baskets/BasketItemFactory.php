<?php

namespace App\Domain\Orders\Tests\Factories\Baskets;

use Ensi\BasketsClient\Dto\BasketItem;
use Ensi\BasketsClient\Dto\SearchBasketItemsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BasketItemFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'basket_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'qty' => $this->faker->numberBetween(1, 10),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): BasketItem
    {
        return new BasketItem($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchBasketItemsResponse
    {
        return $this->generateResponseSearch(SearchBasketItemsResponse::class, $extras, $count, $pagination);
    }
}
