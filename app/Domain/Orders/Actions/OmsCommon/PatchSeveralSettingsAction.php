<?php

namespace App\Domain\Orders\Actions\OmsCommon;

use Ensi\OmsClient\Api\CommonApi;
use Ensi\OmsClient\Dto\PatchSeveralSettingsRequest;

class PatchSeveralSettingsAction
{
    public function __construct(protected readonly CommonApi $commonApi)
    {
    }

    public function execute(array $fields): array
    {
        $request = new PatchSeveralSettingsRequest($fields);

        return $this->commonApi->patchSettings($request)->getData();
    }
}
