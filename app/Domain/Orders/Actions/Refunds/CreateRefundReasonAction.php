<?php

namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\CreateRefundReasonRequest;
use Ensi\OmsClient\Dto\RefundReason;

class CreateRefundReasonAction
{
    public function __construct(protected readonly EnumsApi $enumsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): RefundReason
    {
        $request = new CreateRefundReasonRequest($fields);

        return $this->enumsApi->createRefundReason($request)->getData();
    }
}
