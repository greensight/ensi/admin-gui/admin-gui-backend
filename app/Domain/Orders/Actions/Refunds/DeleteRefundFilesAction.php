<?php

namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\DeleteRefundFilesRequest;

class DeleteRefundFilesAction
{
    public function __construct(protected readonly RefundsApi $refundsApi)
    {
    }

    public function execute(int $id, array $fileIds): void
    {
        $request = new DeleteRefundFilesRequest();
        $request->setFileIds($fileIds);

        $this->refundsApi->deleteRefundFiles($id, $request);
    }
}
