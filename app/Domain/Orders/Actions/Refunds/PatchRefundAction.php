<?php

namespace App\Domain\Orders\Actions\Refunds;

use App\Domain\Orders\Data\Refunds\RefundData;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\PatchRefundRequest;

class PatchRefundAction
{
    public function __construct(protected readonly RefundsApi $refundsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): RefundData
    {
        $request = new PatchRefundRequest($fields);

        return new RefundData($this->refundsApi->patchRefund($id, $request)->getData());
    }
}
