<?php

namespace App\Domain\Orders\Actions\Refunds;

use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\PatchRefundReasonRequest;
use Ensi\OmsClient\Dto\RefundReason;

class PatchRefundReasonAction
{
    public function __construct(protected readonly EnumsApi $enumsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): RefundReason
    {
        $request = new PatchRefundReasonRequest($fields);

        return $this->enumsApi->patchRefundReason($id, $request)->getData();
    }
}
