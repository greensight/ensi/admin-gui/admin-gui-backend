<?php

namespace App\Domain\Orders\Actions\Refunds;

use App\Domain\Orders\Data\Refunds\RefundData;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\CreateRefundRequest;

class CreateRefundAction
{
    public function __construct(protected readonly RefundsApi $refundsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): RefundData
    {
        $request = new CreateRefundRequest($fields);

        return new RefundData($this->refundsApi->createRefund($request)->getData());
    }
}
