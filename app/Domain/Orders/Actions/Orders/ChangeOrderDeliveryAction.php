<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\PatchOrderDeliveryRequest;

class ChangeOrderDeliveryAction
{
    public function __construct(protected readonly OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, array $fields): OrderData
    {
        $request = new PatchOrderDeliveryRequest($fields);

        return new OrderData($this->ordersApi->changeOrderDelivery($orderId, $request)->getData());
    }
}
