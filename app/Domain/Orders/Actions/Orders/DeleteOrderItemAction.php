<?php

namespace App\Domain\Orders\Actions\Orders;

use Ensi\OmsClient\Api\OrderItemsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\OrderItemsDeleteRequest;

class DeleteOrderItemAction
{
    public function __construct(protected OrderItemsApi $orderItemsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $orderId, array $fields): void
    {
        $this->orderItemsApi->deleteOrderItems($orderId, new OrderItemsDeleteRequest($fields));
    }
}
