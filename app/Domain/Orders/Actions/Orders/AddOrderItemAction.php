<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use Ensi\OmsClient\Api\OrderItemsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\OrderItemsAddRequest;

class AddOrderItemAction
{
    public function __construct(protected OrderItemsApi $orderItemsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $orderId, array $fields): OrderData
    {
        $order = $this->orderItemsApi->addOrderItems($orderId, new OrderItemsAddRequest($fields))->getData();

        return new OrderData($order);
    }
}
