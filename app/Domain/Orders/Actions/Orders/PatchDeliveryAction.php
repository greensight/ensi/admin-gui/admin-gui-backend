<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\DeliveryData;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Dto\PatchDeliveryRequest;

class PatchDeliveryAction
{
    public function __construct(protected readonly DeliveriesApi $deliveriesApi)
    {
    }

    public function execute(int $id, array $fields): DeliveryData
    {
        $request = new PatchDeliveryRequest($fields);

        return new DeliveryData($this->deliveriesApi->patchDelivery($id, $request)->getData());
    }
}
