<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\PatchOrderRequest;

class PatchOrderAction
{
    public function __construct(protected readonly OrdersApi $ordersApi)
    {
    }

    public function execute(int $id, array $fields): OrderData
    {
        $request = new PatchOrderRequest($fields);

        return new OrderData($this->ordersApi->patchOrder($id, $request)->getData());
    }
}
