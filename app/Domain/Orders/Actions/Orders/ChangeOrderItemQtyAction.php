<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderItemData;
use Ensi\OmsClient\Api\OrderItemsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\OrderItemChangeQtyRequest;
use Illuminate\Support\Collection;

class ChangeOrderItemQtyAction
{
    public function __construct(protected OrderItemsApi $orderItemsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $orderId, array $fields): Collection
    {
        $orderItemsRaw = $this->orderItemsApi->changeOrderItemQty($orderId, new OrderItemChangeQtyRequest($fields))->getData();
        $orderItems = [];
        foreach ($orderItemsRaw as $item) {
            $orderItems[] = new OrderItemData($item);
        }

        return collect($orderItems);
    }
}
