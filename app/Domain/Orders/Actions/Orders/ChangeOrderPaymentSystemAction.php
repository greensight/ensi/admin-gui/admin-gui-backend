<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderChangePaymentSystemRequest;

class ChangeOrderPaymentSystemAction
{
    public function __construct(protected readonly OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, int $paymentSystem): OrderData
    {
        $request = new OrderChangePaymentSystemRequest();
        $request->setPaymentSystem($paymentSystem);

        return new OrderData(
            $this->ordersApi->changeOrderPaymentSystem($orderId, $request)->getData(),
        );
    }
}
