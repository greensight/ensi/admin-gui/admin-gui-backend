<?php

namespace App\Domain\Orders\Actions\Orders;

use App\Domain\Orders\Data\Orders\ShipmentData;
use Ensi\OmsClient\Api\ShipmentsApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\PatchShipmentRequest;

class PatchShipmentAction
{
    public function __construct(protected readonly ShipmentsApi $shipmentsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): ShipmentData
    {
        $request = new PatchShipmentRequest($fields);

        return new ShipmentData($this->shipmentsApi->patchShipment($id, $request)->getData());
    }
}
