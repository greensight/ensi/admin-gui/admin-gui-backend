<?php

namespace App\Domain\Orders\Actions\Orders;

use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderDeleteFilesRequest;

class DeleteOrderFilesAction
{
    public function __construct(protected readonly OrdersApi $ordersApi)
    {
    }

    public function execute(int $orderId, array $fileIds): void
    {
        $request = new OrderDeleteFilesRequest();
        $request->setFileIds($fileIds);

        $this->ordersApi->deleteOrderFiles($orderId, $request);
    }
}
