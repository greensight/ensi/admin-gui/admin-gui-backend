<?php

namespace App\Domain\Orders\Actions\BasketsCommon;

use Ensi\BasketsClient\Api\CommonApi;
use Ensi\BasketsClient\Dto\PatchSeveralSettingsRequest;

class PatchSeveralSettingsAction
{
    public function __construct(protected readonly CommonApi $commonApi)
    {
    }

    public function execute(array $fields): array
    {
        $request = new PatchSeveralSettingsRequest($fields);

        return $this->commonApi->patchSettings($request)->getData();
    }
}
