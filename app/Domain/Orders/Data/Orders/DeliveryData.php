<?php

namespace App\Domain\Orders\Data\Orders;

use App\Domain\Catalog\Data\Offers\OfferData;
use Ensi\BuClient\Dto\Store;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\PimClient\Dto\Product;

class DeliveryData
{
    /** @var OfferData[] */
    public array $offersData = [];
    /** @var Product[] */
    public array $products = [];
    /** @var Store[] */
    public array $stores = [];

    public function __construct(public Delivery $delivery)
    {
    }

    /**
     * @return ShipmentData[]|null
     */
    public function getShipments(): ?array
    {
        if (is_null($this->delivery->getShipments())) {
            return null;
        }

        $stores = collect($this->stores)->keyBy('id');

        $shipmentsData = [];
        foreach ($this->delivery->getShipments() as $shipment) {
            $shipmentData = new ShipmentData($shipment);
            $shipmentData->offersData = $this->offersData;
            $shipmentData->products = $this->products;
            $shipmentData->store = $stores->get($shipment->getStoreId());
            $shipmentsData[] = $shipmentData;
        }

        return $shipmentsData;
    }
}
