<?php

namespace App\Domain\Marketing\Actions\ProductDiscounts;

use Ensi\MarketingClient\Api\DiscountRelationsApi;
use Ensi\MarketingClient\Dto\PatchDiscountProductsRequest;

class PatchDiscountProductsAction
{
    public function __construct(protected readonly DiscountRelationsApi $api)
    {
    }

    public function execute(int $discountId, array $fields): void
    {
        $request = new PatchDiscountProductsRequest($fields);

        $this->api->patchDiscountProducts($discountId, $request);
    }
}
