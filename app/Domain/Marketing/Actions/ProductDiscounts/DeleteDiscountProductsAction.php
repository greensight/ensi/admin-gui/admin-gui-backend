<?php

namespace App\Domain\Marketing\Actions\ProductDiscounts;

use Ensi\MarketingClient\Api\DiscountRelationsApi;
use Ensi\MarketingClient\Dto\DeleteDiscountProductsRequest;

class DeleteDiscountProductsAction
{
    public function __construct(protected readonly DiscountRelationsApi $api)
    {
    }

    public function execute(int $discountId, array $fields): void
    {
        $request = new DeleteDiscountProductsRequest($fields);

        $this->api->deleteDiscountProducts($discountId, $request);
    }
}
