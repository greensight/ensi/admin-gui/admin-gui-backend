<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use App\Domain\Marketing\Data\Discounts\PromoCodeData;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\MarketingClient\Dto\CreatePromoCodeRequest;

class CreatePromoCodeAction
{
    public function __construct(protected readonly PromoCodesApi $api)
    {
    }

    public function execute(array $data): PromoCodeData
    {
        $request = new CreatePromoCodeRequest($data);

        return new PromoCodeData($this->api->createPromoCode($request)->getData());
    }
}
