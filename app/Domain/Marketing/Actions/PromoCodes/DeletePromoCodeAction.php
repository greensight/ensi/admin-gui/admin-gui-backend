<?php

namespace App\Domain\Marketing\Actions\PromoCodes;

use Ensi\MarketingClient\Api\PromoCodesApi;

class DeletePromoCodeAction
{
    public function __construct(protected readonly PromoCodesApi $promoCodesApi)
    {
    }

    public function execute(int $id): void
    {
        $this->promoCodesApi->deletePromoCode($id);
    }
}
