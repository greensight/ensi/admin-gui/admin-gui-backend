<?php

namespace App\Domain\Marketing\Actions\Discounts;

use App\Domain\Marketing\Data\Discounts\DiscountData;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\CreateDiscountRequest;

class CreateDiscountAction
{
    public function __construct(protected readonly DiscountsApi $api)
    {
    }

    public function execute(array $data): DiscountData
    {
        $request = new CreateDiscountRequest($data);

        return new DiscountData($this->api->createDiscount($request)->getData());
    }
}
