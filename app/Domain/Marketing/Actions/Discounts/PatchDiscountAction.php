<?php

namespace App\Domain\Marketing\Actions\Discounts;

use App\Domain\Marketing\Data\Discounts\DiscountData;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\PatchDiscountRequest;

class PatchDiscountAction
{
    public function __construct(protected readonly DiscountsApi $api)
    {
    }

    public function execute(int $id, array $data): DiscountData
    {
        $request = new PatchDiscountRequest($data);

        return new DiscountData($this->api->patchDiscount($id, $request)->getData());
    }
}
