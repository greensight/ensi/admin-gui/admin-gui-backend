<?php

namespace App\Domain\Marketing\Actions\Discounts;

use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Dto\MassDiscountsStatusUpdateRequest;

class MassStatusUpdateAction
{
    public function __construct(protected readonly DiscountsApi $api)
    {
    }

    public function execute(array $data): void
    {
        $requestMassDiscountStatusApi = new MassDiscountsStatusUpdateRequest($data);

        $this->api->massDiscountsStatusUpdate($requestMassDiscountStatusApi);
    }
}
