<?php

namespace App\Domain\Marketing\Data\Discounts;

use Ensi\MarketingClient\Dto\DiscountProduct;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Collection;

class DiscountProductData
{
    public function __construct(public DiscountProduct $data, public ?Product $product = null)
    {
    }

    /**
     * @param Collection $discountProducts
     * @param ?Collection<Product> $products
     * @return Collection<DiscountProductData>
     */
    public static function mapCollect(Collection $discountProducts, ?Collection $products = null): Collection
    {
        $discountProductsData = collect();

        foreach ($discountProducts as $discountProduct) {
            $product = $products?->get($discountProduct->getProductId());
            $productItemData = new DiscountProductData($discountProduct, $product);

            $discountProductsData->put($productItemData->data->getId(), $productItemData);
        }

        return $discountProductsData;
    }
}
