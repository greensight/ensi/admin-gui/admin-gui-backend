<?php

namespace App\Domain\Marketing\Data\Discounts;

use Ensi\MarketingClient\Dto\PromoCode;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Collection;

class PromoCodeData
{
    public function __construct(public PromoCode $data, public ?DiscountData $discount = null)
    {
    }

    /**
     * @param Collection<PromoCode> $promoCodes
     * @param ?Collection<Product> $products
     * @return Collection<PromoCodeData>
     */
    public static function mapCollect(Collection $promoCodes, ?Collection $products = null): Collection
    {
        $promoCodesData = collect();
        foreach ($promoCodes as $promoCode) {
            $promoCodeData = new PromoCodeData($promoCode);

            $discount = $promoCode->getDiscount();
            if ($discount) {
                $discountData = new DiscountData($discount);
                if ($discount->getProducts()) {
                    $discountData->products = DiscountProductData::mapCollect(collect($discount->getProducts()), $products);
                }

                $promoCodeData->discount = $discountData;
            }

            $promoCodesData->put($promoCodeData->data->getId(), $promoCodeData);
        }

        return $promoCodesData;
    }
}
