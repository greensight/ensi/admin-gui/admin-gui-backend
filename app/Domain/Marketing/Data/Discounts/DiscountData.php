<?php

namespace App\Domain\Marketing\Data\Discounts;

use Ensi\MarketingClient\Dto\Discount;
use Ensi\PimClient\Dto\Product;
use Illuminate\Support\Collection;

class DiscountData
{
    /**
     * @param Discount $data
     * @param ?Collection<DiscountProductData> $products
     */
    public function __construct(public Discount $data, public ?Collection $products = null)
    {
    }

    /**
     * @param Collection<Discount> $discounts
     * @param ?Collection<Product> $products
     * @return Collection<DiscountData>
     */
    public static function mapCollect(Collection $discounts, ?Collection $products = null): Collection
    {
        $discountsData = collect();

        foreach ($discounts as $discount) {
            $discountData = new DiscountData($discount);

            if ($discount->getProducts()) {
                $discountData->products = DiscountProductData::mapCollect(collect($discount->getProducts()), $products);
            }

            $discountsData->put($discountData->data->getId(), $discountData);
        }

        return $discountsData;
    }
}
