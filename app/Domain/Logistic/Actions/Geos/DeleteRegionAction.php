<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;

class DeleteRegionAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    public function execute(int $id): void
    {
        $this->geosApi->deleteRegion($id);
    }
}
