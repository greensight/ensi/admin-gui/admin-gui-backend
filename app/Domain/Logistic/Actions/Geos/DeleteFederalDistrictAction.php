<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;

class DeleteFederalDistrictAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->geosApi->deleteFederalDistrict($id);
    }
}
