<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\PatchRegionRequest;
use Ensi\LogisticClient\Dto\Region;

class PatchRegionAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Region
    {
        $request = new PatchRegionRequest($fields);

        return $this->geosApi->patchRegion($id, $request)->getData();
    }
}
