<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateRegionRequest;
use Ensi\LogisticClient\Dto\Region;

class CreateRegionAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): Region
    {
        $request = new CreateRegionRequest($fields);

        return $this->geosApi->createRegion($request)->getData();
    }
}
