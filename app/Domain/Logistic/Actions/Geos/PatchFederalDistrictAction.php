<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\PatchFederalDistrictRequest;

class PatchFederalDistrictAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): FederalDistrict
    {
        $request = new PatchFederalDistrictRequest($fields);

        return $this->geosApi->patchFederalDistrict($id, $request)->getData();
    }
}
