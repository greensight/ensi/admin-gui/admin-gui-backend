<?php

namespace App\Domain\Logistic\Actions\Geos;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateFederalDistrictRequest;
use Ensi\LogisticClient\Dto\FederalDistrict;

class CreateFederalDistrictAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): FederalDistrict
    {
        $request = new CreateFederalDistrictRequest($fields);

        return $this->geosApi->createFederalDistrict($request)->getData();
    }
}
