<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;

class DeleteDeliveryPriceAction
{
    public function __construct(protected readonly DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->deliveryPricesApi->deleteDeliveryPrice($id);
    }
}
