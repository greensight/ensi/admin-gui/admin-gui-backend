<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateDeliveryPriceRequest;
use Ensi\LogisticClient\Dto\DeliveryPrice;

class CreateDeliveryPriceAction
{
    public function __construct(protected readonly DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): DeliveryPrice
    {
        $request = new CreateDeliveryPriceRequest($fields);

        return $this->deliveryPricesApi->createDeliveryPrice($request)->getData();
    }
}
