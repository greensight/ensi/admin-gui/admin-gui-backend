<?php

namespace App\Domain\Logistic\Actions\DeliveryPrices;

use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\LogisticClient\Dto\PatchDeliveryPriceRequest;

class PatchDeliveryPriceAction
{
    public function __construct(protected readonly DeliveryPricesApi $deliveryPricesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): DeliveryPrice
    {
        $request = new PatchDeliveryPriceRequest($fields);

        return $this->deliveryPricesApi->patchDeliveryPrice($id, $request)->getData();
    }
}
