<?php

namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;

class DeleteDeliveryKpiCtAction
{
    public function __construct(protected readonly KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $sellerId): void
    {
        $this->kpiApi->deleteDeliveryKpiCt($sellerId);
    }
}
