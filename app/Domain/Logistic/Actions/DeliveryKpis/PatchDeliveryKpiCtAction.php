<?php

namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiCt;
use Ensi\LogisticClient\Dto\PatchDeliveryKpiCtRequest;

class PatchDeliveryKpiCtAction
{
    public function __construct(protected readonly KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiCt
    {
        $request = new PatchDeliveryKpiCtRequest($fields);

        return $this->kpiApi->patchDeliveryKpiCt($sellerId, $request)->getData();
    }
}
