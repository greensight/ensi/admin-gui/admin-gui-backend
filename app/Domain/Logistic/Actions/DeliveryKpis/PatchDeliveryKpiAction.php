<?php

namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpi;
use Ensi\LogisticClient\Dto\PatchDeliveryKpiRequest;

class PatchDeliveryKpiAction
{
    public function __construct(protected readonly KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): DeliveryKpi
    {
        $request = new PatchDeliveryKpiRequest($fields);

        return $this->kpiApi->patchDeliveryKpi($request)->getData();
    }
}
