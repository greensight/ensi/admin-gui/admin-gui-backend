<?php

namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateDeliveryKpiCtRequest;
use Ensi\LogisticClient\Dto\DeliveryKpiCt;

class CreateDeliveryKpiCtAction
{
    public function __construct(protected readonly KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiCt
    {
        $request = new CreateDeliveryKpiCtRequest($fields);

        return $this->kpiApi->createDeliveryKpiCt($sellerId, $request)->getData();
    }
}
