<?php

namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;
use Ensi\LogisticClient\Dto\PatchDeliveryKpiPptRequest;

class PatchDeliveryKpiPptAction
{
    public function __construct(protected readonly KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiPpt
    {
        $request = new PatchDeliveryKpiPptRequest($fields);

        return $this->kpiApi->patchDeliveryKpiPpt($sellerId, $request)->getData();
    }
}
