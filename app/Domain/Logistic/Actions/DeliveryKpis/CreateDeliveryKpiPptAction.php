<?php

namespace App\Domain\Logistic\Actions\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CreateDeliveryKpiPptRequest;
use Ensi\LogisticClient\Dto\DeliveryKpiPpt;

class CreateDeliveryKpiPptAction
{
    public function __construct(protected readonly KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $sellerId, array $fields): DeliveryKpiPpt
    {
        $request = new CreateDeliveryKpiPptRequest($fields);

        return $this->kpiApi->createDeliveryKpiPpt($sellerId, $request)->getData();
    }
}
