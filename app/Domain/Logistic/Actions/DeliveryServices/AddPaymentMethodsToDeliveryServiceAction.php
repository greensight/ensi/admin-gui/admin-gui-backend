<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\AddPaymentMethodsToDeliveryServiceRequest;

class AddPaymentMethodsToDeliveryServiceAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $deliveryServiceId, array $paymentMethods): void
    {
        $request = new AddPaymentMethodsToDeliveryServiceRequest();
        $request->setPaymentMethods($paymentMethods);

        $this->deliveryServicesApi->addPaymentMethodsToDeliveryService($deliveryServiceId, $request);
    }
}
