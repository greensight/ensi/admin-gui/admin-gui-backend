<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\PatchDeliveryServiceRequest;

class PatchDeliveryServiceAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $id, array $fields): DeliveryService
    {
        $request = new PatchDeliveryServiceRequest($fields);

        return $this->deliveryServicesApi->patchDeliveryService($id, $request)->getData();
    }
}
