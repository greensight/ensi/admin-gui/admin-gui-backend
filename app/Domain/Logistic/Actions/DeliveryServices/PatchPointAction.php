<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Dto\PatchPointRequest;
use Ensi\LogisticClient\Dto\Point;

class PatchPointAction
{
    public function __construct(protected readonly GeosApi $geosApi)
    {
    }

    public function execute(int $id, array $fields): Point
    {
        $request = new PatchPointRequest($fields);

        return $this->geosApi->patchPoint($id, $request)->getData();
    }
}
