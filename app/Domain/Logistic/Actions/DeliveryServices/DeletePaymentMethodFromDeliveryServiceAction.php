<?php

namespace App\Domain\Logistic\Actions\DeliveryServices;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeletePaymentMethodFromDeliveryServiceRequest;

class DeletePaymentMethodFromDeliveryServiceAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $deliveryServiceId, int $paymentMethod): void
    {
        $request = new DeletePaymentMethodFromDeliveryServiceRequest();
        $request->setPaymentMethod($paymentMethod);

        $this->deliveryServicesApi->deletePaymentMethodFromDeliveryService($deliveryServiceId, $request);
    }
}
