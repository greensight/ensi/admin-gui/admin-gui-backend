<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManagers;

use Ensi\LogisticClient\Api\DeliveryServicesApi;

class DeleteDeliveryServiceManagerAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $id): void
    {
        $this->deliveryServicesApi->deleteDeliveryServiceManager($id);
    }
}
