<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManagers;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceManager;
use Ensi\LogisticClient\Dto\PatchDeliveryServiceManagerRequest;

class PatchDeliveryServiceManagerAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $id, array $fields): DeliveryServiceManager
    {
        $request = new PatchDeliveryServiceManagerRequest($fields);

        return $this->deliveryServicesApi->patchDeliveryServiceManager($id, $request)->getData();
    }
}
