<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceManagers;

use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\CreateDeliveryServiceManagerRequest;
use Ensi\LogisticClient\Dto\DeliveryServiceManager;

class CreateDeliveryServiceManagerAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(array $fields): DeliveryServiceManager
    {
        $request = new CreateDeliveryServiceManagerRequest($fields);

        return $this->deliveryServicesApi->createDeliveryServiceManager($request)->getData();
    }
}
