<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocuments;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\CreateDeliveryServiceDocumentRequest;

class CreateDeliveryServiceDocumentAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(array $fields): DeliveryServiceDocumentData
    {
        $request = new CreateDeliveryServiceDocumentRequest($fields);

        return new DeliveryServiceDocumentData(
            $this->deliveryServicesApi->createDeliveryServiceDocument($request)->getData()
        );
    }
}
