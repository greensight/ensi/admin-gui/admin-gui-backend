<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocuments;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;

class DeleteDeliveryServiceDocumentFileAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData(
            $this->deliveryServicesApi->deleteDeliveryServiceDocumentFile($deliveryServiceDocumentId)->getData()
        );
    }
}
