<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocuments;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Illuminate\Http\UploadedFile;

class SaveDeliveryServiceDocumentFileAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $deliveryServiceDocumentId, UploadedFile $file): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData(
            $this->deliveryServicesApi->uploadDeliveryServiceDocumentFile(
                $deliveryServiceDocumentId,
                $file->openFile(),
                $file->getClientOriginalName()
            )->getData()
        );
    }
}
