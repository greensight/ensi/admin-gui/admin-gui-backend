<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocuments;

use Ensi\LogisticClient\Api\DeliveryServicesApi;

class DeleteDeliveryServiceDocumentAction
{
    public function __construct(protected readonly DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function execute(int $id)
    {
        $this->deliveryServicesApi->deleteDeliveryServiceDocument($id);
    }
}
