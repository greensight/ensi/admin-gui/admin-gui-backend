<?php

namespace App\Domain\Logistic\Actions\DeliveryServiceDocuments;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\PatchDeliveryServiceDocumentRequest;

class PatchDeliveryServiceDocumentAction
{
    public function __construct(protected readonly DeliveryServicesApi $api)
    {
    }

    public function execute(int $id, array $fields): DeliveryServiceDocumentData
    {
        $request = new PatchDeliveryServiceDocumentRequest($fields);

        return new DeliveryServiceDocumentData($this->api->patchDeliveryServiceDocument($id, $request)->getData());
    }
}
