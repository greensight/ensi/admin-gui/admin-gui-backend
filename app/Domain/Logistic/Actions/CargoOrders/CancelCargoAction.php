<?php

namespace App\Domain\Logistic\Actions\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\ApiException;

class CancelCargoAction
{
    public function __construct(protected readonly CargoOrdersApi $cargoOrdersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): CargoData
    {
        return new CargoData($this->cargoOrdersApi->cancelCargo($id)->getData());
    }
}
