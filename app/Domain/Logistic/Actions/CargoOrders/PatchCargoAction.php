<?php

namespace App\Domain\Logistic\Actions\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\PatchCargoRequest;

class PatchCargoAction
{
    public function __construct(protected readonly CargoOrdersApi $cargoOrdersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): CargoData
    {
        $request = new PatchCargoRequest($fields);

        return new CargoData($this->cargoOrdersApi->patchCargo($id, $request)->getData());
    }
}
