<?php

namespace App\Domain\Logistic\Actions\CargoOrders;

use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\CargoOrder;

class CancelCargoOrderAction
{
    public function __construct(protected readonly CargoOrdersApi $cargoOrdersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): CargoOrder
    {
        return $this->cargoOrdersApi->cancelCargoOrder($id)->getData();
    }
}
