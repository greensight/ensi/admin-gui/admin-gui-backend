<?php

namespace App\Domain\Logistic\Tests\CargoOrders\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticCargoStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\Cargo;
use Ensi\LogisticClient\Dto\CargoResponse;
use Ensi\LogisticClient\Dto\SearchCargoResponse;

class CargoFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'delivery_service_id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(array_column(LogisticCargoStatusEnum::cases(), 'value')),
            'status_at' => $this->faker->dateTime(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->dateTime(),
            'shipping_problem_comment' => $this->faker->text(200),
            'is_canceled' => $this->faker->boolean(),
            'is_canceled_at' => $this->faker->dateTime(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
        ];
    }

    public function make(array $extra = []): Cargo
    {
        return new Cargo($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CargoResponse
    {
        return new CargoResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCargoResponse
    {
        return $this->generateResponseSearch(SearchCargoResponse::class, $extras, $count, $pagination);
    }
}
