<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\Point;
use Ensi\LogisticClient\Dto\PointResponse;
use Ensi\LogisticClient\Dto\PointWorking;
use Ensi\LogisticClient\Dto\SearchPointsResponse;

class PointFactory extends BaseApiFactory
{
    protected array $pointWorkings = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'delivery_service_id' => $this->faker->nullable()->modelId(),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'is_active' => $this->faker->boolean(),
            'name' => $this->faker->text(50),
            'description' => $this->faker->nullable()->text(50),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'timezone' => $this->faker->nullable()->timezone(),
            'address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'address_reduce' => $this->faker->nullable()->address(),
            'metro_station' => $this->faker->nullable()->text(50),
            'city_guid' => $this->faker->nullable()->uuid(),
            'geo_lat' => $this->faker->nullable()->exactly((string)$this->faker->latitude()),
            'geo_lon' => $this->faker->nullable()->exactly((string)$this->faker->longitude()),
            'only_online_payment' => $this->faker->nullable()->boolean(),
            'has_payment_card' => $this->faker->nullable()->boolean(),
            'has_courier' => $this->faker->nullable()->boolean(),
            'is_postamat' => $this->faker->nullable()->boolean(),
            'max_value' => $this->faker->nullable()->exactly((string)$this->faker->numberBetween(1, 100)),
            'max_weight' => $this->faker->numberBetween(1, 100),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->pointWorkings) {
            $definition['point_workings'] = $this->pointWorkings;
        }

        return $definition;
    }

    public function make(array $extra = []): Point
    {
        return new Point($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): PointResponse
    {
        return new PointResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPointsResponse
    {
        return $this->generateResponseSearch(SearchPointsResponse::class, $extras, $count, $pagination);
    }

    public function withPointWorking(?PointWorking $pointWorking = null): self
    {
        $this->pointWorkings[] = $pointWorking ?: PointWorkingFactory::new()->make();

        return $this;
    }
}
