<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryService;
use Ensi\LogisticClient\Dto\DeliveryServiceManager;
use Ensi\LogisticClient\Dto\DeliveryServiceManagerResponse;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceManagersResponse;

class DeliveryServiceManagerFactory extends BaseApiFactory
{
    protected ?DeliveryService $deliveryService = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'delivery_service_id' => $this->faker->randomElement(array_column(LogisticDeliveryServiceEnum::cases(), 'value')),
            'name' => $this->faker->name(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->deliveryService) {
            $definition['delivery_service'] = $this->deliveryService;
        }

        return $definition;
    }

    public function make(array $extra = []): DeliveryServiceManager
    {
        return new DeliveryServiceManager($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DeliveryServiceManagerResponse
    {
        return new DeliveryServiceManagerResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDeliveryServiceManagersResponse
    {
        return $this->generateResponseSearch(SearchDeliveryServiceManagersResponse::class, $extras, $count, $pagination);
    }

    public function withDeliveryService(?DeliveryService $deliveryService = null): self
    {
        $this->deliveryService = $deliveryService ?: DeliveryServiceFactory::new()->make();

        return $this;
    }
}
