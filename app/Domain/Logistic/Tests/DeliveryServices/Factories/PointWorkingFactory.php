<?php

namespace App\Domain\Logistic\Tests\DeliveryServices\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\PointWorking;

class PointWorkingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'point_id' => $this->faker->modelId(),
            'is_active' => $this->faker->boolean(),
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->numerify('##:##'),
            'working_end_time' => $this->faker->numerify('##:##'),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): PointWorking
    {
        return new PointWorking($this->makeArray($extra));
    }
}
