<?php

namespace App\Domain\Logistic\Tests\DeliveryPrices\Factories;

use App\Domain\Logistic\Tests\Geos\Factories\FederalDistrictsFactory;
use App\Domain\Logistic\Tests\Geos\Factories\RegionsFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryPrice;
use Ensi\LogisticClient\Dto\DeliveryPriceResponse;
use Ensi\LogisticClient\Dto\FederalDistrict;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse;

class DeliveryPricesFactory extends BaseApiFactory
{
    protected ?FederalDistrict $federalDistrict = null;
    protected ?Region $region = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'federal_district_id' => $this->faker->modelId(),
            'region_id' => $this->faker->modelId(),
            'region_guid' => $this->faker->uuid(),
            'delivery_service' => $this->faker->randomElement(array_column(LogisticDeliveryServiceEnum::cases(), 'value')),
            'delivery_method' => $this->faker->randomElement(array_column(LogisticDeliveryMethodEnum::cases(), 'value')),
            'price' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->region) {
            $definition['region'] = $this->region;
        }

        if ($this->federalDistrict) {
            $definition['federal_district'] = $this->federalDistrict;
        }

        return $definition;
    }

    public function make(array $extra = []): DeliveryPrice
    {
        return new DeliveryPrice($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DeliveryPriceResponse
    {
        return new DeliveryPriceResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDeliveryPricesResponse
    {
        return $this->generateResponseSearch(SearchDeliveryPricesResponse::class, $extras, $count, $pagination);
    }

    public function withRegion(?Region $region = null): self
    {
        $this->region = $region ?: RegionsFactory::new()->make();

        return $this;
    }

    public function withFederalDistrict(?FederalDistrict $federalDistrict = null): self
    {
        $this->federalDistrict = $federalDistrict ?: FederalDistrictsFactory::new()->make();

        return $this;
    }
}
