<?php

namespace App\Domain\Logistic\Tests\Geos\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\City;
use Ensi\LogisticClient\Dto\Region;
use Ensi\LogisticClient\Dto\RegionResponse;
use Ensi\LogisticClient\Dto\SearchRegionsResponse;

class RegionsFactory extends BaseApiFactory
{
    protected array $cities = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'federal_district_id' => $this->faker->modelId(),
            'name' => $this->faker->name(),
            'guid' => $this->faker->uuid(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->cities) {
            $definition['cities'] = $this->cities;
        }

        return $definition;
    }

    public function make(array $extra = []): Region
    {
        return new Region($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): RegionResponse
    {
        return new RegionResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchRegionsResponse
    {
        return $this->generateResponseSearch(SearchRegionsResponse::class, $extras, $count, $pagination);
    }

    public function withCity(?City $city = null): self
    {
        $this->cities[] = $city ?: CityFactory::new()->make();

        return $this;
    }
}
