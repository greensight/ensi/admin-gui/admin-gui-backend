<?php

namespace App\Domain\Logistic\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFile;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;
use Ensi\LogisticClient\Dto\FileOrNull;

class DeliveryServiceDocumentFile extends ProtectedFile
{
    public static function entity(): string
    {
        return 'logistic/delivery-service-document';
    }

    public static function createFromModel(DeliveryServiceDocument $deliveryServiceDocument, FileOrNull $file): static
    {
        $object = new static();
        $object->entity_id = $deliveryServiceDocument->getId();
        $object->file = $file->getPath();

        return $object;
    }
}
