<?php

namespace App\Domain\Logistic\ProtectedFiles;

use App\Domain\Common\ProtectedFiles\ProtectedFileLoader;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;

class DeliveryServiceDocumentFileLoader extends ProtectedFileLoader
{
    public function __construct(protected DeliveryServicesApi $deliveryServicesApi)
    {
    }

    public function getRootPath(): ?string
    {
        $deliveryServiceDocument = $this->loadDeliveryServiceDocument();

        $file = $deliveryServiceDocument->getFile();
        if ($file && $file->getPath() == $this->file->file) {
            return $file->getRootPath();
        }

        return null;
    }

    protected function loadDeliveryServiceDocument(): DeliveryServiceDocument
    {
        return $this->deliveryServicesApi->getDeliveryServiceDocument($this->file->entity_id)->getData();
    }
}
