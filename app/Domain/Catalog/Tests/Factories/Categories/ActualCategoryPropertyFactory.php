<?php

namespace App\Domain\Catalog\Tests\Factories\Categories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\ActualCategoryProperty;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\Property;
use Ensi\PimClient\Dto\SearchActualCategoryPropertiesResponse;

class ActualCategoryPropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
            'is_required' => $this->faker->boolean,
            'is_gluing' => $this->faker->boolean,
            'is_inherited' => $this->faker->boolean,
            'is_common' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): ActualCategoryProperty
    {
        return new ActualCategoryProperty($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchActualCategoryPropertiesResponse
    {
        return $this->generateResponseSearch(SearchActualCategoryPropertiesResponse::class, $extras, $count, $pagination);
    }

    public function withCategory(?Category $category): static
    {
        return $this->state(['category' => $category ?? CategoryFactory::new()->make()]);
    }

    public function withProperty(?Property $property): static
    {
        return $this->state(['property' => $property ?? PropertyFactory::new()->make()]);
    }
}
