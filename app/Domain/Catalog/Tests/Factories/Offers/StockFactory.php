<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\SearchStocksResponse;
use Ensi\OffersClient\Dto\Stock;
use Ensi\OffersClient\Dto\StockResponse;

class StockFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'qty' => $this->faker->nullable()->randomFloat(0, 1, 99),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Stock
    {
        return new Stock($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): StockResponse
    {
        return new StockResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStocksResponse
    {
        return $this->generateResponseSearch(SearchStocksResponse::class, $extras, $count, $pagination);
    }
}
