<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\MassOperationResult;
use Ensi\OffersClient\Dto\MassOperationResultData;
use Ensi\OffersClient\Dto\MassOperationResultDataErrors;

class MassOperationResultFactory extends BaseApiFactory
{
    protected array $processed = [];
    protected array $errors = [];

    protected function definition(): array
    {
        return [
            'processed' => $this->processed,
            'errors' => $this->errors,
        ];
    }

    public function withProcessed(int $id): self
    {
        $this->processed[] = $id;

        return $this;
    }

    public function withError(int $id, string $message): self
    {
        $this->errors[] = (new MassOperationResultDataErrors())
            ->setId($id)
            ->setMessage($message);

        return $this;
    }

    public function make(array $extra = []): MassOperationResultData
    {
        return new MassOperationResultData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): MassOperationResult
    {
        return new MassOperationResult([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
