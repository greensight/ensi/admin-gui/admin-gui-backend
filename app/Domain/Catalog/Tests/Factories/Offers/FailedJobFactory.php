<?php

namespace App\Domain\Catalog\Tests\Factories\Offers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\FailedJob;
use Ensi\OffersClient\Dto\SearchFailedJobsResponse;

class FailedJobFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'uuid' => $this->faker->unique()->uuid(),
            'connection' => $this->faker->word(),
            'queue' => $this->faker->word(),
            'payload' => $this->faker->text(),
            'exception' => $this->faker->text(),
            'failed_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): FailedJob
    {
        return new FailedJob($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1): SearchFailedJobsResponse
    {
        return $this->generateResponseSearch(SearchFailedJobsResponse::class, $extras, $count);
    }
}
