<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductStatusTypeEnum;

abstract class BaseProductStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $type = $this->faker->randomElement(ProductStatusTypeEnum::getAllowableEnumValues());
        $isAutomatic = $type == ProductStatusTypeEnum::AUTOMATIC;

        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug(),
            'color' => $this->faker->nullable()->hexColor(),
            'type' => $type,
            'is_active' => $this->faker->boolean(),
            'is_publication' => $this->faker->boolean(),
            'events' => $this->faker->nullable($isAutomatic)->exactly(ProductStatusEventsFactory::new()->make()),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function withEvent(): static
    {
        return $this->state([
            'type' => ProductStatusTypeEnum::MANUAL,
            'events' => ProductStatusEventsFactory::new()->make(),
        ]);
    }
}
