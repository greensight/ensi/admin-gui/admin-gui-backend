<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use Ensi\PimClient\Dto\StatusSetting;
use Ensi\PimClient\Dto\StatusSettingProperties;
use Ensi\PimClient\Dto\StatusSettingResponse;
use Ensi\PimClient\Dto\StatusSettingsResponse;

class ProductStatusFactory extends BaseProductStatusFactory
{
    public array $previousStatuses = [];
    public array $nextStatuses = [];

    protected function definition(): array
    {
        $definition = parent::definition();

        if ($this->previousStatuses) {
            $definition['previous_statuses'] = $this->previousStatuses;
        }

        if ($this->nextStatuses) {
            $definition['next_statuses'] = $this->nextStatuses;
        }

        return $definition;
    }

    public function make(array $extra = []): StatusSetting
    {
        return new StatusSetting($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): StatusSettingResponse
    {
        return new StatusSettingResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): StatusSettingsResponse
    {
        return $this->generateResponseSearch(StatusSettingsResponse::class, $extras, $count, $pagination);
    }

    public function withPreviousStatus(?StatusSettingProperties $status = null): self
    {
        $this->previousStatuses[] = $status ?: ProductStatusPropertiesFactory::new()->make();

        return $this;
    }

    public function withNextStatus(?StatusSettingProperties $status = null): self
    {
        $this->nextStatuses[] = $status ?: ProductStatusPropertiesFactory::new()->make();

        return $this;
    }
}
