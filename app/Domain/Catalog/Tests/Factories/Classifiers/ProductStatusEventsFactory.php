<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\EventConditions;
use Ensi\PimClient\Dto\EventOperationEnum;
use Ensi\PimClient\Dto\ProductEventEnum;

class ProductStatusEventsFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'operation' => $this->faker->nullable()->randomElement(EventOperationEnum::getAllowableEnumValues()),
            'events' => $this->faker->randomList(fn () => $this->faker->randomElement(ProductEventEnum::getAllowableEnumValues()), 1, 3),
        ];
    }

    public function make(array $extra = []): EventConditions
    {
        return new EventConditions($this->makeArray($extra));
    }
}
