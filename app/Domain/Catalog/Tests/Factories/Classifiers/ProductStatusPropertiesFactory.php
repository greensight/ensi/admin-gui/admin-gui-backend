<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use Ensi\PimClient\Dto\StatusSettingProperties;

class ProductStatusPropertiesFactory extends BaseProductStatusFactory
{
    public function make(array $extra = []): StatusSettingProperties
    {
        return new StatusSettingProperties($this->makeArray($extra));
    }
}
