<?php

namespace App\Domain\Catalog\Tests\Factories\Classifiers;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Status;
use Ensi\PimClient\Dto\StatusesResponse;

class ProductStatusDataFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->text(20),
            'available' => $this->faker->boolean(),
        ];

    }

    public function make(array $extra = []): Status
    {
        return new Status($this->makeArray($extra));
    }

    public function makeResponse(array $extras = [], int $count = 1): StatusesResponse
    {
        $data = [];
        $count = $extras ? count($extras) : $count;
        for ($i = 0; $i < $count; $i++) {
            $data[] = $this->make($extras[$i] ?? []);
        }

        return new StatusesResponse(['data' => $data]);
    }
}
