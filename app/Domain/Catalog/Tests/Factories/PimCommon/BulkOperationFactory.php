<?php

namespace App\Domain\Catalog\Tests\Factories\PimCommon;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\BulkOperation;
use Ensi\PimClient\Dto\BulkOperationActionEnum;
use Ensi\PimClient\Dto\BulkOperationEntityEnum;
use Ensi\PimClient\Dto\BulkOperationError;
use Ensi\PimClient\Dto\BulkOperationStatusEnum;
use Ensi\PimClient\Dto\SearchBulkOperationsResponse;

class BulkOperationFactory extends BaseApiFactory
{
    protected array $errors = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'action' => $this->faker->randomElement(BulkOperationActionEnum::getAllowableEnumValues()),
            'entity' => $this->faker->randomElement(BulkOperationEntityEnum::getAllowableEnumValues()),
            'status' => $this->faker->randomElement(BulkOperationStatusEnum::getAllowableEnumValues()),
            'ids' => range($this->faker->randomDigit(), $this->faker->numberBetween(10, 20)),
            'error_message' => $this->faker->nullable()->text(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->errors) {
            $definition['errors'] = $this->errors;
        }

        return $definition;
    }

    public function withError(?BulkOperationError $error = null): self
    {
        $this->errors[] = $error ?: BulkOperationErrorFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): BulkOperation
    {
        return new BulkOperation($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchBulkOperationsResponse
    {
        return $this->generateResponseSearch(SearchBulkOperationsResponse::class, $extra, $count);
    }
}
