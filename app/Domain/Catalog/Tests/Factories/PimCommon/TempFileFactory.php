<?php

namespace App\Domain\Catalog\Tests\Factories\PimCommon;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\SearchTempFilesResponse;
use Ensi\PimClient\Dto\TempFile;

class TempFileFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'path' => $this->faker->filePath(),
            'created_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): TempFile
    {
        return new TempFile($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchTempFilesResponse
    {
        return $this->generateResponseSearch(SearchTempFilesResponse::class, $extras, $count, $pagination);
    }
}
