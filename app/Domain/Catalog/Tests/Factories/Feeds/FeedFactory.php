<?php

namespace App\Domain\Catalog\Tests\Factories\Feeds;

use Ensi\FeedClient\Dto\Feed;
use Ensi\FeedClient\Dto\FeedResponse;
use Ensi\FeedClient\Dto\FeedSettings;
use Ensi\FeedClient\Dto\File;
use Ensi\FeedClient\Dto\SearchFeedsResponse;
use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class FeedFactory extends BaseApiFactory
{
    protected ?FeedSettings $feedSettings = null;

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'feed_settings_id' => $this->faker->modelId(),
            'code' => $this->faker->unique()->word(),
            'file' => new File(EnsiFileFactory::new()->make()),
            'planned_delete_at' => $this->faker->dateTime(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->feedSettings) {
            $definition['feed_settings'] = $this->feedSettings;
        }

        return $definition;
    }

    public function withFeedSettings(?FeedSettings $feedSettings = null): self
    {
        $this->feedSettings = $feedSettings ?? FeedSettingsFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): Feed
    {
        return new Feed($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): FeedResponse
    {
        return new FeedResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchFeedsResponse
    {
        return $this->generateResponseSearch(SearchFeedsResponse::class, $extras, $count, $pagination);
    }
}
