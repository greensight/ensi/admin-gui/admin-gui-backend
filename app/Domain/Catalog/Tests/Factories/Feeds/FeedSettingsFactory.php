<?php

namespace App\Domain\Catalog\Tests\Factories\Feeds;

use Ensi\FeedClient\Dto\Feed;
use Ensi\FeedClient\Dto\FeedPlatformEnum;
use Ensi\FeedClient\Dto\FeedSettings;
use Ensi\FeedClient\Dto\FeedSettingsResponse;
use Ensi\FeedClient\Dto\FeedTypeEnum;
use Ensi\FeedClient\Dto\SearchFeedSettingsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class FeedSettingsFactory extends BaseApiFactory
{
    protected array $feeds = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->word(),
            'active' => $this->faker->boolean(),
            'type' => $this->faker->randomElement(FeedTypeEnum::getAllowableEnumValues()),
            'platform' => $this->faker->randomElement(FeedPlatformEnum::getAllowableEnumValues()),
            'active_product' => $this->faker->boolean(),
            'active_category' => $this->faker->boolean(),
            'shop_name' => $this->faker->sentence(),
            'shop_url' => $this->faker->sentence(),
            'shop_company' => $this->faker->sentence(),
            'update_time' => $this->faker->numberBetween(1, 10),
            'delete_time' => $this->faker->numberBetween(1, 10),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if (filled($this->feeds)) {
            $definition['feeds'] = $this->feeds;
        }

        return $definition;
    }

    public function withFeeds(?Feed $feeds = null): self
    {
        $this->feeds[] = $feeds ?? FeedFactory::new()->make();

        return $this;
    }

    public function make(array $extra = []): FeedSettings
    {
        return new FeedSettings($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): FeedSettingsResponse
    {
        return new FeedSettingsResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchFeedSettingsResponse
    {
        return $this->generateResponseSearch(SearchFeedSettingsResponse::class, $extras, $count, $pagination);
    }
}
