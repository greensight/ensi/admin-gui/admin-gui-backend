<?php

namespace App\Domain\Catalog\Tests\Factories\Feeds;

use Ensi\FeedClient\Dto\CloudIntegration;
use Ensi\FeedClient\Dto\CloudIntegrationResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CloudIntegrationFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $integration = $this->faker->boolean;

        return [
            'private_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),
            'public_api_key' => $this->faker->nullable($integration ? 1 : 0)->ean8(),

            'integration' => $integration,

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): CloudIntegration
    {
        return new CloudIntegration($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CloudIntegrationResponse
    {
        return new CloudIntegrationResponse(['data' => $this->make($extra)]);
    }
}
