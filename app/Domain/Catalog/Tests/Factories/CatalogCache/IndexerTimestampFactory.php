<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\IndexerTimestamp;
use Ensi\CatalogCacheClient\Dto\IndexerTimestampResponse;
use Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class IndexerTimestampFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'index' => $this->faker->word(),
            'stage' => $this->faker->word(),
            'index_hash' => $this->faker->ean8(),
            'last_schedule' => $this->faker->dateTime(),

            'is_current_index' => $this->faker->boolean(),
            'is_current_stage' => $this->faker->boolean(),
            'is_current' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): IndexerTimestamp
    {
        return new IndexerTimestamp($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchIndexerTimestampsResponse
    {
        return $this->generateResponseSearch(SearchIndexerTimestampsResponse::class, $extras, $count, $pagination);
    }

    public function makeResponse(array $extra = []): IndexerTimestampResponse
    {
        return new IndexerTimestampResponse(['data' => $this->make($extra)]);
    }
}
