<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\ActualCategoryProperty;
use Ensi\CatalogCacheClient\Dto\Category;
use Ensi\CatalogCacheClient\Dto\SearchCategoriesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CategoryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'name' => $this->faker->company,
            'code' => $this->faker->slug,
            'parent_id' => $this->faker->nullable()->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): Category
    {
        return new Category($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCategoriesResponse
    {
        return $this->generateResponseSearch(SearchCategoriesResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param ActualCategoryProperty[]|null $properties
     */
    public function withProperties(?array $properties = null): static
    {
        return $this->state(['actual_properties' => $properties ?? ActualCategoryPropertyFactory::new()->makeSeveral(1)->all()]);
    }
}
