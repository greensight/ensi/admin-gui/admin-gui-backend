<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Discount;
use Ensi\CatalogCacheClient\Dto\Offer;
use Ensi\CatalogCacheClient\Dto\SearchDiscountsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class DiscountFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'value_type' => $this->faker->numberBetween(0, 1),
            'value' => $this->faker->numberBetween(0, 1),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Discount
    {
        return new Discount($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDiscountsResponse
    {
        return $this->generateResponseSearch(SearchDiscountsResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param Offer|null $offer
     */
    public function withOffer(?Offer $offer = null): static
    {
        return $this->state(['offer' => $offer ?? OfferFactory::new()->make()]);
    }
}
