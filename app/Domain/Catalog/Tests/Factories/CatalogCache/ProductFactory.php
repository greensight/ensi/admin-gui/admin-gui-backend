<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Brand;
use Ensi\CatalogCacheClient\Dto\Category;
use Ensi\CatalogCacheClient\Dto\Image;
use Ensi\CatalogCacheClient\Dto\Nameplate;
use Ensi\CatalogCacheClient\Dto\Offer;
use Ensi\CatalogCacheClient\Dto\Product;
use Ensi\CatalogCacheClient\Dto\ProductGroup;
use Ensi\CatalogCacheClient\Dto\ProductPropertyValue;
use Ensi\CatalogCacheClient\Dto\SearchProductsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'allow_publish' => $this->faker->boolean,
            'main_image_file' => $this->faker->filePath(),
            'category_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), 1),
            'brand_id' => $this->faker->nullable()->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->text,
            'type' => $this->faker->randomNumber(),
            'vendor_code' => $this->faker->numerify('###-###-###'),
            'barcode' => $this->faker->nullable()->ean13(),
            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,
            'uom' => $this->faker->nullable()->randomNumber(),
            'tariffing_volume' => $this->faker->nullable()->randomNumber(),
            'order_step' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable()->randomFloat(2, 1, 100),
            'is_migrated' => $this->faker->boolean,
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Product
    {
        return new Product($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductsResponse
    {
        return $this->generateResponseSearch(SearchProductsResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param Category[]|null $categories
     */
    public function withCategories(?array $categories = null): static
    {
        return $this->state(['categories' => $categories ?? CategoryFactory::new()->makeSeveral(1)->all()]);
    }

    /**
     * @param Brand|null $brand
     */
    public function withBrand(?Brand $brand = null): static
    {
        return $this->state(['brand' => $brand ?? BrandFactory::new()->make()]);
    }

    /**
     * @param Image[]|null $images
     */
    public function withImages(?array $images = null): static
    {
        return $this->state(['images' => $images ?? ImageFactory::new()->makeSeveral(1)->all()]);
    }

    /**
     * @param ProductPropertyValue[]|null $propertyValues
     */
    public function withProductPropertyValues(?array $propertyValues = null): static
    {
        return $this->state(['product_property_values' => $propertyValues ?? ProductPropertyValueFactory::new()->makeSeveral(1)->all()]);
    }

    /**
     * @param Nameplate[]|null $nameplates
     */
    public function withNameplates(?array $nameplates = null): static
    {
        return $this->state(['nameplates' => $nameplates ?? NameplateFactory::new()->makeSeveral(1)->all()]);
    }

    /**
     * @param ProductGroup|null $productGroup
     */
    public function withProductGroup(?ProductGroup $productGroup = null): static
    {
        return $this->state(['product_group' => $productGroup ?? ProductGroupFactory::new()->make()]);
    }

    /**
     * @param Offer[]|null $offers
     */
    public function withOffers(?array $offers = null): static
    {
        return $this->state(['offers' => $offers ?? OfferFactory::new()->makeSeveral(1)->all()]);
    }

    public function withAllRelations(): static
    {
        return $this->withCategories()->withBrand()->withImages()->withProductPropertyValues()->withNameplates()->withProductGroup()->withOffers();
    }
}
