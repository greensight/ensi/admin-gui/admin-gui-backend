<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Property;
use Ensi\CatalogCacheClient\Dto\SearchPropertiesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'name' => $this->faker->company,
            'code' => $this->faker->slug,
            'type' => $this->faker->text,

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_public' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): Property
    {
        return new Property($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPropertiesResponse
    {
        return $this->generateResponseSearch(SearchPropertiesResponse::class, $extras, $count, $pagination);
    }
}
