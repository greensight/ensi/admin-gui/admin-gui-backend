<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\PropertyDirectoryValue;
use Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PropertyDirectoryValueFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'directory_value_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'name' => $this->faker->nullable()->name,
            'code' => $this->faker->nullable()->text,
            'type' => $this->faker->text,
            'value' => $this->faker->text,

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): PropertyDirectoryValue
    {
        return new PropertyDirectoryValue($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPropertyDirectoryValuesResponse
    {
        return $this->generateResponseSearch(SearchPropertyDirectoryValuesResponse::class, $extras, $count, $pagination);
    }
}
