<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\NameplateProduct;
use Ensi\CatalogCacheClient\Dto\SearchNameplateProductsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'nameplate_product_id' => $this->faker->modelId(),
            'nameplate_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): NameplateProduct
    {
        return new NameplateProduct($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchNameplateProductsResponse
    {
        return $this->generateResponseSearch(SearchNameplateProductsResponse::class, $extras, $count, $pagination);
    }
}
