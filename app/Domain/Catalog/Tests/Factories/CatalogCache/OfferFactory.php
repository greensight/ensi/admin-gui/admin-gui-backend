<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Discount;
use Ensi\CatalogCacheClient\Dto\Offer;
use Ensi\CatalogCacheClient\Dto\Product;
use Ensi\CatalogCacheClient\Dto\ProductGroup;
use Ensi\CatalogCacheClient\Dto\ProductGroupProduct;
use Ensi\CatalogCacheClient\Dto\SearchOffersResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class OfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'base_price' => $this->faker->numberBetween(2, 10),
            'price' => $this->faker->numberBetween(2, 10),
            'is_active' => $this->faker->boolean,
            'is_migrated' => $this->faker->boolean,
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param Product|null $product
     */
    public function withProduct(?Product $product = null): static
    {
        return $this->state(['product' => $product ?? ProductFactory::new()->make()]);
    }

    /**
     * @param Discount|null $discount
     */
    public function withDiscount(?Discount $discount = null): static
    {
        return $this->state(['discount' => $discount ?? DiscountFactory::new()->make()]);
    }

    /**
     * @param ProductGroup|null $productGroup
     */
    public function withProductGroup(?ProductGroup $productGroup = null): static
    {
        return $this->state(['product_group' => $productGroup ?? ProductGroupFactory::new()->make()]);
    }

    /**
     * @param ProductGroupProduct|null $productGroupProduct
     */
    public function withProductGroupProduct(?ProductGroupProduct $productGroupProduct = null): static
    {
        return $this->state(['product_group_product' => $productGroupProduct ?? ProductGroupProductFactory::new()->make()]);
    }

    public function withAllRelations(): static
    {
        return $this->withProduct()->withDiscount()->withProductGroup()->withProductGroupProduct();
    }
}
