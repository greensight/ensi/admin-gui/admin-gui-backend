<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Nameplate;
use Ensi\CatalogCacheClient\Dto\NameplateProduct;
use Ensi\CatalogCacheClient\Dto\SearchNameplatesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'nameplate_id' => $this->faker->modelId(),
            'name' => $this->faker->company,
            'code' => $this->faker->slug,
            'background_color' => $this->faker->hexColor,
            'text_color' => $this->faker->hexColor,

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_active' => $this->faker->boolean(),
            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): Nameplate
    {
        return new Nameplate($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchNameplatesResponse
    {
        return $this->generateResponseSearch(SearchNameplatesResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param NameplateProduct[]|null $products
     */
    public function withProducts(?array $products = null): static
    {
        return $this->state(['product_links' => $products ?? NameplateProductFactory::new()->makeSeveral(1)->all()]);
    }
}
