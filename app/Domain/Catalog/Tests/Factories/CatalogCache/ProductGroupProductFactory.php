<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Product;
use Ensi\CatalogCacheClient\Dto\ProductGroup;
use Ensi\CatalogCacheClient\Dto\ProductGroupProduct;
use Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductGroupProductFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_group_id' => $this->faker->modelId(),
            'product_group_product_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): ProductGroupProduct
    {
        return new ProductGroupProduct($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductGroupProductsResponse
    {
        return $this->generateResponseSearch(SearchProductGroupProductsResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param ProductGroup|null $productGroup
     */
    public function withProductGroup(?ProductGroup $productGroup = null): static
    {
        return $this->state(['product_group' => $productGroup ?? ProductGroupFactory::new()->make()]);
    }

    /**
     * @param Product|null $product
     */
    public function withProduct(?Product $product = null): static
    {
        return $this->state(['product' => $product ?? ProductFactory::new()->make()]);
    }

    public function withAllRelations(): static
    {
        return $this->withProduct()->withProductGroup();
    }
}
