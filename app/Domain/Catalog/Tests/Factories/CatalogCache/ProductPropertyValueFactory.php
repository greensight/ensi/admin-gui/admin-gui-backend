<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\ProductPropertyValue;
use Ensi\CatalogCacheClient\Dto\Property;
use Ensi\CatalogCacheClient\Dto\PropertyDirectoryValue;
use Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductPropertyValueFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_property_value_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'directory_value_id' => $this->faker->modelId(),
            'name' => $this->faker->name(),
            'type' => $this->faker->word(),
            'value' => $this->faker->word(),
            'is_migrated' => $this->faker->boolean,
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): ProductPropertyValue
    {
        return new ProductPropertyValue($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductPropertyValuesResponse
    {
        return $this->generateResponseSearch(SearchProductPropertyValuesResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param Property|null $property
     */
    public function withProperty(?Property $property = null): static
    {
        return $this->state(['property' => $property ?? PropertyFactory::new()->make()]);
    }

    /**
     * @param PropertyDirectoryValue|null $value
     */
    public function withDirectoryValue(?PropertyDirectoryValue $value = null): static
    {
        return $this->state(['directory_value' => $value ?? PropertyDirectoryValueFactory::new()->make()]);
    }

    public function withAllRelations(): static
    {
        return $this->withProperty()->withDirectoryValue();
    }
}
