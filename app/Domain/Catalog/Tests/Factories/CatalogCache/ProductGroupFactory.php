<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Category;
use Ensi\CatalogCacheClient\Dto\Product;
use Ensi\CatalogCacheClient\Dto\ProductGroup;
use Ensi\CatalogCacheClient\Dto\ProductGroupProduct;
use Ensi\CatalogCacheClient\Dto\SearchProductGroupsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductGroupFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_group_id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'name' => $this->faker->text,
            'main_product_id' => $this->faker->modelId(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_active' => $this->faker->boolean(),
            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): ProductGroup
    {
        return new ProductGroup($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductGroupsResponse
    {
        return $this->generateResponseSearch(SearchProductGroupsResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param ProductGroupProduct[]|null $productGroupProducts
     */
    public function withProductGroupProducts(?array $productGroupProducts = null): static
    {
        return $this->state(['product_links' => $productGroupProducts ?? ProductGroupProductFactory::new()->makeSeveral(1)->all()]);
    }

    /**
     * @param Category|null $category
     */
    public function withCategory(?Category $category = null): static
    {
        return $this->state(['category' => $category ?? CategoryFactory::new()->make()]);
    }

    /**
     * @param Product|null $mainProduct
     */
    public function withMainProduct(?Product $mainProduct = null): static
    {
        return $this->state(['main_product' => $mainProduct ?? ProductFactory::new()->make()]);
    }

    /**
     * @param Product[]|null $products
     */
    public function withProducts(?array $products = null): static
    {
        return $this->state(['products' => $products ?? ProductFactory::new()->makeSeveral(1)->all()]);
    }

    public function withAllRelations(): static
    {
        return $this->withCategory()->withMainProduct()->withProducts()->withProductGroupProducts();
    }
}
