<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Brand;
use Ensi\CatalogCacheClient\Dto\SearchBrandsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BrandFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'brand_id' => $this->faker->modelId(),
            'name' => $this->faker->company,
            'code' => $this->faker->slug,
            'description' => $this->faker->nullable()->text(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): Brand
    {
        return new Brand($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchBrandsResponse
    {
        return $this->generateResponseSearch(SearchBrandsResponse::class, $extras, $count, $pagination);
    }
}
