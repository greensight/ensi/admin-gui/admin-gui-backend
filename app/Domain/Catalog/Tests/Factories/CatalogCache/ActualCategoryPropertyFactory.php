<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\ActualCategoryProperty;
use Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ActualCategoryPropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'actual_category_property_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'is_gluing' => $this->faker->boolean(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'is_migrated' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): ActualCategoryProperty
    {
        return new ActualCategoryProperty($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchActualCategoryPropertiesResponse
    {
        return $this->generateResponseSearch(SearchActualCategoryPropertiesResponse::class, $extras, $count, $pagination);
    }
}
