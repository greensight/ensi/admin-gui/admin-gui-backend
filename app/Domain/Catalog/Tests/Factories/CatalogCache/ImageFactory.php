<?php

namespace App\Domain\Catalog\Tests\Factories\CatalogCache;

use Ensi\CatalogCacheClient\Dto\Image;
use Ensi\CatalogCacheClient\Dto\SearchImagesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class ImageFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'image_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'name' => $this->faker->name(),
            'sort' => $this->faker->numberBetween(1, 5),
            'file' => $this->faker->filePath(),
            'is_migrated' => $this->faker->boolean,
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Image
    {
        return new Image($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchImagesResponse
    {
        return $this->generateResponseSearch(SearchImagesResponse::class, $extras, $count, $pagination);
    }
}
