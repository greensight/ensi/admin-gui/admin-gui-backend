<?php

namespace App\Domain\Catalog\Tests\Factories\ProductGroups;

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductDraftFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\ProductDraft;
use Ensi\PimClient\Dto\ProductGroup;
use Ensi\PimClient\Dto\ProductGroupResponse;
use Ensi\PimClient\Dto\SearchProductGroupsResponse;

class ProductGroupFactory extends BaseApiFactory
{
    public ?Category $category = null;
    public ?ProductDraft $mainProduct = null;
    public ?int $productsCount = 0;

    protected function definition(): array
    {
        $hasMainProduct = $this->faker->boolean;

        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'category_id' => $this->faker->modelId(),
            'is_active' => $this->faker->boolean,
            'products_count' => $this->faker->randomNumber(),
            'main_product_image' => $hasMainProduct ? $this->faker->url : null,
            'main_product_id' => $hasMainProduct ? $this->faker->modelId() : null,
            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,

            'category' => $this->notNull($this->category),
            'main_product' => $this->notNull($this->mainProduct),
            'products' => $this->when(
                $this->productsCount > 0,
                fn () => ProductDraftFactory::new()->makeSeveral($this->productsCount)
            ),
        ];
    }

    public function make(array $extra = []): ProductGroup
    {
        return new ProductGroup($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductGroupResponse
    {
        return new ProductGroupResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductGroupsResponse
    {
        return $this->generateResponseSearch(SearchProductGroupsResponse::class, $extras, $count, $pagination);
    }

    public function withCategory(): self
    {
        return $this->immutableSet('category', CategoryFactory::new()->make());
    }

    public function withMainProduct(): self
    {
        return $this->immutableSet('mainProduct', ProductDraftFactory::new()->make());
    }

    public function withProducts(int $count): self
    {
        return $this->immutableSet('productsCount', $count);
    }
}
