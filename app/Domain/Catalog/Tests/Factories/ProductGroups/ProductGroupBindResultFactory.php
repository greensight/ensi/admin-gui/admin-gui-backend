<?php

namespace App\Domain\Catalog\Tests\Factories\ProductGroups;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductGroup;
use Ensi\PimClient\Dto\ProductGroupsBindResponse;
use Ensi\PimClient\Dto\ProductGroupsBindResult;

class ProductGroupBindResultFactory extends BaseApiFactory
{
    public ?ProductGroup $productGroup = null;
    public array $productErrors = [];

    protected function definition(): array
    {
        return [
            'product_group' => $this->productGroup ?? ProductGroupFactory::new()->make(),
            'product_errors' => $this->productErrors,
        ];
    }

    public function make(array $extra = []): ProductGroupsBindResult
    {
        return new ProductGroupsBindResult($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductGroupsBindResponse
    {
        return new ProductGroupsBindResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function withProductGroup(ProductGroup $productGroup): self
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    public function withProductErrors(array $productIds): self
    {
        $this->productErrors = $productIds;

        return $this;
    }
}
