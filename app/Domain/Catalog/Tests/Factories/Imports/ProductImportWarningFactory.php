<?php

namespace App\Domain\Catalog\Tests\Factories\Imports;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductImportTypeEnum;
use Ensi\PimClient\Dto\ProductImportWarning;
use Ensi\PimClient\Dto\SearchProductImportWarningsResponse;

class ProductImportWarningFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'import_id' => $this->faker->randomNumber(1),
            'vendor_code' => $this->faker->nullable()->uuid(),
            'import_type' => $this->faker->randomElement(ProductImportTypeEnum::getAllowableEnumValues()),
            'message' => $this->faker->sentence(),
        ];
    }

    public function make(array $extra = []): ProductImportWarning
    {
        return new ProductImportWarning($this->makeArray($extra));
    }

    public function makeSearchResponse(array $extra = [], int $count = 1): SearchProductImportWarningsResponse
    {
        return $this->generateResponseSearch(SearchProductImportWarningsResponse::class, $extra, $count);
    }
}
