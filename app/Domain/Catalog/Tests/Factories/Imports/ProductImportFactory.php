<?php

namespace App\Domain\Catalog\Tests\Factories\Imports;

use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductImport;
use Ensi\PimClient\Dto\ProductImportResponse;
use Ensi\PimClient\Dto\ProductImportStatusEnum;
use Ensi\PimClient\Dto\ProductImportTypeEnum;
use Ensi\PimClient\Dto\SearchProductImportsResponse;

class ProductImportFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $file = new File(
            EnsiFileFactory::new()
                ->fileExt($this->faker->randomElement(['csv', 'xls', 'xlsx']))
                ->make()
        );

        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'type' => $this->faker->randomElement(ProductImportTypeEnum::getAllowableEnumValues()),
            'status' => ProductImportStatusEnum::NEW,
            'file' => $file,
            'file_name' => basename($file),
            'chunks_count' => $this->faker->nullable()->numberBetween(1),
            'chunks_finished' => $this->faker->numberBetween(),
        ];
    }

    public function make(array $extra = []): ProductImport
    {
        return new ProductImport($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductImportResponse
    {
        return new ProductImportResponse(['data' => $this->make($extra)]);
    }

    public function makeSearchResponse(array $extra = [], int $count = 1): SearchProductImportsResponse
    {
        return $this->generateResponseSearch(SearchProductImportsResponse::class, $extra, $count);
    }
}
