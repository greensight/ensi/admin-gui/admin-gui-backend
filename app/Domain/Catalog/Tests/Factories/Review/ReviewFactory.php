<?php

namespace App\Domain\Catalog\Tests\Factories\Review;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\ReviewsClient\Dto\Review;
use Ensi\ReviewsClient\Dto\ReviewResponse;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;
use Ensi\ReviewsClient\Dto\SearchReviewsResponse;

class ReviewFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'comment' => $this->faker->optional()->text(255),
            'grade' => $this->faker->numberBetween(1, 5),
            'status_id' => $this->faker->randomElement(ReviewStatusEnum::getAllowableEnumValues()),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Review
    {
        return new Review($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ReviewResponse
    {
        return new ReviewResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extra = [], int $count = 1): SearchReviewsResponse
    {
        return $this->generateResponseSearch(SearchReviewsResponse::class, $extra, $count);
    }
}
