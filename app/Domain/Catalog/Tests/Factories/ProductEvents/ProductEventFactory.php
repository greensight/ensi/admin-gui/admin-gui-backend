<?php

namespace App\Domain\Catalog\Tests\Factories\ProductEvents;

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductEvent;
use Ensi\PimClient\Dto\SearchProductEventsResponse;

class ProductEventFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'event_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): ProductEvent
    {
        return new ProductEvent($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductEventsResponse
    {
        return $this->generateResponseSearch(SearchProductEventsResponse::class, $extras, $count, $pagination);
    }

    /**
     * @param Product|null $product
     */
    public function withProduct(?Product $product = null): static
    {
        return $this->state(['product' => $product ?? ProductFactory::new()->make()]);
    }
}
