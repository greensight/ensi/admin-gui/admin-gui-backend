<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;
use Ensi\PimClient\Dto\ProductImage;
use Ensi\PimClient\Dto\ProductImageResponse;
use Ensi\PimClient\Dto\ProductImagesResponse;

class ProductImageFactory extends BaseApiFactory
{
    public ?File $file = null;

    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'sort' => $this->faker->numberBetween(1, 500),
            'name' => $this->faker->nullable()->sentence(3),
            'file' => $this-> file ?? new File(EnsiFileFactory::new()->make()),
        ];
    }

    public function withFile(File $file): self
    {
        return $this->immutableSet('file', $file);
    }

    public function make(array $extra = []): ProductImage
    {
        return new ProductImage($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductImageResponse
    {
        return new ProductImageResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSeveral(int $count = 1, array $extra = []): ProductImagesResponse
    {
        return new ProductImagesResponse([
            'data' => $this->makeSeveral($count, $extra)->all(),
        ]);
    }
}
