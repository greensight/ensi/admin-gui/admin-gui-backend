<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LaravelTestFactories\FactoryMissingValue;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\ProductDraft;
use Ensi\PimClient\Dto\ProductDraftResponse;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Ensi\PimClient\Dto\SearchProductDraftsResponse;
use Illuminate\Support\Collection;

class ProductDraftFactory extends BaseApiFactory
{
    public ?Brand $brand = null;
    public ?Collection $categories = null;
    public ?Collection $attributes = null;
    public ?Collection $images = null;

    protected function definition(): array
    {
        $type = $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues());
        $isWeigh = $type === ProductTypeEnum::WEIGHT;

        return [
            'id' => $this->faker->modelId(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'external_id' => $this->faker->uuid(),
            'category_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), min: 1),
            'brand_id' => $this->faker->modelId(),
            'status_id' => $this->faker->modelId(),
            'status_comment' => $this->faker->nullable()->text(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $type,
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),

            'uom' => $this->faker->nullable($isWeigh)->randomElement(ProductUomEnum::getAllowableEnumValues()),
            'tariffing_volume' => $this->faker->nullable($isWeigh)->randomElement(ProductTariffingVolumeEnum::getAllowableEnumValues()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'picking_weight_deviation' => $this->faker->nullable($isWeigh)->randomFloat(2, 0, 100),

            'brand' => $this->notNull($this->brand),
            'categories' => $this->executeNested($this->categories, new FactoryMissingValue()),
            'attributes' => $this->executeNested($this->attributes, new FactoryMissingValue()),
            'images' => $this->executeNested($this->images, new FactoryMissingValue()),
        ];
    }

    public function make(array $extra = []): ProductDraft
    {
        return new ProductDraft($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductDraftResponse
    {
        return new ProductDraftResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductDraftsResponse
    {
        return $this->generateResponseSearch(SearchProductDraftsResponse::class, $extras, $count, $pagination);
    }

    public function withBrand(): self
    {
        return $this->immutableSet('brand', BrandFactory::new()->make());
    }

    public function withCategories(int $count = 1): self
    {
        $categories = Collection::times($count, fn () => CategoryFactory::new());

        return $this->immutableSet('categories', $categories);
    }

    public function withAttributes(int $count = 1): self
    {
        $attributes = Collection::times($count, fn () => ProductPropertyValueFactory::new());

        return $this->immutableSet('attributes', $attributes);
    }

    public function withImages(int $count = 1): self
    {
        $images = Collection::times($count, fn () => ProductImageFactory::new());

        return $this->immutableSet('images', $images);
    }

    public function withImage(ProductImageFactory ...$productImages): self
    {
        return $this->immutableSet('images', collect($productImages));
    }
}
