<?php

namespace App\Domain\Catalog\Tests\Factories\Products;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\MetricsCategoryEnum;
use Ensi\PimClient\Dto\ProductField;
use Ensi\PimClient\Dto\ProductFieldResponse;
use Ensi\PimClient\Dto\SearchProductFieldsResponse;

class ProductFieldFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'code' => $this->faker->domainWord,
            'name' => $this->faker->sentence(3),
            'edit_mask' => 7,
            'is_required' => $this->faker->boolean,
            'is_moderated' => $this->faker->boolean,
            'metrics_category' => $this->faker->randomElement(MetricsCategoryEnum::getAllowableEnumValues()),

            'created_at' => $this->faker->dateTime,
            'updated_at' => $this->faker->dateTime,
        ];
    }

    public function make(array $extra = []): ProductField
    {
        return new ProductField($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductFieldResponse
    {
        return new ProductFieldResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductFieldsResponse
    {
        return $this->generateResponseSearch(SearchProductFieldsResponse::class, $extras, $count, $pagination);
    }
}
