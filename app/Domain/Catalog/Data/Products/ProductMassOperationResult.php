<?php

namespace App\Domain\Catalog\Data\Products;

use App\Domain\Common\Data\MassOperationResult;
use Ensi\PimClient\Dto\MassOperationResultData as PimMassOperationResult;

class ProductMassOperationResult extends MassOperationResult
{
    public static function createFromModel(PimMassOperationResult $result): static
    {
        $object = static::createEmpty();
        $object->processed = $result->getProcessed();

        foreach ($result->getErrors() as $error) {
            $object->errors[$error->getId()] = $error->getMessage();
        }

        return $object;
    }
}
