<?php

namespace App\Domain\Catalog\Data\Reviews;

use App\Domain\Customers\Data\CustomerData;
use Ensi\PimClient\Dto\Product;
use Ensi\ReviewsClient\Dto\Review;

class ReviewData
{
    public function __construct(
        public readonly Review $review,
        public readonly ?Product $product = null,
        public readonly ?CustomerData $customer = null,
    ) {
    }
}
