<?php

namespace App\Domain\Catalog\Data\Offers;

use App\Domain\Common\Data\MassOperationResult;
use Ensi\OffersClient\Dto\MassOperationResultData as OffersMassOperationResult;

class OfferMassOperationResult extends MassOperationResult
{
    public static function createFromModel(OffersMassOperationResult $result): static
    {
        $object = static::createEmpty();
        $object->processed = $result->getProcessed();

        foreach ($result->getErrors() as $error) {
            $object->errors[$error->getId()] = $error->getMessage();
        }

        return $object;
    }
}
