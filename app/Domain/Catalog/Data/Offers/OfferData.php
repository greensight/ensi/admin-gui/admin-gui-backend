<?php

namespace App\Domain\Catalog\Data\Offers;

use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\Stock;

class OfferData
{
    public function __construct(public Offer $offer)
    {
    }

    public function getStock(): ?Stock
    {
        if (!$this->offer->getStocks()) {
            return null;
        }

        return current($this->offer->getStocks());
    }
}
