<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\ProductDraft;
use Ensi\PimClient\Dto\ReplaceProductRequest;

class ReplaceProductAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $productId, array $fields): ProductDraft
    {
        $request = new ReplaceProductRequest($fields);

        return $this->api->replaceProduct($productId, $request)->getData();
    }
}
