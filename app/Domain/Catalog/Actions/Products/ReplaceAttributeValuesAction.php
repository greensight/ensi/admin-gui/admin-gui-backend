<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\ReplaceProductAttributesRequest;

class ReplaceAttributeValuesAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $productId, array $attributes): array
    {
        $request = new ReplaceProductAttributesRequest($attributes);
        $request->setAttributes($attributes);

        return $this->api->replaceProductAttributes($productId, $request)->getData();
    }
}
