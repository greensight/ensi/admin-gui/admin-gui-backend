<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PatchProductAttributesRequest;

class PatchAttributeValuesAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $productId, array $attributes): array
    {
        $request = new PatchProductAttributesRequest($attributes);
        $request->setAttributes($attributes);

        return $this->api->patchProductAttributes($productId, $request)->getData();
    }
}
