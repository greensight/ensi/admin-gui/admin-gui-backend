<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductFieldsApi;
use Ensi\PimClient\Dto\PatchProductFieldRequest;
use Ensi\PimClient\Dto\ProductField;

class PatchProductFieldAction
{
    public function __construct(private ProductFieldsApi $api)
    {
    }

    public function execute(int $fieldId, array $attributes): ProductField
    {
        $request = new PatchProductFieldRequest($attributes);

        return $this->api->patchProductField($fieldId, $request)->getData();
    }
}
