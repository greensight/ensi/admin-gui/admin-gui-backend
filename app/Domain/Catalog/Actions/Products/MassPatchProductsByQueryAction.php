<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\MassPatchProductsByQueryRequest;

class MassPatchProductsByQueryAction
{
    public function __construct(protected readonly ProductsApi $productsApi)
    {
    }

    public function execute(array $productsFilter, array $fields, array $attributes): void
    {
        if (!empty($fields) || !empty($attributes)) {
            $request = new MassPatchProductsByQueryRequest([
                'filter' => $productsFilter,
                'fields' => $fields,
                'attributes' => $attributes,
            ]);

            $this->productsApi->massPatchProductsByQuery($request);
        }
    }
}
