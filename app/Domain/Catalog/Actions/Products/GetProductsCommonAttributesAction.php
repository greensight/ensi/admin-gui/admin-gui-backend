<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\ProductsCommonAttributesRequest;

class GetProductsCommonAttributesAction
{
    public function __construct(private readonly ProductsApi $api)
    {
    }

    public function execute(array $filter): array
    {
        $request = new ProductsCommonAttributesRequest();
        $request->setFilter((object)$filter);

        return $this->api->getProductsCommonAttributes($request)->getData();
    }
}
