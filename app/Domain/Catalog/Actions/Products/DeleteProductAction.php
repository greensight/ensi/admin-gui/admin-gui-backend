<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;

class DeleteProductAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteProduct($id);
    }
}
