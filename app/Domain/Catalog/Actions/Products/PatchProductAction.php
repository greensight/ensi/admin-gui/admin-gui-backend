<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PatchProductRequest;
use Ensi\PimClient\Dto\ProductDraft;

class PatchProductAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $id, array $fields): ProductDraft
    {
        $request = new PatchProductRequest($fields);

        return $this->api->patchProduct($id, $request)->getData();
    }
}
