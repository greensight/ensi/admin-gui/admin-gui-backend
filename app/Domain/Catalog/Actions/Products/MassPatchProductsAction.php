<?php

namespace App\Domain\Catalog\Actions\Products;

use App\Domain\Catalog\Data\Products\MassPatchProductsResult;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\MassPatchProductsRequest;

class MassPatchProductsAction
{
    public function __construct(protected readonly ProductsApi $productsApi)
    {
    }

    public function execute(array $productIds, array $fields, array $attributes): MassPatchProductsResult
    {
        if (!empty($fields) || !empty($attributes)) {
            $request = new MassPatchProductsRequest([
                'ids' => $productIds,
                'fields' => $fields,
                'attributes' => $attributes,
            ]);
            $productsResult = $this->productsApi->massPatchProducts($request)->getData();
        }

        return new MassPatchProductsResult(
            productsResult: $productsResult ?? null,
        );
    }
}
