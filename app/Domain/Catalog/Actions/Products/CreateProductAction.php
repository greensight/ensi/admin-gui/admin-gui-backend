<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\CreateProductRequest;
use Ensi\PimClient\Dto\ProductDraft;

class CreateProductAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(array $fields): ProductDraft
    {
        $request = new CreateProductRequest($fields);

        return $this->api->createProduct($request)->getData();
    }
}
