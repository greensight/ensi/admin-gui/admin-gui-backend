<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\ReplaceProductImagesRequest;

class ReplaceProductImagesAction
{
    public function __construct(private ProductsApi $api)
    {
    }

    public function execute(int $productId, array $images): array
    {
        $request = new ReplaceProductImagesRequest(['images' => $images]);

        return $this->api->replaceProductImages($productId, $request)->getData();
    }
}
