<?php

namespace App\Domain\Catalog\Actions\Products;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\DeleteProductAttributesRequest;

class DeleteProductAttributesAction
{
    public function __construct(protected readonly ProductsApi $api)
    {
    }

    public function execute(int $productId, array $propertyIds): void
    {
        $request = new DeleteProductAttributesRequest();
        $request->setPropertyIds($propertyIds);

        $this->api->deleteProductAttributes($productId, $request);
    }
}
