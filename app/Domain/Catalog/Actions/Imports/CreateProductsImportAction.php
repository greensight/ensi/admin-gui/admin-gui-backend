<?php

namespace App\Domain\Catalog\Actions\Imports;

use Ensi\PimClient\Api\ImportsApi;
use Ensi\PimClient\Dto\CreateProductImportRequest;
use Ensi\PimClient\Dto\ProductImport;

class CreateProductsImportAction
{
    public function __construct(
        protected readonly ImportsApi $api,
    ) {
    }

    public function execute(array $fields): ProductImport
    {
        $request = new CreateProductImportRequest($fields);

        return $this->api->createProductImport($request)->getData();
    }
}
