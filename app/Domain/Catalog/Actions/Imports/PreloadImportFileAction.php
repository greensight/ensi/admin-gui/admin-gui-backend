<?php

namespace App\Domain\Catalog\Actions\Imports;

use Ensi\PimClient\Api\ImportsApi;
use Ensi\PimClient\Dto\PreloadFileData;
use Illuminate\Http\UploadedFile;

class PreloadImportFileAction
{
    public function __construct(
        protected readonly ImportsApi $api,
    ) {
    }

    public function execute(UploadedFile $file): PreloadFileData
    {
        return $this->api->preloadImportFile($file)->getData();
    }
}
