<?php

namespace App\Domain\Catalog\Actions\Categories;

use Ensi\PimClient\Api\CategoriesApi;

class ActualizeCategoriesAction
{
    public function __construct(protected readonly CategoriesApi $api)
    {
    }

    public function execute(): void
    {
        $this->api->actualizeCategories();
    }
}
