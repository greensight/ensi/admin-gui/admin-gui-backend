<?php

namespace App\Domain\Catalog\Actions\Categories;

use Ensi\PimClient\Api\CategoriesApi;

class DeleteCategoryAction
{
    public function __construct(protected readonly CategoriesApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteCategory($id);
    }
}
