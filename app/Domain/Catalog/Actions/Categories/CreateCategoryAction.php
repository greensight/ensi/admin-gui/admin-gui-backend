<?php

namespace App\Domain\Catalog\Actions\Categories;

use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Dto\Category;
use Ensi\PimClient\Dto\CreateCategoryRequest;

class CreateCategoryAction
{
    public function __construct(protected readonly CategoriesApi $api)
    {
    }

    public function execute(array $fields): Category
    {
        $request = new CreateCategoryRequest($fields);

        return $this->api->createCategory($request)->getData();
    }
}
