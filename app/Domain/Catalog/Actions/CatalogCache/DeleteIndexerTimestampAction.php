<?php

namespace App\Domain\Catalog\Actions\CatalogCache;

use Ensi\CatalogCacheClient\Api\ElasticApi;

class DeleteIndexerTimestampAction
{
    public function __construct(protected readonly ElasticApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteIndexerTimestamp($id);
    }
}
