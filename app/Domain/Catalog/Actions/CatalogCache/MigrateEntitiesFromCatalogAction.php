<?php

namespace App\Domain\Catalog\Actions\CatalogCache;

use Ensi\CatalogCacheClient\Api\CommonApi;

class MigrateEntitiesFromCatalogAction
{
    public function __construct(protected readonly CommonApi $api)
    {
    }

    public function execute(): void
    {
        $this->api->migrateEntities();
    }
}
