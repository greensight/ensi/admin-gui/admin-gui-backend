<?php

namespace App\Domain\Catalog\Actions\CatalogCache;

use Ensi\CatalogCacheClient\Api\ElasticApi;
use Ensi\CatalogCacheClient\Dto\IndexerTimestamp;
use Ensi\CatalogCacheClient\Dto\PatchIndexerTimestampRequest;

class PatchIndexerTimestampAction
{
    public function __construct(protected readonly ElasticApi $api)
    {
    }

    public function execute(int $id, array $fields): IndexerTimestamp
    {
        $request = new PatchIndexerTimestampRequest($fields);

        return $this->api->patchIndexerTimestamp($id, $request)->getData();
    }
}
