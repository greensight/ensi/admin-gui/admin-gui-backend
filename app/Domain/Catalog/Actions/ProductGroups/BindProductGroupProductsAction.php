<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\BindProductGroupProductsRequest;
use Ensi\PimClient\Dto\ProductGroupsBindResult;

class BindProductGroupProductsAction
{
    public function __construct(protected readonly ProductGroupsApi $api)
    {
    }

    public function execute(int $propertyGroupId, array $data): ProductGroupsBindResult
    {
        $request = new BindProductGroupProductsRequest($data);

        return $this->api->bindProductGroupProducts($propertyGroupId, $request)->getData();
    }
}
