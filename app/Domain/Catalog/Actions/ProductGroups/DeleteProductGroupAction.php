<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;

class DeleteProductGroupAction
{
    public function __construct(protected readonly ProductGroupsApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteProductGroup($id);
    }
}
