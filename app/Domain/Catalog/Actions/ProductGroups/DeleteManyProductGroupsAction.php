<?php

namespace App\Domain\Catalog\Actions\ProductGroups;

use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Dto\RequestBodyMassOperation;

class DeleteManyProductGroupsAction
{
    public function __construct(protected readonly ProductGroupsApi $api)
    {
    }

    public function execute(array $propertyGroupIds): void
    {
        $request = new RequestBodyMassOperation($propertyGroupIds);

        $this->api->deleteProductGroups($request);
    }
}
