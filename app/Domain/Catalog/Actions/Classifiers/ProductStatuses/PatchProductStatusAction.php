<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductStatuses;

use Ensi\PimClient\Api\ProductStatusesApi;
use Ensi\PimClient\Dto\PatchStatusSettingRequest;
use Ensi\PimClient\Dto\StatusSetting;

class PatchProductStatusAction
{
    public function __construct(protected readonly ProductStatusesApi $api)
    {
    }

    public function execute(int $id, array $fields): StatusSetting
    {
        $request = new PatchStatusSettingRequest($fields);

        return $this->api->patchProductStatus($id, $request)->getData();
    }
}
