<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductStatuses;

use Ensi\PimClient\Api\ProductStatusesApi;
use Ensi\PimClient\Dto\SetPreviousStatusRequest;
use Ensi\PimClient\Dto\StatusSetting;

class SetPreviousProductStatusesAction
{
    public function __construct(protected readonly ProductStatusesApi $api)
    {
    }

    public function execute(int $statusId, array $nextStatusIds): StatusSetting
    {
        $request = new SetPreviousStatusRequest();
        $request->setIds($nextStatusIds);

        return $this->api->setPreviousProductStatuses($statusId, $request)->getData();
    }
}
