<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductStatuses;

use Ensi\PimClient\Api\ProductStatusesApi;
use Ensi\PimClient\Dto\GetNextStatusRequest;

class GetNextProductStatusesAction
{
    public function __construct(protected readonly ProductStatusesApi $api)
    {
    }

    public function execute(?int $currentStatusId): array
    {
        $request = new GetNextStatusRequest();
        $request->setId($currentStatusId);

        return $this->api->nextProductStatuses($request)->getData();
    }
}
