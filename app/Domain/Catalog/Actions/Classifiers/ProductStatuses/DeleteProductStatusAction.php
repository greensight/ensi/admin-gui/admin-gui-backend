<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductStatuses;

use Ensi\PimClient\Api\ProductStatusesApi;

class DeleteProductStatusAction
{
    public function __construct(protected readonly ProductStatusesApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteProductStatus($id);
    }
}
