<?php

namespace App\Domain\Catalog\Actions\Classifiers\ProductStatuses;

use Ensi\PimClient\Api\ProductStatusesApi;
use Ensi\PimClient\Dto\CreateStatusSettingRequest;
use Ensi\PimClient\Dto\StatusSetting;

class CreateProductStatusAction
{
    public function __construct(protected readonly ProductStatusesApi $api)
    {
    }

    public function execute(array $fields): StatusSetting
    {
        $request = new CreateStatusSettingRequest($fields);

        return $this->api->createProductStatus($request)->getData();
    }
}
