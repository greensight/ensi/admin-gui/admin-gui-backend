<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;

class DeleteBrandAction
{
    public function __construct(protected readonly BrandsApi $brandsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->brandsApi->deleteBrand($id);
    }
}
