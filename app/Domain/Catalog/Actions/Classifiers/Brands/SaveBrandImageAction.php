<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Brand;
use Illuminate\Http\UploadedFile;

class SaveBrandImageAction
{
    public function __construct(protected readonly BrandsApi $brandsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $brandId, UploadedFile $file): Brand
    {
        return $this->brandsApi->uploadBrandImage($brandId, $file)->getData();
    }
}
