<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\Brand;
use Ensi\PimClient\Dto\PatchBrandRequest;

class PatchBrandAction
{
    public function __construct(protected readonly BrandsApi $brandsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Brand
    {
        $request = new PatchBrandRequest($fields);

        return $this->brandsApi->patchBrand($id, $request)->getData();
    }
}
