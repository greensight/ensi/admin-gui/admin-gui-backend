<?php

namespace App\Domain\Catalog\Actions\Classifiers\Brands;

use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\PreloadFileData;
use Illuminate\Http\UploadedFile;

class PreloadBrandImageAction
{
    public function __construct(protected readonly BrandsApi $brandsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(UploadedFile $file): PreloadFileData
    {
        return $this->brandsApi->preloadBrandImage($file)->getData();
    }
}
