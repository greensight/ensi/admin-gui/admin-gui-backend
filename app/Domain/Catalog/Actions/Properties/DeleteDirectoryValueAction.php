<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;

class DeleteDirectoryValueAction
{
    public function __construct(protected readonly PropertiesApi $api)
    {
    }

    public function execute(int $id): void
    {
        $this->api->deleteDirectoryValue($id);
    }
}
