<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\CreateDirectoryValuesRequest;

class MassCreateDirectoryValueAction
{
    public function __construct(protected readonly PropertiesApi $api)
    {
    }

    public function execute(int $propertyId, array $data): array
    {
        $request = new CreateDirectoryValuesRequest($data);

        return $this->api->createDirectoryValues($propertyId, $request)->getData();
    }
}
