<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\Property;
use Ensi\PimClient\Dto\ReplacePropertyRequest;

class ReplaceProductPropertyAction
{
    public function __construct(protected readonly PropertiesApi $api)
    {
    }

    public function execute(int $propertyId, array $fields): Property
    {
        $request = new ReplacePropertyRequest($fields);

        return $this->api->replaceProperty($propertyId, $request)->getData();
    }
}
