<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;

class DeleteProductPropertyAction
{
    public function __construct(protected readonly PropertiesApi $api)
    {
    }

    public function execute(int $propertyId): void
    {
        $this->api->deleteProperty($propertyId);
    }
}
