<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\PatchPropertyRequest;
use Ensi\PimClient\Dto\Property;

class PatchProductPropertyAction
{
    public function __construct(protected readonly PropertiesApi $api)
    {
    }

    public function execute(int $propertyId, array $fields): Property
    {
        $request = new PatchPropertyRequest($fields);

        return $this->api->patchProperty($propertyId, $request)->getData();
    }
}
