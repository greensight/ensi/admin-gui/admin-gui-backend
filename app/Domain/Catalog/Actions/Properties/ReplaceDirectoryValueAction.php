<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\DirectoryValue;
use Ensi\PimClient\Dto\ReplaceDirectoryValueRequest;

class ReplaceDirectoryValueAction
{
    public function __construct(protected PropertiesApi $api)
    {
    }

    public function execute(int $id, array $fields): DirectoryValue
    {
        $request = new ReplaceDirectoryValueRequest($fields);

        return $this->api->replaceDirectoryValue($id, $request)->getData();
    }
}
