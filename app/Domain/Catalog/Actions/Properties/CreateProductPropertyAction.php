<?php

namespace App\Domain\Catalog\Actions\Properties;

use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\CreatePropertyRequest;
use Ensi\PimClient\Dto\Property;

class CreateProductPropertyAction
{
    public function __construct(protected readonly PropertiesApi $api)
    {
    }

    public function execute(array $fields): Property
    {
        $request = new CreatePropertyRequest($fields);

        return $this->api->createProperty($request)->getData();
    }
}
