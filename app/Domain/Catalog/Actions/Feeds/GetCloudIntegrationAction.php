<?php

namespace App\Domain\Catalog\Actions\Feeds;

use Ensi\FeedClient\Api\CloudIntegrationsApi;
use Ensi\FeedClient\Dto\CloudIntegration;

class GetCloudIntegrationAction
{
    public function __construct(protected CloudIntegrationsApi $api)
    {
    }

    public function execute(): CloudIntegration
    {
        return $this->api->getCloudIntegration()->getData();
    }
}
