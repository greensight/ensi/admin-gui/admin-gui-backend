<?php

namespace App\Domain\Catalog\Actions\Feeds;

use Ensi\FeedClient\Api\CommonApi;

class MigrateEntitiesFromCatalogAction
{
    public function __construct(protected readonly CommonApi $api)
    {
    }

    public function execute(): void
    {
        $this->api->migrateEntities();
    }
}
