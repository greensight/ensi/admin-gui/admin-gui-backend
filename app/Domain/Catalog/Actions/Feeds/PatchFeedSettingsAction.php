<?php

namespace App\Domain\Catalog\Actions\Feeds;

use Ensi\FeedClient\Api\FeedSettingsApi;
use Ensi\FeedClient\Dto\FeedSettings;
use Ensi\FeedClient\Dto\FeedSettingsForPatch;

class PatchFeedSettingsAction
{
    public function __construct(protected FeedSettingsApi $api)
    {
    }

    public function execute(int $id, array $fields): FeedSettings
    {
        $request = new FeedSettingsForPatch($fields);

        return $this->api->patchFeedSettings($id, $request)->getData();
    }
}
