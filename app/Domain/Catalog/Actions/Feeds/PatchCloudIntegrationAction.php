<?php

namespace App\Domain\Catalog\Actions\Feeds;

use Ensi\FeedClient\Api\CloudIntegrationsApi;
use Ensi\FeedClient\Dto\CloudIntegration;
use Ensi\FeedClient\Dto\PatchCloudIntegrationRequest;

class PatchCloudIntegrationAction
{
    public function __construct(protected CloudIntegrationsApi $api)
    {
    }

    public function execute(array $fields): CloudIntegration
    {
        $request = new PatchCloudIntegrationRequest($fields);

        return $this->api->patchCloudIntegration($request)->getData();
    }
}
