<?php

namespace App\Domain\Catalog\Actions\Feeds;

use Ensi\FeedClient\Api\FeedSettingsApi;
use Ensi\FeedClient\Dto\FeedSettings;
use Ensi\FeedClient\Dto\FeedSettingsForCreate;

class CreateFeedSettingsAction
{
    public function __construct(protected FeedSettingsApi $api)
    {
    }

    public function execute(array $fields): FeedSettings
    {
        $request = new FeedSettingsForCreate($fields);

        return $this->api->createFeedSettings($request)->getData();
    }
}
