<?php

namespace App\Domain\Catalog\Actions\Feeds;

use Ensi\FeedClient\Api\CloudIntegrationsApi;
use Ensi\FeedClient\Dto\CloudIntegration;
use Ensi\FeedClient\Dto\CreateCloudIntegrationRequest;

class CreateCloudIntegrationAction
{
    public function __construct(protected CloudIntegrationsApi $api)
    {
    }

    public function execute(array $fields): CloudIntegration
    {
        $request = new CreateCloudIntegrationRequest($fields);

        return $this->api->createCloudIntegration($request)->getData();
    }
}
