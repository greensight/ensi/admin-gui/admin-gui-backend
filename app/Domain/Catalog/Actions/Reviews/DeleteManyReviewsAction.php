<?php

namespace App\Domain\Catalog\Actions\Reviews;

use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\Dto\ReviewMassDeleteRequest;

class DeleteManyReviewsAction
{
    public function __construct(protected readonly ReviewsApi $reviewsApi)
    {
    }

    public function execute(array $ids): void
    {
        $request = new ReviewMassDeleteRequest();
        $request->setIds($ids);

        $this->reviewsApi->massDeleteReviews($request);
    }
}
