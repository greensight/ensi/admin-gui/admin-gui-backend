<?php

namespace App\Domain\Catalog\Actions\Reviews;

use App\Domain\Catalog\Data\Reviews\ReviewData;
use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\ApiException;
use Ensi\ReviewsClient\Dto\PatchReviewRequest;

class PatchReviewAction
{
    public function __construct(protected readonly ReviewsApi $reviewsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): ReviewData
    {
        $request = new PatchReviewRequest($fields);

        return new ReviewData($this->reviewsApi->patchReview($id, $request)->getData());
    }
}
