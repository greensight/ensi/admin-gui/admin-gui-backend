<?php

namespace App\Domain\Catalog\Actions\Reviews;

use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\ApiException;
use Ensi\ReviewsClient\Dto\EmptyDataResponse;

class DeleteReviewAction
{
    public function __construct(protected readonly ReviewsApi $reviewsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): EmptyDataResponse
    {
        return $this->reviewsApi->deleteReview($id);
    }
}
