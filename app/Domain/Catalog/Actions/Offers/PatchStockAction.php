<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\StocksApi;
use Ensi\OffersClient\Dto\PatchStockRequest;
use Ensi\OffersClient\Dto\Stock;

class PatchStockAction
{
    public function __construct(protected readonly StocksApi $api)
    {
    }

    public function execute(int $id, array $fields): Stock
    {
        $request = new PatchStockRequest($fields);

        return $this->api->patchStock($id, $request)->getData();
    }
}
