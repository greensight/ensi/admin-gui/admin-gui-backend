<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\PatchOfferRequest;

class PatchOfferAction
{
    public function __construct(protected readonly OffersApi $api)
    {
    }

    public function execute(int $id, array $fields): Offer
    {
        $request = new PatchOfferRequest($fields);

        return $this->api->patchOffer($id, $request)->getData();
    }
}
