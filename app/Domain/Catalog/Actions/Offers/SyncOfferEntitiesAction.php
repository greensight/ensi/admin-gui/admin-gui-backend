<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\CommonApi;

class SyncOfferEntitiesAction
{
    public function __construct(protected readonly CommonApi $api)
    {
    }

    public function execute(): void
    {
        $this->api->syncEntities();
    }
}
