<?php

namespace App\Domain\Catalog\Actions\Offers;

use Ensi\OffersClient\Api\OffersApi;

class MigrateEntitiesFromCatalogAction
{
    public function __construct(protected readonly OffersApi $api)
    {
    }

    public function execute(): void
    {
        $this->api->migrateOffers();
    }
}
