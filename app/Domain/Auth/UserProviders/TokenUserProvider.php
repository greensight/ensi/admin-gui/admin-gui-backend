<?php

namespace App\Domain\Auth\UserProviders;

use App\Domain\Auth\Actions\LoadUserByTokenAction;
use App\Domain\Auth\Models\User;
use Ensi\InitialEventPropagation\InitialEventDTO;
use Ensi\LaravelInitialEventPropagation\InitialEventHolderFacade;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider as UserProviderContract;
use Illuminate\Log\LogManager;

class TokenUserProvider implements UserProviderContract
{
    public function __construct(
        protected readonly LoadUserByTokenAction $loadUserByTokenAction,
        protected LogManager $logger
    ) {
    }

    /**
     * Устанавливает текущего пользователя по авторизационной паре.
     */
    public function retrieveByCredentials(array $credentials): User|Authenticatable|null
    {
        $token = $credentials[config('auth.storage_key') ?? 'api_token'];
        if (blank($token)) {
            return null;
        }

        try {
            $user = $this->loadUserByTokenAction->execute($token);
            $this->fillInitialEvent($user);

            return $user;
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());

            return null;
        }
    }

    public function retrieveById($identifier)
    {
        return null;
    }

    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    public function validateCredentials(Authenticatable|User $user, array $credentials): bool
    {
        return true;
    }

    public function updateRememberToken(Authenticatable|User $user, $token)
    {
    }

    public function rehashPasswordIfRequired(Authenticatable $user, array $credentials, bool $force = false)
    {
    }

    protected function fillInitialEvent(?User $user): void
    {
        /** @var InitialEventDTO|null $initialEvent */
        $initialEvent = InitialEventHolderFacade::getInitialEvent();
        if ($initialEvent && $user) {
            $initialEvent->userId = (string) $user->getAuthIdentifier();
            $initialEvent->userType = config('initial-event-propagation.set_initial_event_http_middleware.default_user_type');
        }
    }
}
