<?php

namespace App\Domain\Auth\Traits;

use Illuminate\Auth\Access\HandlesAuthorization as BaseHandlesAuthorization;
use Illuminate\Auth\Access\Response;

trait HandlesAuthorization
{
    use BaseHandlesAuthorization;

    protected function allowOnlySuperAdmin($message = null, $code = null): Response
    {
        return user()->isSuperAdmin() ? $this->allow($message, $code) : $this->denyWithStatus(403, $message, $code);
    }
}
