<?php

namespace App\Domain\Auth\Traits;

use Illuminate\Auth\Access\Response as AccessResponse;

trait HandlesRules
{
    /**
     * @param array $data - данные передаваемые для создания/редактирования
     * @param array $userPermissions - правила пользователя
     * @param array $mainPermissions - главные правила, которое позволяет взаимодействовать со всеми полями
     * @param array<int, array<int, string>> $permissionsFields - массив описывающий какие правила позволяют взаимодействовать с какими полями
     */
    public function allowFieldsOf(array $data, array $userPermissions, array $mainPermissions, array $permissionsFields): AccessResponse
    {
        if ($this->checkPermissions($mainPermissions, $userPermissions)) {
            return AccessResponse::allow();
        } elseif (count($data) === 0) {
            return AccessResponse::denyWithStatus(403);
        }

        $fieldsPermissions = $this->flip($permissionsFields);

        foreach ($data as $key => $value) {
            $permissionsField = data_get($fieldsPermissions, $key);
            if ($permissionsField === null) {
                return AccessResponse::denyWithStatus(403);
            }

            if ($this->checkPermissions($permissionsField, $userPermissions)) {
                unset($data[$key]);
            }
        }

        return count($data) === 0 ? AccessResponse::allow() : AccessResponse::denyWithStatus(403);
    }

    protected function flip(array $permissionsFields): array
    {
        $fieldsPermissions = [];
        foreach ($permissionsFields as $permission => $fields) {
            foreach ($fields as $field) {
                $fieldsPermissions[$field][] = $permission;
            }
        }

        return $fieldsPermissions;
    }

    protected function checkPermissions(array $permissions, array $userPermissions): bool
    {
        foreach ($permissions as $permission) {
            if (in_array($permission, $userPermissions)) {
                return true;
            }
        }

        return false;
    }
}
