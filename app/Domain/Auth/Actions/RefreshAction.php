<?php

namespace App\Domain\Auth\Actions;

use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Dto\CreateTokenRequest;
use Ensi\AdminAuthClient\Dto\CreateTokenResponse;
use Ensi\AdminAuthClient\Dto\GrantTypeEnum;

class RefreshAction
{
    public function __construct(private readonly OauthApi $oauthApi)
    {
    }

    public function execute(string $refreshToken): CreateTokenResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::REFRESH_TOKEN);
        $request->setClientId(config('openapi-clients.units.admin-auth.client.id'));
        $request->setClientSecret(config('openapi-clients.units.admin-auth.client.secret'));
        $request->setRefreshToken($refreshToken);

        return $this->oauthApi->createToken($request);
    }
}
