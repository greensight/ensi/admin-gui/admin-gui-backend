<?php

namespace App\Domain\Auth\Actions;

use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Dto\CreateTokenRequest;
use Ensi\AdminAuthClient\Dto\CreateTokenResponse;
use Ensi\AdminAuthClient\Dto\GrantTypeEnum;

class LoginAction
{
    public function __construct(private readonly OauthApi $oauthApi)
    {
    }

    public function execute(string $login, string $password): CreateTokenResponse
    {
        $request = new CreateTokenRequest();
        $request->setGrantType(GrantTypeEnum::PASSWORD);
        $request->setClientId(config('openapi-clients.units.admin-auth.client.id'));
        $request->setClientSecret(config('openapi-clients.units.admin-auth.client.secret'));
        $request->setUsername($login);
        $request->setPassword($password);

        return $this->oauthApi->createToken($request);
    }
}
