<?php

namespace App\Domain\Auth\Actions;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\ApiException;
use Ensi\AdminAuthClient\Dto\UserRole;

class LoadUserByTokenAction
{
    public function __construct(private readonly UsersApi $usersApi)
    {
    }

    public function execute(string $token): ?User
    {
        try {
            $this->usersApi->getConfig()->setAccessToken($token);
            $response = $this->usersApi->getCurrentUser();

            $user = $response->getData();

            return new User(
                id: $user->getId(),
                login: $user->getLogin(),
                fullName: $user->getFullName(),
                shortName: $user->getShortName(),
                lastName: $user->getLastName(),
                firstName: $user->getFirstName(),
                middleName: $user->getMiddleName(),
                email: $user->getEmail(),
                phone: $user->getPhone(),
                active: $user->getActive(),
                rememberToken: $token,
                roles: array_map(fn (UserRole $role) => $role->getId(), $user->getRoles() ?? []),
                rightsAccess: collect($user->getRoles() ?? [])->reduce(function ($rightsAccess, UserRole $role) {
                    return array_merge($rightsAccess ?? [], $role->getRightsAccess());
                }),
            );
        } catch (ApiException $e) {
            return null;
        }
    }
}
