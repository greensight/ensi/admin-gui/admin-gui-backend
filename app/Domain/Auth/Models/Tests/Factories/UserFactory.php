<?php

namespace App\Domain\Auth\Models\Tests\Factories;

use App\Domain\Auth\Models\User;
use App\Domain\Common\Tests\Factories\JWTFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class UserFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'login' => $this->faker->userName(),
            'full_name' => $this->faker->name(),
            'short_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->lastName(),
            'email' => $this->faker->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'active' => $this->faker->boolean,
            'token' => JWTFactory::new()->make(),
            'roles' => [],
            'rights_access' => [],
        ];
    }

    public function make(array $extra = []): User
    {
        $userData = $this->makeArray($extra);

        return new User(
            id: $userData['id'],
            login: $userData['login'],
            fullName: $userData['full_name'],
            shortName: $userData['short_name'],
            lastName: $userData['last_name'],
            firstName: $userData['first_name'],
            middleName: $userData['middle_name'],
            email: $userData['email'],
            phone: $userData['phone'],
            active: $userData['active'],
            rememberToken: $userData['token'],
            roles: $userData['roles'],
            rightsAccess: $userData['rights_access'],
        );
    }
}
