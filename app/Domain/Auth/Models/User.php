<?php

namespace App\Domain\Auth\Models;

use App\Domain\Auth\Models\Tests\Factories\UserFactory;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Illuminate\Auth\Access\Response as AccessResponse;
use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public function __construct(
        public readonly ?int $id,
        public readonly ?string $login,
        public readonly ?string $fullName,
        public readonly ?string $shortName,
        public readonly ?string $lastName,
        public readonly ?string $firstName,
        public readonly ?string $middleName,
        public readonly ?string $email,
        public readonly ?string $phone,
        public readonly ?bool $active,
        public ?string $rememberToken,
        public readonly array $roles = [],
        public readonly array $rightsAccess = [],
    ) {
    }

    /**
     * Возвращает логин пользователя.
     */
    public function getAuthIdentifierName(): string
    {
        return $this->login;
    }

    /**
     * Возвращает идентификатор пользователя.
     */
    public function getAuthIdentifier(): int
    {
        return $this->id;
    }

    /**
     * Заглушка для пароля.
     */
    public function getAuthPassword(): string
    {
        return '';
    }

    /**
     * Возвращает токен авторизации.
     */
    public function getRememberToken(): string
    {
        return $this->rememberToken ?? '';
    }

    /**
     * Устанавливает токен авторизации.
     * @param string $value
     */
    public function setRememberToken($value): void
    {
        $this->rememberToken = $value;
    }

    /**
     * Возвращает имя токена авторизации.
     */
    public function getRememberTokenName(): string
    {
        return 'remember_token';
    }

    public function getAuthPasswordName(): string
    {
        return '';
    }

    /**
     * Возвращает истину, если у пользователя есть любая из запрашиваемых ролей.
     */
    public function hasRole(array|int $needle): bool
    {
        $roles = (array)$needle;

        return !empty(array_intersect($this->expandRoles(), $roles));
    }

    /**
     * Возвращает истину, если пользователь имеет роль администратора.
     */
    public function isAdmin(): bool
    {
        return $this->hasRole([RoleEnum::ADMIN]);
    }

    /**
     * Возвращает истину, если пользователь имеет роль администратора.
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole([RoleEnum::SUPER_ADMIN]);
    }

    /**
     * Раскрывает список ролей с учетом уровня доступа имеющихся.
     */
    private function expandRoles(): array
    {
        $result = $this->roles;
        if (in_array(RoleEnum::SUPER_ADMIN, $result)) {
            $result[] = RoleEnum::ADMIN;
        }

        return array_unique($result);
    }

    public function allowAll(array $permissions): AccessResponse
    {
        if (count(array_diff($permissions, $this->rightsAccess)) === 0) {
            return AccessResponse::allow();
        }

        return AccessResponse::denyWithStatus(403);
    }

    public function allowOneOf(array $permissions): AccessResponse
    {
        if (count(array_intersect($permissions, $this->rightsAccess)) > 0) {
            return AccessResponse::allow();
        }

        return AccessResponse::denyWithStatus(403);
    }

    public static function factory(): UserFactory
    {
        return UserFactory::new();
    }
}
