<?php

namespace App\Domain\Common\Actions;

use App\Domain\Common\Data\AsyncLoader;
use GuzzleHttp\Promise\Utils;

class AsyncLoadAction
{
    /**
     * @param AsyncLoader[] $loaders
     */
    public function execute(array $loaders): void
    {
        $promises = [];
        foreach ($loaders as $loader) {
            $requestAsync = $loader->requestAsync();
            if ($requestAsync) {
                $promises[$loader::class] = $requestAsync;
            }
        }

        $responses = Utils::unwrap($promises);

        foreach ($loaders as $loader) {
            if (isset($responses[$loader::class])) {
                $loader->processResponse($responses[$loader::class]);
            }
        }
    }
}
