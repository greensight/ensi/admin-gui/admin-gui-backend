<?php

namespace App\Domain\Common\Data\Meta\Enum\Communication;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;

class CommunicationNotificationSettingEventEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(NotificationEventEnum::class);
    }
}
