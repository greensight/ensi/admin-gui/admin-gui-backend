<?php

namespace App\Domain\Common\Data\Meta\Enum\Communication;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;

class CommunicationNotificationSettingChannelEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(NotificationChannelEnum::class);
    }
}
