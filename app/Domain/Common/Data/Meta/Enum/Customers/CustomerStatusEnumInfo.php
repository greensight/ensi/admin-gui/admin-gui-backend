<?php

namespace App\Domain\Common\Data\Meta\Enum\Customers;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;

class CustomerStatusEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'customers.searchCustomerStatusEnumValues';
    }
}
