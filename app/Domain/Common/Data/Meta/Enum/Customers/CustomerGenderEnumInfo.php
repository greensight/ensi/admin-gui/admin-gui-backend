<?php

namespace App\Domain\Common\Data\Meta\Enum\Customers;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\CustomersClient\Dto\CustomerGenderEnum;

class CustomerGenderEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(CustomerGenderEnum::class);
    }
}
