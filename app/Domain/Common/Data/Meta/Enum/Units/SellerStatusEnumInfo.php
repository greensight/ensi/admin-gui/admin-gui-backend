<?php

namespace App\Domain\Common\Data\Meta\Enum\Units;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\BuClient\Dto\SellerStatusEnum;

class SellerStatusEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(SellerStatusEnum::class);
    }
}
