<?php

namespace App\Domain\Common\Data\Meta\Enum\Units;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;

class AdminUserEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'units.searchAdminUserEnumValues';
    }
}
