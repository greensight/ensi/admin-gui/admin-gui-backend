<?php

namespace App\Domain\Common\Data\Meta\Enum\Orders;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\OmsClient\Dto\OrderStatusEnum;

class OrderStatusEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(OrderStatusEnum::class);
    }
}
