<?php

namespace App\Domain\Common\Data\Meta\Enum\Cms;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\CmsClient\Dto\SeoTemplateTypeEnum;

class CmsSeoTemplateTypeInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(SeoTemplateTypeEnum::class);
    }
}
