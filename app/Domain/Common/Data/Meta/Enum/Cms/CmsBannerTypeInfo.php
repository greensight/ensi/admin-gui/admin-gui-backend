<?php

namespace App\Domain\Common\Data\Meta\Enum\Cms;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\CmsClient\Dto\BannerTypeEnum;

class CmsBannerTypeInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(BannerTypeEnum::class);
    }
}
