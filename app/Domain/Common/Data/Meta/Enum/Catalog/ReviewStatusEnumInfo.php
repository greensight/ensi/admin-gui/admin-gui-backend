<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;

class ReviewStatusEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(ReviewStatusEnum::class);
    }
}
