<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;

class ProductByNameEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->endpointName = 'catalog.searchProductsEnumValues';
        $this->endpointParams = [
            'property' => 'name',
        ];
    }
}
