<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\FeedClient\Dto\FeedPlatformEnum;

class FeedPlatformEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(FeedPlatformEnum::class);
    }
}
