<?php

namespace App\Domain\Common\Data\Meta\Enum\Catalog;

use App\Domain\Common\Data\Meta\Enum\AbstractEnumInfo;
use Ensi\PimClient\Dto\BulkOperationEntityEnum;

class BulkOperationEntityEnumInfo extends AbstractEnumInfo
{
    public function __construct()
    {
        $this->enumClassToValues(BulkOperationEntityEnum::class);
    }
}
