<?php

namespace App\Domain\Common\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\MenuItemCodeEnum;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;

class MenuData
{
    public static function menu(): array
    {
        return [
            // Products
            MenuItemCodeEnum::PRODUCTS->value => [
                'items' => [
                    // Product catalog
                    MenuItemCodeEnum::PRODUCTS_CATALOG->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
                    ],
                    // Import of products
                    MenuItemCodeEnum::PRODUCTS_IMPORTS->value => [
                        'rightsAccess' => RightsAccessEnum::IMPORT_CREATE,
                    ],
                    // Categories
                    MenuItemCodeEnum::PRODUCTS_CATEGORIES->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ,
                    ],
                    // Product attributes
                    MenuItemCodeEnum::PRODUCTS_ATTRIBUTES->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_ATTRIBUTES_LIST_READ,
                    ],
                    // Brands
                    MenuItemCodeEnum::PRODUCTS_BRANDS->value => [
                        'rightsAccess' => RightsAccessEnum::BRAND_LIST_READ,
                    ],
                    // Product groups
                    MenuItemCodeEnum::PRODUCTS_PRODUCT_GROUPS->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_GROUP_LIST_READ,
                    ],
                    // Status model of the products
                    MenuItemCodeEnum::PRODUCTS_STATUSES->value => [
                        'rightsAccess' => RightsAccessEnum::PRODUCT_STATUS_LIST_READ,
                    ],
                    // Products entities
                    MenuItemCodeEnum::PRODUCTS_ENTITIES->value => [
                        'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                        'items' => [
                            // Failed Jobs
                            MenuItemCodeEnum::PRODUCTS_ENTITIES_FAILED_JOBS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Temp Files
                            MenuItemCodeEnum::PRODUCTS_ENTITIES_TEMP_FILES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Product Events
                            MenuItemCodeEnum::PRODUCTS_ENTITIES_PRODUCT_EVENTS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Actual Category Properties
                            MenuItemCodeEnum::PRODUCTS_ENTITIES_ACTUAL_CATEGORY_PROPERTIES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Actualize Categories
                            MenuItemCodeEnum::PRODUCTS_ENTITIES_ACTUALIZE_CATEGORIES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_ALL,
                            ],
                        ],
                    ],
                    // Catalog Cache entities
                    MenuItemCodeEnum::CATALOG_CACHE_ENTITIES->value => [
                        'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                        'items' => [
                            // Failed Jobs
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_FAILED_JOBS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Indexer Timestamps
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_INDEXER_TIMESTAMPS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Property Directory Values
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_PROPERTY_DIRECTORY_VALUES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Properties
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_PROPERTIES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Products
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_PRODUCTS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Images
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_IMAGES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Product Property Values
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_PRODUCT_PROPERTY_VALUES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Product Group Products
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_PRODUCT_GROUP_PRODUCTS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Product Groups
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_PRODUCT_GROUPS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Offers
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_OFFERS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Discounts
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_DISCOUNTS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Nameplates
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_NAMEPLATES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Nameplate Products
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_NAMEPLATES_PRODUCTS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Actual Category Properties
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_ACTUAL_CATEGORY_PROPERTIES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Categories
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_CATEGORIES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Brands
                            MenuItemCodeEnum::CATALOG_CACHE_ENTITIES_BRANDS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                        ],
                    ],
                    // Baskets entities
                    MenuItemCodeEnum::BASKETS_ENTITIES->value => [
                        'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                        'items' => [
                            // Baskets
                            MenuItemCodeEnum::BASKETS_ENTITIES_BASKETS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                            // Basket Items
                            MenuItemCodeEnum::BASKETS_ENTITIES_BASKET_ITEMS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                        ],
                    ],
                    // Offers
                    MenuItemCodeEnum::OFFERS->value => [
                        'rightsAccess' => RightsAccessEnum::OFFER_LIST_READ,
                    ],
                    // Offers entities
                    MenuItemCodeEnum::OFFERS_ENTITIES->value => [
                        'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                        'items' => [
                            // Failed Jobs
                            MenuItemCodeEnum::OFFERS_ENTITIES_FAILED_JOBS->value => [
                                'rightAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                        ],
                    ],
                ],
            ],

            // Orders
            MenuItemCodeEnum::ORDERS->value => [
                'items' => [
                    // List of orders
                    MenuItemCodeEnum::ORDERS_ORDERS->value => [
                        'rightsAccess' => RightsAccessEnum::ORDER_LIST_READ,
                    ],
                    // Refunds
                    MenuItemCodeEnum::ORDERS_REFUNDS->value => [
                        'rightsAccess' => RightsAccessEnum::REFUND_LIST_READ,
                    ],
                    // Settings
                    MenuItemCodeEnum::ORDERS_SETTINGS->value => [
                        'rightsAccess' => RightsAccessEnum::SETTING_LIST_READ,
                    ],
                ],
            ],

            // Feeds
            MenuItemCodeEnum::FEEDS->value => [
                'rightsAccess' => RightsAccessEnum::FEED_SETTINGS_LIST_READ,
            ],

            // Content
            MenuItemCodeEnum::CONTENT->value => [
                'items' => [
                    // Banners
                    MenuItemCodeEnum::CONTENT_BANNERS->value => [
                        'rightsAccess' => RightsAccessEnum::BANNER_LIST_READ,
                    ],
                    // Product Tags
                    MenuItemCodeEnum::CONTENT_NAMEPLATES->value => [
                        'rightsAccess' => RightsAccessEnum::NAMEPLATE_LIST_READ,
                    ],
                    // Pages
                    MenuItemCodeEnum::CONTENT_PAGES->value => [
                        'rightsAccess' => RightsAccessEnum::PAGE_LIST_READ,
                    ],
                    // SEO templates
                    MenuItemCodeEnum::CONTENT_SEO_TEMPLATES->value => [
                        'rightsAccess' => RightsAccessEnum::SEO_TEMPLATE_LIST_READ,
                    ],
                    MenuItemCodeEnum::CONTENT_TECHNICAL_TABLES->value => [
                        'items' => [
                            MenuItemCodeEnum::CONTENT_TECHNICAL_TABLES_BANNER_TYPES->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                        ],
                    ],
                ],
            ],

            // Logistics
            MenuItemCodeEnum::LOGISTIC->value => [
                'items' => [
                    // Pick-up points
                    MenuItemCodeEnum::LOGISTIC_PICKUP_POINTS->value => [
                        'rightsAccess' => RightsAccessEnum::POINT_LIST_READ,
                    ],
                    // Delivery rates
                    MenuItemCodeEnum::LOGISTIC_DELIVERY_PRICES->value => [
                        'rightsAccess' => RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
                    ],
                    // Delivery Parameters
                    MenuItemCodeEnum::LOGISTIC_DELIVERY_OPTIONS_SETTINGS->value => [
                        'rightsAccess' => RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_READ,
                    ],
                ],
            ],

            // Customers
            MenuItemCodeEnum::CUSTOMERS->value => [
                'rightsAccess' => RightsAccessEnum::CUSTOMER_LIST_READ,
            ],

            // Sellers
            MenuItemCodeEnum::SELLERS->value => [
                'items' => [
                    // List of sellers
                    MenuItemCodeEnum::SELLERS_SELLERS->value => [
                        'rightsAccess' => RightsAccessEnum::SELLER_LIST_READ,
                    ],
                    // Warehouses
                    MenuItemCodeEnum::SELLERS_STORES->value => [
                        'rightsAccess' => RightsAccessEnum::STORE_LIST_READ,
                    ],
                ],
            ],

            // Marketing
            MenuItemCodeEnum::MARKETING->value => [
                'items' => [
                    // Discounts
                    MenuItemCodeEnum::MARKETING_DISCOUNTS->value => [
                        'rightsAccess' => RightsAccessEnum::DISCOUNT_LIST_READ,
                    ],
                    // Promo codes
                    MenuItemCodeEnum::MARKETING_PROMO_CODES->value => [
                        'rightsAccess' => RightsAccessEnum::PROMO_CODE_LIST_READ,
                    ],
                ],
            ],

            // Communications
            MenuItemCodeEnum::COMMUNICATIONS->value => [
                'items' => [
                    // Service Notifications
                    MenuItemCodeEnum::COMMUNICATIONS_NOTIFICATIONS->value => [
                        'rightsAccess' => RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
                    ],
                    // Communications entities
                    MenuItemCodeEnum::COMMUNICATIONS_ENTITIES->value => [
                        'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                        'items' => [
                            // Notifications
                            MenuItemCodeEnum::COMMUNICATIONS_ENTITIES_NOTIFICATIONS->value => [
                                'rightsAccess' => RightsAccessEnum::TECHNICAL_TABLES_READ,
                            ],
                        ],
                    ],
                ],
            ],

            // Reviews
            MenuItemCodeEnum::REVIEWS->value => [
                'items' => [
                    // List of reviews
                    MenuItemCodeEnum::REVIEWS_REVIEWS->value => [
                        'rightsAccess' => RightsAccessEnum::REVIEW_LIST_READ,
                    ],
                ],
            ],

            // Settings
            MenuItemCodeEnum::SETTINGS->value => [
                'items' => [
                    // Users
                    MenuItemCodeEnum::SETTINGS_USERS->value => [
                        'rightsAccess' => RightsAccessEnum::USER_LIST_READ,
                    ],
                    // User Roles
                    MenuItemCodeEnum::SETTINGS_USERS_ROLES->value => [
                        'rightsAccess' => RightsAccessEnum::ROLE_LIST_READ,
                    ],
                    // Search Parameters
                    MenuItemCodeEnum::SETTINGS_CLOUD_INTEGRATION->value => [
                        'rightsAccess' => RightsAccessEnum::CLOUD_INTEGRATION_READ,
                    ],
                ],
            ],
        ];
    }
}
