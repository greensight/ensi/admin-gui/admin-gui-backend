<?php

namespace App\Domain\Common\Data;

use JsonSerializable;

abstract class MassOperationResult implements JsonSerializable
{
    protected array $processed = [];
    protected array $errors = [];

    public static function createEmpty(): static
    {
        return new static();
    }

    public function jsonSerialize(): array
    {
        return [
            'processed' => $this->processed,
            'errors' => $this->convertErrors(),
        ];
    }

    private function convertErrors(): array
    {
        $result = [];

        foreach ($this->errors as $id => $message) {
            $result[] = ['id' => $id, 'message' => $message];
        }

        return $result;
    }
}
