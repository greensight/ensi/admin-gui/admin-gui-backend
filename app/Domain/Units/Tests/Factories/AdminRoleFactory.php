<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\AdminAuthClient\Dto\Role;
use Ensi\AdminAuthClient\Dto\RoleResponse;
use Ensi\AdminAuthClient\Dto\SearchRolesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class AdminRoleFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'active' => $this->faker->boolean(),
            'title' => $this->faker->title(),
            'rights_access' => $this->faker->randomElements(
                RightsAccessEnum::getAllowableEnumValues(),
                $this->faker->numberBetween(1, 3)
            ),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Role
    {
        return new Role($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): RoleResponse
    {
        return new RoleResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchRolesResponse
    {
        return $this->generateResponseSearch(SearchRolesResponse::class, $extras, $count, $pagination);
    }
}
