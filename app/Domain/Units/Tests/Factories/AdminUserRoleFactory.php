<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\AdminAuthClient\Dto\UserRole;
use Ensi\LaravelTestFactories\BaseApiFactory;

class AdminUserRoleFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'active' => $this->faker->boolean(),
            'title' => $this->faker->title(),
            'rights_access' => $this->faker->randomElements(
                RightsAccessEnum::getAllowableEnumValues(),
                $this->faker->numberBetween(1, 3)
            ),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): UserRole
    {
        return new UserRole($this->makeArray($extra));
    }
}
