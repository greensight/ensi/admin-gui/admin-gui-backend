<?php

namespace App\Domain\Units\Tests\Factories;

use Ensi\BuClient\Dto\SearchStoreWorkingsResponse;
use Ensi\BuClient\Dto\StoreWorking;
use Ensi\BuClient\Dto\StoreWorkingResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreWorkingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'active' => $this->faker->boolean,
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->nullable()->time('H:i'),
            'working_end_time' => $this->faker->nullable()->time('H:i'),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): StoreWorking
    {
        return new StoreWorking($this->makeArray($extra));
    }

    public function makeResponseOne(array $extra = []): StoreWorkingResponse
    {
        return new StoreWorkingResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchStoreWorkingsResponse
    {
        return $this->generateResponseSearch(SearchStoreWorkingsResponse::class, $extras, $count, $pagination);
    }
}
