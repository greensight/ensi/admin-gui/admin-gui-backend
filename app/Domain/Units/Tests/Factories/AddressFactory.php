<?php

namespace App\Domain\Units\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseAddressFactory;
use Ensi\BuClient\Dto\Address;

class AddressFactory extends BaseAddressFactory
{
    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
