<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\PatchUserRequest;
use Ensi\SellerAuthClient\Dto\User;

class PatchSellerUserAction
{
    public function __construct(protected readonly UsersApi $sellerUsersApi)
    {
    }

    public function execute(int $id, array $fields): User
    {
        $request = new PatchUserRequest($fields);

        return $this->sellerUsersApi->patchUser($id, $request)->getData();
    }
}
