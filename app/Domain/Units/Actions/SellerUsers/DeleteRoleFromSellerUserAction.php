<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\DeleteRoleFromUserRequest;

class DeleteRoleFromSellerUserAction
{
    public function __construct(protected readonly UsersApi $sellerUsersApi)
    {
    }

    public function execute(int $id, array $data): void
    {
        $request = new DeleteRoleFromUserRequest($data);

        $this->sellerUsersApi->deleteRoleFromUser($id, $request);
    }
}
