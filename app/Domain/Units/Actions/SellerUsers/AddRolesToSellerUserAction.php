<?php

namespace App\Domain\Units\Actions\SellerUsers;

use Ensi\SellerAuthClient\Api\UsersApi;
use Ensi\SellerAuthClient\Dto\AddRolesToUserRequest;

class AddRolesToSellerUserAction
{
    public function __construct(protected readonly UsersApi $sellerUsersApi)
    {
    }

    public function execute(int $id, array $data): void
    {
        $request = new AddRolesToUserRequest($data);

        $this->sellerUsersApi->addRolesToUser($id, $request);
    }
}
