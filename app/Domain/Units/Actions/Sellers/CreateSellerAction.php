<?php

namespace App\Domain\Units\Actions\Sellers;

use App\Domain\Units\Data\SellerData;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\CreateSellerRequest;

class CreateSellerAction
{
    public function __construct(protected readonly SellersApi $sellersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): SellerData
    {
        $seller = new CreateSellerRequest($fields);

        return new SellerData($this->sellersApi->createSeller($seller)->getData());
    }
}
