<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\CreateUserRequest as CreateAdminUserRequest;
use Ensi\AdminAuthClient\Dto\User as AdminUser;

class CreateAdminUserAction
{
    public function __construct(protected readonly AdminUsersApi $adminUsersApi)
    {
    }

    public function execute(array $data): AdminUser
    {
        $request = new CreateAdminUserRequest($data);

        return $this->adminUsersApi->createUser($request)->getData();
    }
}
