<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\PatchUserRequest;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\AdminAuthClient\Dto\User;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SetPasswordForAdminUserAction
{
    public function __construct(protected AdminUsersApi $adminUsersApi)
    {
    }

    public function execute(string $password, string $passwordToken): void
    {
        $user = $this->getUser($passwordToken);
        $this->changePassword($user->getId(), $password);
    }

    private function getUser(string $token): User
    {
        $request = new SearchUsersRequest();
        $request->setFilter((object)['password_token' => $token]);
        /** @var User[] $users */
        $users = $this->adminUsersApi->searchUsers($request)->getData();

        if (empty($users)) {
            throw new BadRequestHttpException('Ссылка установки пароля недействительна. Для генерации новой обратитесь к администратору');
        }

        return current($users);
    }

    private function changePassword(int $userId, string $password): void
    {
        $request = new PatchUserRequest();
        $request->setPassword($password);

        $this->adminUsersApi->patchUser($userId, $request);
    }
}
