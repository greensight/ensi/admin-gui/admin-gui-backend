<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\UsersApi as AdminUsersApi;
use Ensi\AdminAuthClient\Dto\MassChangeActiveRequest;

class MassChangeActiveAction
{
    public function __construct(protected readonly AdminUsersApi $adminUsersApi)
    {
    }

    public function execute(array $data): void
    {
        $request = new MassChangeActiveRequest($data);

        $this->adminUsersApi->massChangeActive($request);
    }
}
