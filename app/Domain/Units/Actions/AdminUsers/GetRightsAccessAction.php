<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\EnumsApi;

class GetRightsAccessAction
{
    public function __construct(protected readonly EnumsApi $enumsApi)
    {
    }

    public function execute(): array
    {
        return $this->enumsApi->getRightsAccess()->getData();
    }
}
