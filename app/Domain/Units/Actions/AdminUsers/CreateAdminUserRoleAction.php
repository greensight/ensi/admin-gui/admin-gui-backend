<?php

namespace App\Domain\Units\Actions\AdminUsers;

use Ensi\AdminAuthClient\Api\RolesApi as AdminUserRolesApi;
use Ensi\AdminAuthClient\Dto\CreateRoleRequest as CreateAdminUserRoleRequest;
use Ensi\AdminAuthClient\Dto\Role as AdminUserRole;

class CreateAdminUserRoleAction
{
    public function __construct(protected readonly AdminUserRolesApi $adminUserRolesApi)
    {
    }

    public function execute(array $data): AdminUserRole
    {
        $request = new CreateAdminUserRoleRequest($data);

        return $this->adminUserRolesApi->createRole($request)->getData();
    }
}
