<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PatchStoreRequest;
use Ensi\BuClient\Dto\Store;

class PatchStoreAction
{
    public function __construct(protected readonly StoresApi $storesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): Store
    {
        $request = new PatchStoreRequest($fields);

        return $this->storesApi->patchStore($id, $request)->getData();
    }
}
