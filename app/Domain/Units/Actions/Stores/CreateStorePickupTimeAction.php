<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\CreateStorePickupTimeRequest;
use Ensi\BuClient\Dto\StorePickupTime;

class CreateStorePickupTimeAction
{
    public function __construct(protected readonly StorePickupTimesApi $storePickupTimesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): StorePickupTime
    {
        $request = new CreateStorePickupTimeRequest($fields);

        return $this->storePickupTimesApi->createStorePickupTime($request)->getData();
    }
}
