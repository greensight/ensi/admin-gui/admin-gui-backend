<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PatchStorePickupTimeRequest;
use Ensi\BuClient\Dto\StorePickupTime;

class PatchStorePickupTimeAction
{
    public function __construct(protected readonly StorePickupTimesApi $storePickupTimesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StorePickupTime
    {
        $request = new PatchStorePickupTimeRequest($fields);

        return $this->storePickupTimesApi->patchStorePickupTime($id, $request)->getData();
    }
}
