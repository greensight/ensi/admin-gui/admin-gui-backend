<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StorePickupTimesApi;

class DeleteStorePickupTimeAction
{
    public function __construct(protected readonly StorePickupTimesApi $storePickupTimesApi)
    {
    }

    public function execute(int $id): void
    {
        $this->storePickupTimesApi->deleteStorePickupTime($id);
    }
}
