<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PatchStoreWorkingRequest;
use Ensi\BuClient\Dto\StoreWorking;

class PatchStoreWorkingAction
{
    public function __construct(protected readonly StoreWorkingsApi $storeWorkingsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreWorking
    {
        $request = new PatchStoreWorkingRequest($fields);

        return $this->storeWorkingsApi->patchStoreWorking($id, $request)->getData();
    }
}
