<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PatchStoreContactRequest;
use Ensi\BuClient\Dto\StoreContact;

class PatchStoreContactAction
{
    public function __construct(protected readonly StoreContactsApi $storeContactsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): StoreContact
    {
        $request = new PatchStoreContactRequest($fields);

        return $this->storeContactsApi->patchStoreContact($id, $request)->getData();
    }
}
