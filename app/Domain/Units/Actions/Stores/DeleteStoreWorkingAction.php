<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreWorkingsApi;

class DeleteStoreWorkingAction
{
    public function __construct(protected readonly StoreWorkingsApi $storeWorkingsApi)
    {
    }

    public function execute(int $id): void
    {
        $this->storeWorkingsApi->deleteStoreWorking($id);
    }
}
