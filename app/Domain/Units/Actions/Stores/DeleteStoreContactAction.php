<?php

namespace App\Domain\Units\Actions\Stores;

use Ensi\BuClient\Api\StoreContactsApi;

class DeleteStoreContactAction
{
    public function __construct(protected readonly StoreContactsApi $storeContactsApi)
    {
    }

    public function execute(int $id): void
    {
        $this->storeContactsApi->deleteStoreContact($id);
    }
}
