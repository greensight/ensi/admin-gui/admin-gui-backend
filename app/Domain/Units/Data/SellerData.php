<?php

namespace App\Domain\Units\Data;

use Ensi\AdminAuthClient\Dto\User;
use Ensi\BuClient\Dto\Seller;

class SellerData
{
    public function __construct(public Seller $seller, public ?User $manager = null)
    {
    }
}
