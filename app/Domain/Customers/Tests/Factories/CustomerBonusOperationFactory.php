<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CrmClient\Dto\BonusOperation;
use Ensi\CrmClient\Dto\BonusOperationResponse;
use Ensi\CrmClient\Dto\SearchBonusOperationsResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerBonusOperationFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'order_number' => $this->faker->nullable()->randomNumber(),
            'bonus_amount' => $this->faker->randomNumber(),
            'comment' => $this->faker->nullable()->text(),
            'activation_date' => $this->faker->nullable()->date(),
            'expiration_date' => $this->faker->nullable()->date(),
        ];
    }

    public function make(array $extra = []): BonusOperation
    {
        return new BonusOperation($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): BonusOperationResponse
    {
        return new BonusOperationResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchBonusOperationsResponse
    {
        return $this->generateResponseSearch(SearchBonusOperationsResponse::class, $extras, $count, $pagination);
    }
}
