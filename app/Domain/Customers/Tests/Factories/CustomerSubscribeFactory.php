<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CrmClient\Dto\ProductSubscribe;
use Ensi\CrmClient\Dto\ProductSubscribeResponse;
use Ensi\CrmClient\Dto\SearchProductSubscribesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerSubscribeFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): ProductSubscribe
    {
        return new ProductSubscribe($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ProductSubscribeResponse
    {
        return new ProductSubscribeResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchProductSubscribesResponse
    {
        return $this->generateResponseSearch(SearchProductSubscribesResponse::class, $extras, $count, $pagination);
    }
}
