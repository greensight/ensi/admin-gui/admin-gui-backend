<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CrmClient\Dto\Preference;
use Ensi\CrmClient\Dto\SearchPreferencesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerPreferenceFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'attribute_name' => $this->faker->text(),
            'attribute_value' => $this->faker->text(),
            'product_count' => $this->faker->randomNumber(),
            'product_sum' => $this->faker->randomNumber(),
        ];
    }

    public function make(array $extra = []): Preference
    {
        return new Preference($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPreferencesResponse
    {
        return $this->generateResponseSearch(SearchPreferencesResponse::class, $extras, $count, $pagination);
    }
}
