<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CustomersClient\Dto\CustomerAddress;
use Ensi\CustomersClient\Dto\CustomerAddressFillablePropertiesAddress;
use Ensi\CustomersClient\Dto\CustomerAddressResponse;
use Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerAddressFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'address' => new CustomerAddressFillablePropertiesAddress([
                'address_string' => $this->faker->address(),
                'post_index' => $this->faker->postcode(),
                'country_code' => $this->faker->countryCode(),
                'region' => $this->faker->nullable()->text(5),
                'region_guid' => $this->faker->nullable()->uuid(),
                'area' => $this->faker->nullable()->text(),
                'area_guid' => $this->faker->nullable()->uuid(),
                'city' => $this->faker->nullable()->city(),
                'city_guid' => $this->faker->uuid(),
                'street' => $this->faker->nullable()->streetAddress(),
                'house' => $this->faker->nullable()->buildingNumber(),
                'block' => $this->faker->nullable()->numerify('##'),
                'porch' => $this->faker->nullable()->numerify('##'),
                'intercom' => $this->faker->nullable()->numerify('##'),
                'floor' => $this->faker->nullable()->numerify('##'),
                'flat' => $this->faker->nullable()->numerify('###'),
                'comment' => $this->faker->nullable()->text(),
                'geo_lat' => $this->faker->latitude(),
                'geo_lon' => $this->faker->longitude(),
            ]),
            'default' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): CustomerAddress
    {
        return new CustomerAddress($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CustomerAddressResponse
    {
        return new CustomerAddressResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCustomerAddressesResponse
    {
        return $this->generateResponseSearch(SearchCustomerAddressesResponse::class, $extras, $count, $pagination);
    }
}
