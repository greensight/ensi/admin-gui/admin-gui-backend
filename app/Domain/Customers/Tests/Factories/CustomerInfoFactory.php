<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CrmClient\Dto\CustomerInfo;
use Ensi\CrmClient\Dto\CustomerInfoResponse;
use Ensi\CrmClient\Dto\SearchCustomersInfoResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerInfoFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'customer_id' => $this->faker->modelId(),
            'kpi_sku_count' => $this->faker->nullable()->randomNumber(),
            'kpi_sku_price_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_order_count' => $this->faker->nullable()->randomNumber(),
            'kpi_shipment_count' => $this->faker->nullable()->randomNumber(),
            'kpi_delivered_count' => $this->faker->nullable()->randomNumber(),
            'kpi_delivered_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_refunded_count' => $this->faker->nullable()->randomNumber(),
            'kpi_refunded_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_canceled_count' => $this->faker->nullable()->randomNumber(),
            'kpi_canceled_sum' => $this->faker->nullable()->randomNumber(),
        ];
    }

    public function make(array $extra = []): CustomerInfo
    {
        return new CustomerInfo($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CustomerInfoResponse
    {
        return new CustomerInfoResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCustomersInfoResponse
    {
        return $this->generateResponseSearch(SearchCustomersInfoResponse::class, $extras, $count, $pagination);
    }
}
