<?php

namespace App\Domain\Customers\Tests\Factories;

use Ensi\CustomersClient\Dto\CustomerStatus;
use Ensi\CustomersClient\Dto\CustomerStatusResponse;
use Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CustomerStatusFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->word(),
        ];
    }

    public function make(array $extra = []): CustomerStatus
    {
        return new CustomerStatus($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CustomerStatusResponse
    {
        return new CustomerStatusResponse([
            'data' => $this->make($extra),
        ]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchCustomerStatusesResponse
    {
        return $this->generateResponseSearch(SearchCustomerStatusesResponse::class, $extras, $count, $pagination);
    }
}
