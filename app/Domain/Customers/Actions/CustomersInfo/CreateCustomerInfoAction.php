<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

use Ensi\CrmClient\Api\CustomersInfoApi;
use Ensi\CrmClient\ApiException;
use Ensi\CrmClient\Dto\CreateCustomerInfoRequest;
use Ensi\CrmClient\Dto\CustomerInfo;

class CreateCustomerInfoAction
{
    public function __construct(protected readonly CustomersInfoApi $customersInfoApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): CustomerInfo
    {
        $request = new CreateCustomerInfoRequest($fields);

        return $this->customersInfoApi->createCustomerInfo($request)->getData();
    }
}
