<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

use Ensi\CrmClient\Api\CustomersInfoApi;
use Ensi\CrmClient\ApiException;

class DeleteCustomerInfoAction
{
    public function __construct(protected readonly CustomersInfoApi $customersInfoApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->customersInfoApi->deleteCustomerInfo($id);
    }
}
