<?php

namespace App\Domain\Customers\Actions\CustomersInfo;

use Ensi\CrmClient\Api\CustomersInfoApi;
use Ensi\CrmClient\ApiException;
use Ensi\CrmClient\Dto\CustomerInfo;
use Ensi\CrmClient\Dto\PatchCustomerInfoRequest;

class PatchCustomerInfoAction
{
    public function __construct(protected readonly CustomersInfoApi $customersInfoApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): CustomerInfo
    {
        $request = new PatchCustomerInfoRequest($fields);

        return $this->customersInfoApi->patchCustomerInfo($id, $request)->getData();
    }
}
