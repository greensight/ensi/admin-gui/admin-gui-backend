<?php

namespace App\Domain\Customers\Actions\Favorites;

use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\ClearCustomerFavoritesRequest;

class ClearCustomerFavoritesAction
{
    public function __construct(protected readonly CustomerFavoritesApi $customerFavoritesApi)
    {
    }

    public function execute(array $fields): void
    {
        $request = new ClearCustomerFavoritesRequest($fields);

        $this->customerFavoritesApi->clearCustomerFavorites($request);
    }
}
