<?php

namespace App\Domain\Customers\Actions\Favorites;

use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\DeleteCustomerFavoritesRequest;

class DeleteCustomerFavoriteAction
{
    public function __construct(protected readonly CustomerFavoritesApi $customerFavoritesApi)
    {
    }

    public function execute(array $fields): void
    {
        $request = new DeleteCustomerFavoritesRequest($fields);

        $this->customerFavoritesApi->deleteCustomerFavorites($request);
    }
}
