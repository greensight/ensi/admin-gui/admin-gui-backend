<?php

namespace App\Domain\Customers\Actions\Favorites;

use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\CreateCustomerFavoritesRequest;

class CreateCustomerFavoriteAction
{
    public function __construct(protected readonly CustomerFavoritesApi $customerFavoritesApi)
    {
    }

    public function execute(array $fields): void
    {
        $request = new CreateCustomerFavoritesRequest($fields);

        $this->customerFavoritesApi->createCustomerFavorites($request);
    }
}
