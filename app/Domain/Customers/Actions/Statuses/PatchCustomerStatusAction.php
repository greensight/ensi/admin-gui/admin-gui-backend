<?php

namespace App\Domain\Customers\Actions\Statuses;

use Ensi\CustomersClient\Api\StatusesApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\CustomerStatus;
use Ensi\CustomersClient\Dto\PatchCustomerStatusRequest;

class PatchCustomerStatusAction
{
    public function __construct(protected readonly StatusesApi $statusApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): CustomerStatus
    {
        $request = new PatchCustomerStatusRequest($fields);

        return $this->statusApi->patchCustomerStatus($id, $request)->getData();
    }
}
