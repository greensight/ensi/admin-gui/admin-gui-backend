<?php

namespace App\Domain\Customers\Actions\Statuses;

use Ensi\CustomersClient\Api\StatusesApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\CreateCustomerStatusRequest;
use Ensi\CustomersClient\Dto\CustomerStatus;

class CreateCustomerStatusAction
{
    public function __construct(protected readonly StatusesApi $statusApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): CustomerStatus
    {
        $request = new CreateCustomerStatusRequest($fields);

        return $this->statusApi->createCustomerStatus($request)->getData();
    }
}
