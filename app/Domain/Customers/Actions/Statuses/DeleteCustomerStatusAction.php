<?php

namespace App\Domain\Customers\Actions\Statuses;

use Ensi\CustomersClient\Api\StatusesApi;
use Ensi\CustomersClient\ApiException;

class DeleteCustomerStatusAction
{
    public function __construct(protected readonly StatusesApi $statusApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->statusApi->deleteCustomerStatus($id);
    }
}
