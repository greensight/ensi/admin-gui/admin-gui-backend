<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;

class DeleteCustomerUserAction
{
    public function __construct(protected readonly UsersApi $usersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->usersApi->deleteUser($id);
    }
}
