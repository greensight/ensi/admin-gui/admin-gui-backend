<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\CreateUserRequest;
use Ensi\CustomerAuthClient\Dto\User;

class CreateCustomerUserAction
{
    public function __construct(protected readonly UsersApi $usersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): User
    {
        $request = new CreateUserRequest($fields);

        return $this->usersApi->createUser($request)->getData();
    }
}
