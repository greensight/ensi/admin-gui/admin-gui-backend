<?php

namespace App\Domain\Customers\Actions\Users;

use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\PatchUserRequest;
use Ensi\CustomerAuthClient\Dto\User;

class PatchCustomerUserAction
{
    public function __construct(protected readonly UsersApi $usersApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): User
    {
        $request = new PatchUserRequest($fields);

        return $this->usersApi->patchUser($id, $request)->getData();
    }
}
