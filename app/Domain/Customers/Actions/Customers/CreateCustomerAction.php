<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomerAuthClient\ApiException as CustomerAuthApiException;
use Ensi\CustomersClient\ApiException as CustomersApiException;
use Ensi\CustomersClient\Dto\CreateCustomerRequest;

class CreateCustomerAction extends CustomerAction
{
    /**
     * @throws CustomerAuthApiException
     * @throws CustomersApiException
     */
    public function execute(array $fields): CustomerData
    {
        $request = new CreateCustomerRequest($fields);

        return $this->prepareCustomer(
            $this->customersApi->createCustomer($request)->getData()
        );
    }
}
