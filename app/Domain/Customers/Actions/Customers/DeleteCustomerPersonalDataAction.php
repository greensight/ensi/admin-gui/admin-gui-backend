<?php

namespace App\Domain\Customers\Actions\Customers;

use Ensi\CustomersClient\ApiException as ApiExceptionCustomers;

class DeleteCustomerPersonalDataAction extends CustomerAction
{
    /**
     * @throws ApiExceptionCustomers
     */
    public function execute(int $customerId): void
    {
        $this->customersApi->deletePersonalData($customerId);
    }
}
