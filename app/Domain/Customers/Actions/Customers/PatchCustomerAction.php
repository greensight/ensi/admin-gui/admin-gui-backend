<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomerAuthClient\ApiException as CustomerAuthApiException;
use Ensi\CustomersClient\ApiException as CustomersApiException;
use Ensi\CustomersClient\Dto\PatchCustomerRequest;

class PatchCustomerAction extends CustomerAction
{
    /**
     * @throws CustomerAuthApiException
     * @throws CustomersApiException
     */
    public function execute(int $id, array $fields): CustomerData
    {
        $request = new PatchCustomerRequest($fields);

        return $this->prepareCustomer(
            $this->customersApi->patchCustomer($id, $request)->getData()
        );
    }
}
