<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomerAuthClient\ApiException as CustomerAuthApiException;
use Ensi\CustomersClient\ApiException as CustomersApiException;
use Illuminate\Http\UploadedFile;

class UploadAvatarAction extends CustomerAction
{
    /**
     * @throws CustomerAuthApiException
     * @throws CustomersApiException
     */
    public function execute(int $customerId, UploadedFile $file): CustomerData
    {
        return $this->prepareCustomer($this->customersApi->uploadCustomerAvatar($customerId, $file)->getData());
    }
}
