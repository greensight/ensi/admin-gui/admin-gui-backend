<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomerAuthClient\ApiException as CustomerAuthApiException;
use Ensi\CustomersClient\ApiException as CustomersApiException;

class DeleteAvatarAction extends CustomerAction
{
    /**
     * @throws CustomerAuthApiException
     * @throws CustomersApiException
     */
    public function execute(int $customerId): CustomerData
    {
        return $this->prepareCustomer($this->customersApi->deleteCustomerAvatar($customerId)->getData());
    }
}
