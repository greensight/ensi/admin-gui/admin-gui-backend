<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Data\CustomerData;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\User;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;

abstract class CustomerAction
{
    public function __construct(protected readonly CustomersApi $customersApi, protected readonly UsersApi $usersApi)
    {
    }

    /**
     * @throws ApiException
     */
    protected function prepareCustomer(Customer $customer, ?User $user = null): CustomerData
    {
        if (is_null($user) && $customer->getUserId()) {
            $user = $this->usersApi->getUser($customer->getUserId())->getData();
        }

        return new CustomerData($customer, $user);
    }
}
