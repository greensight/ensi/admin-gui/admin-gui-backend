<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Api\ProductSubscribesApi;
use Ensi\CrmClient\Dto\DeleteProductSubscribesRequest;

class DeleteCustomerProductSubscribeAction
{
    public function __construct(protected readonly ProductSubscribesApi $productSubscribesApi)
    {
    }

    public function execute(array $fields): void
    {
        $request = new DeleteProductSubscribesRequest($fields);

        $this->productSubscribesApi->deleteProductFromProductSubscribes($request);
    }
}
