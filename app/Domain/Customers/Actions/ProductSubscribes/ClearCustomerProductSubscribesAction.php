<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Api\ProductSubscribesApi;
use Ensi\CrmClient\Dto\ClearProductSubscribesRequest;

class ClearCustomerProductSubscribesAction
{
    public function __construct(protected readonly ProductSubscribesApi $productSubscribesApi)
    {
    }

    public function execute(array $fields): void
    {
        $request = new ClearProductSubscribesRequest($fields);

        $this->productSubscribesApi->clearProductSubscribes($request);
    }
}
