<?php

namespace App\Domain\Customers\Actions\ProductSubscribes;

use Ensi\CrmClient\Api\ProductSubscribesApi;
use Ensi\CrmClient\Dto\CreateProductSubscribeRequest;
use Ensi\CrmClient\Dto\ProductSubscribe;

class CreateCustomerProductSubscribeAction
{
    public function __construct(protected readonly ProductSubscribesApi $productSubscribesApi)
    {
    }

    public function execute(array $fields): ProductSubscribe
    {
        $request = new CreateProductSubscribeRequest($fields);

        return $this->productSubscribesApi->createProductSubscribe($request)->getData();
    }
}
