<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use Ensi\CrmClient\Api\BonusOperationsApi;
use Ensi\CrmClient\ApiException;

class DeleteBonusOperationAction
{
    public function __construct(protected readonly BonusOperationsApi $bonusOperationsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->bonusOperationsApi->deleteBonusOperation($id);
    }
}
