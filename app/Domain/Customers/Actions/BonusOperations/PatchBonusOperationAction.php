<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use Ensi\CrmClient\Api\BonusOperationsApi;
use Ensi\CrmClient\ApiException;
use Ensi\CrmClient\Dto\BonusOperation;
use Ensi\CrmClient\Dto\PatchBonusOperationRequest;

class PatchBonusOperationAction
{
    public function __construct(protected readonly BonusOperationsApi $bonusOperationsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): BonusOperation
    {
        $request = new PatchBonusOperationRequest($fields);

        return $this->bonusOperationsApi->patchBonusOperation($id, $request)->getData();
    }
}
