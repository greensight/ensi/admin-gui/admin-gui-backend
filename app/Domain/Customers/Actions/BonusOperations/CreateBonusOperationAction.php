<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use Ensi\CrmClient\Api\BonusOperationsApi;
use Ensi\CrmClient\ApiException;
use Ensi\CrmClient\Dto\BonusOperation;
use Ensi\CrmClient\Dto\CreateBonusOperationRequest;

class CreateBonusOperationAction
{
    public function __construct(protected readonly BonusOperationsApi $bonusOperationsApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): BonusOperation
    {
        $request = new CreateBonusOperationRequest($fields);

        return $this->bonusOperationsApi->createBonusOperation($request)->getData();
    }
}
