<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\CreateCustomerAddressRequest;
use Ensi\CustomersClient\Dto\CustomerAddress;

class CreateCustomerAddressAction
{
    public function __construct(protected readonly AddressesApi $addressesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(array $fields): CustomerAddress
    {
        $request = new CreateCustomerAddressRequest($fields);

        return $this->addressesApi->createCustomerAddress($request)->getData();
    }
}
