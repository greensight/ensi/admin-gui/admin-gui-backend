<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\ApiException;

class SetDefaultCustomerAddressAction
{
    public function __construct(protected readonly AddressesApi $addressesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $addressId): void
    {
        $this->addressesApi->setCustomerAddressesAsDefault($addressId);
    }
}
