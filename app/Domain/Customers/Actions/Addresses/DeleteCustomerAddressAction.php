<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\ApiException;

class DeleteCustomerAddressAction
{
    public function __construct(protected readonly AddressesApi $addressesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id): void
    {
        $this->addressesApi->deleteCustomerAddress($id);
    }
}
