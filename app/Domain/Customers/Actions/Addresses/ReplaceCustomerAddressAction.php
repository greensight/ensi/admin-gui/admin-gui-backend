<?php

namespace App\Domain\Customers\Actions\Addresses;

use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\CustomerAddress;
use Ensi\CustomersClient\Dto\ReplaceCustomerAddressRequest;

class ReplaceCustomerAddressAction
{
    public function __construct(protected readonly AddressesApi $addressesApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function execute(int $id, array $fields): CustomerAddress
    {
        $request = new ReplaceCustomerAddressRequest($fields);

        return $this->addressesApi->replaceCustomerAddress($id, $request)->getData();
    }
}
