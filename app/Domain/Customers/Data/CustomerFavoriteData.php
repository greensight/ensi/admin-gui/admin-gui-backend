<?php

namespace App\Domain\Customers\Data;

use Ensi\CrmClient\Dto\CustomerFavorite;
use Ensi\PimClient\Dto\Product;

class CustomerFavoriteData
{
    public function __construct(
        public readonly CustomerFavorite $favorite,
        public readonly ?Product $product = null,
    ) {
    }
}
