<?php

namespace App\Domain\Customers\Data;

use Ensi\CustomerAuthClient\Dto\User;
use Ensi\CustomersClient\Dto\Customer;

class CustomerData
{
    public function __construct(public Customer $customer, public ?User $user = null)
    {
    }
}
