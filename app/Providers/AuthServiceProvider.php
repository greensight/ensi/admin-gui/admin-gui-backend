<?php

namespace App\Providers;

use App\Domain\Auth\UserProviders\TokenUserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     */
    protected $policies = [
      // 'App\Model' => 'App\Policies\ModelPolicy',
   ];

    public function boot(): void
    {
        $this->registerPolicies();

        Gate::guessPolicyNamesUsing(
            fn ($className) => str_replace('Controllers', 'Policies', $className) . 'Policy'
        );
    }

    public function register(): void
    {
        parent::register();

        $this->app->make('auth')->provider('ensi_token', function ($app) {
            return $app->make(TokenUserProvider::class);
        });
    }
}
