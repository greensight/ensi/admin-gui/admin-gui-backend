<?php

namespace App\Http\ApiV1\Support\Queries;

use App\Exceptions\EmptyResultException;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;

trait QueryBuilderFilterEnumTrait
{
    use QueryBuilderGetTrait;

    public function searchEnums(): array
    {
        $requestClass = $this->requestGetClass();
        $request = new $requestClass();

        $filter = $this->httpRequest->get('filter');

        try {
            $this->prepareEnumRequest($request, data_get($filter, 'id'), data_get($filter, 'query'));

            $response = $this->search($request);

            return $this->convertGetToItems($response);
        } catch (EmptyResultException) {
            return [];
        }
    }

    /**
     * @throws EmptyResultException
     */
    abstract protected function prepareEnumRequest($request, ?array $id, ?string $query): void;

    protected function prepareRequestForName($request, ?array $id, ?string $query): void
    {
        $this->prepareRequest($request, $id, $query, queryFilter: 'name_like');
    }

    protected function prepareRequest($request, ?array $id, ?string $query, ?string $queryFilter): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter[$queryFilter] = $query;
        }

        $request->setFilter((object)$filter);

        $class = $this->paginationClass();
        $pagination = new $class();
        $pagination->setType(PaginationTypeEnum::CURSOR);

        $request->setPagination($id ? $pagination->setLimit(count($id)) : $pagination);
    }
}
