<?php

namespace App\Http\ApiV1\Support\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class BaseFormRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }

    public static function nestedRules(string $prefix, array $rules, bool $required = false): array
    {
        return collect($rules)
            ->mapWithKeys(function ($rules, $key) use ($prefix) {
                $requiredKey = array_search("required", $rules);
                if ($requiredKey !== false) {
                    $rules[$requiredKey] = "required_with:$prefix";
                }

                return ["{$prefix}.{$key}" => $rules];
            })
            ->put($prefix, [$required ? 'required' : 'nullable', 'array'])
            ->all();
    }
}
