<?php

namespace App\Http\ApiV1\Support\Tests;

use App\Domain\Auth\Models\User;
use App\Http\Middleware\UserHasRightsAccess;

class ApiV1ComponentWithRightsTestCase extends ApiV1ComponentTestCase
{
    public function setRights(array $rolesAccess): void
    {
        $user = User::factory()->make([
            'rights_access' => $rolesAccess,
        ]);

        auth()->setUser($user);
    }

    public function setRoles(array $roles): void
    {
        $user = User::factory()->make([
            'roles' => $roles,
        ]);

        auth()->setUser($user);
    }

    public function disableMiddlewareForAllTests(): void
    {
        $this->withMiddleware(UserHasRightsAccess::class);
    }
}
