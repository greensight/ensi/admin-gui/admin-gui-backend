<?php

namespace App\Http\ApiV1\Support\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Http\UploadedFile;
use Webmozart\Assert\Assert;

class FilenameLength implements ValidationRule
{
    public function __construct(
        protected ?int $min = null,
        protected ?int $max = null,
    ) {
        Assert::nullOrGreaterThanEq($this->max, $this->min);
    }

    /**
     * @param string $attribute
     * @param UploadedFile|null $value
     * @param Closure $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($value === null) {
            return;
        }

        $filename = pathinfo($value->getClientOriginalName(), PATHINFO_FILENAME);

        if (filled($this->min) && strlen($filename) < $this->min) {
            $fail(__('validation.filename_min_length', ['value' => $this->min]));
        }

        if (filled($this->max) && strlen($filename) > $this->max) {
            $fail(__('validation.filename_max_length', ['value' => $this->max]));
        }
    }
}
