<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Marketing\Tests\Factories\DiscountProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'marketing', 'discount');

test('GET /api/v1/marketing/discounts/discount-products/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $discountProductId = 1;

    $this->mockMarketingDiscountProductsApi()->allows([
        'getDiscountProduct' => $discountProduct = DiscountProductFactory::new()
            ->makeResponse(['id' => $discountProductId]),
    ]);

    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(
            ProductFactory::new()->makeResponseSearch([['id' => $discountProduct->getData()->getProductId()]])
        ),
    ]);

    getJson("/api/v1/marketing/discounts/discount-products/{$discountProductId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'discount_id', 'product_id']])
        ->assertJsonPath('data.id', $discountProductId);
});

test('POST /api/v1/marketing/discounts/discount-products:search success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $discountProductId = 1;
    $count = 2;

    $this->mockMarketingDiscountProductsApi()->allows([
        'searchDiscountProducts' => $discountProduct = DiscountProductFactory::new()
            ->makeResponseSearch([
                ['id' => $discountProductId],
                ['id' => $discountProductId + 1],
            ], $count),
    ]);

    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(
            ProductFactory::new()->makeResponseSearch([['id' => current($discountProduct->getData())->getProductId()]])
        ),
    ]);

    $request = [
        'filter' => ['discount_id' => 1],
    ];

    postJson('/api/v1/marketing/discounts/discount-products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'discount_id', 'product_id']]])
        ->assertJsonPath('data.0.id', $discountProductId);
});

test('GET /api/v1/marketing/discounts/discount-products:meta success', function () {
    getJson('/api/v1/marketing/discounts/discount-products:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
