<?php

use App\Http\ApiV1\Modules\Marketing\Tests\Factories\DeleteDiscountProductsRequestFactory;
use App\Http\ApiV1\Modules\Marketing\Tests\Factories\PatchDiscountProductsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\MarketingClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/marketing/discounts/{id}/products 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountRelationsApi()->allows([
        'patchDiscountProducts' => new EmptyDataResponse(),
    ]);
    $request = PatchDiscountProductsRequestFactory::new()->make();

    postJson("/api/v1/marketing/discounts/{$discountId}/products", $request)
        ->assertStatus(200);
});

test('DELETE /api/v1/marketing/discounts/{id}/products 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $discountId = 1;

    $this->mockMarketingDiscountRelationsApi()->allows([
        'deleteDiscountProducts' => new EmptyDataResponse(),
    ]);
    $request = DeleteDiscountProductsRequestFactory::new()->make();

    deleteJson("/api/v1/marketing/discounts/{$discountId}/products", $request)
        ->assertStatus(200);
});
