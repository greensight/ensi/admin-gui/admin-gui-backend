<?php

namespace App\Http\ApiV1\Modules\Marketing\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;

class PatchDiscountRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $startData = $this->faker->nullable()->date();

        return [
            'name' => $this->faker->word(),
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->numberBetween(1, 100),
            'status' => $this->faker->randomElement(DiscountStatusEnum::getAllowableEnumValues()),
            'start_date' => $startData,
            'end_date' => $startData ?
                $this->faker->nullable()->dateTimeBetween($startData, '+5 days')?->format('Y-m-d') :
                $this->faker->nullable()->date(),
            'promo_code_only' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
