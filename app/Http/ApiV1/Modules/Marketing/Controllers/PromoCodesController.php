<?php

namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Common\Data\Meta\Enum\Marketing\PromoCodeStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Marketing\Actions\PromoCodes\CreatePromoCodeAction;
use App\Domain\Marketing\Actions\PromoCodes\DeletePromoCodeAction;
use App\Domain\Marketing\Actions\PromoCodes\PatchPromoCodeAction;
use App\Http\ApiV1\Modules\Marketing\Queries\PromoCodesQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreatePromoCodeRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchPromoCodeRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\PromoCodesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PromoCodesController
{
    public function get(int $id, PromoCodesQuery $query): PromoCodesResource
    {
        return new PromoCodesResource($query->find($id));
    }

    public function search(PromoCodesQuery $query): AnonymousResourceCollection
    {
        return PromoCodesResource::collectPage($query->get());
    }

    public function create(CreatePromoCodeRequest $request, CreatePromoCodeAction $action): PromoCodesResource
    {
        return new PromoCodesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchPromoCodeRequest $request, PatchPromoCodeAction $action): PromoCodesResource
    {
        return new PromoCodesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeletePromoCodeAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    /**
     * @throws Exception
     */
    public function meta(
        PromoCodeStatusEnumInfo $promoCodeStatus,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),

            Field::text('name', 'Название')->listDefault()->filterDefault(),
            Field::string('code', 'Код')->listDefault(),

            Field::integer('counter', 'Доступное количество применений'),
            Field::integer('current_counter', 'Текущее количество применений'),

            Field::enum('status', 'Статус', $promoCodeStatus)->listDefault()->filterDefault(),

            Field::date('start_date', 'Дата начала')->listDefault()->filterDefault(),
            Field::date('end_date', 'Дата окончания')->listDefault()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
