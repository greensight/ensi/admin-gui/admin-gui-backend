<?php

namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Common\Data\Meta\Enum\Catalog\ProductDraftEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Marketing\DiscountStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Marketing\DiscountTypeEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Marketing\DiscountValueTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Marketing\Actions\Discounts\CreateDiscountAction;
use App\Domain\Marketing\Actions\Discounts\DeleteDiscountAction;
use App\Domain\Marketing\Actions\Discounts\MassStatusUpdateAction;
use App\Domain\Marketing\Actions\Discounts\PatchDiscountAction;
use App\Http\ApiV1\Modules\Marketing\Queries\DiscountsQuery;
use App\Http\ApiV1\Modules\Marketing\Requests\CreateDiscountRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\MassDiscountsStatusUpdateRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountRequest;
use App\Http\ApiV1\Modules\Marketing\Resources\DiscountsResource;
use App\Http\ApiV1\OpenApiGenerated\Enums\FieldTypeEnum;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DiscountsController
{
    public function get(int $id, DiscountsQuery $query): DiscountsResource
    {
        return new DiscountsResource($query->find($id));
    }

    public function search(DiscountsQuery $query): AnonymousResourceCollection
    {
        return DiscountsResource::collectPage($query->get());
    }

    public function create(CreateDiscountRequest $request, CreateDiscountAction $action): DiscountsResource
    {
        return new DiscountsResource($action->execute($request->validated()));
    }

    public function patch(
        int $id,
        PatchDiscountRequest $request,
        PatchDiscountAction $action
    ): DiscountsResource {
        return new DiscountsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDiscountAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massStatusUpdate(MassDiscountsStatusUpdateRequest $request, MassStatusUpdateAction $action): EmptyResource
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    /**
     * @throws Exception
     */
    public function meta(
        DiscountTypeEnumInfo $discountType,
        DiscountValueTypeEnumInfo $discountValueType,
        DiscountStatusEnumInfo $discountStatus,
        ProductDraftEnumInfo $productDraftEnumInfo,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),

            Field::text('name', 'Название')->sort()->filterDefault()->listDefault(),

            Field::enum('type', 'Тип скидки', $discountType)->sort(),
            Field::enum('value_type', 'Тип значения', $discountValueType)->sort(),

            Field::pluralNumeric('value', 'Размер скидки')
                ->addValueType(FieldTypeEnum::PRICE, DiscountValueTypeEnum::RUB)
                ->addValueType(FieldTypeEnum::INT, DiscountValueTypeEnum::PERCENT)
                ->resetSort()->resetFilter(),

            Field::enum('status', 'Статус', $discountStatus)->sort()->filterDefault()->listDefault(),

            Field::date('start_date', 'Дата начала')->filterDefault()->listDefault(),
            Field::date('end_date', 'Дата окончания')->filterDefault()->listDefault(),

            Field::boolean('promo_code_only', 'Только по промокоду')->filterDefault()->filterDefault(),

            Field::enum('product_id', 'Название продукта', $productDraftEnumInfo)->listHide(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
