<?php

namespace App\Http\ApiV1\Modules\Marketing\Controllers;

use App\Domain\Marketing\Actions\ProductDiscounts\DeleteDiscountProductsAction;
use App\Domain\Marketing\Actions\ProductDiscounts\PatchDiscountProductsAction;
use App\Http\ApiV1\Modules\Marketing\Requests\DeleteDiscountProductsRequest;
use App\Http\ApiV1\Modules\Marketing\Requests\PatchDiscountProductsRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DiscountRelationsController
{
    public function patchProducts(int $id, PatchDiscountProductsRequest $request, PatchDiscountProductsAction $action): Responsable
    {
        $action->execute($id, $request->validated());

        return new EmptyResource();
    }

    public function deleteProducts(int $id, DeleteDiscountProductsRequest $request, DeleteDiscountProductsAction $action): Responsable
    {
        $action->execute($id, $request->validated());

        return new EmptyResource();
    }
}
