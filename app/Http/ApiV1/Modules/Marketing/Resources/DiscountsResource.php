<?php

namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Domain\Marketing\Data\Discounts\DiscountData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin DiscountData */
class DiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->data->getId(),
            'type' => $this->data->getType(),
            'name' => $this->data->getName(),
            'value_type' => $this->data->getValueType(),
            'value' => $this->data->getValue(),
            'status' => $this->data->getStatus(),
            'start_date' => $this->dateToIso($this->data->getStartDate()),
            'end_date' => $this->dateToIso($this->data->getEndDate()),
            'promo_code_only' => $this->data->getPromoCodeOnly(),

            'created_at' => $this->dateTimeToIso($this->data->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->data->getUpdatedAt()),

            'products' => DiscountProductsResource::collection($this->whenNotNull($this->products)),
        ];
    }
}
