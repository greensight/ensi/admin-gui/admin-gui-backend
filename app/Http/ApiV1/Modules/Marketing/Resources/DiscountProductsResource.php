<?php

namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Domain\Marketing\Data\Discounts\DiscountProductData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin DiscountProductData */
class DiscountProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->data->getId(),
            'discount_id' => $this->data->getDiscountId(),
            'product_id' => $this->data->getProductId(),
            'product' => ProductsResource::make($this->whenNotNull($this->product)),

            'created_at' => $this->dateTimeToIso($this->data->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->data->getUpdatedAt()),
        ];
    }
}
