<?php

namespace App\Http\ApiV1\Modules\Marketing\Resources;

use App\Domain\Marketing\Data\Discounts\PromoCodeData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin PromoCodeData */
class PromoCodesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->data->getId(),
            'discount_id' => $this->data->getDiscountId(),
            'name' => $this->data->getName(),
            'code' => $this->data->getCode(),
            'counter' => $this->data->getCounter(),
            'current_counter' => $this->data->getCurrentCounter(),
            'start_date' => $this->dateToIso($this->data->getStartDate()),
            'end_date' => $this->dateToIso($this->data->getEndDate()),
            'status' => $this->data->getStatus(),

            'created_at' => $this->dateTimeToIso($this->data->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->data->getUpdatedAt()),

            'discount' => DiscountsResource::make($this->whenNotNull($this->discount)),
        ];
    }
}
