<?php

namespace App\Http\ApiV1\Modules\Marketing\Queries\IncludeLoaders;

use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;

class MarketingProductsLoader extends MarketingLoader
{
    protected array $productIds = [];
    /* @var Collection<Product> */
    public Collection $products;
    public bool $load = false;

    public function __construct(protected ProductsApi $productsApi)
    {
        $this->products = collect();
    }

    public function setProductIds(Collection $value): void
    {
        $this->productIds = $this->prepareFilterValues($value);
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->productIds) {
            return null;
        }

        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $this->productIds]);
        $request->setPagination((new RequestBodyPagination())->setLimit(count($this->productIds)));

        return $this->productsApi->searchProductsAsync($request);
    }

    /**
     * @param SearchProductsResponse $response
     */
    public function processResponse($response): void
    {
        $this->products = collect($response->getData())->keyBy(function (Product $product) {
            return $product->getId();
        });
    }
}
