<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Illuminate\Validation\Rule;

class MassDiscountsStatusUpdateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ids' => ['required', 'array', 'min:1'],
            'ids.*' => ['required', 'integer', 'min:1', 'distinct'],
            'status' => ['required', 'integer', Rule::in(DiscountStatusEnum::getAllowableEnumValues())],
        ];
    }
}
