<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\MarketingClient\Dto\DiscountStatusEnum;
use Ensi\MarketingClient\Dto\DiscountTypeEnum;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Illuminate\Validation\Rule;

class CreateDiscountRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', 'integer', Rule::in(DiscountTypeEnum::getAllowableEnumValues())],
            'name' => ['required', 'string'],
            'value_type' => ['required', 'integer', Rule::in(DiscountValueTypeEnum::getAllowableEnumValues())],
            'value' => ['required', 'integer', 'min:1'],
            'status' => ['required', 'integer', Rule::in(DiscountStatusEnum::getAllowableEnumValues())],
            'start_date' => ['nullable', 'date'],
            'end_date' => ['nullable', 'date', 'after_or_equal:start_date'],
            'promo_code_only' => ['required', 'boolean'],

            'products' => ['sometimes', 'array'],
            'products.*.product_id' => ['required_with:products', 'integer', 'min:1', 'distinct'],
        ];
    }
}
