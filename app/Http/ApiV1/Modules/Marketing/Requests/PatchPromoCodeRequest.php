<?php

namespace App\Http\ApiV1\Modules\Marketing\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\MarketingClient\Dto\PromoCodeStatusEnum;
use Illuminate\Validation\Rule;

class PatchPromoCodeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'code' => ['sometimes', 'string'],
            'counter' => ['sometimes', 'nullable', 'integer', 'gt:0'],
            'start_date' => ['sometimes', 'nullable', 'date'],
            'end_date' => ['sometimes', 'nullable', 'date', 'after_or_equal:start_date'],
            'status' => ['sometimes', 'integer', Rule::in(PromoCodeStatusEnum::getAllowableEnumValues())],
            'discount_id' => ['sometimes', 'integer', 'min:1'],
        ];
    }
}
