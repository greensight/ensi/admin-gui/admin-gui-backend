<?php

namespace App\Http\ApiV1\Modules\Common\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class MenuResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'items' => $this->resource,
        ];
    }
}
