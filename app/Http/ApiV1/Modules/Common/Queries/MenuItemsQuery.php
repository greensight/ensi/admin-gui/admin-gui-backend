<?php

namespace App\Http\ApiV1\Modules\Common\Queries;

use App\Domain\Auth\Models\User;
use App\Domain\Common\Data\MenuData;
use Illuminate\Support\Facades\Auth;

class MenuItemsQuery
{
    protected User $user;

    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function getMenuItems(): array
    {
        $menu = MenuData::menu();
        $menuItems = $this->getMenuWithAccessRight($menu);

        return $this->getMenuItemCodes($menuItems);
    }

    /**
     * Получить пункты меню текущего уровня вложенности, к которому у пользователя есть права доступа
     * @param array $menu - пункты меню для текущего уровня вложенности и все вложенные в них пункты меню
     */
    protected function getMenuWithAccessRight(array &$menu): array
    {
        $isSuperAdmin = $this->user->isSuperAdmin();
        $userRightsAccess = $this->user->rightsAccess;

        foreach ($menu as $code => &$menuItem) {
            if (isset($menuItem['rightsAccess']) && !in_array($menuItem['rightsAccess'], $userRightsAccess)) {
                unset($menu[$code]);

                continue;
            }

            if (isset($menuItem['onlySuper']) && !$isSuperAdmin) {
                unset($menu[$code]);

                continue;
            }

            if (!empty($menuItem['items'])) {
                $menuItem['items'] = $this->getMenuWithAccessRight($menuItem['items']);
            }
        }

        return $menu;
    }

    /**
     * Получить коды пунктов меню со всех уровней вложенности
     * @return string[]
     */
    protected function getMenuItemCodes(array $menuItems): array
    {
        $codes = [];

        foreach ($menuItems as $code => $menuItem) {
            $codes[] = $code;

            if (!empty($menuItem['items'])) {
                $codes = array_merge($codes, $this->getMenuItemCodes($menuItem['items']));
            }
        }

        return $codes;
    }
}
