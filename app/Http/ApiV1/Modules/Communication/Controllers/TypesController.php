<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateTypeAction;
use App\Domain\Communication\Actions\DeleteTypeAction;
use App\Domain\Communication\Actions\PatchTypeAction;
use App\Http\ApiV1\Modules\Communication\Queries\TypesQuery;
use App\Http\ApiV1\Modules\Communication\Requests\CreateTypeRequest;
use App\Http\ApiV1\Modules\Communication\Requests\PatchTypeRequest;
use App\Http\ApiV1\Modules\Communication\Resources\TypesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class TypesController
{
    public function search(TypesQuery $query): Responsable
    {
        return TypesResource::collectPage($query->get());
    }

    public function create(CreateTypeRequest $request, CreateTypeAction $action): Responsable
    {
        return new TypesResource($action->execute($request->validated()));
    }

    public function patch(int $typeId, PatchTypeRequest $request, PatchTypeAction $action): Responsable
    {
        return new TypesResource($action->execute($typeId, $request->validated()));
    }

    public function delete(int $typeId, DeleteTypeAction $action): Responsable
    {
        $action->execute($typeId);

        return new EmptyResource();
    }
}
