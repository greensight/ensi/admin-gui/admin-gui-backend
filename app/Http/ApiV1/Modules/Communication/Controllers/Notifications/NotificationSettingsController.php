<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers\Notifications;

use App\Domain\Common\Data\Meta\Enum\Communication\CommunicationNotificationSettingChannelEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Communication\CommunicationNotificationSettingEventEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Communication\Actions\Notifications\CreateNotificationSettingAction;
use App\Domain\Communication\Actions\Notifications\DeleteNotificationSettingAction;
use App\Domain\Communication\Actions\Notifications\PatchNotificationSettingAction;
use App\Http\ApiV1\Modules\Communication\Queries\Notifications\NotificationSettingsQuery;
use App\Http\ApiV1\Modules\Communication\Requests\Notifications\CreateNotificationSettingRequest;
use App\Http\ApiV1\Modules\Communication\Requests\Notifications\PatchNotificationSettingRequest;
use App\Http\ApiV1\Modules\Communication\Resources\Notifications\NotificationSettingsResource;
use App\Http\ApiV1\Modules\Communication\Resources\Notifications\VariablesDictionaryResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Illuminate\Contracts\Support\Responsable;

class NotificationSettingsController
{
    public function create(CreateNotificationSettingRequest $request, CreateNotificationSettingAction $action): Responsable
    {
        return NotificationSettingsResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchNotificationSettingRequest $request, PatchNotificationSettingAction $action): Responsable
    {
        return NotificationSettingsResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteNotificationSettingAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, NotificationSettingsQuery $query): Responsable
    {
        return NotificationSettingsResource::make($query->find($id));
    }

    public function search(NotificationSettingsQuery $query): Responsable
    {
        return NotificationSettingsResource::collectPage($query->get());
    }

    public function settingVariables(NotificationsApi $notificationsApi): Responsable
    {
        return VariablesDictionaryResource::collection(
            $notificationsApi->getVariables()->getData(),
        );
    }

    public function meta(
        CommunicationNotificationSettingChannelEnumInfo $channelEnumInfo,
        CommunicationNotificationSettingEventEnumInfo $eventEnumInfo,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Название')->sort()->listDefault(),
            Field::enum('event', 'Событие', $eventEnumInfo)->sort()->listDefault(),
            Field::enum('channels', 'Канал', $channelEnumInfo)->array()->filterMany('channels_like')->listDefault(),
            Field::text('theme', 'Тема')->listDefault(),
            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
