<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers\Notifications;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Communication\Queries\Notifications\NotificationsQuery;
use App\Http\ApiV1\Modules\Communication\Resources\Notifications\NotificationsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Contracts\Support\Responsable;

class NotificationsController
{
    public function search(NotificationsQuery $query): Responsable
    {
        return NotificationsResource::collectPage($query->get());
    }

    /**
     * @throws Exception
     */
    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('customer_id', __('meta.notifications.customer_id'))->listDefault()->filterDefault(),
            Field::integer('event', __('meta.notifications.event'))->listDefault()->filterDefault()->sort(),
            Field::integer('channels', __('meta.notifications.channels'))->listDefault()->filterDefault(),
            Field::text('theme', __('meta.notifications.theme'))->listDefault()->filterDefault(),
            Field::text('text', __('meta.notifications.text'))->listDefault()->filterDefault(),
            Field::boolean('is_viewed', __('meta.notifications.is_viewed'))->listDefault()->filterDefault()->sort(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
