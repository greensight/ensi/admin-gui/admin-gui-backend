<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Http\ApiV1\Modules\Communication\Queries\MessagesQuery;
use App\Http\ApiV1\Modules\Communication\Resources\MessagesResource;
use Illuminate\Contracts\Support\Responsable;

class MessagesController
{
    public function search(MessagesQuery $query): Responsable
    {
        return MessagesResource::collectPage($query->get());
    }
}
