<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Domain\Communication\Actions\CreateAttachmentAction;
use App\Domain\Communication\Actions\DeleteAttachmentsAction;
use App\Http\ApiV1\Modules\Communication\Requests\CreateAttachmentRequest;
use App\Http\ApiV1\Modules\Communication\Requests\MassDeleteAttachmentsRequest;
use App\Http\ApiV1\Modules\Communication\Resources\AttachmentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class AttachmentsController
{
    public function create(CreateAttachmentRequest $request, CreateAttachmentAction $action): Responsable
    {
        return new AttachmentsResource($action->execute($request->getFile()));
    }

    public function massDelete(MassDeleteAttachmentsRequest $request, DeleteAttachmentsAction $action): Responsable
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }
}
