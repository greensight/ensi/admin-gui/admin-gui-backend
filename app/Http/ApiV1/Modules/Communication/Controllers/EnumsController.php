<?php

namespace App\Http\ApiV1\Modules\Communication\Controllers;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Illuminate\Contracts\Support\Responsable;

class EnumsController
{
    public function notificationSettingChannels(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(NotificationChannelEnum::getDescriptions()));
    }

    public function notificationSettingEvents(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(NotificationEventEnum::getDescriptions()));
    }
}
