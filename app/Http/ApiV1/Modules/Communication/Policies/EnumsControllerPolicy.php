<?php

namespace App\Http\ApiV1\Modules\Communication\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EnumsControllerPolicy
{
    use HandlesAuthorization;

    public function notificationSettingChannels(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
        ]);
    }

    public function notificationSettingEvents(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
        ]);
    }
}
