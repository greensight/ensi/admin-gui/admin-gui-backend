<?php

namespace App\Http\ApiV1\Modules\Communication\Policies\Notifications;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class NotificationSettingsControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_CREATE,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_DETAIL_READ,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_DELETE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_DETAIL_EDIT,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
        ]);
    }

    public function settingVariables(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ,
        ]);
    }
}
