<?php

namespace App\Http\ApiV1\Modules\Communication\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CommunicationManagerClient\Dto\RequestBodyPagination;
use Illuminate\Http\Request;

abstract class CommunicationQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(Request $request, protected string $requestGetClass)
    {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return $this->requestGetClass;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }
}
