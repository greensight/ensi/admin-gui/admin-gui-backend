<?php

namespace App\Http\ApiV1\Modules\Communication\Queries\Notifications;

use App\Http\ApiV1\Modules\Communication\Queries\CommunicationQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CommunicationManagerClient\Dto\NotificationSettingResponse;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsRequest;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationSettingsResponse;
use Illuminate\Http\Request;

class NotificationSettingsQuery extends CommunicationQuery
{
    use QueryBuilderFindTrait;

    public function __construct(Request $request, protected readonly NotificationsApi $api)
    {
        parent::__construct($request, SearchNotificationSettingsRequest::class);
    }

    protected function searchById($id, string $include): NotificationSettingResponse
    {
        return $this->api->getNotificationSetting($id);
    }

    protected function search($request): SearchNotificationSettingsResponse
    {
        return $this->api->searchNotificationSettings($request);
    }
}
