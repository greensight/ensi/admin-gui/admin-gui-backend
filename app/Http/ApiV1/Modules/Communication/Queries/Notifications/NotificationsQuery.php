<?php

namespace App\Http\ApiV1\Modules\Communication\Queries\Notifications;

use App\Http\ApiV1\Modules\Communication\Queries\CommunicationQuery;
use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationsRequest;
use Ensi\CommunicationManagerClient\Dto\SearchNotificationsResponse;
use Illuminate\Http\Request;

class NotificationsQuery extends CommunicationQuery
{
    public function __construct(Request $request, protected readonly NotificationsApi $api)
    {
        parent::__construct($request, SearchNotificationsRequest::class);
    }

    protected function search($request): SearchNotificationsResponse
    {
        return $this->api->searchNotifications($request);
    }
}
