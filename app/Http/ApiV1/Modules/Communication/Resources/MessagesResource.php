<?php

namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Domain\Communication\Data\MessageData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin MessageData */
class MessagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->message->getId(),
            "created_at" => $this->dateTimeToIso($this->message->getCreatedAt()),
            "user_id" => $this->message->getUserId(),
            "user_type" => $this->message->getUserType(),
            "chat_id" => $this->message->getChatId(),
            "text" => $this->message->getText(),
            "files" => $this->getFiles(),
        ];
    }
}
