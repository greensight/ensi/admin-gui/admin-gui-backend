<?php

namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CommunicationManagerClient\Dto\Type;

/** @mixin Type */
class TypesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'active' => $this->getActive(),
            'channel' => $this->getChannel(),
        ];
    }
}
