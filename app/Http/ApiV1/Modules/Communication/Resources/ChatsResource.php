<?php

namespace App\Http\ApiV1\Modules\Communication\Resources;

use App\Domain\Communication\Data\ChatData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin ChatData */
class ChatsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->chat->getId(),
            "user_id" => $this->chat->getUserId(),
            "user_type" => $this->chat->getUserType(),
            "theme" => $this->chat->getTheme(),
            "type_id" => $this->chat->getTypeId(),
            "messages" => MessagesResource::collection($this->whenNotNull($this->getMessages())),
        ];
    }
}
