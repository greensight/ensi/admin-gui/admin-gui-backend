<?php

namespace App\Http\ApiV1\Modules\Communication\Resources\Notifications;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CommunicationManagerClient\Dto\Variable;

/**
 * @mixin Variable
 */
class NotificationVariablesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'items' => self::collection($this->getItems()),
        ];
    }
}
