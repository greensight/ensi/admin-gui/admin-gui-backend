<?php

namespace App\Http\ApiV1\Modules\Communication\Resources\Notifications;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CommunicationManagerClient\Dto\VariablesDictionary;

/**
 * @mixin VariablesDictionary
 */
class VariablesDictionaryResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'event' => $this->getEvent(),
            'variables' => NotificationVariablesResource::collection($this->getVariables()),
        ];
    }
}
