<?php

namespace App\Http\ApiV1\Modules\Communication\Resources\Notifications;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CommunicationManagerClient\Dto\Notification;

/**
 * @mixin Notification
 */
class NotificationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'customer_id' => $this->getCustomerId(),
            'event' => $this->getEvent(),
            'channels' => $this->getChannels(),
            'theme' => $this->getTheme(),
            'text' => $this->getText(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'is_viewed' => $this->getIsViewed(),
        ];
    }
}
