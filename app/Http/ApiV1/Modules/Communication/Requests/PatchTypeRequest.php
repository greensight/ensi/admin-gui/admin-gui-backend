<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchTypeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'active' => ['nullable', 'boolean'],
            'channel' => ['nullable', 'array'],
            'channel.*' => ['integer'],
        ];
    }
}
