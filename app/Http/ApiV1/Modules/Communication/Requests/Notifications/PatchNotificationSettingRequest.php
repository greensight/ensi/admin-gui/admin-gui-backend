<?php

namespace App\Http\ApiV1\Modules\Communication\Requests\Notifications;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Illuminate\Validation\Rule;

class PatchNotificationSettingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'event' => ['sometimes', 'integer', Rule::in(NotificationEventEnum::getAllowableEnumValues()),],
            'channels' => ['sometimes', 'array', 'min:1'],
            'channels.*' => ['required_with:channels', 'integer', Rule::in(NotificationChannelEnum::getAllowableEnumValues())],
            'theme' => ['nullable', 'string'],
            'text' => ['sometimes', 'string'],
        ];
    }
}
