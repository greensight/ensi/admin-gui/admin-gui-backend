<?php

namespace App\Http\ApiV1\Modules\Communication\Requests\Notifications;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Illuminate\Validation\Rule;

class CreateNotificationSettingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'event' => ['required', 'integer', Rule::in(NotificationEventEnum::getAllowableEnumValues())],
            'channels' => ['required', 'array', 'min:1'],
            'channels.*' => ['required_with:channels', 'integer', Rule::in(NotificationChannelEnum::getAllowableEnumValues())],
            'theme' => ['nullable', 'string'],
            'text' => ['required', 'string'],
        ];
    }
}
