<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class CreateAttachmentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:10240'],
            'name' => ['sometimes', 'required', 'string', 'max:50'],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
