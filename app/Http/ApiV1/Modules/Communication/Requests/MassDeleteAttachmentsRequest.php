<?php

namespace App\Http\ApiV1\Modules\Communication\Requests;

use App\Http\ApiV1\Support\Requests\MassDeleteRequest;

class MassDeleteAttachmentsRequest extends MassDeleteRequest
{
}
