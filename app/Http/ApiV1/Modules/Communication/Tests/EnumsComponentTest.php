<?php


use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'enums');

test('GET /api/v1/communication/notification-setting-channels 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ]);

    $item = [
        'id' => NotificationChannelEnum::EMAIL,
        'name' => NotificationChannelEnum::getDescriptions()[NotificationChannelEnum::EMAIL],
    ];

    getJson('/api/v1/communication/notification-setting-channels')
        ->assertOk()
        ->assertJsonFragment($item);
});

test('GET /api/v1/communication/notification-setting-events 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ]);

    $item = [
        'id' => NotificationEventEnum::ORDER_WAIT_PAY,
        'name' => NotificationEventEnum::getDescriptions()[NotificationEventEnum::ORDER_WAIT_PAY],
    ];

    getJson('/api/v1/communication/notification-setting-events')
        ->assertOk()
        ->assertJsonFragment($item);
});
