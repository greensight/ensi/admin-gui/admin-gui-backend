<?php

namespace App\Http\ApiV1\Modules\Communication\Tests\Notifications\Factories;

use Ensi\CommunicationManagerClient\Dto\NotificationChannelEnum;
use Ensi\CommunicationManagerClient\Dto\NotificationEventEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NotificationSettingRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'event' => $this->faker->unique()->randomElement(NotificationEventEnum::getAllowableEnumValues()),
            'channels' => $this->faker->randomList(fn () => $this->faker->randomElement(NotificationChannelEnum::getAllowableEnumValues()), min: 1),
            'theme' => $this->faker->nullable()->sentence(3),
            'text' => $this->faker->text(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
