<?php

use App\Domain\Communication\Tests\Factories\Notifications\NotificationSettingFactory;
use App\Domain\Communication\Tests\Factories\Notifications\VariableDictionaryFactory;
use App\Http\ApiV1\Modules\Communication\Tests\Notifications\Factories\NotificationSettingRequestFactory;

use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CommunicationManagerClient\Dto\EmptyDataResponse;
use Ensi\CommunicationManagerClient\Dto\PatchNotificationSettingRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'notification-settings');

test('GET /api/v1/communication/notification-settings/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_DETAIL_READ]);

    $notificationSettingId = 1;

    $this->mockCommunicationNotificationsApi()
        ->shouldReceive('getNotificationSetting')
        ->andReturn(NotificationSettingFactory::new()->makeResponse(['id' => $notificationSettingId]));

    getJson("/api/v1/communication/notification-settings/{$notificationSettingId}")
        ->assertOk()
        ->assertJsonPath('data.id', $notificationSettingId)
        ->assertJsonStructure(['data' => ['id', 'event', 'channels', 'text']]);
});

test('POST /api/v1/communication/notification-settings:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ]);

    $notificationSettingId = 1;
    $count = 2;

    $this->mockCommunicationNotificationsApi()->allows([
        'searchNotificationSettings' => NotificationSettingFactory::new()
            ->makeResponseSearch([
                ['id' => $notificationSettingId],
                ['id' => $notificationSettingId + 1],
            ], $count),
    ]);

    $request = [
        'filter' => ['id' => [$notificationSettingId]],
    ];

    postJson('/api/v1/communication/notification-settings:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'event', 'channels', 'text']]])
        ->assertJsonPath('data.0.id', $notificationSettingId);
});

test('GET /api/v1/communication/notification-settings:meta 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ]);

    getJson('/api/v1/communication/notification-settings:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/communication/notification-settings 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_CREATE]);

    $this->mockCommunicationNotificationsApi()
        ->shouldReceive('createNotificationSetting')
        ->andReturn(NotificationSettingFactory::new()->makeResponse());

    $request = NotificationSettingRequestFactory::new()->make();

    postJson('/api/v1/communication/notification-settings', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'event', 'channels', 'text']]);
});

test('PATCH /api/v1/communication/notification-settings/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_DETAIL_EDIT]);

    $notificationSettingId = 1;

    $this->mockCommunicationNotificationsApi()
        ->shouldReceive('patchNotificationSetting')
        ->withArgs(fn (int $id, PatchNotificationSettingRequest $notificationSetting) => $id == $notificationSettingId)
        ->andReturn(NotificationSettingFactory::new()->makeResponse(['id' => $notificationSettingId]));

    $request = NotificationSettingRequestFactory::new()->make();

    patchJson("/api/v1/communication/notification-settings/{$notificationSettingId}")
        ->assertOk()
        ->assertJsonPath('data.id', $notificationSettingId)
        ->assertJsonStructure(['data' => ['id', 'event', 'channels', 'text']]);
});

test('DELETE /api/v1/communication/notification-settings/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_DELETE]);

    $notificationSettingId = 1;

    $this->mockCommunicationNotificationsApi()
        ->shouldReceive('deleteNotificationSetting')
        ->andReturn(new EmptyDataResponse());

    deleteJson("/api/v1/communication/notification-settings/{$notificationSettingId}")
        ->assertOk();
});

test('GET /api/v1/communication/notification-setting-variables 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NOTIFICATION_SETTING_LIST_READ]);

    $this->mockCommunicationNotificationsApi()
        ->shouldReceive('getVariables')
        ->andReturn(VariableDictionaryFactory::new()->makeResponse());

    getJson('/api/v1/communication/notification-setting-variables')
        ->assertOk()
        ->assertJsonStructure(['data' => [['event', 'variables' => [['id', 'title', 'items']]]]]);
});
