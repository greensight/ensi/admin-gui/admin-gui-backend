<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Cms\Actions\Seo\AddTemplateProductsAction;
use App\Domain\Cms\Actions\Seo\CreateTemplateAction;
use App\Domain\Cms\Actions\Seo\DeleteTemplateAction;
use App\Domain\Cms\Actions\Seo\DeleteTemplateProductsAction;
use App\Domain\Cms\Actions\Seo\PatchTemplateAction;
use App\Domain\Common\Data\Meta\Enum\Cms\CmsSeoTemplateTypeInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Cms\Queries\SeoTemplatesQuery;
use App\Http\ApiV1\Modules\Cms\Requests\AddSeoTemplateProductsRequest;
use App\Http\ApiV1\Modules\Cms\Requests\CreateSeoTemplateRequest;
use App\Http\ApiV1\Modules\Cms\Requests\DeleteSeoTemplateProductsRequest;
use App\Http\ApiV1\Modules\Cms\Requests\PatchSeoTemplateRequest;
use App\Http\ApiV1\Modules\Cms\Resources\SeoTemplatesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class SeoTemplatesController
{
    public function create(CreateSeoTemplateRequest $request, CreateTemplateAction $action): Responsable
    {
        return SeoTemplatesResource::make($action->execute($request->validated()));
    }

    public function delete(int $id, DeleteTemplateAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function patch(int $id, PatchSeoTemplateRequest $request, PatchTemplateAction $action): Responsable
    {
        return SeoTemplatesResource::make($action->execute($id, $request->validated()));
    }

    public function addProducts(int $id, AddSeoTemplateProductsRequest $request, AddTemplateProductsAction $action): Responsable
    {
        $action->execute($id, $request->getProductIds());

        return new EmptyResource();
    }

    public function deleteProducts(int $id, DeleteSeoTemplateProductsRequest $request, DeleteTemplateProductsAction $action): Responsable
    {
        $action->execute($id, $request->getProductIds());

        return new EmptyResource();
    }

    public function get(int $id, SeoTemplatesQuery $query): Responsable
    {
        return SeoTemplatesResource::make($query->find($id));
    }

    public function search(SeoTemplatesQuery $query): Responsable
    {
        return SeoTemplatesResource::collectPage($query->get());
    }

    public function meta(
        CmsSeoTemplateTypeInfo $type,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Название')->sort()->listDefault()->filterDefault(),
            Field::enum('type', 'Тип', $type)->sort()->listDefault()->filterDefault(),
            Field::text('header', 'Заголовок')->listDefault()->filterDefault(),

            Field::boolean('is_active', 'Активность')->listDefault()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
