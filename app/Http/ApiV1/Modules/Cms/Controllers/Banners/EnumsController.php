<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers\Banners;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\CmsClient\Dto\BannerButtonLocationEnum;
use Ensi\CmsClient\Dto\BannerButtonTypeEnum;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function bannerButtonTypes(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(BannerButtonTypeEnum::getDescriptions()));
    }

    public function bannerButtonLocations(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(BannerButtonLocationEnum::getDescriptions()));
    }
}
