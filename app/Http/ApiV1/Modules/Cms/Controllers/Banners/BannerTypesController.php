<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers\Banners;

use App\Domain\Common\Data\Meta\Enum\Cms\CmsBannerTypeInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Cms\Queries\BannerTypesQuery;
use App\Http\ApiV1\Modules\Cms\Resources\Banners\BannerTypesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class BannerTypesController
{
    public function search(BannerTypesQuery $query): Responsable
    {
        return BannerTypesResource::collectPage($query->get());
    }

    public function meta(CmsBannerTypeInfo $bannerTypeInfo): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->sortDefault(direction: 'desc'),
            Field::enum('code', __('meta.code'), $bannerTypeInfo)->filterDefault()->listDefault(),
            Field::string('name', __('meta.name'))->filter()->filterDefault()->listDefault(),
            Field::boolean('active', __('meta.banner_types.active'))->filterDefault()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->listDefault()->filterDefault(),
            Field::datetime('updated_at', __('meta.updated_at'))->listDefault()->filterDefault(),
        ]);
    }
}
