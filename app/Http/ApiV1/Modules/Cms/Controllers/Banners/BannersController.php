<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers\Banners;

use App\Domain\Cms\Actions\Banners\CreateBannerAction;
use App\Domain\Cms\Actions\Banners\DeleteBannerAction;
use App\Domain\Cms\Actions\Banners\ReplaceBannerAction;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Cms\Queries\BannersQuery;
use App\Http\ApiV1\Modules\Cms\Requests\Banners\CreateBannerRequest;
use App\Http\ApiV1\Modules\Cms\Requests\Banners\ReplaceBannerRequest;
use App\Http\ApiV1\Modules\Cms\Resources\Banners\BannersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class BannersController
{
    public function create(CreateBannerRequest $request, CreateBannerAction $action): Responsable
    {
        return new BannersResource($action->execute($request->validated()));
    }

    public function replace(int $id, ReplaceBannerRequest $request, ReplaceBannerAction $action): Responsable
    {
        return new BannersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteBannerAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(BannersQuery $query): Responsable
    {
        return BannersResource::collectPage($query->get());
    }

    public function searchOne(BannersQuery $query): Responsable
    {
        return BannersResource::make($query->first());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()
                ->listDefault()
                ->filterDefault()
                ->sortDefault(direction: 'desc')
                ->detailLink(),

            Field::text('name', 'Наименование')
                ->sort()
                ->filterDefault()
                ->listDefault(),

            Field::photo('desktop_image', 'Изображение')->listDefault(),

            Field::boolean('is_active', 'Статус')
                ->filterMany()
                ->filterDefault()
                ->listDefault(),

            Field::text('code', 'Код')
                ->sort()
                ->filterDefault()
                ->listDefault(),

            Field::integer('sort', 'Сортировка')->resetFilter(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
