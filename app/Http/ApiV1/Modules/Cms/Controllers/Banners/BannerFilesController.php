<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers\Banners;

use App\Domain\Cms\Actions\Banners\DeleteBannerFileAction;
use App\Domain\Cms\Actions\Banners\UploadBannerFileAction;
use App\Http\ApiV1\Modules\Cms\Requests\Banners\DeleteBannerFileRequest;
use App\Http\ApiV1\Modules\Cms\Requests\Banners\UploadBannerFileRequest;
use App\Http\ApiV1\Modules\Cms\Resources\Banners\BannerFilesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class BannerFilesController
{
    public function upload($id, UploadBannerFileRequest $request, UploadBannerFileAction $action): Responsable
    {
        return new BannerFilesResource($action->execute($id, $request->getType(), $request->getFile()));
    }

    public function delete($id, DeleteBannerFileRequest $request, DeleteBannerFileAction $action): Responsable
    {
        $action->execute($id, $request->getType());

        return new EmptyResource();
    }
}
