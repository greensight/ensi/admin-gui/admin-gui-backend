<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Modules\Cms\Queries\MenusQuery;
use App\Http\ApiV1\Modules\Cms\Resources\MenusResource;
use Illuminate\Contracts\Support\Responsable;

class MenusController
{
    public function search(MenusQuery $query): Responsable
    {
        return MenusResource::collectPage($query->get());
    }

    public function searchOne(MenusQuery $query): Responsable
    {
        return MenusResource::make($query->first());
    }
}
