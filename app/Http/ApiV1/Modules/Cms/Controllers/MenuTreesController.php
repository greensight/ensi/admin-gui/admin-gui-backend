<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Cms\Actions\Menu\ReplaceMenuTreesAction;
use App\Http\ApiV1\Modules\Cms\Requests\ReplaceMenuTreesRequest;
use App\Http\ApiV1\Modules\Cms\Resources\MenuTreesResource;
use Illuminate\Contracts\Support\Responsable;

class MenuTreesController
{
    public function update(int $id, ReplaceMenuTreesRequest $request, ReplaceMenuTreesAction $action): Responsable
    {
        return MenuTreesResource::collection($action->execute($id, $request->validated()));
    }
}
