<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\CmsClient\Dto\SeoTemplateTypeEnum;
use Ensi\CmsClient\Dto\SeoVariableEnum;
use Illuminate\Contracts\Support\Responsable;

class EnumsController
{
    public function templateTypes(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(SeoTemplateTypeEnum::getDescriptions()));
    }

    public function templateVariables(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(SeoVariableEnum::getDescriptions()));
    }
}
