<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Cms\Queries\SeoTemplateProductsQuery;
use App\Http\ApiV1\Modules\Cms\Resources\SeoTemplateProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class SeoTemplateProductsController
{
    public function get(int $id, SeoTemplateProductsQuery $query): Responsable
    {
        return SeoTemplateProductsResource::make($query->find($id));
    }

    public function search(SeoTemplateProductsQuery $query): Responsable
    {
        return SeoTemplateProductsResource::collectPage($query->get());
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->filter()->listDefault()->filterDefault()->detailLink(),

            Field::integer('product.id', 'ID продукта')->object('product')->listDefault(),
            Field::string('product.name', 'Название продукта')->object('product')->listDefault(),
            Field::photo('product.main_image_file', 'Изображение')->object('product')->listDefault(),
            Field::string('product.vendor_code', 'Артикул')->object('product')->listDefault(),
            Field::string('product.barcode', 'Штрихкод (EAN)')->object('product')->listDefault(),

            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
