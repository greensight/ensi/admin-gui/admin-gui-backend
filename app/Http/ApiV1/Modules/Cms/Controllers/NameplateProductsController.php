<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Common\Data\Meta\Enum\Catalog\ProductDraftEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Cms\Queries\NameplateProductsQuery;
use App\Http\ApiV1\Modules\Cms\Resources\NameplateProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class NameplateProductsController
{
    public function get(int $id, NameplateProductsQuery $query): Responsable
    {
        return NameplateProductsResource::make($query->find($id));
    }

    public function search(NameplateProductsQuery $query): Responsable
    {
        return NameplateProductsResource::collectPage($query->get());
    }

    public function meta(
        ProductDraftEnumInfo $productDraftEnumInfo,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->filter()->listDefault()->filterDefault()->detailLink(),

            Field::enum('product_id', 'Название продукта', $productDraftEnumInfo)->listHide(),

            Field::integer('product.id', 'ID продукта')->object('product')->listDefault(),
            Field::string('product.name', 'Название продукта')->object('product')->listDefault(),
            Field::photo('product.main_image_file', 'Изображение')->object('product')->listDefault(),
            Field::string('product.vendor_code', 'Артикул')->object('product')->listDefault(),
            Field::string('product.barcode', 'Штрихкод (EAN)')->object('product')->listDefault(),

            Field::datetime('created_at', 'Дата создания'),
            Field::datetime('updated_at', 'Дата обновления'),
        ]);
    }
}
