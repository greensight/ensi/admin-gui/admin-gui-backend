<?php

namespace App\Http\ApiV1\Modules\Cms\Controllers;

use App\Domain\Cms\Actions\Nameplates\AddProductsAction;
use App\Domain\Cms\Actions\Nameplates\CreateNameplateAction;
use App\Domain\Cms\Actions\Nameplates\DeleteNameplateAction;
use App\Domain\Cms\Actions\Nameplates\DeleteProductsAction;
use App\Domain\Cms\Actions\Nameplates\PatchNameplateAction;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Cms\Queries\NameplatesQuery;
use App\Http\ApiV1\Modules\Cms\Requests\AddNameplateProductsRequest;
use App\Http\ApiV1\Modules\Cms\Requests\CreateNameplateRequest;
use App\Http\ApiV1\Modules\Cms\Requests\DeleteNameplateProductsRequest;
use App\Http\ApiV1\Modules\Cms\Requests\PatchNameplateRequest;
use App\Http\ApiV1\Modules\Cms\Resources\NameplatesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class NameplatesController
{
    public function create(CreateNameplateRequest $request, CreateNameplateAction $action): Responsable
    {
        return NameplatesResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchNameplateRequest $request, PatchNameplateAction $action): Responsable
    {
        return NameplatesResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteNameplateAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, NameplatesQuery $query): Responsable
    {
        return NameplatesResource::make($query->find($id));
    }

    public function search(NameplatesQuery $query): Responsable
    {
        return NameplatesResource::collectPage($query->get());
    }

    public function addProducts(int $id, AddNameplateProductsRequest $request, AddProductsAction $action): Responsable
    {
        $action->execute($id, $request->getProductIds());

        return new EmptyResource();
    }

    public function deleteProducts(int $id, DeleteNameplateProductsRequest $request, DeleteProductsAction $action): Responsable
    {
        $action->execute($id, $request->getProductIds());

        return new EmptyResource();
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),

            Field::text('name', 'Название')->sort()->listDefault()->filterDefault(),

            Field::boolean('is_active', 'Активность')->listDefault()->filterDefault(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
