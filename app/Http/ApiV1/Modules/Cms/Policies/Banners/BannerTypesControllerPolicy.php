<?php

namespace App\Http\ApiV1\Modules\Cms\Policies\Banners;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class BannerTypesControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_LIST_READ,
            RightsAccessEnum::BANNER_DETAIL_READ,
            RightsAccessEnum::TECHNICAL_TABLES_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::TECHNICAL_TABLES_READ,
        ]);
    }
}
