<?php

namespace App\Http\ApiV1\Modules\Cms\Policies\Banners;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EnumsControllerPolicy
{
    use HandlesAuthorization;

    public function bannerButtonTypes(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_LIST_READ,
        ]);
    }

    public function bannerButtonLocations(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_LIST_READ,
        ]);
    }
}
