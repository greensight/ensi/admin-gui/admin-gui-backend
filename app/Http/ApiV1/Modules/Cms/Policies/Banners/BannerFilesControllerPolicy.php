<?php

namespace App\Http\ApiV1\Modules\Cms\Policies\Banners;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class BannerFilesControllerPolicy
{
    use HandlesAuthorization;

    public function upload(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_CREATE,
            RightsAccessEnum::BANNER_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_DETAIL_EDIT,
        ]);
    }
}
