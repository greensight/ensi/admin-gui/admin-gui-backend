<?php

namespace App\Http\ApiV1\Modules\Cms\Policies\Banners;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class BannersControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_LIST_READ,
            RightsAccessEnum::BANNER_DETAIL_READ,
        ]);
    }

    public function searchOne(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_LIST_READ,
            RightsAccessEnum::BANNER_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_CREATE,
        ]);
    }

    public function replace(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_DELETE,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BANNER_LIST_READ,
            RightsAccessEnum::BANNER_DETAIL_READ,
        ]);
    }
}
