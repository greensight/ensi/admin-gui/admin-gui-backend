<?php

namespace App\Http\ApiV1\Modules\Cms\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SeoTemplateProductsControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }
}
