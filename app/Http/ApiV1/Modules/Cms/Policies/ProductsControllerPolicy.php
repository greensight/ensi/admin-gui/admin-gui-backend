<?php

namespace App\Http\ApiV1\Modules\Cms\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductsControllerPolicy
{
    use HandlesAuthorization;

    public function addNameplates(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
        ]);
    }

    public function deleteNameplates(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
        ]);
    }
}
