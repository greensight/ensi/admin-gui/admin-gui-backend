<?php

namespace App\Http\ApiV1\Modules\Cms\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SeoTemplatesControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT,
        ]);
    }

    public function addProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT,
        ]);
    }

    public function deleteProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_DELETE,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SEO_TEMPLATE_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }
}
