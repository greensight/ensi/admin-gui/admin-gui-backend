<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Domain\Cms\Data\Seo\TemplateProductData;
use App\Domain\Common\Actions\AsyncLoadAction;
use App\Http\ApiV1\Modules\Cms\Queries\IncludeLoaders\CmsProductsLoader;
use Ensi\CmsClient\Api\SeoTemplateProductsApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\SearchSeoTemplateProductsRequest;
use Ensi\CmsClient\Dto\SearchSeoTemplateProductsResponse;
use Ensi\CmsClient\Dto\SeoTemplateProduct;
use Ensi\CmsClient\Dto\SeoTemplateProductResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SeoTemplateProductsQuery extends CmsQuery
{
    public function __construct(
        Request $request,
        protected AsyncLoadAction $asyncLoadAction,
        protected CmsProductsLoader $productsLoader,
        protected SeoTemplateProductsApi $api,
    ) {
        parent::__construct($request, SearchSeoTemplateProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): SeoTemplateProductResponse
    {
        return $this->api->getSeoTemplateProduct($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchSeoTemplateProductsResponse
    {
        return $this->api->searchSeoTemplateProducts($request);
    }

    protected function convertFindToItem($response): TemplateProductData
    {
        return $this->convertArray([$response->getData()])->first();
    }

    protected function convertGetToItems($response): Collection
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $array): Collection
    {
        /** @var Collection<SeoTemplateProduct> $templateProducts */
        $templateProducts = collect($array);

        $this->productsLoader->setProductIds($templateProducts->pluck('product_id'));
        $this->asyncLoadAction->execute([$this->productsLoader]);

        return TemplateProductData::mapCollect($templateProducts, $this->productsLoader->products);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "product":
                    $this->productsLoader->load = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
