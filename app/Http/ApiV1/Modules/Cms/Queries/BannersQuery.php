<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\BannerResponse;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchBannersRequest;
use Ensi\CmsClient\Dto\SearchBannersResponse;
use Ensi\CmsClient\Dto\SearchOneBannerRequest;
use Illuminate\Http\Request;

class BannersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFirstTrait;

    public function __construct(
        Request $httpRequest,
        protected readonly BannersApi $bannersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchBannersRequest::class;
    }

    protected function requestFirstClass(): string
    {
        return SearchOneBannerRequest::class;
    }

    protected function search($request): SearchBannersResponse
    {
        return $this->bannersApi->searchBanners($request);
    }

    protected function searchOne($request): BannerResponse
    {
        return $this->bannersApi->searchBanner($request);
    }
}
