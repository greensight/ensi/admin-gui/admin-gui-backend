<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\NameplateResponse;
use Ensi\CmsClient\Dto\SearchNameplatesRequest;
use Ensi\CmsClient\Dto\SearchNameplatesResponse;
use Illuminate\Http\Request;

class NameplatesQuery extends CmsQuery
{
    public function __construct(Request $request, protected NameplatesApi $api)
    {
        parent::__construct($request, SearchNameplatesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchNameplatesResponse
    {
        return $this->api->searchNameplates($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): NameplateResponse
    {
        return $this->api->getNameplate($id);
    }
}
