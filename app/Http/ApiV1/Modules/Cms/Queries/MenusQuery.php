<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\MenusApi;
use Ensi\CmsClient\Dto\MenuResponse;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchMenusRequest;
use Ensi\CmsClient\Dto\SearchMenusResponse;
use Ensi\CmsClient\Dto\SearchOneMenuRequest;
use Illuminate\Http\Request;

class MenusQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFirstTrait;

    public function __construct(
        Request $httpRequest,
        protected readonly MenusApi $menusApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchMenusRequest::class;
    }

    protected function requestFirstClass(): string
    {
        return SearchOneMenuRequest::class;
    }

    protected function search($request): SearchMenusResponse
    {
        return $this->menusApi->searchMenus($request);
    }

    protected function searchOne($request): MenuResponse
    {
        return $this->menusApi->searchMenu($request);
    }
}
