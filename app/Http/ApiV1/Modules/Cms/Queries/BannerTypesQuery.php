<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchBannerTypesRequest;
use Ensi\CmsClient\Dto\SearchBannerTypesResponse;
use Illuminate\Http\Request;

class BannerTypesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(
        Request $httpRequest,
        protected readonly BannersApi $bannersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchBannerTypesRequest::class;
    }

    protected function search($request): SearchBannerTypesResponse
    {
        return $this->bannersApi->searchBannerTypes($request);
    }
}
