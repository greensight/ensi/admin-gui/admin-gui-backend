<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Illuminate\Http\Request;

abstract class CmsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(Request $request, protected string $requestGetClass)
    {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return $this->requestGetClass;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }
}
