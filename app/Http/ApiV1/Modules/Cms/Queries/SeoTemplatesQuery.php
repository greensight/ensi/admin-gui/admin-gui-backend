<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\SearchSeoTemplatesRequest;
use Ensi\CmsClient\Dto\SearchSeoTemplatesResponse;
use Ensi\CmsClient\Dto\SeoTemplateResponse;
use Illuminate\Http\Request;

class SeoTemplatesQuery extends CmsQuery
{
    public function __construct(Request $request, protected SeoTemplatesApi $api)
    {
        parent::__construct($request, SearchSeoTemplatesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): SeoTemplateResponse
    {
        return $this->api->getSeoTemplate($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchSeoTemplatesResponse
    {
        return $this->api->searchSeoTemplates($request);
    }
}
