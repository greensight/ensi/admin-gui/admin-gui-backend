<?php

namespace App\Http\ApiV1\Modules\Cms\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\ApiException;
use Ensi\CmsClient\Dto\PageResponse;
use Ensi\CmsClient\Dto\RequestBodyPagination;
use Ensi\CmsClient\Dto\SearchOnePageRequest;
use Ensi\CmsClient\Dto\SearchPagesRequest;
use Ensi\CmsClient\Dto\SearchPagesResponse;
use Illuminate\Http\Request;

class SearchPagesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        Request $httpRequest,
        protected PagesApi $pagesApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchPagesRequest::class;
    }

    protected function requestFirstClass(): string
    {
        return SearchOnePageRequest::class;
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): PageResponse
    {
        return $this->pagesApi->getPage($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchPagesResponse
    {
        return $this->pagesApi->searchPages($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchOne($request): PageResponse
    {
        return $this->pagesApi->searchPage($request);
    }
}
