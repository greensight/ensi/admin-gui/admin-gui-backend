<?php

namespace App\Http\ApiV1\Modules\Cms\Resources\Banners;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\Banner;

/** @mixin Banner */
class BannersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'is_active' => $this->getIsActive(),
            'sort' => $this->getSort(),
            'url' => $this->getUrl(),
            'desktop_image' => $this->fileUrl($this->getDesktopImage()),
            'mobile_image' => $this->fileUrl($this->getMobileImage()),
            'type_id' => $this->getTypeId(),
            'button_id' => $this->getButtonId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'type' => BannerTypesResource::make($this->whenNotNull($this->getType())),
            'button' => BannerButtonsResource::make($this->whenNotNull($this->getButton())),
        ];
    }
}
