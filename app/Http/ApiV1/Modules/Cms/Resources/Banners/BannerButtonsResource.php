<?php

namespace App\Http\ApiV1\Modules\Cms\Resources\Banners;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\BannerButton;

/** @mixin BannerButton */
class BannerButtonsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'url' => $this->getUrl(),
            'text' => $this->getText(),
            'location' => $this->getLocation(),
            'type' => $this->getType(),
        ];
    }
}
