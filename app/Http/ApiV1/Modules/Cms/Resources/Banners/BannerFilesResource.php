<?php

namespace App\Http\ApiV1\Modules\Cms\Resources\Banners;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\BannerFile;

/** @mixin BannerFile */
class BannerFilesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'url' => $this->getUrl(),
        ];
    }
}
