<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\MenuItem;

/** @mixin MenuItem */
class MenuItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'url' => $this->getUrl(),
            'name' => $this->getName(),
            'menu_id' => $this->getMenuId(),
            '_lft' => $this->getLft(),
            '_rgt' => $this->getRgt(),
            'parent_id' => $this->getParentId(),
            'active' => $this->getActive(),
        ];
    }
}
