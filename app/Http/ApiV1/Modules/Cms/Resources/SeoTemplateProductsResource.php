<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Domain\Cms\Data\Seo\TemplateProductData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin TemplateProductData */
class SeoTemplateProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->data->getId(),
            'template_id' => $this->data->getTemplateId(),
            'product_id' => $this->data->getProductId(),
            'product' => ProductsResource::make($this->whenNotNull($this->product)),

            'created_at' => $this->dateTimeToIso($this->data->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->data->getUpdatedAt()),
        ];
    }
}
