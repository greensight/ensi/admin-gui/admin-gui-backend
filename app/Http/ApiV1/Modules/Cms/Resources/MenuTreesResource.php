<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\MenuTree;

/** @mixin MenuTree */
class MenuTreesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        if (is_array($this->resource)) {
            return $this->resource;
        }

        return [
            'name' => $this->getName(),
            'url' => $this->getUrl(),
            'active' => $this->getActive(),
            'children' => static::collection($this->whenNotNull($this->getChildren())),
        ];
    }
}
