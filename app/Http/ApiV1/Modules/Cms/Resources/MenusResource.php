<?php

namespace App\Http\ApiV1\Modules\Cms\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CmsClient\Dto\Menu;

/** @mixin Menu */
class MenusResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'items' => MenuItemsResource::collection($this->whenNotNull($this->getItems())),
            'items_tree' => MenuTreesResource::collection($this->whenNotNull($this->getItemsTree())),
        ];
    }
}
