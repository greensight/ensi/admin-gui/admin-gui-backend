<?php

use App\Domain\Cms\Tests\Factories\NameplateFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Factories\NameplateRequestFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Factories\RelationIdsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CmsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'cms', 'nameplate');

test('POST /api/v1/cms/nameplates 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRights([RightsAccessEnum::NAMEPLATE_CREATE]);

    $this->mockCmsNameplatesApi()->allows([
        'createNameplate' => NameplateFactory::new()->makeResponse(['id' => $nameplateId]),
    ]);
    $request = NameplateRequestFactory::new()->make();

    postJson('/api/v1/cms/nameplates', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $nameplateId);
});

test('POST /api/v1/cms/nameplates 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/cms/nameplates', NameplateRequestFactory::new()->make())
        ->assertStatus(403);
});

test('PATCH /api/v1/cms/nameplates/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRights([RightsAccessEnum::NAMEPLATE_DETAIL_EDIT]);

    $this->mockCmsNameplatesApi()->allows([
        'patchNameplate' => NameplateFactory::new()->makeResponse(['id' => $nameplateId]),
    ]);
    $request = NameplateRequestFactory::new()->make();

    patchJson("/api/v1/cms/nameplates/{$nameplateId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $nameplateId);
});

test('PATCH /api/v1/cms/nameplates/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRoles([]);

    patchJson("/api/v1/cms/nameplates/{$nameplateId}", NameplateRequestFactory::new()->make())
        ->assertStatus(403);
});

test('DELETE /api/v1/cms/nameplates/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRights([RightsAccessEnum::NAMEPLATE_DELETE]);

    $this->mockCmsNameplatesApi()->allows([
        'deleteNameplate' => new EmptyDataResponse(),
    ]);
    deleteJson("/api/v1/cms/nameplates/{$nameplateId}")
        ->assertOk();
});

test('DELETE /api/v1/cms/nameplates/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRoles([]);

    deleteJson("/api/v1/cms/nameplates/{$nameplateId}")
        ->assertStatus(403);
});

test('GET /api/v1/cms/nameplates/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRights([RightsAccessEnum::NAMEPLATE_DETAIL_READ]);

    $this->mockCmsNameplatesApi()->allows([
        'getNameplate' => NameplateFactory::new()->makeResponse(['id' => $nameplateId]),
    ]);

    getJson("/api/v1/cms/nameplates/{$nameplateId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'is_active']])
        ->assertJsonPath('data.id', $nameplateId);
});

test('GET /api/v1/cms/nameplates/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRoles([]);

    getJson("/api/v1/cms/nameplates/{$nameplateId}")
        ->assertStatus(403);
});

test('POST /api/v1/cms/nameplates/{id}:add-products 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRights([RightsAccessEnum::NAMEPLATE_DETAIL_EDIT]);

    $this->mockCmsNameplatesApi()->allows([
        'addNameplateProducts' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    postJson("/api/v1/cms/nameplates/{$nameplateId}:add-products", $request)
        ->assertOk();
});

test('POST /api/v1/cms/nameplates/{id}:add-products 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRoles([]);

    postJson("/api/v1/cms/nameplates/{$nameplateId}:add-products", RelationIdsRequestFactory::new()->make())
        ->assertStatus(403);
});

test('DELETE /api/v1/cms/nameplates/{id}:delete-products 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRights([RightsAccessEnum::NAMEPLATE_DETAIL_EDIT]);

    $this->mockCmsNameplatesApi()->allows([
        'deleteNameplateProducts' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    deleteJson("/api/v1/cms/nameplates/{$nameplateId}:delete-products", $request)
        ->assertOk();
});

test('DELETE /api/v1/cms/nameplates/{id}:delete-products 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $this->setRoles([]);

    deleteJson("/api/v1/cms/nameplates/{$nameplateId}:delete-products", RelationIdsRequestFactory::new()->make())
        ->assertStatus(403);
});

test('POST /api/v1/cms/nameplates:search 200', function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $nameplateId = 1;
    $count = 2;
    $this->setRights([$right]);

    $this->mockCmsNameplatesApi()->allows([
        'searchNameplates' => NameplateFactory::new()->makeResponseSearch([
            ['id' => $nameplateId],
            ['id' => $nameplateId + 1],
        ], count: $count),
    ]);

    $request = [
        'filter' => ['name_like' => 'тег'],
    ];

    postJson('/api/v1/cms/nameplates:search', $request)
        ->assertOk()
        ->assertJsonCount($count, 'data')
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'is_active']]])
        ->assertJsonPath('data.0.id', $nameplateId);
})->with([RightsAccessEnum::NAMEPLATE_LIST_READ, RightsAccessEnum::PRODUCT_CATALOG_DETAIL_READ, RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_READ]);

test('POST /api/v1/cms/nameplates:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson("/api/v1/cms/nameplates:search")
        ->assertStatus(403);
});

test('GET /api/v1/cms/nameplates:meta 200', function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([$right]);

    getJson('/api/v1/cms/nameplates:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
})->with([RightsAccessEnum::NAMEPLATE_LIST_READ, RightsAccessEnum::PRODUCT_CATALOG_DETAIL_READ, RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_READ]);

test('GET /api/v1/cms/nameplates:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson("/api/v1/cms/nameplates:meta")
        ->assertStatus(403);
});

test('POST /api/v1/cms/nameplates:meta all fields 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::NAMEPLATE_LIST_READ]);

    $request = getJson("/api/v1/cms/nameplates:meta");
    $metaFields = Arr::pluck($request->decodeResponseJson()['data']['fields'], 'code');

    $this->mockCmsNameplatesApi()->allows([
        'searchNameplates' => NameplateFactory::new()->makeResponseSearch(),
    ]);
    $request = postJson("/api/v1/cms/nameplates:search");
    $nameplatesFields = array_keys($request->decodeResponseJson()['data'][0]);
    $nameplatesFields = array_diff($nameplatesFields, ['code', 'background_color', 'text_color']);

    sort($metaFields);
    sort($nameplatesFields);
    assertEquals($metaFields, $nameplatesFields);
});
