<?php

use App\Domain\Cms\Tests\Factories\BannerButtonFactory;
use App\Domain\Cms\Tests\Factories\BannerFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Banners\Factories\BannerRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CmsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'cms');

test('POST /api/v1/cms/banners:search success', function () {
    $this->setRights([RightsAccessEnum::BANNER_DETAIL_READ]);

    $bannerId = 1;
    $bannerName = 'Example banner';

    $this->mockCmsBannersApi()->allows([
        'searchBanners' => BannerFactory::new()->makeResponseSearch([
            ['id' => $bannerId, 'name' => $bannerName],
        ]),
    ]);

    postJson('/api/v1/cms/banners:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $bannerId)
        ->assertJsonPath('data.0.name', $bannerName);
});

test('POST /api/v1/cms/banners:search with button success', function () {
    $this->setRights([RightsAccessEnum::BANNER_DETAIL_READ]);

    $bannerId = 1;
    $bannerName = 'Example banner';

    $button = BannerButtonFactory::new()->make();

    $this->mockCmsBannersApi()->allows([
        'searchBanners' => BannerFactory::new()->withButton($button)->makeResponseSearch([
            ['id' => $bannerId, 'name' => $bannerName,],
        ]),
    ]);

    postJson('/api/v1/cms/banners:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $bannerId)
        ->assertJsonPath('data.0.name', $bannerName)
        ->assertJsonPath('data.0.button.id', $button->getId());
});

test('POST /api/v1/cms/banners:search-one success', function () {
    $this->setRights([RightsAccessEnum::BANNER_DETAIL_READ]);

    $bannerId = 1;
    $bannerName = 'Example banner';

    $this->mockCmsBannersApi()->allows([
        'searchBanner' => BannerFactory::new()->makeResponse([
            'id' => $bannerId,
            'name' => $bannerName,
        ]),
    ]);

    postJson('/api/v1/cms/banners:search-one')
        ->assertStatus(200)
        ->assertJsonPath('data.id', $bannerId)
        ->assertJsonPath('data.name', $bannerName);
});

test('POST /api/v1/cms/banners create success', function (bool $useSort) {
    $this->setRights([RightsAccessEnum::BANNER_CREATE]);

    $bannerId = 1;
    $bannerName = 'Example banner';

    $this->mockCmsBannersApi()->allows([
        'createBanner' => BannerFactory::new()->makeResponse([
            'id' => $bannerId,
            'name' => $bannerName,
        ]),
    ]);

    $request = BannerRequestFactory::new()->withSort($useSort)->make();

    postJson('/api/v1/cms/banners', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $bannerId)
        ->assertJsonPath('data.name', $bannerName);
})->with([
    [true],
    [false],
]);

test('PUT /api/v1/cms/banners/{id} replace success', function (bool $useSort) {
    $this->setRights([RightsAccessEnum::BANNER_DETAIL_EDIT]);

    $bannerId = 1;
    $this->mockCmsBannersApi()->allows([
        'replaceBanner' => BannerFactory::new()->makeResponse(['id' => $bannerId]),
    ]);

    $request = BannerRequestFactory::new()->withSort($useSort)->make();

    putJson("/api/v1/cms/banners/{$bannerId}", $request)
        ->assertJsonPath('data.id', $bannerId)
        ->assertOk();
})->with([
    [true],
    [false],
]);

test('PUT /api/v1/cms/banners/{id} delete success', function () {
    $this->setRights([RightsAccessEnum::BANNER_DELETE]);

    $bannerId = 1;
    $this->mockCmsBannersApi()->allows(['deleteBanner' => new EmptyDataResponse()]);

    deleteJson("/api/v1/cms/banners/{$bannerId}")
        ->assertOk();
});
