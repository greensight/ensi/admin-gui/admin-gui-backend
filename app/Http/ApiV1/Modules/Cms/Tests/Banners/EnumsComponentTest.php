<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CmsClient\Dto\BannerButtonLocationEnum;
use Ensi\CmsClient\Dto\BannerButtonTypeEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/cms/banner-button-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $item = [
        'id' => BannerButtonTypeEnum::TYPE_WHITE,
        'name' => BannerButtonTypeEnum::getDescriptions()[BannerButtonTypeEnum::TYPE_WHITE],
    ];

    getJson("/api/v1/cms/banner-button-types")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/cms/banner-button-locations 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $item = [
        'id' => BannerButtonLocationEnum::LOCATION_TOP,
        'name' => BannerButtonLocationEnum::getDescriptions()[BannerButtonLocationEnum::LOCATION_TOP],
    ];

    getJson("/api/v1/cms/banner-button-locations")
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
