<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Banners\Factories;

use Ensi\CmsClient\Dto\BannerImageTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerDeleteFileRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'type' => $this->faker->randomElement(BannerImageTypeEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
