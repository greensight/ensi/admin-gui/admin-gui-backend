<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Banners\Factories;

use Ensi\CmsClient\Dto\BannerImageTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Illuminate\Http\UploadedFile;

class BannerUploadFileRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'file' => UploadedFile::fake()->create('file.png', kilobytes: 20),
            'type' => $this->faker->randomElement(BannerImageTypeEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
