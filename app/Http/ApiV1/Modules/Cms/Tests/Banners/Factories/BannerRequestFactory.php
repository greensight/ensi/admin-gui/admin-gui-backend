<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Banners\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class BannerRequestFactory extends BaseApiFactory
{
    protected bool $useSort = false;

    protected function definition(): array
    {
        $data = [
            'name' => $this->faker->words(3, true),
            'is_active' => $this->faker->boolean,
            'url' => $this->faker->optional()->url,
            'type_id' => $this->faker->optional()->modelId(),
        ];

        if ($this->useSort) {
            $data['sort'] = $this->faker->numberBetween();
        }

        return $data;
    }

    public function withSort(bool $useSort = true): self
    {
        $this->useSort = $useSort;

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
