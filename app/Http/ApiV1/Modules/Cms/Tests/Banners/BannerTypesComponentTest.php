<?php

use App\Domain\Cms\Tests\Factories\BannerTypesFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms', 'banner_types');

test('POST /api/v1/cms/banner-types:search success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCmsBannersApi()->allows([
        'searchBannerTypes' => BannerTypesFactory::new()->makeResponseSearch([
            ['id' => 33],
            ['id' => 34],
        ], 2),
    ]);

    $request = [
        'filter' => ['code' => 'code'],
    ];

    postJson('/api/v1/cms/banner-types:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'code']]])
        ->assertJsonPath('data.0.id', 33);
});

test('GET /api/v1/cms/banner-types:meta success', function () {
    getJson('/api/v1/cms/banner-types:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
