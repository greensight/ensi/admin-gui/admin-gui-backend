<?php

use App\Domain\Cms\Tests\Factories\BannerFileFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Banners\Factories\BannerDeleteFileRequestFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Banners\Factories\BannerUploadFileRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'cms');

test('POST /api/v1/cms/banners/{id}:upload-file upload file success', function () {
    $this->setRights([RightsAccessEnum::BANNER_DETAIL_EDIT]);

    $bannerId = 1;
    $this->mockCmsBannersApi()->allows([
        'uploadBannerFile' => BannerFileFactory::new()->make(),
    ]);

    $request = BannerUploadFileRequestFactory::new()->make();

    postFormData("/api/v1/cms/banners/{$bannerId}:upload-file", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['url']]);
});

test('POST /api/v1/cms/banners/{id}:delete-file delete file success', function () {
    $this->setRights([RightsAccessEnum::BANNER_DETAIL_EDIT]);

    $bannerId = 1;
    $this->mockCmsBannersApi()->allows([
        'deleteBannerFile' => BannerFileFactory::new()->make(),
    ]);

    $request = BannerDeleteFileRequestFactory::new()->make();

    postJson("/api/v1/cms/banners/{$bannerId}:delete-file", $request)
        ->assertOk();
});
