<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),

            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),

            'is_active' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
