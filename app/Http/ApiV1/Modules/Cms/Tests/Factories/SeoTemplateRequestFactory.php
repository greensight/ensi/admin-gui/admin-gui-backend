<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Factories;

use Ensi\CmsClient\Dto\SeoTemplateTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SeoTemplateRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->word(),
            'type' => $this->faker->randomElement(SeoTemplateTypeEnum::getAllowableEnumValues()),
            'header' => $this->faker->text(),
            'title' => $this->faker->nullable()->text(),
            'description' => $this->faker->nullable()->text(),
            'seo_text' => $this->faker->nullable()->text(),
            'is_active' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
