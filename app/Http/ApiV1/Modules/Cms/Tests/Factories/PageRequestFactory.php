<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PageRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $dateTime = $this->faker->dateTime();

        return [
            'name' => $this->faker->words(3, true),
            'slug' => $this->faker->slug(),
            'content' => $this->faker->randomHtml(),
            'is_active' => $this->faker->boolean(),
            'active_from' => $dateTime->format('Y-m-d\TH:i:s.u\Z'),
            'active_to' => $dateTime->add(new \DateInterval('P1D'))->format('Y-m-d\TH:i:s.u\Z'),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
