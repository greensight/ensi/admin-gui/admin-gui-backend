<?php

namespace App\Http\ApiV1\Modules\Cms\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class RelationIdsRequestFactory extends BaseApiFactory
{
    protected array $ids = [];

    protected function definition(): array
    {
        return [
            'ids' => $this->ids ?: $this->faker->randomList(fn () => $this->faker->unique()->modelId(), 1),
        ];
    }

    public function fillIds(array $ids): self
    {
        $this->ids = array_unique(array_merge($this->ids, $ids));

        return $this;
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
