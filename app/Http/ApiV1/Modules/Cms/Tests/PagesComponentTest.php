<?php

namespace App\Http\ApiV1\Modules\Cms\Tests;

use App\Domain\Cms\Tests\Factories\PageFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Factories\PageRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CmsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms');

test('POST /api/v1/cms/pages:search 200', function () {
    $pageId = 1;
    $pageName = 'Important news';

    $this->mockCmsPagesApi()->allows([
        'searchPages' => PageFactory::new()->makeResponseSearch([[
            'id' => $pageId,
            'name' => $pageName,
        ]]),
    ]);

    postJson('/api/v1/cms/pages:search')
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $pageId)
        ->assertJsonPath('data.0.name', $pageName);
});

test('POST /api/v1/cms/pages:search 200 empty response', function () {
    $this->mockCmsPagesApi()->allows([
        'searchPages' => PageFactory::new()->makeResponseSearch([], 0),
    ]);

    postJson('/api/v1/cms/pages:search')
        ->assertStatus(200)
        ->assertJsonPath('data', []);
});

test('POST /api/v1/cms/pages 200 create page', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCmsPagesApi()->allows([
        'createPage' => PageFactory::new()->makeResponse(),
    ]);

    $request = PageRequestFactory::new()->make();

    postJson('/api/v1/cms/pages', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'slug', 'content']]);
});

test('GET /api/v1/cms/pages/{id} 200', function () {
    $this->mockCmsPagesApi()->allows([
        'getPage' => PageFactory::new()->makeResponse(),
    ]);

    getJson("/api/v1/cms/pages/5")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'slug', 'content']]);
});

test('PATCH /api/v1/cms/pages/{id} 200', function () {
    $this->mockCmsPagesApi()->allows([
        'patchPage' => PageFactory::new()->makeResponse(['id' => 15]),
    ]);

    $request = PageRequestFactory::new()->make();

    patchJson('/api/v1/cms/pages/15', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'slug', 'content']]);
});

test('DELETE /api/v1/cms/pages/{id} 200', function () {
    $this->mockCmsPagesApi()->allows([
        'deletePage' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/cms/pages/5')
        ->assertOk();
});
