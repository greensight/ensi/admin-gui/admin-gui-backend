<?php

use App\Domain\Cms\Tests\Factories\SeoTemplateFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Factories\RelationIdsRequestFactory;
use App\Http\ApiV1\Modules\Cms\Tests\Factories\SeoTemplateRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CmsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'cms', 'seo');

test('POST /api/v1/cms/seo/templates 201', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_CREATE]);

    $templateId = 1;

    $this->mockCmsSeoTemplatesApi()->allows([
        'createSeoTemplate' => SeoTemplateFactory::new()->makeResponse(['id' => $templateId]),
    ]);
    $request = SeoTemplateRequestFactory::new()->make();

    postJson('/api/v1/cms/seo/templates', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $templateId);
});

test('PATCH /api/v1/cms/seo/templates/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT]);

    $templateId = 1;

    $this->mockCmsSeoTemplatesApi()->allows([
        'patchSeoTemplate' => SeoTemplateFactory::new()->makeResponse(['id' => $templateId]),
    ]);
    $request = SeoTemplateRequestFactory::new()->make();

    patchJson("/api/v1/cms/seo/templates/{$templateId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $templateId);
});

test('DELETE /api/v1/cms/seo/templates/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_DELETE]);

    $templateId = 1;

    $this->mockCmsSeoTemplatesApi()->allows([
        'deleteSeoTemplate' => new EmptyDataResponse(),
    ]);
    deleteJson("/api/v1/cms/seo/templates/{$templateId}")
        ->assertOk();
});

test('GET /api/v1/cms/seo/templates/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_DETAIL_READ]);

    $templateId = 1;

    $this->mockCmsSeoTemplatesApi()->allows([
        'getSeoTemplate' => SeoTemplateFactory::new()->makeResponse(['id' => $templateId]),
    ]);

    getJson("/api/v1/cms/seo/templates/{$templateId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'type', 'name', 'header', 'is_active']])
        ->assertJsonPath('data.id', $templateId);
});

test('POST /api/v1/cms/seo/templates/{id}:add-products 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT]);

    $templateId = 1;
    $this->mockCmsSeoTemplatesApi()->allows([
        'addSeoTemplateProducts' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    postJson("/api/v1/cms/seo/templates/{$templateId}:add-products", $request)
        ->assertOk();
});


test('DELETE /api/v1/cms/seo/templates/{id}:delete-products 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_DETAIL_EDIT]);

    $templateId = 1;
    $this->mockCmsSeoTemplatesApi()->allows([
        'deleteSeoTemplateProducts' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    deleteJson("/api/v1/cms/seo/templates/{$templateId}:delete-products", $request)
        ->assertOk();
});

test('POST /api/v1/cms/seo/templates:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_LIST_READ]);

    $templateId = 1;
    $count = 2;

    $this->mockCmsSeoTemplatesApi()->allows([
        'searchSeoTemplates' => SeoTemplateFactory::new()->makeResponseSearch([
            ['id' => $templateId],
            ['id' => $templateId + 1],
        ], count: $count),
    ]);

    $request = [
        'filter' => ['name_like' => 'шаблон'],
    ];

    postJson('/api/v1/cms/seo/templates:search', $request)
        ->assertOk()
        ->assertJsonCount($count, 'data')
        ->assertJsonStructure(['data' => [['id', 'type', 'name', 'header', 'is_active']]])
        ->assertJsonPath('data.0.id', $templateId);
});

test('GET /api/v1/cms/seo/templates:meta 200', function () {
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_LIST_READ]);

    getJson('/api/v1/cms/seo/templates:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/cms/seo/templates:meta all fields 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->withoutMiddleware();

    $request = getJson("/api/v1/cms/seo/templates:meta");
    $metaFields = Arr::pluck($request->decodeResponseJson()['data']['fields'], 'code');

    $this->mockCmsSeoTemplatesApi()->allows([
        'searchSeoTemplates' => SeoTemplateFactory::new()->makeResponseSearch(),
    ]);
    $request = postJson("/api/v1/cms/seo/templates:search");
    $templatesFields = array_keys($request->decodeResponseJson()['data'][0]);
    $templatesFields = array_diff($templatesFields, ['title', 'description', 'seo_text']);

    sort($metaFields);
    sort($templatesFields);
    assertEquals($metaFields, $templatesFields);
});
