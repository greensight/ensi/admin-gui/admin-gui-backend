<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Cms\Tests\Factories\SeoTemplateProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'cms', 'seo');

test('GET /api/v1/cms/seo/template-products/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_DETAIL_READ]);

    $templateProductId = 1;
    $this->mockCmsSeoTemplateProductsApi()->allows([
        'getSeoTemplateProduct' => SeoTemplateProductFactory::new()->makeResponse(['id' => $templateProductId]),
    ]);

    getJson("/api/v1/cms/seo/template-products/{$templateProductId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'template_id', 'product_id']])
        ->assertJsonPath('data.id', $templateProductId);
});

test('POST /api/v1/cms/seo/template-products:search 200 include success', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_LIST_READ]);

    $templateProductId = 1;
    $count = 2;

    $templateProducts = SeoTemplateProductFactory::new()->makeResponseSearch([
        ['id' => $templateProductId],
        ['id' => $templateProductId + 1],
    ], $count);

    $this->mockCmsSeoTemplateProductsApi()->allows([
        'searchSeoTemplateProducts' => $templateProducts,
    ]);

    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(
            ProductFactory::new()->makeResponseSearch([['id' => current($templateProducts->getData())->getProductId()]])
        ),
    ]);

    $request = [
        'filter' => ['template_id' => $templateProductId],
        'include' => ['product'],
    ];

    postJson('/api/v1/cms/seo/template-products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'template_id', 'product_id']]])
        ->assertJsonPath('data.0.id', $templateProductId);
});

test('GET /api/v1/cms/seo/template-products:meta 200', function () {
    $this->setRights([RightsAccessEnum::SEO_TEMPLATE_LIST_READ]);
    getJson('/api/v1/cms/seo/template-products:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
