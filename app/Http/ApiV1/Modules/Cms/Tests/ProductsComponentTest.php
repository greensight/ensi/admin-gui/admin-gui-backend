<?php

use App\Http\ApiV1\Modules\Cms\Tests\Factories\RelationIdsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CmsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'cms', 'nameplate');

test('POST /api/v1/cms/products/{id}:add-nameplates 200', function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $productId = 1;
    $this->setRights([$right]);
    $this->mockCmsProductsApi()->allows([
        'addProductNameplates' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    postJson("/api/v1/cms/products/{$productId}:add-nameplates", $request)
        ->assertOk();
})->with([RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT, RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE]);

test('POST /api/v1/cms/products/{id}:add-nameplates 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $productId = 1;
    $this->setRights([]);

    postJson("/api/v1/cms/products/{$productId}:add-nameplates", RelationIdsRequestFactory::new()->make())
        ->assertStatus(403);
});

test('DELETE /api/v1/cms/products/{id}:delete-nameplates 200', function (int $right) {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 1;
    $this->setRights([$right]);
    $this->mockCmsProductsApi()->allows([
        'deleteProductNameplates' => new EmptyDataResponse(),
    ]);

    $request = RelationIdsRequestFactory::new()->make();

    deleteJson("/api/v1/cms/products/{$productId}:delete-nameplates", $request)
        ->assertOk();
})->with([RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT, RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE]);

test('DELETE /api/v1/cms/products/{id}:delete-nameplates 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $productId = 1;
    $this->setRights([]);

    deleteJson("/api/v1/cms/products/{$productId}:delete-nameplates", RelationIdsRequestFactory::new()->make())
        ->assertStatus(403);
});
