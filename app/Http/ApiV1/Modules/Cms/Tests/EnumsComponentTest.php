<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\CmsClient\Dto\SeoTemplateTypeEnum;
use Ensi\CmsClient\Dto\SeoVariableEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'cms', 'enums');

test('GET /api/v1/cms/seo/template-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => SeoTemplateTypeEnum::PRODUCT,
        'name' => SeoTemplateTypeEnum::getDescriptions()[SeoTemplateTypeEnum::PRODUCT],
    ];

    getJson('/api/v1/cms/seo/template-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/cms/seo/template-variables 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => SeoVariableEnum::PRODUCT_NAME,
        'name' => SeoVariableEnum::getDescriptions()[SeoVariableEnum::PRODUCT_NAME],
    ];

    getJson('/api/v1/cms/seo/template-variables')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
