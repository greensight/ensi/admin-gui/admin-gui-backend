<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchSeoTemplateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'is_active' => ['boolean'],
            'seo_text' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'title' => ['nullable', 'string'],
            'header' => ['string'],
            'type' => ['integer'],
            'name' => ['string'],
        ];
    }
}
