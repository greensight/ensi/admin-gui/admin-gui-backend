<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchPageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
            'slug' => ['nullable', 'string'],
            'content' => ['nullable', 'string'],
            'is_active' => ['nullable', 'boolean'],
            'active_from' => ['nullable', 'string', 'date'],
            'active_to' => ['nullable', 'string', 'date', 'after_or_equal:active_from'],
        ];
    }
}
