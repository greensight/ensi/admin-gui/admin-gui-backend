<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreatePageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'slug' => ['required', 'string'],
            'content' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
            'active_from' => ['nullable', 'string', 'date'],
            'active_to' => ['nullable', 'string', 'date', 'after_or_equal:active_from'],
        ];
    }
}
