<?php

namespace App\Http\ApiV1\Modules\Cms\Requests\Banners;

use App\Http\ApiV1\OpenApiGenerated\Enums\CmsBannerButtonLocationEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\CmsBannerButtonTypeEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateBannerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'required'],
            'is_active' => ['boolean', 'required'],
            'url' => ['string', 'nullable'],
            'type_id' => ['integer', 'nullable'],
            'sort' => ['integer', 'gte:0'],

            'button' => ['array', 'min:1', 'nullable'],
            'button.url' => ['string', 'required_with:button'],
            'button.text' => ['string', 'required_with:button'],
            'button.location' => ['string', 'required_with:button', new Enum(CmsBannerButtonLocationEnum::class)],
            'button.type' => ['string', 'required_with:button', new Enum(CmsBannerButtonTypeEnum::class)],
        ];
    }
}
