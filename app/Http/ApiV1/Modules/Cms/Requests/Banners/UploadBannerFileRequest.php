<?php

namespace App\Http\ApiV1\Modules\Cms\Requests\Banners;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\CmsClient\Dto\BannerImageTypeEnum;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rule;

class UploadBannerFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'image', 'max:250'],
            'type' => ['required', Rule::in(BannerImageTypeEnum::getAllowableEnumValues())],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }

    public function getType(): string
    {
        return $this->input('type');
    }
}
