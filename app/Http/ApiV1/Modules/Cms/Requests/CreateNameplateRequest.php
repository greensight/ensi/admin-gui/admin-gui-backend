<?php

namespace App\Http\ApiV1\Modules\Cms\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateNameplateRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['required', 'string'],
            'text_color' => ['required', 'string'],
            'background_color' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
        ];
    }
}
