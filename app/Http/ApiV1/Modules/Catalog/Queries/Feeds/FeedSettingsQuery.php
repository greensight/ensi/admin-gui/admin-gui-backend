<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Feeds;

use App\Http\ApiV1\Modules\Catalog\Queries\FeedQuery;
use Ensi\FeedClient\Api\FeedSettingsApi;
use Ensi\FeedClient\ApiException;
use Ensi\FeedClient\Dto\FeedSettingsResponse;
use Ensi\FeedClient\Dto\SearchFeedSettingsRequest;
use Ensi\FeedClient\Dto\SearchFeedSettingsResponse;
use Illuminate\Http\Request;

class FeedSettingsQuery extends FeedQuery
{
    public function __construct(Request $request, private FeedSettingsApi $api)
    {
        parent::__construct($request, SearchFeedSettingsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): FeedSettingsResponse
    {
        return $this->api->getFeedSettings($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchFeedSettingsResponse
    {
        return $this->api->searchFeedSettings($request);
    }
}
