<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Feeds;

use App\Http\ApiV1\Modules\Catalog\Queries\FeedQuery;
use Ensi\FeedClient\Api\FeedsApi;
use Ensi\FeedClient\ApiException;
use Ensi\FeedClient\Dto\FeedResponse;
use Ensi\FeedClient\Dto\SearchFeedsRequest;
use Ensi\FeedClient\Dto\SearchFeedsResponse;
use Illuminate\Http\Request;

class FeedsQuery extends FeedQuery
{
    public function __construct(Request $request, private FeedsApi $api)
    {
        parent::__construct($request, SearchFeedsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): FeedResponse
    {
        return $this->api->getFeed($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchFeedsResponse
    {
        return $this->api->searchFeeds($request);
    }
}
