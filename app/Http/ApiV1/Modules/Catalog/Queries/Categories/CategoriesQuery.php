<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Categories;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\CategoryResponse;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchCategoriesRequest;
use Ensi\PimClient\Dto\SearchCategoriesResponse;
use Illuminate\Http\Request;

class CategoriesQuery extends PimQuery
{
    use QueryBuilderFilterEnumTrait;

    public function __construct(Request $request, private CategoriesApi $api)
    {
        parent::__construct($request, SearchCategoriesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): CategoryResponse
    {
        return $this->api->getCategory($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchCategoriesResponse
    {
        return $this->api->searchCategories($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }

        $request->setFilter((object) $filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
