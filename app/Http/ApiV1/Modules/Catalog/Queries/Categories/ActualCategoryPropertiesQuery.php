<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Categories;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\SearchActualCategoryPropertiesRequest;
use Ensi\PimClient\Dto\SearchActualCategoryPropertiesResponse;
use Illuminate\Http\Request;

class ActualCategoryPropertiesQuery extends PimQuery
{
    public function __construct(Request $request, private CategoriesApi $api)
    {
        parent::__construct($request, SearchActualCategoryPropertiesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchActualCategoryPropertiesResponse
    {
        return $this->api->searchActualCategoryProperties($request);
    }

    protected function searchById($id, string $include)
    {
        // Not used for ActualCategoryProperty, yet
    }
}
