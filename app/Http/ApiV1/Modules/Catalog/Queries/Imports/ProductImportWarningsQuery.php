<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Imports;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ImportsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductImportResponse;
use Ensi\PimClient\Dto\SearchProductImportWarningsRequest;
use Ensi\PimClient\Dto\SearchProductImportWarningsResponse;
use Illuminate\Http\Request;
use LogicException;

class ProductImportWarningsQuery extends PimQuery
{
    public function __construct(Request $request, protected readonly ImportsApi $api)
    {
        parent::__construct($request, SearchProductImportWarningsRequest::class);
    }

    protected function searchById($id, string $include): ProductImportResponse
    {
        throw new LogicException('Not supported');
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductImportWarningsResponse
    {
        return $this->api->searchProductImportWarnings($request);
    }
}
