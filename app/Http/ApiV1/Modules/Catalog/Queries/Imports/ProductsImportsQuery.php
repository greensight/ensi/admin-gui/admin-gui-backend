<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Imports;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ImportsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\ProductImportResponse;
use Ensi\PimClient\Dto\SearchProductImportsRequest;
use Ensi\PimClient\Dto\SearchProductImportsResponse;
use Illuminate\Http\Request;

class ProductsImportsQuery extends PimQuery
{
    public function __construct(Request $request, protected readonly ImportsApi $api)
    {
        parent::__construct($request, SearchProductImportsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): ProductImportResponse
    {
        return $this->api->getProductImport($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductImportsResponse
    {
        return $this->api->searchProductImports($request);
    }
}
