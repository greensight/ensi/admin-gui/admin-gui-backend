<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\ProductEvents;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\PimClient\Api\ProductEventsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductEventsRequest;
use Ensi\PimClient\Dto\SearchProductEventsResponse;
use Illuminate\Http\Request;

class ProductEventsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(Request $request, protected ProductEventsApi $api)
    {
        parent::__construct($request);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductEventsResponse
    {
        return $this->api->searchProductEvents($request);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchProductEventsRequest::class;
    }
}
