<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Offers;

use App\Http\ApiV1\Modules\Catalog\Queries\BaseOffersQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\ApiException;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OffersClient\Dto\SearchOffersResponse;
use Illuminate\Http\Request;

class OffersQuery extends BaseOffersQuery
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        Request $request,
        private readonly OffersApi $api,
    ) {
        parent::__construct($request, SearchOffersRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): OfferResponse
    {
        return $this->api->getOffer($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchOffersResponse
    {
        return $this->api->searchOffers($request);
    }
}
