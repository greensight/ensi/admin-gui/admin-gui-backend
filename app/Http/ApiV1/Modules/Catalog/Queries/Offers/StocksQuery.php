<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Offers;

use App\Http\ApiV1\Modules\Catalog\Queries\BaseOffersQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\OffersClient\ApiException;
use Ensi\OffersClient\Dto\SearchStocksRequest;
use Ensi\OffersClient\Dto\SearchStocksResponse;
use Ensi\OffersClient\Dto\StockResponse;
use Illuminate\Http\Request;

class StocksQuery extends BaseOffersQuery
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        Request $request,
        private readonly StocksApi $api,
    ) {
        parent::__construct($request, SearchStocksRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): StockResponse
    {
        return $this->api->getStock($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchStocksResponse
    {
        return $this->api->searchStocks($request);
    }
}
