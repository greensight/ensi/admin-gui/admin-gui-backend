<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Illuminate\Http\Request;

abstract class PimQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(Request $request, protected string $requestGetClass)
    {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return $this->requestGetClass;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }
}
