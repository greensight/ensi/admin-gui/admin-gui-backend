<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Classifiers;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\PimClient\Api\ProductStatusesApi;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchStatusSettingsRequest;
use Ensi\PimClient\Dto\StatusSettingResponse;
use Ensi\PimClient\Dto\StatusSettingsResponse;
use Illuminate\Http\Request;

class ProductStatusesQuery extends PimQuery
{
    use QueryBuilderFilterEnumTrait;

    public function __construct(Request $request, protected readonly ProductStatusesApi $api)
    {
        parent::__construct($request, SearchStatusSettingsRequest::class);
    }

    protected function searchById($id, string $include): StatusSettingResponse
    {
        return $this->api->getProductStatus($id, $this->getStringInclude());
    }

    protected function search($request): StatusSettingsResponse
    {
        return $this->api->searchProductStatuses($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }

        $request->setFilter((object) $filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
