<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Classifiers;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\BrandResponse;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchBrandsRequest;
use Ensi\PimClient\Dto\SearchBrandsResponse;
use Illuminate\Http\Request;

class BrandsQuery extends PimQuery
{
    use QueryBuilderFilterEnumTrait;

    public function __construct(Request $request, protected BrandsApi $brandsApi)
    {
        parent::__construct($request, SearchBrandsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): BrandResponse
    {
        return $this->brandsApi->getBrand($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBrandsResponse
    {
        return $this->brandsApi->searchBrands($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }

        $request->setFilter((object) $filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
