<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\PimCommon;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\PimClient\Api\CommonApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchBulkOperationsRequest;
use Ensi\PimClient\Dto\SearchBulkOperationsResponse;
use Illuminate\Http\Request;

class BulkOperationsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(Request $request, protected CommonApi $api)
    {
        parent::__construct($request);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBulkOperationsResponse
    {
        return $this->api->searchBulkOperations($request);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchBulkOperationsRequest::class;
    }
}
