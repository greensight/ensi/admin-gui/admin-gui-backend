<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\PimCommon;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\PimClient\Api\CommonApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchFailedJobsRequest;
use Ensi\PimClient\Dto\SearchFailedJobsResponse;
use Illuminate\Http\Request;

class FailedJobsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(Request $request, protected CommonApi $api)
    {
        parent::__construct($request);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchFailedJobsResponse
    {
        return $this->api->searchFailedJobs($request);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchFailedJobsRequest::class;
    }
}
