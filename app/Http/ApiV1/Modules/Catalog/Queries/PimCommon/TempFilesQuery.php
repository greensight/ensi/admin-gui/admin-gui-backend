<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\PimCommon;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\PimClient\Api\TempFilesApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchTempFilesRequest;
use Ensi\PimClient\Dto\SearchTempFilesResponse;
use Illuminate\Http\Request;

class TempFilesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;

    public function __construct(Request $request, protected TempFilesApi $api)
    {
        parent::__construct($request);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchTempFilesResponse
    {
        return $this->api->searchTempFiles($request);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchTempFilesRequest::class;
    }
}
