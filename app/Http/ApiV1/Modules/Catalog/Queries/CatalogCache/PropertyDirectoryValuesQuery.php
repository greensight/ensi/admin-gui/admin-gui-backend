<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\PropertyDirectoryValuesApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesRequest;
use Ensi\CatalogCacheClient\Dto\SearchPropertyDirectoryValuesResponse;
use Illuminate\Http\Request;

class PropertyDirectoryValuesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private PropertyDirectoryValuesApi $api)
    {
        parent::__construct($request, SearchPropertyDirectoryValuesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchPropertyDirectoryValuesResponse
    {
        return $this->api->searchPropertyDirectoryValues($request);
    }
}
