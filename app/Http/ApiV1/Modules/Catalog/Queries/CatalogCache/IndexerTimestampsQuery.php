<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\ElasticApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsRequest;
use Ensi\CatalogCacheClient\Dto\SearchIndexerTimestampsResponse;
use Illuminate\Http\Request;

class IndexerTimestampsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private ElasticApi $api)
    {
        parent::__construct($request, SearchIndexerTimestampsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchIndexerTimestampsResponse
    {
        return $this->api->searchIndexerTimestamps($request);
    }
}
