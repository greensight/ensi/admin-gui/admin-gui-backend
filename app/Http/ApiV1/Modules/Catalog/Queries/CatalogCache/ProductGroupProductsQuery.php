<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\ProductGroupsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsRequest;
use Ensi\CatalogCacheClient\Dto\SearchProductGroupProductsResponse;
use Illuminate\Http\Request;

class ProductGroupProductsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private ProductGroupsApi $api)
    {
        parent::__construct($request, SearchProductGroupProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductGroupProductsResponse
    {
        return $this->api->searchProductGroupProducts($request);
    }
}
