<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\ProductsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesRequest;
use Ensi\CatalogCacheClient\Dto\SearchProductPropertyValuesResponse;
use Illuminate\Http\Request;

class ProductPropertyValuesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private ProductsApi $api)
    {
        parent::__construct($request, SearchProductPropertyValuesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductPropertyValuesResponse
    {
        return $this->api->searchProductPropertyValues($request);
    }
}
