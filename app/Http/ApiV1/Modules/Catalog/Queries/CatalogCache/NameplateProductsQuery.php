<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\NameplatesApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchNameplateProductsRequest;
use Ensi\CatalogCacheClient\Dto\SearchNameplateProductsResponse;
use Illuminate\Http\Request;

class NameplateProductsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private NameplatesApi $api)
    {
        parent::__construct($request, SearchNameplateProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchNameplateProductsResponse
    {
        return $this->api->searchNameplateProducts($request);
    }
}
