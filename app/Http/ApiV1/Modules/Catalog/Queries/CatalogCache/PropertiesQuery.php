<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\PropertiesApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchPropertiesRequest;
use Ensi\CatalogCacheClient\Dto\SearchPropertiesResponse;
use Illuminate\Http\Request;

class PropertiesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private PropertiesApi $api)
    {
        parent::__construct($request, SearchPropertiesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchPropertiesResponse
    {
        return $this->api->searchProperties($request);
    }
}
