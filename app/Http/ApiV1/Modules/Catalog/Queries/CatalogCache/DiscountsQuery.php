<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\DiscountsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchDiscountsRequest;
use Ensi\CatalogCacheClient\Dto\SearchDiscountsResponse;
use Illuminate\Http\Request;

class DiscountsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private DiscountsApi $api)
    {
        parent::__construct($request, SearchDiscountsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchDiscountsResponse
    {
        return $this->api->searchDiscounts($request);
    }
}
