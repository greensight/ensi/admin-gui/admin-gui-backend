<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\CategoriesApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchCategoriesRequest;
use Ensi\CatalogCacheClient\Dto\SearchCategoriesResponse;
use Illuminate\Http\Request;

class CategoriesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private CategoriesApi $api)
    {
        parent::__construct($request, SearchCategoriesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchCategoriesResponse
    {
        return $this->api->searchCategories($request);
    }
}
