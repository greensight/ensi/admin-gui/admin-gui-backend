<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\ProductGroupsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchProductGroupsRequest;
use Ensi\CatalogCacheClient\Dto\SearchProductGroupsResponse;
use Illuminate\Http\Request;

class ProductGroupsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private ProductGroupsApi $api)
    {
        parent::__construct($request, SearchProductGroupsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductGroupsResponse
    {
        return $this->api->searchProductGroups($request);
    }
}
