<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\ProductsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchProductsRequest;
use Ensi\CatalogCacheClient\Dto\SearchProductsResponse;
use Illuminate\Http\Request;

class ProductsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private ProductsApi $api)
    {
        parent::__construct($request, SearchProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductsResponse
    {
        return $this->api->searchProducts($request);
    }
}
