<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\BrandsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchBrandsRequest;
use Ensi\CatalogCacheClient\Dto\SearchBrandsResponse;
use Illuminate\Http\Request;

class BrandsQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private BrandsApi $api)
    {
        parent::__construct($request, SearchBrandsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBrandsResponse
    {
        return $this->api->searchBrands($request);
    }
}
