<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\NameplatesApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchNameplatesRequest;
use Ensi\CatalogCacheClient\Dto\SearchNameplatesResponse;
use Illuminate\Http\Request;

class NameplatesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private NameplatesApi $api)
    {
        parent::__construct($request, SearchNameplatesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchNameplatesResponse
    {
        return $this->api->searchNameplates($request);
    }
}
