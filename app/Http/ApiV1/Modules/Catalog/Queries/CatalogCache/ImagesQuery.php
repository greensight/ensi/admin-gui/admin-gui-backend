<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\ProductsApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchImagesRequest;
use Ensi\CatalogCacheClient\Dto\SearchImagesResponse;
use Illuminate\Http\Request;

class ImagesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private ProductsApi $api)
    {
        parent::__construct($request, SearchImagesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchImagesResponse
    {
        return $this->api->searchImages($request);
    }
}
