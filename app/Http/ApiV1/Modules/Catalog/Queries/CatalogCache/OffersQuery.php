<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\OffersApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchOffersRequest;
use Ensi\CatalogCacheClient\Dto\SearchOffersResponse;
use Illuminate\Http\Request;

class OffersQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private OffersApi $api)
    {
        parent::__construct($request, SearchOffersRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchOffersResponse
    {
        return $this->api->searchOffers($request);
    }
}
