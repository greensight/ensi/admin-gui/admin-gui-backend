<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache;

use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCacheQuery;
use Ensi\CatalogCacheClient\Api\CategoriesApi;
use Ensi\CatalogCacheClient\ApiException;
use Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesRequest;
use Ensi\CatalogCacheClient\Dto\SearchActualCategoryPropertiesResponse;
use Illuminate\Http\Request;

class ActualCategoryPropertiesQuery extends CatalogCacheQuery
{
    public function __construct(Request $request, private CategoriesApi $api)
    {
        parent::__construct($request, SearchActualCategoryPropertiesRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchActualCategoryPropertiesResponse
    {
        return $this->api->searchActualCategoryProperties($request);
    }
}
