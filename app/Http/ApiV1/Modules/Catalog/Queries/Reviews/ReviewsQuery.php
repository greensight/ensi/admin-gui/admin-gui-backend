<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Reviews;

use App\Domain\Catalog\Data\Reviews\ReviewData;
use App\Domain\Customers\Data\CustomerData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\PaginationTypeEnum as CustomersPaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination as CustomersRequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\CustomersClient\Dto\SearchCustomersResponse;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum as PimPaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\ReviewsClient\ApiException;
use Ensi\ReviewsClient\Dto\RequestBodyPagination;
use Ensi\ReviewsClient\Dto\Review;
use Ensi\ReviewsClient\Dto\ReviewResponse;
use Ensi\ReviewsClient\Dto\SearchReviewsRequest;
use Ensi\ReviewsClient\Dto\SearchReviewsResponse;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReviewsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    protected Collection $products;
    protected Collection $customers;

    protected bool $loadCustomers = false;
    protected bool $loadProducts = false;

    public function __construct(
        Request $request,
        protected ReviewsApi $reviewsApi,
        protected CustomersApi $customersApi,
        protected ProductsApi $productsApi
    ) {
        parent::__construct($request);
    }

    protected function requestGetClass(): string
    {
        return SearchReviewsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): ReviewResponse
    {
        return $this->reviewsApi->getReview($id);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchReviewsResponse
    {
        return $this->reviewsApi->searchReviews($request);
    }

    protected function convertGetToItems(SearchReviewsResponse $response): array
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem(ReviewResponse $response): ReviewData
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertArray(array $reviews): array
    {
        /** @var Collection|Review[] $reviews */
        $reviews = collect($reviews);

        $customerIds = $reviews->pluck('customer_id')->all();
        $productIds = $reviews->pluck('product_id')->all();

        $this->customers = collect();
        $this->products = collect();

        $promises = [];
        if ($this->loadCustomers && !empty($customerIds)) {
            $promises[] = $this->loadCustomers($customerIds);
        }

        if ($this->loadProducts && !empty($productIds)) {
            $promises[] = $this->loadProducts($productIds);
        }

        Utils::all(array_filter($promises))->wait();

        $reviewsData = [];
        foreach ($reviews as $review) {
            $customer = $this->customers->get($review->getCustomerId());
            if (!empty($customer)) {
                $customer = new CustomerData($customer);
            }

            $product = $this->products->get($review->getProductId());

            $reviewsData[] = new ReviewData($review, $product, $customer);
        }

        return $reviewsData;
    }

    protected function loadCustomers(array $ids): PromiseInterface
    {
        $request = new SearchCustomersRequest();
        $request->setFilter((object)['id' => $ids]);
        $request->setPagination((new CustomersRequestBodyPagination())->setLimit(count($ids))->setType(CustomersPaginationTypeEnum::CURSOR));

        return $this->customersApi->searchCustomersAsync($request)
            ->then(function (SearchCustomersResponse $response) {
                $this->customers = collect($response->getData())->keyBy('id');
            });
    }

    protected function loadProducts(array $ids): PromiseInterface
    {
        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $ids]);
        $request->setPagination((new PimRequestBodyPagination())->setLimit(count($ids))->setType(PimPaginationTypeEnum::CURSOR));

        return $this->productsApi->searchProductsAsync($request)
            ->then(function (SearchProductsResponse $response) {
                $this->products = collect($response->getData())->keyBy('id');
            });
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case 'customers':
                    $this->loadCustomers = true;

                    break;
                case 'products':
                    $this->loadProducts = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
