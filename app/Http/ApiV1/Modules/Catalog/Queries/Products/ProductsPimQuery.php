<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\ProductResponse;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use Illuminate\Http\Request;

class ProductsPimQuery extends PimQuery
{
    public function __construct(
        Request $request,
        private readonly ProductsApi $productsApi,
    ) {
        parent::__construct($request, SearchProductsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): ProductResponse
    {
        return $this->productsApi->getProduct($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchProductsResponse
    {
        return $this->productsApi->searchProducts($request);
    }

    public function searchEnums(string $property): array
    {
        $requestClass = $this->requestGetClass();
        $request = new $requestClass();

        $filter = $this->httpRequest->get('filter');
        $ids = data_get($filter, 'id');
        if (!empty($ids)) {
            $filter['id'] = $ids;
        }
        $filter[$property] = data_get($filter, 'query');
        unset($filter['query']);
        $request->setFilter((object)$filter);

        $pagination = (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR);
        $request->setPagination($ids ? $pagination->setLimit(count($ids)) : $pagination);

        try {
            $response = $this->search($request);

            return $this->convertGetToItems($response);
        } catch (ApiException) {
            return [];
        }
    }
}
