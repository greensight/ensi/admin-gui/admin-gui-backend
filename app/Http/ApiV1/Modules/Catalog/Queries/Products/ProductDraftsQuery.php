<?php

namespace App\Http\ApiV1\Modules\Catalog\Queries\Products;

use App\Http\ApiV1\Modules\Catalog\Queries\PimQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException;
use Ensi\PimClient\Dto\PaginationTypeEnum;
use Ensi\PimClient\Dto\ProductDraftResponse;
use Ensi\PimClient\Dto\RequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductDraftsRequest;
use Ensi\PimClient\Dto\SearchProductDraftsResponse;
use Illuminate\Http\Request;

class ProductDraftsQuery extends PimQuery
{
    use QueryBuilderFilterEnumTrait;

    public function __construct(
        Request $request,
        protected readonly ProductsApi $productsApi,
    ) {
        parent::__construct($request, SearchProductDraftsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): ProductDraftResponse
    {
        return $this->productsApi->getProductDraft($id, $include);
    }

    protected function search($request): SearchProductDraftsResponse
    {
        return $this->productsApi->searchProductDrafts($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name'] = $query;
        }

        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
