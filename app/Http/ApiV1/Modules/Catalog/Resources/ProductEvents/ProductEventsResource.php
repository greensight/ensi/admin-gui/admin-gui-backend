<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\ProductEvents;

use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductEvent;

/**
 * @mixin ProductEvent
 */
class ProductEventsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'event_id' => $this->getEventId(),
            'product_id' => $this->getProductId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'product' => ProductsResource::make($this->whenNotNull($this->getProduct())),
        ];
    }
}
