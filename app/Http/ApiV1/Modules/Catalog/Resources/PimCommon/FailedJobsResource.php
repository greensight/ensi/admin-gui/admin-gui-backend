<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\PimCommon;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\FailedJob;

/**
 * @mixin FailedJob
 */
class FailedJobsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'uuid' => $this->getUuid(),
            'connection' => $this->getConnection(),
            'queue' => $this->getQueue(),
            'payload' => $this->getPayload(),
            'exception' => $this->getException(),
            'failed_at' => $this->dateTimeToIso($this->getFailedAt()),
        ];
    }
}
