<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\PimCommon;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\BulkOperationError;

/** @mixin BulkOperationError */
class BulkOperationErrorsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'operation_id' => $this->getOperationId(),
            'entity_id' => $this->getEntityId(),
            'message' => $this->getMessage(),
        ];
    }
}
