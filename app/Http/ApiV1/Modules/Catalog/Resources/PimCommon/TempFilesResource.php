<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\PimCommon;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\TempFile;

/**
 * @mixin TempFile
 */
class TempFilesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'path' => $this->getPath(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
        ];
    }
}
