<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\ProductGroups;

use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductDraftsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductGroup;
use Ensi\PimClient\Dto\ProductImage;
use Illuminate\Support\Arr;

/**
 * @mixin ProductGroup
 */
class ProductGroupsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $mainProductImages = $this->getMainProduct()?->getImages();
        /** @var ProductImage|null $mainProductImage */
        $mainProductImage = $mainProductImages ? Arr::first($mainProductImages) : null;

        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'category_id' => $this->getCategoryId(),
            'is_active' => $this->getIsActive(),
            'main_product_id' => $this->getMainProductId(),
            'products_count' => $this->getProductsCount(),
            'main_product_image' => $mainProductImage?->getFile()?->getUrl(),

            'category' => CategoriesResource::make($this->whenNotNull($this->getCategory())),
            'main_product' => ProductDraftsResource::make($this->whenNotNull($this->getMainProduct())),
            'products' => ProductDraftsResource::collection($this->whenNotNull($this->getProducts())),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
