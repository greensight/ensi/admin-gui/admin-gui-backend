<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ProductPropertyValue;

/**
 * @mixin ProductPropertyValue
 */
class ProductPropertyValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_property_value_id' => $this->getProductPropertyValueId(),
            'product_id' => $this->getProductId(),
            'property_id' => $this->getPropertyId(),
            'directory_value_id' => $this->getDirectoryValueId(),
            'type' => $this->getType(),
            'value' => $this->getValue(),
            'name' => $this->getName(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'property' => PropertiesResource::make($this->whenNotNull($this->getProperty())),
            'directory_value' => PropertyDirectoryValuesResource::make($this->whenNotNull($this->getDirectoryValue())),
        ];
    }
}
