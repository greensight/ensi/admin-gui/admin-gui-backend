<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\Discount;

/**
 * @mixin Discount
 */
class DiscountsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'offer_id' => $this->getOfferId(),
            'value_type' => $this->getValueType(),
            'value' => $this->getValue(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'offer' => OffersResource::make($this->whenNotNull($this->getOffer())),
        ];
    }
}
