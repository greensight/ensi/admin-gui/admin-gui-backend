<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ProductGroupProduct;

/**
 * @mixin ProductGroupProduct
 */
class ProductGroupProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_group_product_id' => $this->getProductGroupId(),
            'product_group_id' => $this->getProductGroupId(),
            'product_id' => $this->getProductId(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'product_group' => ProductGroupsResource::make($this->whenNotNull($this->getProductGroup())),
            'product' => ProductsResource::make($this->whenNotNull($this->getProduct())),
        ];
    }
}
