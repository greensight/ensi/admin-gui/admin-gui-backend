<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\Offer;

/**
 * @mixin Offer
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'offer_id' => $this->getOfferId(),
            'product_id' => $this->getProductId(),
            'base_price' => $this->getBasePrice(),
            'price' => $this->getPrice(),
            'is_active' => $this->getIsActive(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'product' => ProductsResource::make($this->whenNotNull($this->getProduct())),
            'discount' => DiscountsResource::make($this->whenNotNull($this->getDiscount())),
            'product_group' => ProductGroupsResource::make($this->whenNotNull($this->getProductGroup())),
            'product_group_product' => ProductGroupProductsResource::make($this->whenNotNull($this->getProductGroupProduct())),
        ];
    }
}
