<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\Property;

/**
 * @mixin Property
 */
class PropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'property_id' => $this->getPropertyId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'type' => $this->getType(),
            'is_public' => $this->getIsPublic(),
            'is_active' => $this->getIsActive(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
