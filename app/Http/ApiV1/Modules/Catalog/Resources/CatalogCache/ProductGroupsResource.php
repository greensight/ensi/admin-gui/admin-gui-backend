<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ProductGroup;

/**
 * @mixin ProductGroup
 */
class ProductGroupsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_group_id' => $this->getProductGroupId(),
            'category_id' => $this->getCategoryId(),
            'name' => $this->getName(),
            'main_product_id' => $this->getMainProductId(),
            'is_active' => $this->getIsActive(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'category' => CategoriesResource::make($this->whenNotNull($this->getCategory())),
            'main_product' => ProductsResource::make($this->whenNotNull($this->getMainProduct())),
            'products' => ProductsResource::collection($this->whenNotNull($this->getProducts())),
            'product_links' => ProductGroupProductsResource::collection($this->whenNotNull($this->getProductLinks())),
        ];
    }
}
