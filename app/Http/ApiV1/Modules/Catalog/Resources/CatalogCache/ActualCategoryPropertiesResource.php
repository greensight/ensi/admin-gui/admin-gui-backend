<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\ActualCategoryProperty;

/**
 * @mixin ActualCategoryProperty
 */
class ActualCategoryPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'actual_category_property_id' => $this->getActualCategoryPropertyId(),
            'property_id' => $this->getPropertyId(),
            'category_id' => $this->getCategoryId(),
            'is_migrated' => $this->getIsMigrated(),
            'is_gluing' => $this->getIsGluing(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
