<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\IndexerTimestamp;

/**
 * @mixin IndexerTimestamp
 */
class IndexerTimestampsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'index' => $this->getIndex(),
            'stage' => $this->getStage(),
            'index_hash' => $this->getIndexHash(),
            'last_schedule' => $this->dateTimeToIso($this->getLastSchedule()),

            'is_current_index' => $this->getIsCurrentIndex(),
            'is_current_stage' => $this->getIsCurrentStage(),
            'is_current' => $this->getIsCurrent(),
        ];
    }
}
