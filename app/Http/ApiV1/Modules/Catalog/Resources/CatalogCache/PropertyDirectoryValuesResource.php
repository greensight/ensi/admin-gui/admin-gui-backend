<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\PropertyDirectoryValue;

/**
 * @mixin PropertyDirectoryValue
 */
class PropertyDirectoryValuesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'directory_value_id' => $this->getDirectoryValueId(),
            'property_id' => $this->getPropertyId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'value' => $this->getValue(),
            'type' => $this->getType(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
