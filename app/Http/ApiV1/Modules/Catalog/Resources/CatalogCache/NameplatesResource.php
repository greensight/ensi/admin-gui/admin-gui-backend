<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\Nameplate;

/**
 * @mixin Nameplate
 */
class NameplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'nameplate_id' => $this->getNameplateId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'background_color' => $this->getBackgroundColor(),
            'text_color' => $this->getTextColor(),
            'is_active' => $this->getIsActive(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'product_links' => NameplateProductsResource::collection($this->whenNotNull($this->getProductLinks())),
        ];
    }
}
