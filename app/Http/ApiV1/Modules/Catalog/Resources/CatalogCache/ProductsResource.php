<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CatalogCacheClient\Dto\Product;

/**
 * @mixin Product
 */
class ProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_id' => $this->getProductId(),
            'allow_publish' => $this->getAllowPublish(),
            'main_image_file' => $this->getMainImageFile(),
            'category_ids' => $this->getCategoryIds(),
            'brand_id' => $this->getBrandId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'description' => $this->getDescription(),
            'type' => $this->getType(),
            'vendor_code' => $this->getVendorCode(),
            'barcode' => $this->getBarcode(),
            'weight' => $this->getWeight(),
            'weight_gross' => $this->getWeightGross(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'length' => $this->getLength(),
            'is_adult' => $this->getIsAdult(),
            'uom' => $this->getUom(),
            'tariffing_volume' => $this->getTariffingVolume(),
            'order_step' => $this->getOrderStep(),
            'order_minvol' => $this->getOrderMinVol(),
            'is_migrated' => $this->getIsMigrated(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'brand' => BrandsResource::make($this->whenNotNull($this->getBrand())),
            'categories' => CategoriesResource::collection($this->whenNotNull($this->getCategories())),
            'images' => ImagesResource::collection($this->whenNotNull($this->getImages())),
            'product_property_values' => ProductPropertyValuesResource::collection($this->whenNotNull($this->getProductPropertyValues())),
            'nameplates' => NameplatesResource::collection($this->whenNotNull($this->getNameplates())),
            'product_group' => ProductGroupsResource::make($this->whenNotNull($this->getProductGroup())),
            'offers' => OffersResource::collection($this->whenNotNull($this->getOffers())),
        ];
    }
}
