<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Reviews;

use App\Domain\Catalog\Data\Reviews\ReviewData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ReviewData
 */
class ReviewsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->review->getId(),

            'grade' => $this->review->getGrade(),
            'comment' => $this->review->getComment(),
            'status_id' => $this->review->getStatusId(),

            'created_at' => $this->dateTimeToIso($this->review->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->review->getUpdatedAt()),

            'customer' => CustomersResource::make($this->whenNotNull($this->customer)),
            'product' => ProductsResource::make($this->whenNotNull($this->product)),
        ];
    }
}
