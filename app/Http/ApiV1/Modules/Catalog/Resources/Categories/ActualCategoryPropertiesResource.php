<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Categories;

use App\Http\ApiV1\Modules\Catalog\Resources\Properties\ProductPropertiesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ActualCategoryProperty;

/**
 * @mixin ActualCategoryProperty
 */
class ActualCategoryPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'category_id' => $this->getCategoryId(),
            'property_id' => $this->getPropertyId(),
            'is_required' => $this->getIsRequired(),
            'is_gluing' => $this->getIsGluing(),
            'is_inherited' => $this->getIsInherited(),
            'is_common' => $this->getIsCommon(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'category' => CategoriesResource::make($this->whenNotNull($this->getCategory())),
            'property' => ProductPropertiesResource::make($this->whenNotNull($this->getProperty())),
        ];
    }
}
