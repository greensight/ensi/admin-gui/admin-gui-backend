<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Offers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OffersClient\Dto\Offer;

/**
 * @mixin Offer
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'product_id' => $this->getProductId(),
            'price' => $this->getPrice(),
            'allow_publish' => $this->getAllowPublish(),
            'is_active' => $this->getIsActive(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'stocks' => StocksResource::collection($this->whenNotNull($this->getStocks())),
        ];
    }
}
