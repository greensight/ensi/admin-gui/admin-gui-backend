<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Offers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OffersClient\Dto\Stock;

/**
 * @mixin Stock
 */
class StocksResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'store_id' => $this->getStoreId(),
            'offer_id' => $this->getOfferId(),
            'qty' => $this->getQty(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
