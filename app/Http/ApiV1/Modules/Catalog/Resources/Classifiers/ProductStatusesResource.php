<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\StatusSetting;
use Ensi\PimClient\Dto\StatusSettingProperties;

/** @mixin StatusSetting */
class ProductStatusesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'color' => $this->getColor(),
            'type' => $this->getType(),
            'is_active' => $this->getIsActive(),
            'is_publication' => $this->getIsPublication(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'events' => ProductStatusEventsResource::make($this->whenNotNull($this->getEvents())),

            'previous_statuses' => ProductStatusPropertiesResource::collection($this->whenNotNull($this->getPreviousStatuses())),
            $this->mergeWhen(filled($this->getPreviousStatuses()), fn () => $this->getPreviousStatusIds()),

            'next_statuses' => ProductStatusPropertiesResource::collection($this->whenNotNull($this->getNextStatuses())),
            $this->mergeWhen(filled($this->getNextStatuses()), fn () => $this->getNextStatusIds()),
        ];
    }

    private function getPreviousStatusIds(): array
    {
        return $this->mapStatusName($this->getPreviousStatuses(), 'previous_status_ids');
    }

    private function getNextStatusIds(): array
    {
        return $this->mapStatusName($this->getNextStatuses(), 'next_status_ids');
    }

    private function mapStatusName(array $statuses, string $key): array
    {
        $names = collect($statuses)->map(fn (StatusSettingProperties $status) => $status->getName());

        return [
            $key => join(', ', $names->all()),
        ];
    }
}
