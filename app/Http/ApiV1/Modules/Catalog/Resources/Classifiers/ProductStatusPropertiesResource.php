<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\StatusSettingProperties;

/** @mixin StatusSettingProperties */
class ProductStatusPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'name' => $this->getName(),
            'color' => $this->getColor(),
            'type' => $this->getType(),
            'is_active' => $this->getIsActive(),
            'is_publication' => $this->getIsPublication(),
            'events' => ProductStatusEventsResource::make($this->whenNotNull($this->getEvents())),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
