<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\EventConditions;

/** @mixin EventConditions */
class ProductStatusEventsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'events' => $this->getEvents(),
            'operation' => $this->getOperation(),
        ];
    }
}
