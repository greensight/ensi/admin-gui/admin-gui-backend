<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Classifiers;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\PimClient\Dto\StatusSetting;

/** @mixin StatusSetting */
class ProductStatusEnumValuesResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
