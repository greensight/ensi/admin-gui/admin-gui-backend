<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Domain\Catalog\Data\Products\MassPatchProductsResult;
use App\Domain\Catalog\Data\Products\ProductMassOperationResult;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin MassPatchProductsResult */
class MassPatchProductsResultResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'products_result' => $this->unless(
                is_null($this->productsResult),
                fn () => ProductMassOperationResult::createFromModel($this->productsResult),
                ProductMassOperationResult::createEmpty()
            ),
        ];
    }
}
