<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\PimClient\Dto\Product;

/**
 * @mixin Product
 */
class ProductEnumValuesResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
