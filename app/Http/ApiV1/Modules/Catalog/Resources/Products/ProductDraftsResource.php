<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

class ProductDraftsResource extends BaseProductsResource
{
    protected function extraFields(): array
    {
        return [
            'main_image_file' => $this->getMainImageFile(),
        ];
    }

    public function getMainImageFile(): ?string
    {
        if (blank($this->getImages())) {
            return null;
        }

        $mainImage = collect($this->getImages())->sortBy('sort')->first();

        return $mainImage->getFile()?->getUrl();
    }
}
