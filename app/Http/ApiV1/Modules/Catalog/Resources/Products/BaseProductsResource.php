<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Products;

use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\BrandsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\ProductGroups\ProductGroupsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\ProductDraft;

/**
 * @mixin ProductDraft
 * @mixin Product
 */
abstract class BaseProductsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return array_merge([
            'id' => $this->getId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'external_id' => $this->getExternalId(),
            'category_ids' => $this->getCategoryIds(),
            'brand_id' => $this->getBrandId(),
            'status_id' => $this->getStatusId(),
            'status_comment' => $this->getStatusComment(),

            'code' => $this->getCode(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'type' => $this->getType(),
            'allow_publish' => $this->getAllowPublish(),
            'vendor_code' => $this->getVendorCode(),
            'barcode' => $this->getBarcode(),

            'weight' => $this->getWeight(),
            'weight_gross' => $this->getWeightGross(),
            'length' => $this->getLength(),
            'height' => $this->getHeight(),
            'width' => $this->getWidth(),
            'is_adult' => $this->getIsAdult(),

            'uom' => $this->getUom(),
            'tariffing_volume' => $this->getTariffingVolume(),
            'order_step' => $this->getOrderStep(),
            'order_minvol' => $this->getOrderMinvol(),
            'picking_weight_deviation' => $this->getPickingWeightDeviation(),

            'no_filled_required_attributes' => $this->whenNotNull($this->getNoFilledRequiredAttributes()),
            'categories' => CategoriesResource::collection($this->whenNotNull($this->getCategories())),
            'brand' => new BrandsResource($this->whenNotNull($this->getBrand())),
            'images' => ProductImagesResource::collection($this->whenNotNull($this->getImages())),
            'attributes' => ProductAttributeValuesResource::collection($this->whenNotNull($this->getAttributes())),
            'product_groups' => ProductGroupsResource::collection($this->whenNotNull($this->getProductGroups())),
        ], $this->extraFields());
    }

    protected function extraFields(): array
    {
        return [];
    }
}
