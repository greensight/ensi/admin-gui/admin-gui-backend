<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Imports;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductImport;

/** @mixin ProductImport */
class ProductImportsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'type' => $this->getType(),
            'status' => $this->getStatus(),
            'file' => $this->getFile()->getUrl(),
            'file_name' => basename($this->getFile()->getUrl()),
            'chunks_count' => $this->getChunksCount(),
            'chunks_finished' => $this->getChunksFinished(),

            'warnings' => ProductImportWarningsResource::collection(
                $this->whenNotNull($this->getWarnings())
            ),
        ];
    }
}
