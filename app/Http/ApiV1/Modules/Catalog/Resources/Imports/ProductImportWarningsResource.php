<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Imports;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductImportWarning;

/** @mixin ProductImportWarning */
class ProductImportWarningsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'import_id' => $this->getImportId(),
            'vendor_code' => $this->getVendorCode(),
            'import_type' => $this->getImportType(),
            'message' => $this->getMessage(),

            'import' => new ProductImportsResource(
                $this->whenNotNull($this->getImport())
            ),
        ];
    }
}
