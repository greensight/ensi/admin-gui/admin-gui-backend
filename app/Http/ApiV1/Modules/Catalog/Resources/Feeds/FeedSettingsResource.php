<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Feeds;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\FeedClient\Dto\FeedSettings;

/** @mixin FeedSettings */
class FeedSettingsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'active' => $this->getActive(),
            'type' => $this->getType(),
            'platform' => $this->getPlatform(),
            'active_product' => $this->getActiveProduct(),
            'active_category' => $this->getActiveCategory(),
            'shop_name' => $this->getShopName(),
            'shop_url' => $this->getShopUrl(),
            'shop_company' => $this->getShopCompany(),
            'update_time' => $this->getUpdateTime(),
            'delete_time' => $this->getDeleteTime(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'feeds' => FeedsResource::collection($this->whenNotNull($this->getFeeds())),
        ];
    }
}
