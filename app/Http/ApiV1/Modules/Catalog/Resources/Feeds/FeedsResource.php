<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Feeds;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\FeedClient\Dto\Feed;

/** @mixin Feed */
class FeedsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'code' => $this->getCode(),
            'file_url' => $this->fileUrl($this->getFile()),
            'planned_delete_at' => $this->dateTimeToIso($this->getPlannedDeleteAt()),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'feed_settings' => FeedSettingsResource::make($this->whenNotNull($this->getFeedSettings())),
        ];
    }
}
