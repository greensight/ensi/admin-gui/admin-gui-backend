<?php

namespace App\Http\ApiV1\Modules\Catalog\Resources\Feeds;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\FeedClient\Dto\CloudIntegration;

/**
 * @mixin CloudIntegration
 */
class CloudIntegrationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'private_api_key' => $this->getPrivateApiKey(),
            'public_api_key' => $this->getPublicApiKey(),
            'integration' => $this->getIntegration(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
