<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Imports;

use App\Domain\Catalog\Actions\Imports\CreateProductsImportAction;
use App\Domain\Catalog\Actions\Imports\PreloadImportFileAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductImportStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Imports\ProductsImportsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Imports\CreateProductImportRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Imports\PreloadImportFileRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Imports\ProductImportsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\PreloadFileResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductImportsController
{
    public function create(
        CreateProductImportRequest $request,
        CreateProductsImportAction $action
    ): Responsable {
        return new ProductImportsResource($action->execute($request->validated()));
    }

    public function search(ProductsImportsQuery $query): Responsable
    {
        return ProductImportsResource::collectPage($query->get());
    }

    public function meta(ProductImportStatusEnumInfo $statuses): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->detailLink()->resetFilter(),
            Field::string('file_name', 'Название файла')->listDefault()->resetFilter(),
            Field::enum('status', 'Статус импорта', $statuses)->listDefault()->resetFilter(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->sort()->resetFilter(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->sort()->resetFilter(),
        ]);
    }

    public function preloadFile(PreloadImportFileRequest $request, PreloadImportFileAction $action): Responsable
    {
        return new PreloadFileResource(
            $action->execute($request->getFile())
        );
    }
}
