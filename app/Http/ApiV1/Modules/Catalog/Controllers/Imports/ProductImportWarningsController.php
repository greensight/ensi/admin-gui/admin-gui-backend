<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Imports;

use App\Domain\Common\Data\Meta\Enum\Catalog\ProductImportTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Imports\ProductImportWarningsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Imports\ProductImportWarningsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductImportWarningsController
{
    public function search(ProductImportWarningsQuery $query): Responsable
    {
        return ProductImportWarningsResource::collectPage($query->get());
    }

    public function meta(ProductImportTypeEnumInfo $types): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->detailLink()->resetFilter(),
            Field::enum('import_type', 'Тип импорта', $types)->listDefault()->sort()->resetFilter(),
            Field::text('vendor_code', 'Артикул товара')->listDefault()->resetFilter(),
            Field::text('message', 'Сообщение')->listDefault()->sort()->resetFilter(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->sort()->resetFilter(),
        ]);
    }
}
