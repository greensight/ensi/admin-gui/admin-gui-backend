<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\ProductGroupsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\ProductGroupsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductGroupsController
{
    public function search(ProductGroupsQuery $query): Responsable
    {
        return ProductGroupsResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('product_group_id', __('meta.product_groups.product_group_id'))->listDefault()->filterDefault(),
            Field::integer('category_id', __('meta.product_groups.db_category_id'))->listDefault()->filterDefault(),
            Field::text('name', __('meta.name'))->sort()->listDefault()->filterDefault(),
            Field::integer('main_product_id', __('meta.product_groups.db_main_product_id'))->listDefault()->filterDefault(),

            Field::boolean('is_active', __('meta.product_groups.is_active'))->filter()->listDefault(),
            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
