<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\ProductsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\ProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductsController
{
    public function search(ProductsQuery $query): Responsable
    {
        return ProductsResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('product_id', __('meta.products.product_id'))->listDefault()->filterDefault(),
            Field::integer('category_id', __('meta.products.category_id'))->listDefault()->filterDefault(),
            Field::integer('brand_id', __('meta.products.brand_id'))->listDefault()->filterDefault(),
            Field::integer('type', __('meta.products.type'))->filter()->listDefault(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.products.code'))->sort()->filter()->listDefault(),
            Field::text('description', __('meta.products.description'))->listDefault(),
            Field::text('barcode', __('meta.products.barcode_ean'))->filter()->listDefault(),
            Field::text('vendor_code', __('meta.products.vendor_code'))->filter()->listDefault(),

            Field::boolean('allow_publish', __('meta.products.allow_publish'))->filter()->listDefault(),
            Field::boolean('is_adult', __('meta.products.is_adult'))->filter()->listDefault(),

            Field::float('weight', __('meta.products.weight'))->filter()->listDefault(),
            Field::float('weight_gross', __('meta.products.weight_gross'))->filter()->listDefault(),
            Field::float('width', __('meta.products.width'))->filter()->listDefault(),
            Field::float('height', __('meta.products.height'))->filter()->listDefault(),
            Field::float('length', __('meta.products.length'))->filter()->listDefault(),

            Field::integer('uom', __('meta.products.uom'))->listDefault(),
            Field::integer('tariffing_volume', __('meta.products.tariffing_volume'))->listDefault(),
            Field::float('order_step', __('meta.products.order_step'))->listDefault(),
            Field::float('order_minvol', __('meta.products.order_minvol'))->listDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
