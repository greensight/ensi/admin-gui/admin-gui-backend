<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Catalog\Actions\CatalogCache\DeleteIndexerTimestampAction;
use App\Domain\Catalog\Actions\CatalogCache\PatchIndexerTimestampAction;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\IndexerTimestampsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\CatalogCache\PatchCatalogCacheIndexerTimestampRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\IndexerTimestampsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class IndexerTimestampsController
{
    public function search(IndexerTimestampsQuery $query): Responsable
    {
        return IndexerTimestampsResource::collectPage($query->get());
    }

    public function delete(int $id, DeleteIndexerTimestampAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function patch(int $id, PatchCatalogCacheIndexerTimestampRequest $request, PatchIndexerTimestampAction $action): Responsable
    {
        return IndexerTimestampsResource::make($action->execute($id, $request->validated()));
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('index', __('meta.indexer_timestamps.index'))->sort()->listDefault()->filterDefault(),
            Field::text('stage', __('meta.indexer_timestamps.stage'))->listDefault()->filterDefault(),
            Field::text('index_hash', __('meta.indexer_timestamps.index_hash'))->listDefault()->filterDefault(),

            Field::datetime('last_schedule', __('meta.indexer_timestamps.last_schedule'))->resetFilter(),

            Field::boolean('is_current_index', __('meta.indexer_timestamps.is_current_index'))->listDefault()->filterDefault(),
            Field::boolean('is_current_stage', __('meta.indexer_timestamps.is_current_stage'))->listDefault()->filterDefault(),
            Field::boolean('is_current', __('meta.indexer_timestamps.is_current'))->listDefault()->filterDefault(),
        ]);
    }
}
