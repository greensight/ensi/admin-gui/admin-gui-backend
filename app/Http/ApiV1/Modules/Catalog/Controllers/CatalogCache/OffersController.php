<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\OffersQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\OffersResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class OffersController
{
    public function search(OffersQuery $query): Responsable
    {
        return OffersResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('offer_id', __('meta.offers.offer_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.offers.product_id'))->listDefault()->filterDefault(),
            Field::integer('base_price', __('meta.offers.base_price'))->listDefault()->filterDefault()->sort(),
            Field::integer('price', __('meta.offers.price'))->listDefault()->filterDefault()->sort(),
            Field::boolean('is_active', __('meta.offers.is_active'))->filter()->listDefault()->sort(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault()->sort(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
