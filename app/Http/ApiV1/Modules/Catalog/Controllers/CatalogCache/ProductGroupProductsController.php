<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\ProductGroupProductsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\ProductGroupProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductGroupProductsController
{
    public function search(ProductGroupProductsQuery $query): Responsable
    {
        return ProductGroupProductsResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('product_group_product_id', __('meta.product_groups.product_group_product_id'))->listDefault()->filterDefault(),
            Field::integer('product_group_id', __('meta.product_groups.product_group_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.product_groups.product_id'))->listDefault()->filterDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
