<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\NameplateProductsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\NameplateProductsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class NameplateProductsController
{
    public function search(NameplateProductsQuery $query): Responsable
    {
        return NameplateProductsResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('nameplate_product_id', __('meta.nameplates.nameplate_product_id'))->listDefault()->filterDefault(),
            Field::integer('nameplate_id', __('meta.nameplates.nameplate_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.nameplates.product_id'))->listDefault()->filterDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
