<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\NameplatesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\NameplatesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class NameplatesController
{
    public function search(NameplatesQuery $query): Responsable
    {
        return NameplatesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('nameplate_id', __('meta.nameplates.nameplate_id'))->listDefault()->filterDefault(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.code'))->sort()->filter()->listDefault(),
            Field::text('background_color', __('meta.nameplates.background_color'))->filter(),
            Field::text('text_color', __('meta.nameplates.category_id'))->filter(),

            Field::boolean('is_active', __('meta.nameplates.is_active'))->filter()->listDefault(),
            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
