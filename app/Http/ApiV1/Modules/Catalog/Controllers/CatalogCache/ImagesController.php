<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\ImagesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\ImagesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ImagesController
{
    public function search(ImagesQuery $query): Responsable
    {
        return ImagesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('image_id', __('meta.products.image_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.products.product_id'))->listDefault()->filterDefault(),
            Field::integer('directory_value_id', 'meta.products.directory_value_id')->listDefault(),
            Field::text('name', __('meta.products.image_name'))->sort()->filter()->listDefault(),
            Field::text('file', __('meta.products.file'))->filter()->listDefault(),
            Field::text('sort', __('meta.sort'))->sort()->filter()->listDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
