<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\PropertiesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\PropertiesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class PropertiesController
{
    public function search(PropertiesQuery $query): Responsable
    {
        return PropertiesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('property_id', __('meta.properties.property_id'))->listDefault()->filterDefault(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.code'))->sort()->filter()->listDefault(),
            Field::text('type', __('meta.type'))->sort()->filter()->listDefault(),

            Field::boolean('is_public', __('meta.properties.is_public'))->filter()->listDefault(),
            Field::boolean('is_active', __('meta.properties.is_active'))->filter()->listDefault(),
            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
