<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Catalog\Actions\CatalogCache\MigrateEntitiesFromCatalogAction;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class EntitiesController
{
    public function migrate(MigrateEntitiesFromCatalogAction $action): Responsable
    {
        $action->execute();

        return new EmptyResource();
    }
}
