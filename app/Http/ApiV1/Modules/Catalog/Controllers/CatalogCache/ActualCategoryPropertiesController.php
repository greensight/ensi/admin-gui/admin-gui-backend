<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\ActualCategoryPropertiesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\ActualCategoryPropertiesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ActualCategoryPropertiesController
{
    public function search(ActualCategoryPropertiesQuery $query): Responsable
    {
        return ActualCategoryPropertiesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.code'))->sort()->filter()->listDefault(),
            Field::integer('property_id', __('meta.categories.property_id'))->filter(),
            Field::integer('category_id', __('meta.categories.category_id'))->filter(),

            Field::boolean('is_gluing', __('meta.properties.is_gluing'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
