<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\PropertyDirectoryValuesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\PropertyDirectoryValuesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class PropertyDirectoryValuesController
{
    public function search(PropertyDirectoryValuesQuery $query): Responsable
    {
        return PropertyDirectoryValuesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('directory_value_id', __('meta.property_directory_values.nameplate_id'))->listDefault()->filterDefault(),
            Field::integer('property_id', __('meta.property_directory_values.nameplate_id'))->listDefault()->filterDefault(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.code'))->sort()->filter()->listDefault(),
            Field::text('value', __('meta.property_directory_values.value'))->sort()->filter()->listDefault(),
            Field::text('type', __('meta.property_directory_values.type'))->sort()->filter()->listDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->sort()->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
