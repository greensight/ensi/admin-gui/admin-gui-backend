<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\CategoriesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\CategoriesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class CategoriesController
{
    public function search(CategoriesQuery $query): Responsable
    {
        return CategoriesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.code'))->sort()->filter()->listDefault(),
            Field::integer('parent_id', __('meta.categories.parent_id'))->filterDefault()->detailLink(),
            Field::integer('category_id', __('meta.categories.category_id'))->filterDefault()->detailLink(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
