<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\ProductPropertyValuesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\ProductPropertyValuesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductPropertyValuesController
{
    public function search(ProductPropertyValuesQuery $query): Responsable
    {
        return ProductPropertyValuesResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('product_property_value_id', __('meta.products.product_property_value_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.products.product_id'))->listDefault()->filterDefault(),
            Field::integer('property_id', __('meta.products.property_id'))->listDefault()->filterDefault(),
            Field::integer('directory_value_id', __('meta.products.directory_value_id'))->listDefault()->filterDefault(),

            Field::text('name', __('meta.products.value_name'))->sort()->filter()->listDefault(),
            Field::text('value', __('meta.products.value_type'))->sort()->filter()->listDefault(),
            Field::text('type', __('meta.products.type'))->sort()->filter()->listDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
