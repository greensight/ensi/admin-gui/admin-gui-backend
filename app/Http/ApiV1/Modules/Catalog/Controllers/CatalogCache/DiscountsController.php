<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\DiscountsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\DiscountsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class DiscountsController
{
    public function search(DiscountsQuery $query): Responsable
    {
        return DiscountsResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('offer_id', __('meta.offers.offer_id'))->listDefault()->filterDefault(),
            Field::integer('value_type', __('meta.discounts.value_type'))->listDefault()->filterDefault(),
            Field::integer('value', __('meta.discounts.value'))->listDefault()->filterDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
