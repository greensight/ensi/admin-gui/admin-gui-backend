<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\CatalogCache;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\CatalogCache\BrandsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\CatalogCache\BrandsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class BrandsController
{
    public function search(BrandsQuery $query): Responsable
    {
        return BrandsResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', __('meta.name'))->sort()->filter()->listDefault(),
            Field::text('code', __('meta.code'))->listDefault()->filterDefault(),
            Field::text('description', __('meta.brands.description'))->listDefault()->filterDefault(),
            Field::integer('brand_id', __('meta.brands.brand_id'))->listDefault()->filterDefault(),

            Field::boolean('is_migrated', __('meta.is_migrated'))->filter()->listDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.updated_at'))->resetFilter(),
        ]);
    }
}
