<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Feeds;

use App\Domain\Catalog\Actions\Feeds\CreateFeedSettingsAction;
use App\Domain\Catalog\Actions\Feeds\PatchFeedSettingsAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\FeedPlatformEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\FeedTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Feeds\FeedSettingsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Feeds\CreateFeedSettingsRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Feeds\PatchFeedSettingsRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Feeds\FeedSettingsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class FeedSettingsController
{
    public function create(CreateFeedSettingsRequest $request, CreateFeedSettingsAction $action): Responsable
    {
        return FeedSettingsResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchFeedSettingsRequest $request, PatchFeedSettingsAction $action): Responsable
    {
        return FeedSettingsResource::make($action->execute($id, $request->validated()));
    }

    public function get(int $id, FeedSettingsQuery $query): Responsable
    {
        return FeedSettingsResource::make($query->find($id));
    }

    public function search(FeedSettingsQuery $query): Responsable
    {
        return FeedSettingsResource::collectPage($query->get());
    }

    public function meta(
        FeedTypeEnumInfo $feedType,
        FeedPlatformEnumInfo $feedPlatform,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Наименование шаблона')->listDefault()->filterDefault()->sort(),
            Field::text('code', 'Код')->listDefault()->filterDefault()->sort(),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault()->sort(),

            Field::enum('type', 'Тип', $feedType)->sort(),
            Field::enum('platform', 'Платформа', $feedPlatform)->sort(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
