<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Feeds;

use App\Domain\Catalog\Actions\Feeds\MigrateEntitiesFromCatalogAction;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class EntitiesController
{
    public function migrate(MigrateEntitiesFromCatalogAction $action): Responsable
    {
        $action->execute();

        return new EmptyResource();
    }
}
