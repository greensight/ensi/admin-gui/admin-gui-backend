<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Feeds;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Feeds\FeedsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Feeds\FeedsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class FeedsController
{
    public function get(int $id, FeedsQuery $query): Responsable
    {
        return FeedsResource::make($query->find($id));
    }

    public function search(FeedsQuery $query): Responsable
    {
        return FeedsResource::collectPage($query->get());
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('code', 'Код')->listDefault(),
            Field::url('file_url', 'Файл фида')->listDefault(),

            Field::datetime('planned_delete_at', 'Плановая дата удаления')->listDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
