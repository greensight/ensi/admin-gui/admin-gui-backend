<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Feeds;

use App\Domain\Catalog\Actions\Feeds\CreateCloudIntegrationAction;
use App\Domain\Catalog\Actions\Feeds\GetCloudIntegrationAction;
use App\Domain\Catalog\Actions\Feeds\PatchCloudIntegrationAction;
use App\Http\ApiV1\Modules\Catalog\Requests\Feeds\CreateCloudIntegrationRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Feeds\PatchCloudIntegrationRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Feeds\CloudIntegrationsResource;
use Illuminate\Contracts\Support\Responsable;

class CloudIntegrationsController
{
    public function get(GetCloudIntegrationAction $action): Responsable
    {
        return new CloudIntegrationsResource($action->execute());
    }

    public function create(CreateCloudIntegrationRequest $request, CreateCloudIntegrationAction $action): Responsable
    {
        return CloudIntegrationsResource::make($action->execute($request->validated()));
    }

    public function patch(PatchCloudIntegrationRequest $request, PatchCloudIntegrationAction $action): Responsable
    {
        return CloudIntegrationsResource::make($action->execute($request->validated()));
    }
}
