<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\ProductEvents;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\ProductEvents\ProductEventsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\ProductEvents\ProductEventsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ProductEventsController
{
    public function search(ProductEventsQuery $query): Responsable
    {
        return ProductEventsResource::collectPage($query->get());
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('event_id', __('meta.product_events.event_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.product_events.product_id'))->listDefault()->filterDefault(),
            Field::datetime('created_at', __('meta.created_at'))->listDefault()->filterDefault(),
            Field::datetime('updated_at', __('meta.created_at'))->listDefault()->filterDefault(),
        ]);
    }
}
