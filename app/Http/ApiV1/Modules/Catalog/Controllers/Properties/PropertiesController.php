<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Properties;

use App\Domain\Catalog\Actions\Properties\CreateProductPropertyAction;
use App\Domain\Catalog\Actions\Properties\DeleteProductPropertyAction;
use App\Domain\Catalog\Actions\Properties\PatchProductPropertyAction;
use App\Domain\Catalog\Actions\Properties\ReplaceProductPropertyAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\PropertyTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Properties\ProductPropertiesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\CreatePropertyRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\PatchPropertyRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Properties\ReplacePropertyRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Properties\ProductPropertiesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PropertiesController
{
    public function create(
        CreatePropertyRequest $request,
        CreateProductPropertyAction $action
    ): ProductPropertiesResource {
        return new ProductPropertiesResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplacePropertyRequest $request,
        ReplaceProductPropertyAction $action
    ): ProductPropertiesResource {
        return new ProductPropertiesResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchPropertyRequest $request,
        PatchProductPropertyAction $action
    ): ProductPropertiesResource {
        return new ProductPropertiesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductPropertyAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(ProductPropertiesQuery $query): AnonymousResourceCollection
    {
        return ProductPropertiesResource::collectPage($query->get());
    }

    public function get(int $id, ProductPropertiesQuery $query): ProductPropertiesResource
    {
        return new ProductPropertiesResource($query->find($id));
    }

    public function meta(PropertyTypeEnumInfo $types): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Рабочее название')->listDefault()->filterDefault()->resetSort(),
            Field::text('display_name', 'Публичное название')->listDefault()->filterDefault()->resetSort(),
            Field::text('code', 'Код')->listDefault()->filterDefault()->resetSort(),
            Field::enum('type', 'Тип данных', $types)->listDefault()->filterDefault()->resetSort(),
            Field::boolean('is_active', 'Активный')->listDefault()->filterDefault()->resetSort(),
            Field::boolean('is_public', 'Выводить на витрине')->listDefault()->filterDefault()->resetSort(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
            Field::boolean('is_filterable', 'Фильтр на витрине')->resetSort(),
            Field::boolean('is_multiple', 'Множественный')->resetSort(),
            Field::boolean('has_directory', 'Справочник')->resetSort(),
        ]);
    }
}
