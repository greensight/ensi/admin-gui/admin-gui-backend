<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Classifiers;

use App\Domain\Catalog\Actions\Classifiers\ProductStatuses\CreateProductStatusAction;
use App\Domain\Catalog\Actions\Classifiers\ProductStatuses\DeleteProductStatusAction;
use App\Domain\Catalog\Actions\Classifiers\ProductStatuses\GetNextProductStatusesAction;
use App\Domain\Catalog\Actions\Classifiers\ProductStatuses\PatchProductStatusAction;
use App\Domain\Catalog\Actions\Classifiers\ProductStatuses\SetPreviousProductStatusesAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductStatusTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Classifiers\ProductStatusesQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\CreateProductStatusRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\NextProductStatusesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\PatchProductStatusRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Classifiers\SetPreviousProductStatusesRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ProductStatusEnumValuesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ProductStatusesDataResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Classifiers\ProductStatusesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductStatusesController
{
    public function get(int $id, ProductStatusesQuery $query): Responsable
    {
        return ProductStatusesResource::make($query->find($id));
    }

    public function search(ProductStatusesQuery $query): Responsable
    {
        return ProductStatusesResource::collectPage($query->get());
    }

    public function searchEnumValues(ProductStatusesQuery $query): AnonymousResourceCollection
    {
        return ProductStatusEnumValuesResource::collection($query->searchEnums());
    }

    public function create(CreateProductStatusRequest $request, CreateProductStatusAction $action): Responsable
    {
        return ProductStatusesResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchProductStatusRequest $request, PatchProductStatusAction $action): Responsable
    {
        return ProductStatusesResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteProductStatusAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function getNext(NextProductStatusesRequest $request, GetNextProductStatusesAction $action): Responsable
    {
        return ProductStatusesDataResource::collection($action->execute($request->getStatusId()));
    }

    public function setPrevious(int $id, SetPreviousProductStatusesRequest $request, SetPreviousProductStatusesAction $action): Responsable
    {
        return ProductStatusesResource::make($action->execute($id, $request->getIds()));
    }

    public function meta(ProductStatusTypeEnumInfo $productStatusType): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Наименование')->sort()->filterDefault()->listDefault(),
            Field::text('code', 'Код')->sort()->filterDefault()->listDefault(),
            Field::string('color', 'Цвет')->listDefault(),
            Field::enum('type', 'Тип', $productStatusType)->listDefault(),
            Field::boolean('is_active', 'Активна')->resetSort()->listDefault(),

            Field::string('previous_status_ids', 'Переход со статусов')
                ->include('previous_statuses')
                ->listDefault(),
            Field::string('next_status_ids', 'Переход в статусы')
                ->include('next_statuses'),

            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
