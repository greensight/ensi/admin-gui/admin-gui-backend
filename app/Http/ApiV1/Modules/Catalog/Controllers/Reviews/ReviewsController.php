<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Reviews;

use App\Domain\Catalog\Actions\Reviews\DeleteManyReviewsAction;
use App\Domain\Catalog\Actions\Reviews\DeleteReviewAction;
use App\Domain\Catalog\Actions\Reviews\PatchReviewAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\ReviewStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Reviews\ReviewsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Reviews\PatchReviewRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Reviews\ReviewsResource;
use App\Http\ApiV1\Support\Requests\MassDeleteRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReviewsController
{
    public function get(int $id, ReviewsQuery $query): ReviewsResource
    {
        return new ReviewsResource($query->find($id));
    }

    public function delete(int $id, DeleteReviewAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function patch(int $id, PatchReviewRequest $request, PatchReviewAction $action): ReviewsResource
    {
        return new ReviewsResource($action->execute($id, $request->validated()));
    }

    public function search(ReviewsQuery $query): AnonymousResourceCollection
    {
        return ReviewsResource::collectPage($query->get());
    }

    public function massDelete(MassDeleteRequest $request, DeleteManyReviewsAction $action): EmptyResource
    {
        $action->execute($request->getIds());

        return new EmptyResource();
    }

    /**
     * @throws Exception
     */
    public function meta(ReviewStatusEnumInfo $statuses): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('grade', 'Оценка (от 1 до 5)')
                ->filterMany()
                ->filterDefault()
                ->listDefault(),
            Field::enum('status_id', 'Статус публикации отзыва', $statuses)
                ->filterDefault()
                ->listDefault(),
            Field::text('comment', 'Комментарий')
                ->listDefault(),
            Field::datetime('created_at', 'Дата создания')
                ->sortDefault(direction: 'desc')
                ->filterDefault()
                ->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')
                ->filterDefault()
                ->listDefault(),
        ]);
    }

    public function customerMeta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('last_name', 'Фамилия')->listDefault()->filterDefault(),
            Field::text('first_name', 'Имя')->listDefault()->filterDefault(),
            Field::text('middle_name', 'Отчество')->listDefault()->filterDefault(),
            Field::text('full_name', 'Полное ФИО')->listDefault()->filterDefault(),
        ]);
    }

    public function productMeta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Название товара')->listDefault()->filterDefault(),
            Field::text('vendor_code', 'Артикул')->listDefault()->filterDefault(),
        ]);
    }
}
