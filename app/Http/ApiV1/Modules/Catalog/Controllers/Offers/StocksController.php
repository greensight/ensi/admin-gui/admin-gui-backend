<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Offers;

use App\Domain\Catalog\Actions\Offers\PatchStockAction;
use App\Domain\Common\Data\Meta\Enum\Units\StoreEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Offers\StocksQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Offers\PatchStockRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Offers\StocksResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class StocksController
{
    public function get(int $id, StocksQuery $query): Responsable
    {
        return StocksResource::make($query->find($id));
    }

    public function patch(int $id, PatchStockRequest $request, PatchStockAction $action): Responsable
    {
        return StocksResource::make($action->execute($id, $request->validated()));
    }

    public function search(StocksQuery $query): Responsable
    {
        return StocksResource::collectPage($query->get());
    }

    public function meta(StoreEnumInfo $storeEnumInfo): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::enum('store_id', 'Название магазина', $storeEnumInfo)->listDefault()->filterDefault(),
            Field::float('qty', 'Остаток')->listDefault()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
