<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Offers;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Offers\FailedJobsQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Offers\FailedJobsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class FailedJobsController
{
    public function search(FailedJobsQuery $query): Responsable
    {
        return FailedJobsResource::collectPage($query->get());
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->detailLink(),
            Field::keyword('uuid', __('meta.failed_jobs.uuid')),

            Field::keyword('connection', __('meta.failed_jobs.connection'))->listDefault()->filterDefault()->sort(),
            Field::keyword('queue', __('meta.failed_jobs.queue'))->listDefault()->filterDefault()->sort(),
            Field::text('payload', __('meta.failed_jobs.payload'))->filterDefault(),
            Field::text('exception', __('meta.failed_jobs.exception'))->filterDefault(),

            Field::datetime('failed_at', __('meta.failed_jobs.failed_at'))->listDefault()->filterDefault(),
        ]);
    }
}
