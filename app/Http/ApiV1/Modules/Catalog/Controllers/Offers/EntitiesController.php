<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Offers;

use App\Domain\Catalog\Actions\Offers\SyncOfferEntitiesAction;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class EntitiesController
{
    public function sync(SyncOfferEntitiesAction $action): Responsable
    {
        $action->execute();

        return new EmptyResource();
    }
}
