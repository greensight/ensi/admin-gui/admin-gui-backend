<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Offers;

use App\Domain\Catalog\Actions\Offers\MigrateEntitiesFromCatalogAction;
use App\Domain\Catalog\Actions\Offers\PatchOfferAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductByNameEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Offers\OffersQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Offers\PatchOfferRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Offers\OffersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class OffersController
{
    public function get(int $id, OffersQuery $query): Responsable
    {
        return OffersResource::make($query->find($id));
    }

    public function patch(int $id, PatchOfferRequest $request, PatchOfferAction $action): Responsable
    {
        return OffersResource::make($action->execute($id, $request->validated()));
    }

    public function search(OffersQuery $query): Responsable
    {
        return OffersResource::collectPage($query->get());
    }

    public function migrate(MigrateEntitiesFromCatalogAction $action): Responsable
    {
        $action->execute();

        return new EmptyResource();
    }

    public function meta(
        ProductByNameEnumInfo $productByNameEnumInfo,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::boolean('is_active', 'Активность')->listDefault()->filterDefault(),
            Field::boolean('allow_publish', 'Публикация разрешена')->listDefault()->filterDefault(),
            Field::enum('product_id', 'Имя товара', $productByNameEnumInfo)->filterDefault(),
            Field::price('price', 'Цена')->listDefault()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
