<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\FeedClient\Dto\FeedPlatformEnum;
use Ensi\FeedClient\Dto\FeedTypeEnum;
use Ensi\PimClient\Dto\EventOperationEnum;
use Ensi\PimClient\Dto\ProductEventEnum;
use Ensi\PimClient\Dto\ProductStatusTypeEnum;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;
use Illuminate\Contracts\Support\Responsable;

class EnumsController
{
    public function productTypes(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ProductTypeEnum::getDescriptions()));
    }

    public function propertyTypes(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(PropertyTypeEnum::getDescriptions()));
    }

    public function reviewStatuses(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ReviewStatusEnum::getDescriptions()));
    }

    public function productStatusType(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ProductStatusTypeEnum::getDescriptions()));
    }

    public function productEvents(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ProductEventEnum::getDescriptions()));
    }

    public function productEventOperations(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(EventOperationEnum::getDescriptions()));
    }

    public function feedTypes(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(FeedTypeEnum::getDescriptions()));
    }

    public function feedPlatforms(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(FeedPlatformEnum::getDescriptions()));
    }

    public function productTariffingVolumes(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ProductTariffingVolumeEnum::getDescriptions()));
    }

    public function productUom(): Responsable
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ProductUomEnum::getDescriptions()));
    }
}
