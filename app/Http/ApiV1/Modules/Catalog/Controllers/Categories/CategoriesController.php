<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Categories;

use App\Domain\Catalog\Actions\Categories\ActualizeCategoriesAction;
use App\Domain\Catalog\Actions\Categories\BindCategoryPropertiesAction;
use App\Domain\Catalog\Actions\Categories\CreateCategoryAction;
use App\Domain\Catalog\Actions\Categories\DeleteCategoryAction;
use App\Domain\Catalog\Actions\Categories\ReplaceCategoryAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\PropertyTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Categories\CategoriesQuery;
use App\Http\ApiV1\Modules\Catalog\Queries\Categories\CategoriesTreeQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Categories\BindCategoryPropertiesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Categories\CreateCategoryRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Categories\ReplaceCategoryRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoriesTreeResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\CategoryEnumValuesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoriesController
{
    public function create(CreateCategoryRequest $request, CreateCategoryAction $action): CategoriesResource
    {
        return new CategoriesResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceCategoryRequest $request,
        ReplaceCategoryAction $action
    ): CategoriesResource {
        return new CategoriesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteCategoryAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function search(CategoriesQuery $query): AnonymousResourceCollection
    {
        return CategoriesResource::collectPage($query->get());
    }

    public function searchEnumValues(CategoriesQuery $query): AnonymousResourceCollection
    {
        return CategoryEnumValuesResource::collection($query->searchEnums());
    }

    public function get(int $id, CategoriesQuery $query): CategoriesResource
    {
        return new CategoriesResource($query->find($id));
    }

    public function tree(CategoriesTreeQuery $query): AnonymousResourceCollection
    {
        return CategoriesTreeResource::collection($query->get());
    }

    public function bindProperties(
        int $id,
        BindCategoryPropertiesRequest $request,
        BindCategoryPropertiesAction $action
    ): CategoriesResource {
        return new CategoriesResource($action->execute($id, $request->validated()));
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Наименование')->sort()->filterDefault()->listDefault(),
            Field::text('code', 'Код')->listDefault(),
            Field::integer('parent_id', 'ID родителя')->filter(),

            Field::boolean('is_active', 'Активна')->listDefault(),
            Field::boolean('is_real_active', 'Фактически активна')->filter(),
            Field::boolean('is_inherits_properties', 'Наследует атрибуты')->resetFilter(),
            Field::boolean('has_is_gluing', 'Есть атрибут для склейки')->listHide()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->resetFilter(),
            Field::datetime('updated_at', 'Дата обновления')->resetFilter(),
        ]);
    }

    public function propertiesMeta(PropertyTypeEnumInfo $types): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Рабочее название')->listDefault()->filterDefault()->resetSort(),
            Field::text('display_name', 'Публичное название')->listDefault()->filterDefault()->resetSort(),
            Field::text('code', 'Код')->listDefault()->filterDefault()->resetSort(),
            Field::enum('type', 'Тип данных', $types)->listDefault()->filterDefault()->resetSort(),
            Field::boolean('is_active', 'Активный')->listDefault()->filterDefault()->resetSort(),
            Field::boolean('is_public', 'Выводить на витрине')->listDefault()->filterDefault()->resetSort(),
            Field::boolean('is_required', 'Обязательность')->listDefault()->filterDefault()->resetSort(),
            Field::boolean('is_gluing', 'Параметр склеивания')->listDefault()->filterDefault()->resetSort(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
            Field::boolean('is_filterable', 'Фильтр на витрине')->resetSort(),
            Field::boolean('is_multiple', 'Множественный')->resetSort(),
            Field::boolean('has_directory', 'Справочник')->resetSort(),
        ]);
    }

    public function actualize(ActualizeCategoriesAction $action): EmptyResource
    {
        $action->execute();

        return new EmptyResource();
    }
}
