<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Categories;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Categories\ActualCategoryPropertiesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\Categories\ActualCategoryPropertiesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class ActualCategoryPropertiesController
{
    public function search(ActualCategoryPropertiesQuery $query): Responsable
    {
        return ActualCategoryPropertiesResource::collectPage($query->get());
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('category_id', __('meta.actual_category_properties.category_id'))->listDefault()->filterDefault(),
            Field::integer('property_id', __('meta.actual_category_properties.property_id'))->listDefault()->filterDefault(),
            Field::boolean('is_required', __('meta.actual_category_properties.is_required'))->listDefault()->filterDefault(),
            Field::boolean('is_gluing', __('meta.actual_category_properties.is_gluing'))->listDefault()->filterDefault(),
            Field::boolean('is_inherited', __('meta.actual_category_properties.is_inherited'))->listDefault()->filterDefault(),
            Field::boolean('is_common', __('meta.actual_category_properties.is_common'))->listDefault()->filterDefault(),

            Field::datetime('created_at', __('meta.created_at'))->resetFilter(),
            Field::datetime('updated_at', __('meta.created_at'))->resetFilter(),
        ]);
    }
}
