<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\PimCommon;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\PimCommon\TempFilesQuery;
use App\Http\ApiV1\Modules\Catalog\Resources\PimCommon\TempFilesResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class TempFilesController
{
    public function search(TempFilesQuery $query): Responsable
    {
        return TempFilesResource::collectPage($query->get());
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('path', __('meta.temp_file_path'))->listDefault()->filterDefault(),
            Field::datetime('created_at', __('meta.created_at'))->listDefault()->filterDefault(),
        ]);
    }
}
