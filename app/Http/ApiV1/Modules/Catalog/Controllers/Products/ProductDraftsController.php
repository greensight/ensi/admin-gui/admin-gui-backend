<?php

namespace App\Http\ApiV1\Modules\Catalog\Controllers\Products;

use App\Domain\Catalog\Actions\Products\CreateProductAction;
use App\Domain\Catalog\Actions\Products\DeleteProductAction;
use App\Domain\Catalog\Actions\Products\DeleteProductAttributesAction;
use App\Domain\Catalog\Actions\Products\DeleteProductImageAction;
use App\Domain\Catalog\Actions\Products\GetProductsCommonAttributesAction;
use App\Domain\Catalog\Actions\Products\MassPatchProductsAction;
use App\Domain\Catalog\Actions\Products\MassPatchProductsByQueryAction;
use App\Domain\Catalog\Actions\Products\PatchAttributeValuesAction;
use App\Domain\Catalog\Actions\Products\PatchProductAction;
use App\Domain\Catalog\Actions\Products\PatchProductImagesAction;
use App\Domain\Catalog\Actions\Products\PreloadProductImageAction;
use App\Domain\Catalog\Actions\Products\ReplaceAttributeValuesAction;
use App\Domain\Catalog\Actions\Products\ReplaceProductAction;
use App\Domain\Catalog\Actions\Products\ReplaceProductImagesAction;
use App\Domain\Catalog\Actions\Products\UploadProductImageAction;
use App\Domain\Common\Data\Meta\Enum\Catalog\BrandEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\CategoryEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Catalog\ProductTypeEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Catalog\Queries\Products\ProductDraftsQuery;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\CreateProductRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\DeleteProductAttributesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\DeleteProductImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\GetProductsCommonAttributesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\MassPatchProductsByQueryRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\MassPatchProductsRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchImagesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchProductAttributesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PatchProductRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\PreloadImageRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\ReplaceImagesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\ReplaceProductAttributesRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\ReplaceProductRequest;
use App\Http\ApiV1\Modules\Catalog\Requests\Products\UploadProductImageRequest;
use App\Http\ApiV1\Modules\Catalog\Resources\PreloadFileResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\MassPatchProductsResultResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductAttributeValuesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductDraftEnumValuesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductDraftsResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductImagesResource;
use App\Http\ApiV1\Modules\Catalog\Resources\Properties\ProductPropertiesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\PimClient\ApiException;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductDraftsController
{
    public function create(CreateProductRequest $request, CreateProductAction $action): ProductDraftsResource
    {
        return new ProductDraftsResource($action->execute($request->validated()));
    }

    public function replace(
        int $id,
        ReplaceProductRequest $request,
        ReplaceProductAction $action
    ): ProductDraftsResource {
        return new ProductDraftsResource($action->execute($id, $request->validated()));
    }

    public function patch(
        int $id,
        PatchProductRequest $request,
        PatchProductAction $action
    ): ProductDraftsResource {
        return new ProductDraftsResource($action->execute($id, $request->validated()));
    }

    public function massPatch(
        MassPatchProductsRequest $request,
        MassPatchProductsAction $action
    ): MassPatchProductsResultResource {
        return new MassPatchProductsResultResource(
            $action->execute($request->getIds(), $request->getFields(), $request->getAttributes())
        );
    }

    public function massPatchByQuery(
        MassPatchProductsByQueryRequest $request,
        MassPatchProductsByQueryAction $action
    ): EmptyResource {
        $action->execute($request->getFilter(), $request->getFields(), $request->getAttributes());

        return new EmptyResource();
    }

    public function delete(int $id, DeleteProductAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, ProductDraftsQuery $query): ProductDraftsResource
    {
        return new ProductDraftsResource($query->find($id));
    }

    public function search(ProductDraftsQuery $query): AnonymousResourceCollection
    {
        return ProductDraftsResource::collectPage($query->get());
    }

    public function meta(
        CategoryEnumInfo $categoryEnumInfo,
        ProductTypeEnumInfo $productTypeEnumInfo,
        BrandEnumInfo $brandEnumInfo,
        ProductStatusEnumInfo $productStatusEnumInfo,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::keyword('name', 'Наименование')->listDefault()->sort(),
            Field::string('barcode', 'Штрихкод')->listDefault()->filter()->filterDefault(),
            Field::string('code', 'Код товара')->listDefault()->filter(),
            Field::photo('main_image_file', 'Изображение')->listDefault()->include('images'),
            Field::string('vendor_code', 'Артикул')->listDefault()->filter()->filterDefault(),
            Field::enum('type', 'Тип товара', $productTypeEnumInfo)->listDefault()->filter(),
            Field::enum('status_id', 'Статус товара', $productStatusEnumInfo)->listDefault(),
            Field::boolean('is_adult', 'Товар 18+')->listDefault()->resetSort(),
            Field::boolean('allow_publish', 'Публикация разрешена')->listDefault()->filter()->resetSort(),
            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),

            Field::enum('category_ids', 'Категории', $categoryEnumInfo)->filter('category_id')->filterDefault(),
            Field::enum('brand_id', 'Бренд', $brandEnumInfo)->filter()->filterDefault(),
            Field::string('external_id', 'Внешний код товара')->filter(),
            Field::float('weight', 'Масса нетто, кг')->resetFilter()->resetSort(),
            Field::float('weight_gross', 'Масса брутто, кг')->resetFilter()->resetSort(),
            Field::float('length', 'Длина, мм')->resetFilter()->resetSort(),
            Field::float('width', 'Ширина, мм')->resetFilter()->resetSort(),
            Field::float('height', 'Высота, мм')->resetFilter()->resetSort(),
            Field::boolean(
                'has_no_filled_required_attributes',
                'Есть ли у товара хотя бы один незаполненный обязательный атрибут'
            )->listHide()->resetSort(),
        ]);
    }

    public function searchEnumValues(ProductDraftsQuery $query): AnonymousResourceCollection
    {
        return ProductDraftEnumValuesResource::collection($query->searchEnums());
    }

    public function replaceAttributes(
        int $id,
        ReplaceProductAttributesRequest $request,
        ReplaceAttributeValuesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributeValuesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function patchAttributes(
        int $id,
        PatchProductAttributesRequest $request,
        PatchAttributeValuesAction $action
    ): AnonymousResourceCollection {
        return ProductAttributeValuesResource::collection($action->execute($id, $request->attributeValues()));
    }

    public function commonAttributes(
        GetProductsCommonAttributesRequest $request,
        GetProductsCommonAttributesAction $action
    ): AnonymousResourceCollection {
        return ProductPropertiesResource::collection($action->execute($request->getFilter()));
    }

    public function replaceImages(
        int $id,
        ReplaceImagesRequest $request,
        ReplaceProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function patchImages(
        int $id,
        PatchImagesRequest $request,
        PatchProductImagesAction $action
    ): AnonymousResourceCollection {
        return ProductImagesResource::collection($action->execute($id, $request->images()));
    }

    public function deleteAttributes(
        int $id,
        DeleteProductAttributesRequest $request,
        DeleteProductAttributesAction $action
    ): Responsable {
        $action->execute($id, $request->getPropertyIds());

        return new EmptyResource();
    }

    public function preloadImage(PreloadImageRequest $request, PreloadProductImageAction $action): PreloadFileResource
    {
        return new PreloadFileResource($action->execute($request->image()));
    }

    /**
     * @throws ApiException
     */
    public function uploadImage(
        int $id,
        UploadProductImageRequest $request,
        UploadProductImageAction $action
    ): ProductImagesResource {
        return new ProductImagesResource($action->execute($id, $request->validated()));
    }

    /**
     * @throws ApiException
     */
    public function deleteImage(
        int $id,
        DeleteProductImageRequest $request,
        DeleteProductImageAction $action
    ): EmptyResource {
        $action->execute($id, $request->getFileId());

        return new EmptyResource();
    }
}
