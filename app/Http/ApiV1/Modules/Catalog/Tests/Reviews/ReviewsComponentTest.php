<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Catalog\Tests\Factories\Review\ReviewFactory;
use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\PatchReviewRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\LaravelTestFactories\PromiseFactory;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\ReviewsClient\ApiException;
use Ensi\ReviewsClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('GET /api/v1/catalog/reviews/{id} 200', function () {
    $id = 1;

    $this->mockReviewsReviewsApi()->allows([
        'getReview' => ReviewFactory::new()->makeResponse(['id' => $id]),
    ]);

    getJson("/api/v1/catalog/reviews/{$id}")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'grade', 'status_id', 'comment', 'created_at', 'updated_at']]);
});

test('GET /api/v1/catalog/reviews/{id} include all 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $customerId = 2;
    $productId = 3;
    $customerRequestApi = null;
    $productRequestApi = null;

    $this->mockReviewsReviewsApi()->allows([
        'getReview' => ReviewFactory::new()->makeResponse(['id' => $id, 'customer_id' => $customerId, 'product_id' => $productId]),
    ]);

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomersAsync')
        ->withArgs(function (SearchCustomersRequest $request) use (&$customerRequestApi) {
            $customerRequestApi = $request;

            return true;
        })
        ->andReturn(PromiseFactory::make(CustomerFactory::new()->makeResponseSearch([['id' => $customerId]])));

    $this->mockPimProductsApi()
        ->shouldReceive('searchProductsAsync')
        ->withArgs(function (SearchProductsRequest $request) use (&$productRequestApi) {
            $productRequestApi = $request;

            return true;
        })
        ->andReturn(PromiseFactory::make(ProductFactory::new()->makeResponseSearch([['id' => $productId]])));


    getJson("/api/v1/catalog/reviews/{$id}?include=products,customers")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonPath('data.customer.id', $customerId)
        ->assertJsonPath('data.product.id', $productId)
        ->assertJsonStructure(['data' => ['id', 'grade', 'status_id', 'comment', 'customer', 'product', 'created_at', 'updated_at']]);

    expect($customerRequestApi->getFilter()->id)->toBe([$customerId])
        ->and($productRequestApi->getFilter()->id)->toBe([$productId]);
});

test('GET /api/v1/catalog/reviews/{id} 404', function () {
    $this->mockReviewsReviewsApi()
        ->shouldReceive('getReview')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/catalog/reviews/404')
        ->assertNotFound();
});

test('DELETE /api/v1/catalog/reviews/{id} 200', function () {
    $this->mockReviewsReviewsApi()->allows([
        'deleteReview' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/reviews/404')
        ->assertOk();
});

test('PATCH /api/v1/catalog/reviews/{id} 200', function () {
    $id = 1;

    $this->mockReviewsReviewsApi()->allows([
        'patchReview' => ReviewFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchReviewRequestFactory::new()->make();

    patchJson("/api/v1/catalog/reviews/$id", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'grade', 'status_id', 'comment', 'created_at', 'updated_at']])
        ->assertJsonPath('data.id', $id);
});

test('PATCH /api/v1/catalog/reviews/{id} 400', function () {
    $id = 1;

    $this->mockReviewsReviewsApi()->allows([
        'patchReview' => ReviewFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchReviewRequestFactory::new()->make(['status_id' => ReviewStatusEnum::NEW]);

    patchJson("/api/v1/catalog/reviews/$id", $request)
        ->assertBadRequest();
});

test('POST /api/v1/catalog/reviews:search 200', function () {
    $reviewId = 1;
    $productId = 1;
    $customerId = 1;

    $customer = CustomerFactory::new()->makeResponseSearch(['id' => $customerId]);
    $this->mockCustomersCustomersApi()->allows([
        'searchCustomers' => $customer,
    ]);
    $product = ProductFactory::new()->makeResponseSearch(['id' => $productId]);
    $this->mockPimProductsApi()->allows([
        'searchProducts' => $product,
    ]);
    $this->mockReviewsReviewsApi()->allows([
        'searchReviews' => ReviewFactory::new()
            ->makeResponseSearch(['id' => $reviewId], 2),
    ]);

    $request = [
        'filter' => ['product_id' => $productId],
    ];

    $response = postJson('/api/v1/catalog/reviews:search', $request);
    $response
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'status_id', 'grade', 'comment', 'created_at', 'updated_at']]]);
});

test('POST /api/v1/catalog/reviews:mass-delete 200', function () {
    $reviews = ReviewFactory::new()->makeSeveral(5);
    $this->mockReviewsReviewsApi()->allows([
        'massDeleteReviews' => new EmptyDataResponse(),
    ]);

    postJson('/api/v1/catalog/reviews:mass-delete', ['ids' => $reviews->pluck('id')])
        ->assertOk();
});

test('POST /api/v1/catalog/reviews:mass-delete 400', function () {
    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/catalog/reviews:mass-delete')
        ->assertBadRequest();
});

test('GET /api/v1/catalog/reviews:meta success', function () {
    getJson('/api/v1/catalog/reviews:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('GET /api/v1/catalog/reviews/review-customer:meta success', function () {
    getJson('/api/v1/catalog/reviews/review-customer:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('GET /api/v1/catalog/reviews/review-product:meta success', function () {
    getJson('/api/v1/catalog/reviews/review-product:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
