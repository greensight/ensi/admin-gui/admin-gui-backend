<?php

use App\Domain\Catalog\Tests\Factories\Offers\OfferFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\OfferRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\OffersClient\Dto\EmptyDataResponse;
use Ensi\OffersClient\Dto\PatchOfferRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'offers');


test('GET /api/v1/catalog/offers/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_DETAIL_READ]);

    $offerId = 1;

    $this->mockOffersOffersApi()
        ->shouldReceive('getOffer')
        ->once()
        ->andReturn(OfferFactory::new()->makeResponse(['id' => $offerId]));

    getJson("/api/v1/catalog/offers/{$offerId}")
        ->assertOk()
        ->assertJsonPath('data.id', $offerId)
        ->assertJsonStructure(['data' => ['id', 'product_id', 'price']]);
});

test('POST /api/v1/catalog/offers:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_LIST_READ]);

    $offerId = 1;
    $count = 2;

    $this->mockOffersOffersApi()
        ->shouldReceive('searchOffers')
        ->once()
        ->andReturn(
            OfferFactory::new()
                ->makeResponseSearch([
                    ['id' => $offerId],
                    ['id' => $offerId + 1],
                ], $count)
        );

    $request = [
        'filter' => ['id' => [$offerId]],
    ];

    postJson("/api/v1/catalog/offers:search", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'product_id', 'price']]])
        ->assertJsonPath('data.0.id', $offerId);
});

test('POST /api/v1/catalog/offers:search include all 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_LIST_READ]);

    $offerId = 1;

    $this->mockOffersOffersApi()
        ->shouldReceive('searchOffers')
        ->once()
        ->andReturn(
            OfferFactory::new()
                ->withStock()
                ->makeResponseSearch([['id' => $offerId]])
        );

    $request = [
        'filter' => ['id' => [$offerId]],
        'include' => ['stocks'],
    ];

    postJson("/api/v1/catalog/offers:search", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'product_id', 'price']]])
        ->assertJsonPath('data.0.id', $offerId)
        ->assertJsonCount(1, 'data.0.stocks');

});

test('GET /api/v1/catalog/offers:meta 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_LIST_READ]);

    getJson('/api/v1/catalog/offers:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('PATCH /api/v1/catalog/offers/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_DETAIL_MAIN_EDIT]);

    $offerId = 1;
    $this->mockOffersOffersApi()
        ->shouldReceive('patchOffer')
        ->once()
        ->withArgs(fn (int $id, PatchOfferRequest $offer) => $id == $offerId)
        ->andReturn(OfferFactory::new()->makeResponse(['id' => $offerId]));

    $request = OfferRequestFactory::new()->make();

    patchJson("/api/v1/catalog/offers/{$offerId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $offerId)
        ->assertJsonStructure(['data' => ['id', 'product_id', 'price']]);
});

test('POST /api/v1/catalog/offers:migrate 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_MIGRATION]);

    $this->mockOffersOffersApi()
        ->shouldReceive('migrateOffers')
        ->once()
        ->andReturn(new EmptyDataResponse());

    postJson('/api/v1/catalog/offers:migrate')
        ->assertStatus(200);
});

test('POST /api/v1/catalog/offers:migrate 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/offers:migrate')
        ->assertStatus(403);
});
