<?php


use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\OffersClient\Dto\EmptyDataResponse;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component');

test('POST /api/v1/catalog/offers/entities:sync 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_ENTITIES_SYNC]);

    $this->mockOffersCommonApi()
        ->shouldReceive('syncEntities')
        ->once()
        ->andReturn(new EmptyDataResponse());

    postJson('/api/v1/catalog/offers/entities:sync')
        ->assertStatus(200);
});

test('POST /api/v1/catalog/offers/entities:sync 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/offers/entities:sync')
        ->assertStatus(403);
});
