<?php

use App\Domain\Catalog\Tests\Factories\Offers\StockFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\StockRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;

use Ensi\OffersClient\Dto\PatchStockRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'stocks');


test('GET /api/v1/catalog/stocks/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_DETAIL_READ]);

    $stockId = 1;

    $this->mockOffersStocksApi()
        ->shouldReceive('getStock')
        ->once()
        ->andReturn(StockFactory::new()->makeResponse(['id' => $stockId]));

    getJson("/api/v1/catalog/stocks/{$stockId}")
        ->assertOk()
        ->assertJsonPath('data.id', $stockId)
        ->assertJsonStructure(['data' => ['id', 'qty', 'offer_id']]);
});

test('POST /api/v1/catalog/stocks:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_DETAIL_READ]);

    $stockId = 1;
    $count = 2;

    $this->mockOffersStocksApi()
        ->shouldReceive('searchStocks')
        ->once()
        ->andReturn(
            StockFactory::new()
                ->makeResponseSearch([
                    ['id' => $stockId],
                    ['id' => $stockId + 1],
                ], $count)
        );

    $request = [
        'filter' => ['id' => [$stockId]],
    ];

    postJson("/api/v1/catalog/stocks:search", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'qty', 'offer_id']]])
        ->assertJsonPath('data.0.id', $stockId);
});

test('GET /api/v1/catalog/stocks:meta 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_DETAIL_READ]);

    getJson('/api/v1/catalog/stocks:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('PATCH /api/v1/catalog/stocks/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::OFFER_DETAIL_STOCKS_EDIT]);

    $stockId = 1;
    $this->mockOffersStocksApi()
        ->shouldReceive('patchStock')
        ->withArgs(fn (int $id, PatchStockRequest $stock) => $id == $stockId)
        ->andReturn(StockFactory::new()->makeResponse(['id' => $stockId]));

    $request = StockRequestFactory::new()->make();

    patchJson("/api/v1/catalog/stocks/{$stockId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $stockId)
        ->assertJsonStructure(['data' => ['id', 'qty', 'offer_id']]);
});
