<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\FeedClient\Dto\FeedPlatformEnum;
use Ensi\FeedClient\Dto\FeedTypeEnum;
use Ensi\PimClient\Dto\EventOperationEnum;
use Ensi\PimClient\Dto\ProductEventEnum;
use Ensi\PimClient\Dto\ProductStatusTypeEnum;
use Ensi\PimClient\Dto\ProductTariffingVolumeEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;

use function Pest\Laravel\getJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('GET /api/v1/catalog/products/product-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ProductTypeEnum::PIECE,
        'name' => ProductTypeEnum::getDescriptions()[ProductTypeEnum::PIECE],
    ];

    getJson('/api/v1/catalog/products/product-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/properties/properties-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => PropertyTypeEnum::STRING,
        'name' => PropertyTypeEnum::getDescriptions()[PropertyTypeEnum::STRING],
    ];

    getJson('/api/v1/catalog/properties/properties-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/reviews/reviews-statuses 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ReviewStatusEnum::NEW,
        'name' => ReviewStatusEnum::getDescriptions()[ReviewStatusEnum::NEW],
    ];

    getJson('/api/v1/catalog/reviews/review-statuses')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/product-status-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ProductStatusTypeEnum::AUTOMATIC,
        'name' => ProductStatusTypeEnum::getDescriptions()[ProductStatusTypeEnum::AUTOMATIC],
    ];

    getJson('/api/v1/catalog/product-status-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/feed-types 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => FeedTypeEnum::YML,
        'name' => FeedTypeEnum::getDescriptions()[FeedTypeEnum::YML],
    ];

    getJson('/api/v1/catalog/feed-types')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/feed-platforms 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => FeedPlatformEnum::YANDEX_MARKET,
        'name' => FeedPlatformEnum::getDescriptions()[FeedPlatformEnum::YANDEX_MARKET],
    ];

    getJson('/api/v1/catalog/feed-platforms')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/products/product-tariffing-volumes 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ProductTariffingVolumeEnum::PRICE_PER_KILOGRAM,
        'name' => ProductTariffingVolumeEnum::getDescriptions()[ProductTariffingVolumeEnum::PRICE_PER_KILOGRAM],
    ];

    getJson('/api/v1/catalog/products/product-tariffing-volumes')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/products/product-uom 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ProductUomEnum::KILOGRAM,
        'name' => ProductUomEnum::getDescriptions()[ProductUomEnum::KILOGRAM],
    ];

    getJson('/api/v1/catalog/products/product-uom')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/product-events 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => ProductEventEnum::PRODUCT_CREATED,
        'name' => ProductEventEnum::getDescriptions()[ProductEventEnum::PRODUCT_CREATED],
    ];

    getJson('/api/v1/catalog/product-events')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});

test('GET /api/v1/catalog/product-event-operations 200', function () {
    /** @var ApiV1ComponentTestCase $this */

    $item = [
        'id' => EventOperationEnum::AND,
        'name' => EventOperationEnum::getDescriptions()[EventOperationEnum::AND],
    ];

    getJson('/api/v1/catalog/product-event-operations')
        ->assertStatus(200)
        ->assertJsonFragment($item);
});
