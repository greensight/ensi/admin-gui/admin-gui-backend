<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductImageFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductImageRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\EmptyDataResponse;

use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('PUT /api/v1/catalog/products/{id}/images success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductImageRequestFactory::new()->makeRequest(3);

    $this->mockPimProductsApi()->allows([
        'replaceProductImages' => ProductImageFactory::new()->makeResponseSeveral(3),
    ]);

    putJson('/api/v1/catalog/products/114/images', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['name', 'sort']]]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id}/images success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductImageRequestFactory::new()->makeRequest(3);

    $this->mockPimProductsApi()->allows([
        'patchProductImages' => ProductImageFactory::new()->makeResponseSeveral(3),
    ]);

    patchJson('/api/v1/catalog/products/165/images', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['name', 'sort']]]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products/{id}:upload-image success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductImageRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'uploadProductImage' => ProductImageFactory::new()->makeResponse(),
    ]);

    postJson('/api/v1/catalog/products/123:upload-image', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['name', 'sort']]);
})->with(FakerProvider::$optionalDataset);


test('POST /api/v1/catalog/products/{id}:delete-image success', function () {
    $this->mockPimProductsApi()->allows([
        'deleteProductImage' => new EmptyDataResponse(),
    ]);

    postJson('/api/v1/catalog/products/123:delete-image', ['file_id' => 123456])
        ->assertOk()
        ->assertJsonPath('data', null);
});
