<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/products:search success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch([['id' => 38]]),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'main_image_file']]]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products:search with flag required attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 38;

    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()
            ->withNoFilledRequiredAttributes(true)
            ->makeResponseSearch([['id' => $productId]]),
    ]);

    $request = [
        'filter' => ['id' => $productId],
    ];

    postJson('/api/v1/catalog/products:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'no_filled_required_attributes']]])
        ->assertJsonPath('data.0.no_filled_required_attributes', true);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products/{property}/enum-values:search 200', function ($property, $key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimProductsApi()->allows([
        'searchProducts' => ProductFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/catalog/products/{$property}/enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['name', 'query', 'value'],
    ['code', 'query', 'name'],
]);

test('GET /api/v1/catalog/products/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 514;
    $this->mockPimProductsApi()->allows([
        'getProduct' => ProductFactory::new()->makeResponse(['id' => $productId]),
    ]);

    getJson("/api/v1/catalog/products/$productId")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'main_image_file']]);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/products/drafts:meta success', function () {
    getJson('/api/v1/catalog/products/drafts:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
