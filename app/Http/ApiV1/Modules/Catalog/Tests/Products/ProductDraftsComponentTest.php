<?php

use App\Domain\Catalog\Tests\Factories\PreloadFileFactory;
use App\Domain\Catalog\Tests\Factories\Products\MassOperationResultFactory as PimMassOperationResultFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductDraftFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Illuminate\Http\UploadedFile;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/products success', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()->make();
    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->makeResponse(['id' => 10]),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand_id', 'category_ids']])
        ->assertJsonPath('data.id', 10);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products only required', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()->onlyRequired()->make();

    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->makeResponse(['id' => 10]),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'vendor_code', 'category_ids']])
        ->assertJsonPath('data.id', 10);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products with images', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()->withImages(2)->make();

    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->withImages()->makeResponse(['id' => 10]),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['images' => [['id', 'sort', 'name']]]]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/products with attributes', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()->withAttributes(2)->make();

    $this->mockPimProductsApi()->allows([
        'createProduct' => ProductDraftFactory::new()->withAttributes()->makeResponse(['id' => 10]),
    ]);

    postJson('/api/v1/catalog/products', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['attributes' => [['property_id', 'type', 'value', 'name']]]]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id} success', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'patchProduct' => ProductDraftFactory::new()->makeResponse(['id' => 25]),
    ]);

    patchJson('/api/v1/catalog/products/25', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand_id', 'category_ids']])
        ->assertJsonPath('data.id', 25);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id} with attributes and images', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()
        ->withAttributes(2)
        ->withImages(3)
        ->make();

    $this->mockPimProductsApi()->allows([
        'patchProduct' => ProductDraftFactory::new()->makeResponse(['id' => 25]),
    ]);

    patchJson('/api/v1/catalog/products/25', $request)
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/products/{id} success', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()->make();

    $this->mockPimProductsApi()->allows([
        'replaceProduct' => ProductDraftFactory::new()->makeResponse(['id' => 151]),
    ]);

    putJson('/api/v1/catalog/products/25', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'brand_id', 'category_ids']])
        ->assertJsonPath('data.id', 151);
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/products/{id} with attributes and images', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $request = ProductRequestFactory::new()
        ->withAttributes(3)
        ->withImages(2)
        ->make();

    $this->mockPimProductsApi()->allows([
        'replaceProduct' => ProductDraftFactory::new()->makeResponse(['id' => 25]),
    ]);

    putJson('/api/v1/catalog/products/25', $request)
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/catalog/products/{id} success', function (?bool $always) {
    /** @var ApiV1ComponentTestCase $this */
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()->allows(['deleteProduct' => new EmptyDataResponse()]);

    deleteJson('/api/v1/catalog/products/47')
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/products/drafts/{id} success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 201;
    $this->mockPimProductsApi()->allows([
        'getProductDraft' => ProductDraftFactory::new()->makeResponse(['id' => $productId]),
    ]);

    getJson('/api/v1/catalog/products/drafts/201')
        ->assertOk()
        ->assertJsonPath('data.id', 201);
});

test('GET /api/v1/catalog/products/drafts/{id} include brand and categories', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 205;
    $response = ProductDraftFactory::new()
        ->withBrand()
        ->withCategories()
        ->makeResponse(['id' => $productId]);

    $this->mockPimProductsApi()->allows(['getProductDraft' => $response]);

    getJson('/api/v1/catalog/products/drafts/205?include=brand,categories')
        ->assertOk()
        ->assertJsonStructure(['data' => ['brand' => ['id', 'name'], 'categories' => [['id', 'name']]]]);
});

test('GET /api/v1/catalog/products/drafts/{id} include attributes', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId = 205;
    $response = ProductDraftFactory::new()
        ->withAttributes(2)
        ->makeResponse(['id' => $productId]);

    $this->mockPimProductsApi()->allows(['getProductDraft' => $response]);

    getJson('/api/v1/catalog/products/drafts/205?include=brand,categories')
        ->assertOk()
        ->assertJsonStructure(['data' => ['attributes' => [['property_id', 'value']]]]);
});

test('POST /api/v1/catalog/products/drafts:search success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $productId1 = 38;
    $productId2 = 31;

    $this->mockPimProductsApi()->allows([
        'searchProductDrafts' => ProductDraftFactory::new()->makeResponseSearch([
            ['id' => $productId1],
            ['id' => $productId2],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products/drafts:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'category_ids']]])
        ->assertJsonPath('data.0.id', 38);
});

test('POST /api/v1/catalog/products:preload-image success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimProductsApi()->allows([
        'preloadProductImage' => PreloadFileFactory::new()->make(),
    ]);

    $file = UploadedFile::fake()->create('foo.png', kilobytes: 20);

    postFile('/api/v1/catalog/products:preload-image', $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']]);
});

test('POST /api/v1/catalog/products:mass-patch products fields', function () {
    $productId = 1;
    $fieldsToPatch = ProductRequestFactory::new()
        ->withAttributes()
        ->except(['category_ids', 'external_id', 'barcode'])
        ->make();

    $request = [
        'ids' => [$productId],
        'fields' => $fieldsToPatch,
        'attributes' => $fieldsToPatch['attributes'],
    ];

    $this->mockPimProductsApi()
        ->expects('massPatchProducts')
        ->andReturn(
            PimMassOperationResultFactory::new()
                ->withProcessed($productId)
                ->makeResponse()
        );

    postJson('/api/v1/catalog/products:mass-patch', $request)
        ->assertOk()
        ->assertJsonPath('data.products_result.processed', [$productId])
        ->assertJsonCount(0, 'data.products_result.errors');
});

test('POST /api/v1/catalog/products:mass-patch with errors', function () {
    /** @var ApiV1ComponentTestCase $this */
    $errorMessage = 'error';
    $productId = 1;
    $fieldsToPatch = ProductRequestFactory::new()
        ->withAttributes()
        ->except(['category_ids', 'external_id', 'barcode'])
        ->make();

    $request = [
        'ids' => [$productId],
        'fields' => $fieldsToPatch,
        'attributes' => $fieldsToPatch['attributes'],
    ];

    $this->mockPimProductsApi()
        ->expects('massPatchProducts')
        ->andReturn(
            PimMassOperationResultFactory::new()
                ->withError($productId, $errorMessage)
                ->makeResponse()
        );

    postJson('/api/v1/catalog/products:mass-patch', $request)
        ->assertOk()
        ->assertJsonCount(0, 'data.products_result.processed')
        ->assertJsonPath('data.products_result.errors.0.id', $productId)
        ->assertJsonPath('data.products_result.errors.0.message', $errorMessage);
});

test('POST /api/v1/catalog/products:mass-patch-by-query success', function () {
    /** @var ApiV1ComponentTestCase $this */
    $fieldsToPatch = ProductRequestFactory::new()
        ->withAttributes()
        ->except(['category_ids', 'external_id', 'barcode'])
        ->make();

    $request = [
        'filter' => ['name' => 'foo'],
        'fields' => $fieldsToPatch,
        'attributes' => $fieldsToPatch['attributes'],
    ];

    $this->mockPimProductsApi()->expects('massPatchProductsByQuery');

    postJson('/api/v1/catalog/products:mass-patch-by-query', $request)
        ->assertOk()
        ->assertJsonPath('data', null);
});
