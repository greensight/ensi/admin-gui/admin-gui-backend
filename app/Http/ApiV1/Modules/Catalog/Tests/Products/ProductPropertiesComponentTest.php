<?php

use App\Domain\Catalog\Tests\Factories\Categories\PropertyFactory;
use App\Domain\Catalog\Tests\Factories\Products\ProductPropertyValueFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductPropertyValueRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\DeleteProductAttributesRequest;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Ensi\PimClient\Dto\PatchProductAttributesRequest;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Ensi\PimClient\Dto\ReplaceProductAttributesRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;
use function PHPUnit\Framework\assertArrayHasKey;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

uses(ApiV1ComponentTestCase::class)->group('catalog', 'component');

test('PUT /api/v1/catalog/products/{id}/attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 105;
    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2);

    $this->mockPimProductsApi()
        ->shouldReceive('replaceProductAttributes')
        ->once()
        ->withArgs(function (int $id, ReplaceProductAttributesRequest $attributesRequest) use ($productId, $request) {
            assertEquals($productId, $id);
            assertArrayHasKey('attributes', $attributesRequest->toArray());
            assertEqualsCanonicalizing(data_get($request, 'attributes'), $attributesRequest->getAttributes());

            return true;
        })
        ->andReturn(ProductPropertyValueFactory::new()->makeResponse(2));

    putJson("/api/v1/catalog/products/{$productId}/attributes", $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/products/{id}/attributes type image success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2, ['type' => PropertyTypeEnum::IMAGE]);

    $this->mockPimProductsApi()
        ->shouldReceive('replaceProductAttributes')
        ->once()
        ->andReturn(
            ProductPropertyValueFactory::new()->makeResponse(2, ['type' => PropertyTypeEnum::IMAGE])
        );

    putJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'url', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/products/{id}/attributes type file success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2, ['type' => PropertyTypeEnum::FILE]);

    $this->mockPimProductsApi()
        ->shouldReceive('replaceProductAttributes')
        ->once()
        ->andReturn(
            ProductPropertyValueFactory::new()->makeResponse(2, ['type' => PropertyTypeEnum::FILE])
        );

    putJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'url', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id}/attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $productId = 105;
    $request = ProductPropertyValueRequestFactory::new()->makeRequest(2);

    $this->mockPimProductsApi()
        ->shouldReceive('patchProductAttributes')
        ->once()
        ->withArgs(function (int $id, PatchProductAttributesRequest $attributesRequest) use ($productId, $request) {
            assertEquals($productId, $id);
            assertArrayHasKey('attributes', $attributesRequest->toArray());
            assertEqualsCanonicalizing(data_get($request, 'attributes'), $attributesRequest->getAttributes());

            return true;
        })
        ->andReturn(ProductPropertyValueFactory::new()->makeResponse(2));

    patchJson("/api/v1/catalog/products/{$productId}/attributes", $request)
        ->assertJsonStructure(['data' => [['property_id', 'value', 'type']]])
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/products/{id}/attributes preload file', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()
        ->shouldReceive('patchProductAttributes')
        ->once()
        ->andReturn(ProductPropertyValueFactory::new()->makeResponse());

    $request = ['attributes' => [
        [
            'property_id' => 105,
            'preload_file_id' => 12540,
        ],
    ]];

    patchJson('/api/v1/catalog/products/105/attributes', $request)
        ->assertOk();
})->with(FakerProvider::$optionalDataset);


test('DELETE /api/v1/catalog/products/{id}/attributes 200', function () {
    $productId = 105;
    $propertyIds = [20, 30];

    $this->mockPimProductsApi()
        ->shouldReceive('deleteProductAttributes')
        ->once()
        ->withArgs(function (int $id, DeleteProductAttributesRequest $attributesRequest) use ($productId, $propertyIds) {
            assertEquals($productId, $id);
            assertEqualsCanonicalizing($propertyIds, $attributesRequest->getPropertyIds());

            return true;
        })
        ->andReturn(new EmptyDataResponse());

    $request = [
        'property_ids' => $propertyIds,
    ];

    deleteJson("/api/v1/catalog/products/{$productId}/attributes", $request)
        ->assertJsonPath('data', null)
        ->assertOk();
});

test('POST /api/v1/catalog/products:common-attributes success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mockPimProductsApi()
        ->shouldReceive('getProductsCommonAttributes')
        ->once()
        ->andReturn(PropertyFactory::new()->withDirectory()->makeResponseCommon());

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/products:common-attributes', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [
            ['id', 'directory' => [['id', 'name', 'code', 'value']]],
        ]]);
})->with(FakerProvider::$optionalDataset);
