<?php

use App\Domain\Catalog\Tests\Factories\Imports\ProductImportWarningFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'product-import-warnings');

test('POST /api/v1/catalog/product-import-warnings:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimImportsApi()
        ->expects('searchProductImportWarnings')
        ->andReturn(ProductImportWarningFactory::new()->makeSearchResponse());

    postJson('/api/v1/catalog/product-import-warnings:search')
        ->assertStatus(200);
});

test('GET /api/v1/catalog/product-import-warnings:meta success', function () {
    getJson('/api/v1/catalog/product-import-warnings:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
