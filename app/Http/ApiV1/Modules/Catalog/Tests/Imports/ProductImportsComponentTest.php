<?php

use App\Domain\Catalog\Tests\Factories\Imports\ProductImportFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\PimClient\Dto\CreateProductImportRequest;
use Ensi\PimClient\Dto\ProductImportTypeEnum;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'product-imports');

test('POST /api/v1/catalog/product-imports 201', function () {
    /** @var ApiV1ComponentTestCase $this */
    /** @var CreateProductImportRequest $apiRequest */
    $this->mockPimImportsApi()
        ->expects('createProductImport')
        ->with(Mockery::capture($apiRequest))
        ->andReturn(ProductImportFactory::new()->makeResponse());

    $type = ProductImportTypeEnum::PRODUCT;
    $fileId = 22;

    postJson('/api/v1/catalog/product-imports', [
        'type' => $type,
        'preload_file_id' => $fileId,
    ])->assertOk();

    assertEquals($type, $apiRequest->getType());
    assertEquals($fileId, $apiRequest->getPreloadFileId());
});

test('POST /api/v1/catalog/product-imports:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockPimImportsApi()
        ->expects('searchProductImports')
        ->andReturn(ProductImportFactory::new()->makeSearchResponse());

    postJson('/api/v1/catalog/product-imports:search')
        ->assertStatus(200);
});

test('GET /api/v1/catalog/product-imports:meta success', function () {
    getJson('/api/v1/catalog/product-imports:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
