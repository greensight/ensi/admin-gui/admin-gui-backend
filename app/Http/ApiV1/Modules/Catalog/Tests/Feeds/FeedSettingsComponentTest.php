<?php


use App\Domain\Catalog\Tests\Factories\Feeds\FeedSettingsFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\FeedSettingsRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog', 'feed-settings');

test('POST /api/v1/catalog/feed-settings 201', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_CREATE]);
    FakerProvider::$optionalAlways = $always;

    $feedSettingsId = 1;

    $this->mockFeedSettingsApi()
        ->shouldReceive('createFeedSettings')
        ->andReturn(FeedSettingsFactory::new()->makeResponse(['id' => $feedSettingsId]));

    $request = FeedSettingsRequestFactory::new()->make();

    postJson('/api/v1/catalog/feed-settings', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $feedSettingsId);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/feed-settings/{id} 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_DETAIL_EDIT]);
    FakerProvider::$optionalAlways = $always;

    $feedSettingsId = 1;

    $this->mockFeedSettingsApi()
        ->shouldReceive('patchFeedSettings')
        ->andReturn(FeedSettingsFactory::new()->makeResponse(['id' => $feedSettingsId]));

    $request = FeedSettingsRequestFactory::new()->make();

    patchJson("/api/v1/catalog/feed-settings/{$feedSettingsId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $feedSettingsId);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/feed-settings/{id} 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_DETAIL_READ]);
    FakerProvider::$optionalAlways = $always;

    $feedSettingsId = 1;

    $this->mockFeedSettingsApi()
        ->shouldReceive('getFeedSettings')
        ->andReturn(FeedSettingsFactory::new()->makeResponse(['id' => $feedSettingsId]));

    getJson("/api/v1/catalog/feed-settings/{$feedSettingsId}")
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'code']]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/feed-settings:search 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_LIST_READ]);
    FakerProvider::$optionalAlways = $always;

    $feedSettingsId = 1;
    $count = 2;

    $this->mockFeedSettingsApi()->allows([
        'searchFeedSettings' => FeedSettingsFactory::new()
            ->withFeeds()
            ->makeResponseSearch([
                ['id' => $feedSettingsId],
                ['id' => $feedSettingsId + 1],
            ], $count),
    ]);

    $request = [
        'filter' => ['id' => [$feedSettingsId]],
        'include' => ['feeds'],
    ];

    postJson('/api/v1/catalog/feed-settings:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'name', 'code', 'feeds']]])
        ->assertJsonPath('data.0.id', $feedSettingsId);

    postJson('/api/v1/catalog/feed-settings:search')
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/feed-settings:meta 200', function () {
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_LIST_READ]);
    getJson('/api/v1/catalog/feed-settings:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
