<?php

use App\Domain\Catalog\Tests\Factories\Feeds\FeedFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog', 'feeds');

test('GET /api/v1/catalog/feeds/{id} 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_DETAIL_READ]);
    FakerProvider::$optionalAlways = $always;

    $feedId = 1;

    $this->mockFeedsApi()
        ->shouldReceive('getFeed')
        ->andReturn(FeedFactory::new()->makeResponse(['id' => $feedId]));

    getJson("/api/v1/catalog/feeds/{$feedId}")
        ->assertOk()
        ->assertJsonPath('data.id', $feedId)
        ->assertJsonStructure(['data' => ['id', 'code', 'file_url']]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/feeds:search include 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_LIST_READ]);
    FakerProvider::$optionalAlways = $always;

    $feedId = 1;
    $count = 2;

    $this->mockFeedsApi()->allows([
        'searchFeeds' => FeedFactory::new()
            ->withFeedSettings()
            ->makeResponseSearch([
                ['id' => $feedId],
                ['id' => $feedId + 1],
            ], $count),
    ]);

    $request = [
        'filter' => ['id' => [$feedId]],
        'include' => ['feed_settings'],
    ];

    postJson('/api/v1/catalog/feeds:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'code', 'file_url', 'feed_settings']]])
        ->assertJsonPath('data.0.id', $feedId);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/catalog/feeds:meta 200', function () {
    $this->setRights([RightsAccessEnum::FEED_SETTINGS_LIST_READ]);
    getJson('/api/v1/catalog/feeds:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
