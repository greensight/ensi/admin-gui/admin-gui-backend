<?php

use App\Domain\Catalog\Tests\Factories\Feeds\CloudIntegrationFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\CloudIntegrationRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog', 'cloud-integration');

test('GET /api/v1/catalog/cloud-integrations 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::CLOUD_INTEGRATION_READ]);
    FakerProvider::$optionalAlways = $always;

    $publicApiKey = "123";

    $this->mockFeedCloudIntegrationApi()
        ->shouldReceive('getCloudIntegration')
        ->andReturn(CloudIntegrationFactory::new()->makeResponse(['public_api_key' => $publicApiKey]));

    getJson("/api/v1/catalog/cloud-integrations")
        ->assertOk()
        ->assertJsonStructure(['data' => ['public_api_key', 'private_api_key', 'integration']]);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/cloud-integrations 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::CLOUD_INTEGRATION_DETAIL_EDIT]);
    FakerProvider::$optionalAlways = $always;

    $publicApiKey = '123';

    $this->mockFeedCloudIntegrationApi()
        ->shouldReceive('createCloudIntegration')
        ->andReturn(CloudIntegrationFactory::new()->makeResponse(['public_api_key' => $publicApiKey]));

    $request = CloudIntegrationRequestFactory::new()->make();

    postJson('/api/v1/catalog/cloud-integrations', $request)
        ->assertOk()
        ->assertJsonPath('data.public_api_key', $publicApiKey);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/cloud-integrations 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::CLOUD_INTEGRATION_DETAIL_EDIT]);
    FakerProvider::$optionalAlways = $always;

    $publicApiKey = '123';

    $this->mockFeedCloudIntegrationApi()
        ->shouldReceive('patchCloudIntegration')
        ->andReturn(CloudIntegrationFactory::new()->makeResponse(['public_api_key' => $publicApiKey]));

    $request = CloudIntegrationRequestFactory::new()->make();

    patchJson("/api/v1/catalog/cloud-integrations", $request)
        ->assertOk()
        ->assertJsonPath('data.public_api_key', $publicApiKey);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/cloud-integrations change only activity 200', function (?bool $always) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::CLOUD_INTEGRATION_ACTIVITY_EDIT]);
    FakerProvider::$optionalAlways = $always;

    $publicApiKey = '123';

    $this->mockFeedCloudIntegrationApi()
        ->shouldReceive('patchCloudIntegration')
        ->andReturn(CloudIntegrationFactory::new()->makeResponse(['public_api_key' => $publicApiKey]));

    patchJson("/api/v1/catalog/cloud-integrations", ['integration' => true])
        ->assertOk()
        ->assertJsonPath('data.public_api_key', $publicApiKey);
})->with(FakerProvider::$optionalDataset);
