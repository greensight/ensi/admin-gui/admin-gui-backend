<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\FeedClient\Dto\EmptyDataResponse;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component');

test('POST /api/v1/catalog/feeds/entities:migrate 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::FEED_ENTITIES_MIGRATION]);

    $this->mockFeedCommonApi()
        ->shouldReceive('migrateEntities')
        ->once()
        ->andReturn(new EmptyDataResponse());

    postJson('/api/v1/catalog/feeds/entities:migrate')
        ->assertStatus(200);
});

test('POST /api/v1/catalog/feeds/entities:migrate 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/feeds/entities:migrate')
        ->assertStatus(403);
});
