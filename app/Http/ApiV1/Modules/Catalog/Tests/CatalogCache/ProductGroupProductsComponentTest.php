<?php

use App\Domain\Catalog\Tests\Factories\CatalogCache\ProductGroupProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog-cache');

test('POST /api/v1/catalog-cache/product-group-products:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;

    $this->mockCatalogCacheProductGroupsApi()->allows([
        'searchProductGroupProducts' => ProductGroupProductFactory::new()->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog-cache/product-group-products:search', ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id);
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/product-group-products:search include success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;

    $this->mockCatalogCacheProductGroupsApi()->allows([
        'searchProductGroupProducts' => ProductGroupProductFactory::new()->withAllRelations()->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog-cache/product-group-products:search',  ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonStructure([
            'data' => [
                '*' => [
                    'product',
                    'product_group',
                ],
            ],
        ]);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/product-group-products:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog-cache/product-group-products:search')
        ->assertStatus(403);
});

test('GET /api/v1/catalog-cache/product-group-products:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog-cache/product-group-products:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/catalog-cache/product-group-products:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog-cache/product-group-products:meta')
        ->assertStatus(403);
});
