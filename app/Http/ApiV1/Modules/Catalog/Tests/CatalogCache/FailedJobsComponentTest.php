<?php

use App\Domain\Catalog\Tests\Factories\CatalogCache\FailedJobFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CatalogCacheClient\Dto\SearchFailedJobsRequest;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog-cache', 'failed-jobs');

test('POST /api/v1/catalog-cache/failed-jobs:search 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $filter = ['id' => 1];
    $sort = ['id'];
    $pagination = PaginationFactory::new()->makeRequestOffset();
    $this->mockCatalogCacheCommonApi()
        ->shouldReceive('searchFailedJobs')
        ->once()
        ->withArgs(function (SearchFailedJobsRequest $request) use ($filter, $sort, $pagination) {
            assertEquals($filter['id'], $request->getFilter()->id);
            assertEqualsCanonicalizing($sort, $request->getSort());
            assertEquals($pagination['limit'], $request->getPagination()->getLimit());
            assertEquals($pagination['offset'], $request->getPagination()->getOffset());

            return true;
        })
        ->andReturn(FailedJobFactory::new()->makeResponseSearch());
    postJson('/api/v1/catalog-cache/failed-jobs:search', [
        'filter' => $filter,
        'sort' => $sort,
        'pagination' => $pagination,
    ])
        ->assertOk();
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/failed-jobs:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog-cache/failed-jobs:search')
        ->assertStatus(403);
});

test('GET /api/v1/catalog-cache/failed-jobs:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog-cache/failed-jobs:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/catalog-cache/failed-jobs:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog-cache/failed-jobs:meta')
        ->assertStatus(403);
});
