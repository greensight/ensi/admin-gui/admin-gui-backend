<?php

use App\Domain\Catalog\Tests\Factories\CatalogCache\NameplateFactory;
use App\Domain\Catalog\Tests\Factories\CatalogCache\NameplateProductFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog-cache');

test('POST /api/v1/catalog-cache/nameplates:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;

    $this->mockCatalogCacheNameplatesApi()->allows([
        'searchNameplates' => NameplateFactory::new()->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog-cache/nameplates:search', ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id);
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/nameplates:search include success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;
    $products = NameplateProductFactory::new()->makeSeveral(1)->toArray();

    $this->mockCatalogCacheNameplatesApi()->allows([
        'searchNameplates' => NameplateFactory::new()->withProducts($products)->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog-cache/nameplates:search',  ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.product_links.0.id', $products[0]->getId());
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/nameplates:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog-cache/nameplates:search')
        ->assertStatus(403);
});

test('GET /api/v1/catalog-cache/nameplates:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog-cache/nameplates:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/catalog-cache/nameplates:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog-cache/nameplates:meta')
        ->assertStatus(403);
});
