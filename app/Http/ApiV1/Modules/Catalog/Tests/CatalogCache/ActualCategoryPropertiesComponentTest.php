<?php

use App\Domain\Catalog\Tests\Factories\CatalogCache\ActualCategoryPropertyFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CatalogCacheClient\ApiException;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog-cache');

test('POST /api/v1/catalog-cache/category-properties:search 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockCatalogCacheCategoriesApi()->allows([
        'searchActualCategoryProperties' => ActualCategoryPropertyFactory::new()
            ->makeResponseSearch([
                ['id' => 10],
                ['id' => 11],
            ]),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog-cache/category-properties:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data');
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/category-properties:search 400', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockCatalogCacheCategoriesApi()
        ->shouldReceive('searchActualCategoryProperties')
        ->andThrowExceptions([new ApiException(code: 400)]);

    postJson('/api/v1/catalog-cache/category-properties:search', [
        "filter" => ["test" => true],
    ])->assertStatus(400);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/category-properties:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog-cache/category-properties:search')
        ->assertStatus(403);
});

test('GET /api/v1/catalog-cache/category-properties:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog-cache/category-properties:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/catalog-cache/category-properties:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog-cache/category-properties:meta')
        ->assertStatus(403);
});
