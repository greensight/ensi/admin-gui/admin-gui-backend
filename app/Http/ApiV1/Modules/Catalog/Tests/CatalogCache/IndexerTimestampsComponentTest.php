<?php

use App\Domain\Catalog\Tests\Factories\CatalogCache\IndexerTimestampFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\IndexerTimestampRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CatalogCacheClient\Dto\EmptyDataResponse;
use Ensi\CatalogCacheClient\Dto\PatchIndexerTimestampRequest;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog-cache');

test('POST /api/v1/catalog-cache/indexer-timestamps:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;

    $this->mockCatalogCacheElasticApi()->allows([
        'searchIndexerTimestamps' => IndexerTimestampFactory::new()->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog-cache/indexer-timestamps:search', ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id);
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog-cache/indexer-timestamps:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog-cache/indexer-timestamps:search')
        ->assertStatus(403);
});

test('PATCH /api/v1/catalog-cache/indexer-timestamps/{id} 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $timestampId = 1;
    $this->mockCatalogCacheElasticApi()
        ->shouldReceive('patchIndexerTimestamp')
        ->once()
        ->withArgs(fn (int $id, PatchIndexerTimestampRequest $last) => $id == $timestampId)
        ->andReturn(IndexerTimestampFactory::new()->makeResponse(['id' => $timestampId]));

    $request = IndexerTimestampRequestFactory::new()->make();

    patchJson("/api/v1/catalog-cache/indexer-timestamps/{$timestampId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $timestampId)
        ->assertJsonStructure(['data' => ['id', 'index', 'stage', 'index_hash', 'last_schedule']]);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('PATCH /api/v1/catalog-cache/indexer-timestamps/{id} 403', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    patchJson('/api/v1/catalog-cache/indexer-timestamps/50')
        ->assertStatus(403);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
]);

test('DELETE /api/v1/catalog-cache/indexer-timestamps/{id} success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockCatalogCacheElasticApi()->allows([
        'deleteIndexerTimestamp' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog-cache/indexer-timestamps/50', [])
        ->assertOk();
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('DELETE /api/v1/catalog-cache/indexer-timestamps/{id} 403', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    deleteJson('/api/v1/catalog-cache/indexer-timestamps/50')
        ->assertStatus(403);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
]);

test('GET /api/v1/catalog-cache/indexer-timestamps:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog-cache/indexer-timestamps:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/catalog-cache/indexer-timestamps:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog-cache/indexer-timestamps:meta')
        ->assertStatus(403);
});
