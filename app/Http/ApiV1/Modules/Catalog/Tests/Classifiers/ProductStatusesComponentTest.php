<?php

use App\Domain\Catalog\Tests\Factories\Classifiers\ProductStatusDataFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\ProductStatusFactory;
use App\Domain\Catalog\Tests\Factories\Classifiers\ProductStatusPropertiesFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\ProductStatusRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Ensi\PimClient\Dto\GetNextStatusRequest;
use Ensi\PimClient\Dto\SearchStatusSettingsRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'catalog', 'product-statuses');

test('POST /api/v1/catalog/product-statuses:next 200', function (?int $currentStatus) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT]);

    $nextStatusId = 2;

    $apiRequest = null;
    $this->mockPimProductStatusesApi()
        ->shouldReceive('nextProductStatuses')
        ->withArgs(function (GetNextStatusRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->once()
        ->andReturn(ProductStatusDataFactory::new()->makeResponse([['id' => $nextStatusId]]));

    postJson("/api/v1/catalog/product-statuses:next", ['id' => $currentStatus])
        ->assertJsonPath('data.0.id', $nextStatusId)
        ->assertOk();

    expect($apiRequest->getId())->toBe($currentStatus);
})->with([
    'without status' => null,
    'with status' => 1,
]);

test('POST /api/v1/catalog/product-statuses:next 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    postJson('/api/v1/catalog/product-statuses:next')
        ->assertForbidden();
});

test('POST /api/v1/catalog/product-statuses:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_LIST_READ]);

    $productStatusId = 1;
    $count = 2;

    $this->mockPimProductStatusesApi()->allows([
        'searchProductStatuses' => ProductStatusFactory::new()
            ->makeResponseSearch([
                ['id' => $productStatusId],
                ['id' => $productStatusId + 1],
            ], $count),
    ]);

    $request = [
        'filter' => ['id' => [$productStatusId]],
    ];

    postJson('/api/v1/catalog/product-statuses:search', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'code', 'name']]])
        ->assertJsonPath('data.0.id', $productStatusId);
});

test('POST /api/v1/catalog/product-statuses:search include all 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_LIST_READ]);

    $productStatusId = 1;
    $previousStatus = ProductStatusPropertiesFactory::new()->make();
    $previousStatusSecond = ProductStatusPropertiesFactory::new()->make();

    $nextStatus = ProductStatusPropertiesFactory::new()->make();

    $this->mockPimProductStatusesApi()->allows([
        'searchProductStatuses' => ProductStatusFactory::new()
            ->withPreviousStatus($previousStatus)
            ->withPreviousStatus($previousStatusSecond)
            ->withNextStatus($nextStatus)
            ->makeResponseSearch([['id' => $productStatusId]]),
    ]);

    $request = [
        'filter' => ['id' => [$productStatusId]],
        'include' => ['previous_statuses', 'next_statuses'],
    ];

    postJson("/api/v1/catalog/product-statuses:search", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['previous_statuses', 'previous_status_ids', 'next_statuses', 'next_status_ids']]])
        ->assertJsonPath('data.0.id', $productStatusId)
        ->assertJsonCount(2, 'data.0.previous_statuses')
        ->assertJsonPath('data.0.previous_statuses.0.id', $previousStatus->getId())
        ->assertJsonPath('data.0.previous_status_ids', "{$previousStatus->getName()}, {$previousStatusSecond->getName()}")
        ->assertJsonCount(1, 'data.0.next_statuses')
        ->assertJsonPath('data.0.next_statuses.0.id', $nextStatus->getId())
        ->assertJsonPath('data.0.next_status_ids', $nextStatus->getName());
});

test('POST /api/v1/catalog/product-statuses:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    postJson('/api/v1/catalog/product-statuses:search')
        ->assertForbidden();
});

test('POST /api/v1/catalog/product-statuses 201', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_CREATE]);

    $request = ProductStatusRequestFactory::new()->make();

    $this->mockPimProductStatusesApi()
        ->shouldReceive('createProductStatus')
        ->andReturn(ProductStatusFactory::new()->makeResponse());

    postJson('/api/v1/catalog/product-statuses', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'code', 'name']]);
});

test('POST /api/v1/catalog/product-statuses 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    $request = ProductStatusRequestFactory::new()->make();
    postJson('/api/v1/catalog/product-statuses', $request)
        ->assertForbidden();
});

test('GET /api/v1/catalog/product-statuses/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_DETAIL_READ]);

    $productStatusId = 1;

    $this->mockPimProductStatusesApi()
        ->shouldReceive('getProductStatus')
        ->andReturn(ProductStatusFactory::new()->withEvent()->makeResponse(['id' => $productStatusId]));

    getJson("/api/v1/catalog/product-statuses/{$productStatusId}")
        ->assertOk()
        ->assertJsonPath('data.id', $productStatusId)
        ->assertJsonStructure(['data' => ['id', 'code', 'name', 'type', 'events' => ['operation', 'events']]]);
});

test('GET /api/v1/catalog/product-statuses/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    getJson('/api/v1/catalog/product-statuses/403')
        ->assertForbidden();
});

test('DELETE /api/v1/catalog/product-statuses/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_DELETE]);

    $productStatusId = 1;

    $this->mockPimProductStatusesApi()
        ->shouldReceive('deleteProductStatus')
        ->andReturn(new EmptyDataResponse());

    deleteJson("/api/v1/catalog/product-statuses/{$productStatusId}")
        ->assertOk();
});

test('DELETE /api/v1/catalog/product-statuses/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    deleteJson('/api/v1/catalog/product-statuses/403')
        ->assertForbidden();
});

test('PATCH /api/v1/catalog/product-statuses/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT]);

    $productStatusId = 1;

    $this->mockPimProductStatusesApi()
        ->shouldReceive('patchProductStatus')
        ->andReturn(ProductStatusFactory::new()->makeResponse(['id' => $productStatusId]));

    $request = ProductStatusRequestFactory::new()->make();

    patchJson("/api/v1/catalog/product-statuses/{$productStatusId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $productStatusId)
        ->assertJsonStructure(['data' => ['id', 'code', 'name']]);
});

test('PATCH /api/v1/catalog/product-statuses/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    $request = ProductStatusRequestFactory::new()->make();
    patchJson('/api/v1/catalog/product-statuses/403', $request)
        ->assertForbidden();
});

test('GET /api/v1/catalog/product-statuses:meta 200', function () {
    getJson('/api/v1/catalog/product-statuses:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/catalog/product-statuses/{id}:set-previous 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT]);

    $productStatusId = 1;

    $this->mockPimProductStatusesApi()
        ->shouldReceive('setPreviousProductStatuses')
        ->andReturn(ProductStatusFactory::new()->makeResponse(['id' => $productStatusId]));

    postJson("/api/v1/catalog/product-statuses/{$productStatusId}:set-previous", ['ids' => [200]])
        ->assertOk()
        ->assertJsonPath('data.id', $productStatusId)
        ->assertJsonStructure(['data' => ['id', 'code', 'name']]);
});

test('POST /api/v1/catalog/product-statuses/{id}:set-previous 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);
    postJson('/api/v1/catalog/product-statuses/403:set-previous', ['ids' => [403]])
        ->assertForbidden();
});

test('POST /api/v1/catalog/product-status-enum-values:search 200', function ($filterCount, $request, $filterKey = null, $filterValue = null) {
    /** @var ApiV1ComponentTestCase $this */

    $apiRequest = null;
    $this->mockPimProductStatusesApi()
        ->shouldReceive('searchProductStatuses')
        ->once()
        ->withArgs(function (SearchStatusSettingsRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(ProductStatusFactory::new()->makeResponseSearch());

    postJson('/api/v1/catalog/product-status-enum-values:search', $request)
        ->assertStatus(200);

    if ($filterKey) {
        assertEquals($filterValue, $apiRequest->getFilter()->{$filterKey});
    }

    assertCount($filterCount, get_object_vars($apiRequest->getFilter()));
})->with([
    [1, ['filter' => ['id' => [1, 2]]], 'id', [1, 2]],
    [1, ['filter' => ['query' => 'foo']], 'name_like', 'foo'],
    [0, []],
]);
