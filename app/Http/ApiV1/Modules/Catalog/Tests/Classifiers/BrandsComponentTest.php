<?php

use App\Domain\Catalog\Tests\Factories\Classifiers\BrandFactory;
use App\Domain\Catalog\Tests\Factories\PreloadFileFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\BrandRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\PimClient\Dto\EmptyDataResponse;
use Illuminate\Http\UploadedFile;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentWithRightsTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/brands success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->setRights([RightsAccessEnum::BRAND_CREATE]);

    $this->mockPimBrandsApi()->allows([
        'createBrand' => BrandFactory::new()->makeResponse(['id' => 10]),
    ]);

    $request = BrandRequestFactory::new()->make();

    postJson('/api/v1/catalog/brands', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'image_url']])
        ->assertJsonPath('data.id', 10);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/brands forbidden', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = BrandRequestFactory::new()->make();

    postJson('/api/v1/catalog/brands', $request)
        ->assertForbidden();
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/brands/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->setRights([RightsAccessEnum::BRAND_DETAIL_EDIT, RightsAccessEnum::BRAND_DETAIL_ACTIVE_EDIT]);

    $brandId = 10;

    $this->mockPimBrandsApi()->allows([
        'patchBrand' => BrandFactory::new()->makeResponse(['id' => $brandId]),
    ]);

    $request = BrandRequestFactory::new()->make();

    patchJson("/api/v1/catalog/brands/$brandId", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'image_url']])
        ->assertJsonPath('data.id', $brandId);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/catalog/brands/{id} forbidden', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $brandId = 10;

    $request = BrandRequestFactory::new()->make();

    patchJson("/api/v1/catalog/brands/$brandId", $request)
        ->assertForbidden();
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/catalog/brands:preload-image success', function () {
    $this->setRights([RightsAccessEnum::BRAND_DETAIL_EDIT]);

    $this->mockPimBrandsApi()->allows([
        'preloadBrandImage' => PreloadFileFactory::new()->make(),
    ]);

    $file = UploadedFile::fake()->create('foo.png', kilobytes: 20);

    postFile('/api/v1/catalog/brands:preload-image', $file)
        ->assertOk()
        ->assertJsonStructure(['data' => ['preload_file_id', 'url']]);
});

test('POST /api/v1/catalog/brands:preload-image forbidden', function () {
    $file = UploadedFile::fake()->create('foo.png', kilobytes: 20);

    postFile('/api/v1/catalog/brands:preload-image', $file)
        ->assertForbidden();
});

test('PUT /api/v1/catalog/brands/{id} success', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->setRights([RightsAccessEnum::BRAND_DETAIL_EDIT, RightsAccessEnum::BRAND_DETAIL_ACTIVE_EDIT]);

    $this->mockPimBrandsApi()->allows([
        'replaceBrand' => BrandFactory::new()->makeResponse(['id' => 15]),
    ]);

    $request = BrandRequestFactory::new()->make();

    putJson('/api/v1/catalog/brands/15', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name', 'code', 'description']])
        ->assertJsonPath('data.id', 15);
})->with(FakerProvider::$optionalDataset);

test('PUT /api/v1/catalog/brands/{id} forbidden', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = BrandRequestFactory::new()->make();

    putJson('/api/v1/catalog/brands/15', $request)
        ->assertForbidden();
})->with(FakerProvider::$optionalDataset);

test('DELETE /api/v1/catalog/brands/{id} success', function () {
    $this->setRights([RightsAccessEnum::BRAND_DELETE]);

    $this->mockPimBrandsApi()->allows(['deleteBrand' => new EmptyDataResponse()]);

    deleteJson('/api/v1/catalog/brands/21')
        ->assertOk();
});

test('DELETE /api/v1/catalog/brands/{id} forbidden', function () {
    deleteJson('/api/v1/catalog/brands/21')
        ->assertForbidden();
});

test('GET /api/v1/catalog/brands/{id} internal logo success', function () {
    $this->setRights([RightsAccessEnum::BRAND_DETAIL_READ]);

    $expectedUrl = 'https://files.ensi.ru/public/catalog/1234.jpg';

    $this->mockPimBrandsApi()->allows([
        'getBrand' => BrandFactory::new()
            ->withFileUrl($expectedUrl)
            ->makeResponse(['id' => 25]),
    ]);

    getJson('/api/v1/catalog/brands/25')
        ->assertOk()
        ->assertJsonPath('data.image_url', $expectedUrl)
        ->assertJsonPath('data.logo_url', null);
});

test('GET /api/v1/catalog/brands/{id} external logo success', function () {
    $this->setRights([RightsAccessEnum::BRAND_DETAIL_READ]);

    $expectedUrl = 'https://ya.ru/images/1234.jpg';

    $this->mockPimBrandsApi()->allows([
        'getBrand' => BrandFactory::new()
            ->withExternalLogo($expectedUrl)
            ->makeResponse(['id' => 27]),
    ]);

    getJson('/api/v1/catalog/brands/27')
        ->assertOk()
        ->assertJsonPath('data.image_url', $expectedUrl)
        ->assertJsonPath('data.logo_url', $expectedUrl);
});

test('GET /api/v1/catalog/brands/{id} forbidden', function () {
    getJson('/api/v1/catalog/brands/27')
        ->assertForbidden();
});

test('POST /api/v1/catalog/brands:search success', function () {
    $this->setRights([RightsAccessEnum::BRAND_LIST_READ]);

    $this->mockPimBrandsApi()->allows([
        'searchBrands' => BrandFactory::new()->makeResponseSearch([
            ['id' => 33],
            ['id' => 34],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/brands:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'name']]])
        ->assertJsonPath('data.0.id', 33);
});

test('POST /api/v1/catalog/brands:search forbidden error', function () {
    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/catalog/brands:search', $request)
        ->assertForbidden();
});

test('GET /api/v1/catalog/brands:meta success', function () {
    getJson('/api/v1/catalog/brands:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
