<?php

use App\Domain\Catalog\Tests\Factories\Categories\ActualCategoryPropertyFactory;
use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Domain\Catalog\Tests\Factories\Categories\PropertyFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'categories');

test('POST /api/v1/catalog/actual-category-properties:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;

    $this->mockPimCategoriesApi()->allows([
        'searchActualCategoryProperties' => ActualCategoryPropertyFactory::new()->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog/actual-category-properties:search', ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id);
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog/actual-category-properties:search include success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $id = 1;
    $category = CategoryFactory::new()->make();
    $property = PropertyFactory::new()->make();

    $this->mockPimCategoriesApi()->allows([
        'searchActualCategoryProperties' => ActualCategoryPropertyFactory::new()->withCategory($category)->withProperty($property)->makeResponseSearch([['id' => $id]]),
    ]);

    postJson('/api/v1/catalog/actual-category-properties:search',  ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $id)
        ->assertJsonPath('data.0.category.id', $category->getId())
        ->assertJsonPath('data.0.property.id', $property->getId());
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog/actual-category-properties:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/actual-category-properties:search')
        ->assertStatus(403);
});

test('GET /api/v1/catalog/actual-category-properties:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog/actual-category-properties:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/catalog/actual-category-properties:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog/actual-category-properties:meta')
        ->assertStatus(403);
});
