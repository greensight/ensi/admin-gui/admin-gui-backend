<?php

use App\Domain\Catalog\Tests\Factories\Categories\CategoriesTreeItemFactory;
use App\Domain\Catalog\Tests\Factories\Categories\CategoryFactory;
use App\Http\ApiV1\Modules\Catalog\Tests\Factories\CategoryRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\PimClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentWithRightsTestCase::class)->group('catalog', 'component');

test('POST /api/v1/catalog/categories success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'createCategory' => CategoryFactory::new()->makeResponse(['id' => 10]),
    ]);
    $request = CategoryRequestFactory::new()->make(['parent_id' => null]);

    postJson('/api/v1/catalog/categories', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 10);
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_CREATE]],
]);

test('POST /api/v1/catalog/categories 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    $request = CategoryRequestFactory::new()->make(['parent_id' => null]);

    postJson('/api/v1/catalog/categories', $request)
        ->assertStatus(403);
});

test('PUT /api/v1/catalog/categories/{id} success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'replaceCategory' => CategoryFactory::new()->makeResponse(['id' => 12]),
    ]);

    $request = CategoryRequestFactory::new()->make();

    putJson('/api/v1/catalog/categories/12', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 12);
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE]],
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_EDIT]],
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ACTIVE_EDIT]],
]);

test('PUT /api/v1/catalog/categories/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    $request = CategoryRequestFactory::new()->make();

    putJson('/api/v1/catalog/categories/2', $request)
        ->assertStatus(403);
});

test('DELETE /api/v1/catalog/categories/{id} success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'deleteCategory' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/catalog/categories/50', [])
        ->assertOk();
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_DELETE]],
]);

test('DELETE /api/v1/catalog/categories/{id} 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    deleteJson('/api/v1/catalog/categories/2')
        ->assertStatus(403);
});

test('GET /api/v1/catalog/categories/{id}?include=properties success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'getCategory' => CategoryFactory::new()->withProperties(2)->makeResponse(['id' => 30]),
    ]);

    getJson('/api/v1/catalog/categories/30?include=properties')
        ->assertOk()
        ->assertJsonPath('data.id', 30)
        ->assertJsonCount(2, 'data.properties')
        ->assertJsonStructure(['data' => ['properties' => [['property_id', 'name', 'type', 'is_system', 'is_moderated']]]]);
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_READ]],
]);

test('GET /api/v1/catalog/categories/{id}?include=properties 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog/categories/1?include=properties')
        ->assertStatus(403);
});

test('POST /api/v1/catalog/categories:search success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'searchCategories' => CategoryFactory::new()
            ->withProperties(2)
            ->makeResponseSearch([
                ['id' => 10],
                ['id' => 11],
            ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
        'include' => ['properties'],
    ];

    postJson('/api/v1/catalog/categories:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonCount(2, 'data.0.properties');
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ]],
]);

test('POST /api/v1/catalog/categories:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/categories:search')
        ->assertStatus(403);
});

test('POST /api/v1/catalog/categories/{id}:bind-properties success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'bindCategoryProperties' => CategoryFactory::new()->withProperties(1)->makeResponse(['id' => 22]),
    ]);

    $request = [
        'replace' => false,
        'properties' => [
            ['id' => 100, 'is_required' => false, 'is_gluing' => false],
        ],
    ];

    postJson('/api/v1/catalog/categories/22:bind-properties', $request)
        ->assertOk()
        ->assertJsonPath('data.id', 22);
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE]],
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ATTRIBUTES_WRITE]],
]);

test('POST /api/v1/catalog/categories/{id}:bind-properties 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/categories/2:bind-properties')
        ->assertStatus(403);
});

test('POST /api/v1/catalog/categories:tree success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->allows([
        'getCategoriesTree' => CategoriesTreeItemFactory::new()
            ->withChildren(2)
            ->depth(2)
            ->makeResponse(2),
    ]);

    $request = [
        'filter' => ['is_active' => true],
    ];

    postJson('/api/v1/catalog/categories:tree', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonCount(2, 'data.0.children');
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ]],
]);

test('POST /api/v1/catalog/categories:tree 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/categories:tree')
        ->assertStatus(403);
});

test('GET /api/v1/catalog/categories:meta success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog/categories:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_LIST_READ]],
]);

test('GET /api/v1/catalog/category-properties:meta success', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog/category-properties:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_READ]],
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE]],
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_EDIT]],
    [[RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ACTIVE_EDIT]],
]);

test('GET /api/v1/catalog/category-properties:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog/category-properties:meta')
        ->assertStatus(403);
});

test('POST /api/v1/catalog/categories:actualize 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockPimCategoriesApi()->shouldReceive(
        'actualizeCategories'
    );

    postJson('/api/v1/catalog/categories:actualize')
        ->assertStatus(200);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/catalog/categories:actualize 403', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    postJson('/api/v1/catalog/categories:actualize')
        ->assertStatus(403);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
]);
