<?php

use App\Domain\Catalog\Tests\Factories\PimCommon\BulkOperationFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'pim-common');

test('POST /api/v1/catalog/bulk-operations:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $processedIds = [1,2,3];
    $this->mockPimCommonApi()->allows([
        'searchBulkOperations' => BulkOperationFactory::new()
            ->withError()
            ->makeResponseSearch([['ids' => $processedIds]]),
    ]);

    $request = [
        'include' => ['errors'],
    ];

    postJson('/api/v1/catalog/bulk-operations:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.ids_string', implode(', ', $processedIds))
        ->assertJsonCount(1, 'data.0.errors');
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH]],
]);

test('POST /api/v1/catalog/bulk-operations:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/catalog/bulk-operations:search')
        ->assertStatus(403);
});

test('GET /api/v1/catalog/bulk-operations:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/catalog/bulk-operations:meta')
        ->assertStatus(200)
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH]],
]);

test('GET /api/v1/catalog/bulk-operations:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/catalog/bulk-operations:meta')
        ->assertStatus(403);
});
