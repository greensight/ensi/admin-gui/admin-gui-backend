<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\ReviewStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchReviewRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'comment' => $this->faker->optional()->text,
            'status_id' => $this->faker->randomElement([ReviewStatusEnum::PUBLISHED, ReviewStatusEnum::DENIED]),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
