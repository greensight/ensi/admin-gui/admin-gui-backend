<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\EventOperationEnum;
use Ensi\PimClient\Dto\ProductEventEnum;
use Ensi\PimClient\Dto\ProductStatusTypeEnum;

class ProductStatusRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $type = $this->faker->randomElement(ProductStatusTypeEnum::getAllowableEnumValues());
        $isAutomatic = $type == ProductStatusTypeEnum::AUTOMATIC;

        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug(),
            'color' => $this->faker->nullable()->hexColor(),
            'type' => $type,
            'is_active' => $this->faker->boolean(),
            'is_publication' => $this->faker->boolean(),
            'events' => $this->faker->nullable($isAutomatic)->exactly([
                'operation' => $this->faker->nullable()->randomElement(EventOperationEnum::getAllowableEnumValues()),
                'events' => $this->faker->randomList(fn () => $this->faker->randomElement(ProductEventEnum::getAllowableEnumValues()), 1, 3),
            ]),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
