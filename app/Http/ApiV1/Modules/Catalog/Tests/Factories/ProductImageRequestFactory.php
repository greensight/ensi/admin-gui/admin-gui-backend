<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\File;

class ProductImageRequestFactory extends BaseApiFactory
{
    public ?string $url = null;
    public ?File $file = null;

    protected function definition(): array
    {
        return [
                'sort' => $this->faker->numberBetween(1, 500),
                'name' => $this->faker->nullable()->sentence(3),
                'preload_file_id' => $this->faker->randomNumber(6),
            ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function makeRequest(int $count = 1, array $extra = []): array
    {
        return ['images' => $this->makeSeveral($count, $extra)->all()];
    }
}
