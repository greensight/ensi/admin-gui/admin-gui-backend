<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LaravelTestFactories\FactoryMissingValue;

class IndexerTimestampRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'last_schedule' => $this->faker->boolean()
                ? new FactoryMissingValue()
                : $this->faker->dateTime()->format('Y-m-d\TH:i:s.u\Z'),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
