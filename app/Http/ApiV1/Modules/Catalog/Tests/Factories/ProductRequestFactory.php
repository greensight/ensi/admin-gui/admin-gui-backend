<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Illuminate\Support\Collection;

class ProductRequestFactory extends BaseApiFactory
{
    public ?Collection $attributes = null;
    public ?Collection $images = null;

    protected function definition(): array
    {
        $type = $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues());
        $isWeigh = $type === ProductTypeEnum::WEIGHT;

        return [
            'external_id' => $this->faker->uuid(),
            'category_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), min: 1),
            'brand_id' => $this->faker->modelId(),
            'status_id' => $this->faker->modelId(),

            'name' => $this->faker->sentence(3),
            'code' => $this->faker->slug(),
            'description' => $this->faker->text(50),
            'type' => $type,
            'allow_publish' => $this->faker->boolean,
            'vendor_code' => $this->faker->numerify('######'),
            'barcode' => $this->faker->ean13(),

            'weight' => $this->faker->randomFloat(4),
            'weight_gross' => $this->faker->randomFloat(4),
            'length' => $this->faker->randomNumber(),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'is_adult' => $this->faker->boolean(),

            'uom' => $this->faker->nullable($isWeigh)->randomElement(ProductUomEnum::getAllowableEnumValues()),
            'order_step' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 1000),
            'order_minvol' => $this->faker->nullable($isWeigh)->randomFloat(2, 1, 100),
            'picking_weight_deviation' => $this->faker->nullable($isWeigh)->randomFloat(2, 0, 100),

            'attributes' => $this->notNull($this->attributes?->all()),
            'images' => $this->notNull($this->images?->all()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withAttributes(int $count = 1, array $extra = []): self
    {
        $attributes = ProductPropertyValueRequestFactory::new()->makeSeveral($count, $extra);

        return $this->immutableSet('attributes', $attributes);
    }

    public function withImages(int $count = 1, array $extra = []): self
    {
        $images = ProductImageRequestFactory::new()->makeSeveral($count, $extra);

        return $this->immutableSet('images', $images);
    }

    public function onlyRequired(): self
    {
        return $this->only(['name', 'vendor_code', 'type', 'category_ids']);
    }
}
