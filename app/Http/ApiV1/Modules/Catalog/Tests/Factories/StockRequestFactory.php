<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class StockRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'qty' => $this->faker->nullable()->randomFloat(0, 1, 99),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
