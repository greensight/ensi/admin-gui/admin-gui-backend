<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class ProductGroupRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'category_id' => $this->faker->modelId(),
            'main_product_id' => $this->faker->boolean ? $this->faker->modelId() : null,
            'is_active' => $this->faker->boolean,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
