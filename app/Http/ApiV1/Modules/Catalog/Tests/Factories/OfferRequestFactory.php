<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class OfferRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
            'allow_publish' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
