<?php

namespace App\Http\ApiV1\Modules\Catalog\Tests\Factories;

use Ensi\FeedClient\Dto\FeedPlatformEnum;
use Ensi\FeedClient\Dto\FeedTypeEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class FeedSettingsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->word(),
            'active' => $this->faker->boolean(),
            'type' => $this->faker->randomElement(FeedTypeEnum::getAllowableEnumValues()),
            'platform' => $this->faker->randomElement(FeedPlatformEnum::getAllowableEnumValues()),
            'active_product' => $this->faker->boolean(),
            'active_category' => $this->faker->boolean(),
            'shop_name' => $this->faker->sentence(),
            'shop_url' => $this->faker->sentence(),
            'shop_company' => $this->faker->sentence(),
            'update_time' => $this->faker->numberBetween(1, 100),
            'delete_time' => $this->faker->numberBetween(1, 100),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
