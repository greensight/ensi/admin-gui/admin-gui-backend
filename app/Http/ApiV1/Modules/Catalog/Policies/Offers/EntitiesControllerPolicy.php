<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Offers;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesAuthorization;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\Response;

class EntitiesControllerPolicy
{
    use HandlesAuthorization;

    public function sync(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::OFFER_ENTITIES_SYNC,
        ]);
    }
}
