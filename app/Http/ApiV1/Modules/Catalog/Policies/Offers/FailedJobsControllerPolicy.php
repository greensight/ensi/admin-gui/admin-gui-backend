<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Offers;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesAuthorization;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\Response;

class FailedJobsControllerPolicy
{
    use HandlesAuthorization;
    use HandlesRules;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::TECHNICAL_TABLES_READ,
            RightsAccessEnum::TECHNICAL_TABLES_ALL,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::TECHNICAL_TABLES_READ,
            RightsAccessEnum::TECHNICAL_TABLES_ALL,
        ]);
    }
}
