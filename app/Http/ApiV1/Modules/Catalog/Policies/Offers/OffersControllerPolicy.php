<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Offers;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesAuthorization;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\Response;

class OffersControllerPolicy
{
    use HandlesAuthorization;
    use HandlesRules;

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::OFFER_LIST_READ,
            RightsAccessEnum::ORDER_DETAIL_PRODUCTS_EDIT,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::OFFER_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::OFFER_LIST_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::OFFER_DETAIL_MAIN_EDIT, RightsAccessEnum::OFFER_DETAIL_EDIT],
            permissionsFields: [
                RightsAccessEnum::OFFER_DETAIL_ALLOW_PUBLISH_EDIT => ['allow_publish'],
                RightsAccessEnum::OFFER_DETAIL_PRICE_EDIT => ['price'],
            ],
        );
    }

    public function migrate(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::OFFER_MIGRATION,
        ]);
    }
}
