<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\ProductGroups;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductGroupsControllerPolicy
{
    use HandlesAuthorization;
    use HandlesRules;

    public function searchOne(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_LIST_READ,
            RightsAccessEnum::PRODUCT_GROUP_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_LIST_READ,
            RightsAccessEnum::PRODUCT_GROUP_DETAIL_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::PRODUCT_GROUP_DETAIL_FULL_EDIT],
            permissionsFields: [
                RightsAccessEnum::PRODUCT_GROUP_DETAIL_FIELDS_EDIT => ['name', 'category_id', 'main_product_id'],
                RightsAccessEnum::PRODUCT_GROUP_DETAIL_ACTIVE_EDIT => ['is_active'],
            ],
        );
    }

    public function bindProducts(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_DETAIL_FIELDS_EDIT,
            RightsAccessEnum::PRODUCT_GROUP_DETAIL_FULL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_DELETE,
        ]);
    }

    public function massDelete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_GROUP_DELETE,
        ]);
    }
}
