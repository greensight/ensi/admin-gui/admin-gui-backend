<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Classifiers;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class BrandsControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_CREATE,
        ]);
    }

    public function replace(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_DETAIL_EDIT,
            RightsAccessEnum::BRAND_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_DETAIL_EDIT,
            RightsAccessEnum::BRAND_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function preloadImage(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_CREATE,
            RightsAccessEnum::BRAND_DETAIL_EDIT,
        ]);
    }

    public function uploadImage(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_DETAIL_EDIT,
        ]);
    }

    public function deleteImage(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_DELETE,
        ]);
    }

    public function searchEnumValues(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::BRAND_LIST_READ,
        ]);
    }
}
