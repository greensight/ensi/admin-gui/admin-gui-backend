<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Classifiers;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductStatusesControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_CREATE,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_DELETE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT,
        ]);
    }

    public function setPrevious(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT,
        ]);
    }

    public function getNext(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_STATUS_DETAIL_EDIT,
        ]);
    }
}
