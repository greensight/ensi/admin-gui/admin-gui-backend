<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\CatalogCache;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EntitiesControllerPolicy
{
    use HandlesAuthorization;

    public function migrate(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CATALOG_CACHE_ENTITIES_MIGRATION,
        ]);
    }
}
