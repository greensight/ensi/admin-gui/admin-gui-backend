<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Feeds;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CloudIntegrationsControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CLOUD_INTEGRATION_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CLOUD_INTEGRATION_DETAIL_EDIT,
        ]);
    }

    public function patch(User $user): Response
    {
        $requestData = request()->input();

        // if only "integration" flag was changes
        if (count($requestData) === 1 && array_key_exists('integration', $requestData)) {
            return $user->allowOneOf([
                RightsAccessEnum::CLOUD_INTEGRATION_DETAIL_EDIT,
                RightsAccessEnum::CLOUD_INTEGRATION_ACTIVITY_EDIT,
            ]);
        }

        return $user->allowOneOf([
            RightsAccessEnum::CLOUD_INTEGRATION_DETAIL_EDIT,
        ]);
    }
}
