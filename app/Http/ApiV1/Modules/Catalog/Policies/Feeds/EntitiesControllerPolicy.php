<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Feeds;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EntitiesControllerPolicy
{
    use HandlesAuthorization;

    public function migrate(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::FEED_ENTITIES_MIGRATION,
        ]);
    }
}
