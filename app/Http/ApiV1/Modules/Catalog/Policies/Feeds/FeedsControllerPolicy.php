<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Feeds;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class FeedsControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::FEED_SETTINGS_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::FEED_SETTINGS_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }
}
