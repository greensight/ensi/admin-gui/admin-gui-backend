<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Imports;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductImportsControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::IMPORT_CREATE,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::IMPORT_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function preloadFile(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::IMPORT_CREATE,
        ]);
    }
}
