<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Products;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ProductDraftsControllerPolicy
{
    use HandlesRules;
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_CREATE,
        ]);
    }

    public function replace(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE],
            permissionsFields: [
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_ACTIVE_EDIT => ['allow_publish'],
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_EDIT => ['description', 'images'],
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_EDIT => ['attributes'],
                RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_EDIT => [
                    'name',
                    'vendor_code',
                    'barcode',
                    'type',
                    'category_ids',
                    'brand_id',
                    'is_adult',

                    'weight',
                    'weight_gross',
                    'length',
                    'width',
                    'height',

                    'uom',
                    'order_step',
                    'order_minvol',
                    'picking_weight_deviation',
                ],
            ],
        );
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_DELETE,
        ]);
    }

    public function massPatch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH,
        ]);
    }

    public function massPatchByQuery(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_MASS_PATCH,
        ]);
    }

    public function preloadImage(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_PRODUCT_CREATE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_EDIT,
        ]);
    }

    public function commonAttributes(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->replace($user);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function searchEnumValues(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        // Полные права на чтение (405 - 407)
        $all = $user->allowOneOf([RightsAccessEnum::PRODUCT_CATALOG_DETAIL_READ]);

        // Отдельные права на 405 - 407
        $partials = $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_MAIN_DATA_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_PRODUCT_GROUPS_READ,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_READ,
        ]);

        return $all->allowed() ? $all : $partials;
    }

    public function replaceAttributes(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_EDIT,
        ]);
    }

    public function patchAttributes(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_EDIT,
        ]);
    }

    public function deleteAttributes(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_FEATURE_EDIT,
        ]);
    }

    public function replaceImages(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_EDIT,
        ]);
    }

    public function patchImages(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATALOG_DETAIL_CONTENT_EDIT,
        ]);
    }
}
