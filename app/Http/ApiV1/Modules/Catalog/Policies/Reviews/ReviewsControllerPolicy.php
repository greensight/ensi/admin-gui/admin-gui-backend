<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Reviews;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ReviewsControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REVIEW_DETAIL_READ,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REVIEW_DELETE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REVIEW_DETAIL_EDIT,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REVIEW_LIST_READ,
        ]);
    }

    public function massDelete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REVIEW_DELETE,
        ]);
    }
}
