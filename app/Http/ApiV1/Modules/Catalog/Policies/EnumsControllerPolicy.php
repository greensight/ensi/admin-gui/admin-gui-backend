<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EnumsControllerPolicy
{
    use HandlesAuthorization;

    public function productTariffingVolumes(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
        ]);
    }

    public function productUom(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_CATALOG_LIST_READ,
        ]);
    }
}
