<?php

namespace App\Http\ApiV1\Modules\Catalog\Policies\Properties;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PropertiesControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_ATTRIBUTES_LIST_READ,
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_WRITE,
            RightsAccessEnum::PRODUCT_CATEGORY_DETAIL_ATTRIBUTES_WRITE,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_EDIT,
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_SHOW_EDIT,
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_ATTRIBUTES_CREATE,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DELETE,
        ]);
    }

    public function replace(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_EDIT,
            RightsAccessEnum::PRODUCT_ATTRIBUTES_CREATE,
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DELETE,
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_ACTIVE_EDIT,
            RightsAccessEnum::PRODUCT_ATTRIBUTES_DETAIL_SHOW_EDIT,
        ]);
    }
}
