<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Offers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStockRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'qty' => ['nullable', 'numeric'],
        ];
    }
}
