<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Offers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchOfferRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'price' => ['nullable', 'integer', 'min:0'],
            'allow_publish' => ['boolean'],
        ];
    }
}
