<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Properties;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class PreloadDirectoryValueFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'file', 'max:10240'],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
