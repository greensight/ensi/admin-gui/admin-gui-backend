<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchProductAttributesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return array_merge(
            ['attributes' => ['present', 'array']],
            self::itemRules()
        );
    }

    public function attributeValues(): array
    {
        return $this->input('attributes', []);
    }

    public static function itemRules(): array
    {
        return [
            'attributes.*.property_id' => ['required', 'integer'],
            'attributes.*.name' => ['sometimes', 'string'],
            'attributes.*.mark_to_delete' => ['sometimes', 'bool'],

            'attributes.*.value' => [
                'required_without_all:attributes.*.directory_value_id,attributes.*.preload_file_id,attributes.*.mark_to_delete',
            ],
            'attributes.*.directory_value_id' => [
                'prohibits:attributes.*.value,attributes.*.preload_file_id,attributes.*.mark_to_delete',
                'integer',
            ],
            'attributes.*.preload_file_id' => [
                'prohibits:attributes.*.directory_value_id,attributes.*.value,attributes.*.mark_to_delete',
                'integer',
            ],
        ];
    }
}
