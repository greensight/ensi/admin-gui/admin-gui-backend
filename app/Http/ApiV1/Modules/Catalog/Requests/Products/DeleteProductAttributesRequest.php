<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteProductAttributesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'property_ids' => ['required', 'array', 'min:1'],
            'property_ids.*' => ['integer'],
        ];
    }

    public function getPropertyIds(): array
    {
        return $this->input('property_ids');
    }
}
