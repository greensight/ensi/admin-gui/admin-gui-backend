<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassPatchProductsByQueryRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $baseRules = [
            'filter' => ['sometimes', 'array'],
            'fields' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge(
            $baseRules,
            MassPatchProductsRequest::fieldsRules('fields'),
            PatchProductAttributesRequest::itemRules()
        );
    }

    public function getFilter(): array
    {
        return $this->input('filter', []);
    }

    public function getFields(): array
    {
        return $this->input('fields', []);
    }

    public function getAttributes(): array
    {
        return $this->input('attributes', []);
    }
}
