<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Illuminate\Validation\Rule;

class ReplaceProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $fieldsRules = [
            'name' => ['required', 'string'],
            'type' => ['required', Rule::in(ProductTypeEnum::getAllowableEnumValues())],
            'category_ids' => ['nullable', 'array'],
            'category_ids.*' => ['integer'],
            'brand_id' => ['nullable', 'integer'],
            'allow_publish' => ['sometimes', 'boolean'],

            'code' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'vendor_code' => ['required', 'string'],
            'barcode' => ['nullable', 'string'],
            'external_id' => ['nullable', 'string'],

            'weight' => ['nullable', 'numeric'],
            'weight_gross' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'is_adult' => ['nullable', 'boolean'],

            'uom' => ['nullable', Rule::in(ProductUomEnum::getAllowableEnumValues())],
            'order_step' => ['nullable', 'numeric'],
            'order_minvol' => ['nullable', 'numeric'],
            'picking_weight_deviation' => ['nullable', 'numeric'],

            'status_id' => ['sometimes', 'integer'],
            'status_comment' => ['nullable', 'string'],

            'images' => ['sometimes', 'array'],
            'attributes' => ['sometimes', 'array'],
        ];

        return array_merge(
            $fieldsRules,
            ReplaceProductAttributesRequest::itemRules(),
            ReplaceImagesRequest::itemRules()
        );
    }
}
