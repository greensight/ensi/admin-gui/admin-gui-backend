<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Ensi\PimClient\Dto\ProductUomEnum;
use Illuminate\Validation\Rule;

class PatchProductRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return array_merge(
            self::baseRules(),
            self::uniqueRules(),
            PatchProductAttributesRequest::itemRules(),
            PatchImagesRequest::itemRules()
        );
    }

    public static function baseRules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'category_ids' => ['sometimes', 'required', 'array'],
            'category_ids.*' => ['integer'],
            'allow_publish' => ['sometimes', 'required', 'boolean'],
            'type' => ['sometimes', 'required', Rule::in(ProductTypeEnum::getAllowableEnumValues())],

            'code' => ['sometimes', 'nullable', 'string'],
            'description' => ['sometimes', 'nullable', 'string'],
            'vendor_code' => ['sometimes', 'required', 'string'],

            'brand_id' => ['sometimes', 'nullable', 'integer'],

            'weight' => ['sometimes', 'nullable', 'numeric'],
            'weight_gross' => ['sometimes', 'nullable', 'numeric'],
            'length' => ['sometimes', 'nullable', 'numeric'],
            'width' => ['sometimes', 'nullable', 'numeric'],
            'height' => ['sometimes', 'nullable', 'numeric'],
            'is_adult' => ['sometimes', 'nullable', 'boolean'],

            'uom' => ['nullable', Rule::in(ProductUomEnum::getAllowableEnumValues())],
            'order_step' => ['nullable', 'numeric'],
            'order_minvol' => ['nullable', 'numeric'],
            'picking_weight_deviation' => ['nullable', 'numeric'],

            'status_id' => ['sometimes', 'required', 'integer'],
            'status_comment' => ['sometimes', 'nullable', 'string'],
        ];
    }

    public static function uniqueRules(): array
    {
        return [
            'barcode' => ['sometimes', 'nullable', 'string'],
            'external_id' => ['sometimes', 'nullable', 'string'],
        ];
    }
}
