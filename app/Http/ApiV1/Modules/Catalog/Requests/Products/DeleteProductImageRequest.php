<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteProductImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file_id' => [
                'required',
                'integer',
            ],
        ];
    }

    public function getFileId(): int
    {
        return $this->integer('file_id');
    }
}
