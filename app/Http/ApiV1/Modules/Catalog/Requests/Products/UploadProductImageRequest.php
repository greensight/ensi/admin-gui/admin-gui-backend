<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Products;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class UploadProductImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'preload_file_id' => [
                'prohibits:url',
                'integer',
            ],
            'name' => ['sometimes', 'nullable', 'string'],
            'sort' => ['sometimes', 'integer'],

            'url' => [
                'required_without:preload_file_id',
                'url',
            ],
        ];
    }
}
