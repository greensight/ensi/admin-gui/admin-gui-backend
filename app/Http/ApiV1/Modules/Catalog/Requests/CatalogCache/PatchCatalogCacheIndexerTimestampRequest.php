<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\CatalogCache;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchCatalogCacheIndexerTimestampRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'last_schedule' => ['date_format:Y-m-d\TH:i:s.u\Z'],
        ];
    }
}
