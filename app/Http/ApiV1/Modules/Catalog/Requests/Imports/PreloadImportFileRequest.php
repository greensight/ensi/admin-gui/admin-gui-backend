<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Imports;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use App\Http\ApiV1\Support\Rules\FilenameLength;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rules\File;

class PreloadImportFileRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                File::types(['csv', 'txt', 'xls', 'xlsx'])->max(10240),
                new FilenameLength(max: 32),
            ],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
