<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Imports;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\PimClient\Dto\ProductImportTypeEnum;
use Illuminate\Validation\Rule;

class CreateProductImportRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'preload_file_id' => ['required', 'integer', 'min:1'],
            'type' => ['required', Rule::in(ProductImportTypeEnum::getAllowableEnumValues())],
        ];
    }
}
