<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Reviews;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\ReviewsClient\Dto\ReviewStatusEnum;
use Illuminate\Validation\Rule;

class PatchReviewRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'comment' => ['nullable', 'string'],
            'status_id' => ['integer', Rule::in([ReviewStatusEnum::PUBLISHED, ReviewStatusEnum::DENIED])],
        ];
    }
}
