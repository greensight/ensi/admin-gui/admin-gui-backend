<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

class PatchBrandRequest extends UploadBrandImageRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'is_active' => ['sometimes', 'required', 'boolean'],
            'code' => ['sometimes', 'required', 'string'],
            'description' => ['sometimes', 'nullable', 'string'],
            'preload_file_id' => ['sometimes', 'required', 'integer', 'min:1'],
        ];
    }
}
