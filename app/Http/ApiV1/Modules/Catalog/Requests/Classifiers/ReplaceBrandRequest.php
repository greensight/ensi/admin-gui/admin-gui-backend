<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ReplaceBrandRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'is_active' => ['required', 'boolean'],
            'code' => ['required', 'string'],
            'description' => ['nullable', 'string'],
            'preload_file_id' => ['sometimes', 'integer', 'min:1'],
        ];
    }
}
