<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Http\UploadedFile;

class UploadBrandImageRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', 'mimes:jpeg,png', 'max:1024'],
        ];
    }

    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
