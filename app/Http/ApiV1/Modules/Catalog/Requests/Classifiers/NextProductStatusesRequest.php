<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class NextProductStatusesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'integer'],
        ];
    }

    public function getStatusId(): ?int
    {
        return $this->input('id');
    }
}
