<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Classifiers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchProductStatusRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'events' => ['nullable'],
            'events.operation' => ['nullable', 'integer'],
            'events.events' => ['required_with:events', 'array'],
            'events.events.*' => ['integer'],
            'is_active' => ['boolean'],
            'is_publication' => ['required', 'boolean'],
            'type' => ['integer'],
            'color' => ['nullable', 'string'],
            'name' => ['string'],
            'code' => ['nullable', 'string'],
        ];
    }
}
