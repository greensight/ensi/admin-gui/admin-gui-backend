<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Feeds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchFeedSettingsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string'],
            'code' => ['string'],
            'active' => ['boolean'],
            'type' => ['integer'],
            'platform' => ['integer'],
            'active_product' => ['boolean'],
            'active_category' => ['boolean'],
            'shop_name' => ['string'],
            'shop_url' => ['string'],
            'shop_company' => ['string'],
            'update_time' => ['integer'],
            'delete_time' => ['integer'],
        ];
    }
}
