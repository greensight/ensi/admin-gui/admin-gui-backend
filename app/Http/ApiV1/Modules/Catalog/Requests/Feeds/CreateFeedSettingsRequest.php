<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Feeds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateFeedSettingsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'code' => ['required', 'string'],
            'active' => ['required', 'boolean'],
            'type' => ['required', 'integer'],
            'platform' => ['required', 'integer'],
            'active_product' => ['required', 'boolean'],
            'active_category' => ['required', 'boolean'],
            'shop_name' => ['required', 'string'],
            'shop_url' => ['required', 'string'],
            'shop_company' => ['required', 'string'],
            'update_time' => ['required', 'integer'],
            'delete_time' => ['required', 'integer'],
        ];
    }
}
