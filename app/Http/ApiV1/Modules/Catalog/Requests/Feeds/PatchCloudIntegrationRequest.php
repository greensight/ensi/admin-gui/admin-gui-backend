<?php

namespace App\Http\ApiV1\Modules\Catalog\Requests\Feeds;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchCloudIntegrationRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'integration' => ['boolean'],
            'public_api_key' => ['nullable', 'string'],
            'private_api_key' => ['nullable', 'string'],
        ];
    }
}
