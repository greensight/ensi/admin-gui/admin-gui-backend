<?php

namespace App\Http\ApiV1\Modules\Units\Resources\SellerUsers;

use App\Http\ApiV1\Modules\Units\Resources\AdminUsers\AdminUserRolesResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\SellerAuthClient\Dto\User;

/** @mixin User */
class SellerUsersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->getId(),
            "full_name" => $this->getFullName(),
            "short_name" => $this->getShortName(),
            "seller_id" => $this->getSellerId(),
            "active" => $this->getActive(),
            "login" => $this->getLogin(),
            "last_name" => $this->getLastName(),
            "first_name" => $this->getFirstName(),
            "middle_name" => $this->getMiddleName(),
            "email" => $this->getEmail(),
            "phone" => $this->getPhone(),

            "created_at" => $this->dateTimeToIso($this->getCreatedAt()),
            "updated_at" => $this->dateTimeToIso($this->getUpdatedAt()),

            "roles" => AdminUserRolesResource::collection($this->whenNotNull($this->getRoles())),
        ];
    }
}
