<?php

namespace App\Http\ApiV1\Modules\Units\Resources\SellerUsers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\SellerAuthClient\Dto\Role;

/** @mixin Role */
class SellerUserRolesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
        ];
    }
}
