<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Stores;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\BuClient\Dto\Store;

/** @mixin Store */
class StoreEnumValuesResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
