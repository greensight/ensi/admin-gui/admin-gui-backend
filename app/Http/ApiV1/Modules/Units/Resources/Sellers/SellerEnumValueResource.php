<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Sellers;

use App\Domain\Units\Data\SellerData;
use App\Http\ApiV1\Support\Resources\EnumValueResource;

/** @mixin SellerData */
class SellerEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->seller->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->seller->getLegalName();
    }
}
