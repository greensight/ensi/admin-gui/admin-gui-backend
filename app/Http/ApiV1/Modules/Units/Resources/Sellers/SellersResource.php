<?php

namespace App\Http\ApiV1\Modules\Units\Resources\Sellers;

use App\Domain\Units\Data\SellerData;
use App\Http\ApiV1\Modules\Units\Resources\AddressResource;
use App\Http\ApiV1\Modules\Units\Resources\AdminUsers\AdminUsersResource;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoresResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin SellerData */
class SellersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->seller->getId(),
            'manager_id' => $this->seller->getManagerId(),
            'legal_name' => $this->seller->getLegalName(),
            'legal_address' => AddressResource::make($this->whenNotNull($this->seller->getLegalAddress())),
            'fact_address' => AddressResource::make($this->whenNotNull($this->seller->getFactAddress())),
            'inn' => $this->seller->getInn(),
            'kpp' => $this->seller->getKpp(),
            'payment_account' => $this->seller->getPaymentAccount(),
            'correspondent_account' => $this->seller->getCorrespondentAccount(),
            'bank' => $this->seller->getBank(),
            'bank_address' => AddressResource::make($this->whenNotNull($this->seller->getBankAddress())),
            'bank_bik' => $this->seller->getBankBik(),
            'status' => $this->seller->getStatus(),
            'status_at' => $this->dateTimeToIso($this->seller->getStatusAt()),
            'site' => $this->seller->getSite(),
            'info' => $this->seller->getInfo(),
            'created_at' => $this->dateTimeToIso($this->seller->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->seller->getUpdatedAt()),

            'manager' => AdminUsersResource::make($this->whenNotNull($this->manager)),
            'stores' => StoresResource::collection($this->whenNotNull($this->seller->getStores())),
        ];
    }
}
