<?php

namespace App\Http\ApiV1\Modules\Units\Resources\AdminUsers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\RightsAccessDictionary;

/** @mixin RightsAccessDictionary */
class RightsAccessDictionaryResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'section' => $this->resource->getSection(),
            'items' => RightsAccessResource::collection($this->resource->getItems()),
        ];
    }
}
