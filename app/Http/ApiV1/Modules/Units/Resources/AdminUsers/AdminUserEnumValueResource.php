<?php

namespace App\Http\ApiV1\Modules\Units\Resources\AdminUsers;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\AdminAuthClient\Dto\User;

/** @mixin User */
class AdminUserEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getFullName();
    }
}
