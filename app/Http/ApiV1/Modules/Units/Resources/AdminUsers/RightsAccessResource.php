<?php

namespace App\Http\ApiV1\Modules\Units\Resources\AdminUsers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\RightsAccess;

/** @mixin RightsAccess */
class RightsAccessResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
        ];
    }
}
