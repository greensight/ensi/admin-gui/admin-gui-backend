<?php

namespace App\Http\ApiV1\Modules\Units\Resources\AdminUsers;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\AdminAuthClient\Dto\Role;

/** @mixin Role */
class AdminUserRolesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'active' => $this->getActive(),
            'title' => $this->getTitle(),
            'rights_access' => $this->getRightsAccess(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
