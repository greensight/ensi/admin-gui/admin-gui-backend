<?php

namespace App\Http\ApiV1\Modules\Units\Queries\SellerUsers;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\SellerAuthClient\Api\UsersApi as SellerUserRolesApi;
use Ensi\SellerAuthClient\Dto\RequestBodyPagination;
use Ensi\SellerAuthClient\Dto\RoleResponse;
use Ensi\SellerAuthClient\Dto\SearchRolesRequest as SearchSellerUserRolesRequest;
use Ensi\SellerAuthClient\Dto\SearchRolesResponse;
use Illuminate\Http\Request;

class SellerUserRolesQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected SellerUserRolesApi $adminUserRolesApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchSellerUserRolesRequest::class;
    }

    protected function search($request): SearchRolesResponse
    {
        return $this->adminUserRolesApi->searchRoles($request);
    }

    protected function searchById($id, string $include): RoleResponse
    {
        return $this->adminUserRolesApi->getRole($id);
    }
}
