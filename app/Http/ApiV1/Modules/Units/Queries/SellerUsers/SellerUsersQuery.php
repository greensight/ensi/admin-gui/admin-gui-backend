<?php

namespace App\Http\ApiV1\Modules\Units\Queries\SellerUsers;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\SellerAuthClient\Api\UsersApi as SellerUsersApi;
use Ensi\SellerAuthClient\Dto\RequestBodyPagination;
use Ensi\SellerAuthClient\Dto\SearchUsersRequest as SearchSellerUsersRequest;
use Ensi\SellerAuthClient\Dto\SearchUsersResponse;
use Ensi\SellerAuthClient\Dto\UserResponse;
use Illuminate\Http\Request;

class SellerUsersQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected SellerUsersApi $adminUsersApi,
    ) {
        parent::__construct($httpRequest);
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchSellerUsersRequest::class;
    }

    protected function search($request): SearchUsersResponse
    {
        return $this->adminUsersApi->searchUsers($request);
    }

    protected function searchById($id, string $include): UserResponse
    {
        return $this->adminUsersApi->getUser($id, $include);
    }
}
