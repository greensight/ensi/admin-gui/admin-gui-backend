<?php

namespace App\Http\ApiV1\Modules\Units\Queries\Sellers\IncludeLoaders;

use App\Domain\Common\Data\AsyncLoader;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\PaginationTypeEnum;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\AdminAuthClient\Dto\SearchUsersResponse;
use Ensi\AdminAuthClient\Dto\User;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Support\Collection;

class ManagersLoader implements AsyncLoader
{
    /** @var Collection<int,User> */
    public Collection $managers;
    public array $managerIds = [];
    protected bool $load = false;
    protected bool $loadRoles = false;

    public function __construct(protected readonly UsersApi $usersApi)
    {
        $this->managers = collect();
    }

    public function load()
    {
        $this->load = true;
    }

    public function loadRoles()
    {
        $this->load();
        $this->loadRoles = true;
    }

    public function requestAsync(): ?PromiseInterface
    {
        if (!$this->load || !$this->managerIds) {
            return null;
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)['id' => $this->managerIds]);

        $includes = [];
        if ($this->loadRoles) {
            $includes[] = 'roles';
        }
        if ($includes) {
            $request->setInclude($includes);
        }

        $request->setPagination(
            (new RequestBodyPagination())
                ->setLimit(count($this->managerIds))
                ->setType(PaginationTypeEnum::CURSOR)
        );

        return $this->usersApi->searchUsersAsync($request);
    }

    /**
     * @param SearchUsersResponse $response
     */
    public function processResponse($response): void
    {
        $this->managers = collect($response->getData())->keyBy('id');
    }
}
