<?php

namespace App\Http\ApiV1\Modules\Units\Queries\Sellers;

use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Units\Data\SellerData;
use App\Http\ApiV1\Modules\Units\Queries\Sellers\IncludeLoaders\ManagersLoader;
use App\Http\ApiV1\Modules\Units\Queries\UnitsQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PaginationTypeEnum;
use Ensi\BuClient\Dto\RequestBodyPagination;
use Ensi\BuClient\Dto\SearchSellersRequest;
use Ensi\BuClient\Dto\SearchSellersResponse;
use Ensi\BuClient\Dto\Seller;
use Ensi\BuClient\Dto\SellerResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SellersQuery extends UnitsQuery
{
    use QueryBuilderFilterEnumTrait;

    public function __construct(
        Request $request,
        readonly protected SellersApi $sellersApi,
        readonly protected AsyncLoadAction $asyncLoadAction,
        readonly protected ManagersLoader $managersLoader,
    ) {
        parent::__construct($request, SearchSellersRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): SellerResponse
    {
        return $this->sellersApi->getSeller($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchSellersResponse
    {
        return $this->sellersApi->searchSellers($request);
    }

    protected function convertGetToItems($response)
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem($response)
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertArray(array $sellers): array
    {
        /** @var Collection<Seller> $sellers */
        $sellers = collect($sellers);
        $this->managersLoader->managerIds = $sellers->pluck('manager_id')->unique()->values()->filter()->all();

        $this->asyncLoadAction->execute([
            $this->managersLoader,
        ]);

        $sellersData = [];
        foreach ($sellers as $seller) {
            $manager = $this->managersLoader->managers->get($seller->getManagerId());
            $sellerData = new SellerData($seller, $manager);

            $sellersData[] = $sellerData;
        }

        return $sellersData;
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case 'manager':
                    $this->managersLoader->load();

                    break;
                case 'manager.roles':
                    $this->managersLoader->loadRoles();

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return $includes;
    }

    /**
     * @param SearchSellersRequest $request
     */
    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['legal_name_like'] = $query;
        }

        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
