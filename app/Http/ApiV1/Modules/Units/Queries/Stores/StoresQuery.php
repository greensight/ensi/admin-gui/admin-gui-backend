<?php

namespace App\Http\ApiV1\Modules\Units\Queries\Stores;

use App\Http\ApiV1\Modules\Units\Queries\UnitsQuery;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\SearchStoresResponse;
use Ensi\BuClient\Dto\StoreResponse;
use Illuminate\Http\Request;

class StoresQuery extends UnitsQuery
{
    use QueryBuilderFilterEnumTrait;

    public function __construct(Request $request, private StoresApi $api)
    {
        parent::__construct($request, SearchStoresRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): StoreResponse
    {
        return $this->api->getStore($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchStoresResponse
    {
        return $this->api->searchStores($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $this->prepareRequestForName($request, $id, $query);
    }
}
