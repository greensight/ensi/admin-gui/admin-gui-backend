<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Sellers;

use App\Domain\Common\Data\Meta\Enum\Units\SellerStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\Sellers\CreateSellerAction;
use App\Domain\Units\Actions\Sellers\PatchSellerAction;
use App\Http\ApiV1\Modules\Units\Queries\Sellers\SellersQuery;
use App\Http\ApiV1\Modules\Units\Requests\Sellers\CreateSellerRequest;
use App\Http\ApiV1\Modules\Units\Requests\Sellers\PatchSellerRequest;
use App\Http\ApiV1\Modules\Units\Resources\Sellers\SellerEnumValueResource;
use App\Http\ApiV1\Modules\Units\Resources\Sellers\SellersResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class SellersController
{
    public function get(int $id, SellersQuery $query): Responsable
    {
        return new SellersResource($query->find($id));
    }

    public function search(SellersQuery $query): Responsable
    {
        return SellersResource::collectPage($query->get());
    }

    public function searchEnumValues(SellersQuery $query): Responsable
    {
        return SellerEnumValueResource::collection($query->searchEnums());
    }

    public function create(CreateSellerRequest $request, CreateSellerAction $action): Responsable
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchSellerRequest $request, PatchSellerAction $action): Responsable
    {
        return new SellersResource($action->execute($id, $request->validated()));
    }

    public function meta(SellerStatusEnumInfo $statuses): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('legal_name', 'Юридическое наименование')->sort()->listDefault()->filterDefault(),
            Field::text('legal_address.address_string', 'Юридический адрес')
                ->object()->filter('legal_address_like')->listDefault()->filterDefault(),
            Field::enum('status', 'Статус', $statuses)->listDefault()->filterDefault(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
