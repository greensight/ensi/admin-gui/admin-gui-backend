<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Stores;

use App\Domain\Common\Data\Meta\Enum\Units\SellerEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\Stores\CreateStoreAction;
use App\Domain\Units\Actions\Stores\PatchStoreAction;
use App\Http\ApiV1\Modules\Units\Queries\Stores\StoresQuery;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateStoreRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoreEnumValuesResource;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoresResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class StoresController
{
    public function search(StoresQuery $query): Responsable
    {
        return StoresResource::collectPage($query->get());
    }

    public function searchEnumValues(StoresQuery $query): Responsable
    {
        return StoreEnumValuesResource::collection($query->searchEnums());
    }

    public function create(CreateStoreRequest $request, CreateStoreAction $action): Responsable
    {
        return new StoresResource($action->execute($request->validated()));
    }

    public function get(int $id, StoresQuery $query): Responsable
    {
        return new StoresResource($query->find($id));
    }

    public function patch(int $id, PatchStoreRequest $request, PatchStoreAction $action): Responsable
    {
        return new StoresResource($action->execute($id, $request->validated()));
    }

    public function meta(
        SellerEnumInfo $sellerEnumInfo,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('name', 'Название')->listDefault()->filterDefault()->sort(),
            Field::enum('seller_id', 'Продавец', $sellerEnumInfo)->listDefault()->filterDefault()->sort(),
            Field::string('address.address_string', 'Полный адрес')->object()->filter('address_string'),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault()->sort(),
            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
