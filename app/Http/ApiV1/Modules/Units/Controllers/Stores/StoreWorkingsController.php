<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Stores;

use App\Domain\Units\Actions\Stores\CreateStoreWorkingAction;
use App\Domain\Units\Actions\Stores\DeleteStoreWorkingAction;
use App\Domain\Units\Actions\Stores\PatchStoreWorkingAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateStoreWorkingRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreWorkingRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoreWorkingsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StoreWorkingsController
{
    public function create(CreateStoreWorkingRequest $request, CreateStoreWorkingAction $action): Responsable
    {
        return new StoreWorkingsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchStoreWorkingRequest $request, PatchStoreWorkingAction $action): Responsable
    {
        return new StoreWorkingsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteStoreWorkingAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
