<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\Stores;

use App\Domain\Units\Actions\Stores\CreateStoreContactAction;
use App\Domain\Units\Actions\Stores\DeleteStoreContactAction;
use App\Domain\Units\Actions\Stores\PatchStoreContactAction;
use App\Http\ApiV1\Modules\Units\Requests\Stores\CreateStoreContactRequest;
use App\Http\ApiV1\Modules\Units\Requests\Stores\PatchStoreContactRequest;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoreContactsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StoreContactsController
{
    public function create(CreateStoreContactRequest $request, CreateStoreContactAction $action): Responsable
    {
        return new StoreContactsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchStoreContactRequest $request, PatchStoreContactAction $action): Responsable
    {
        return new StoreContactsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteStoreContactAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
