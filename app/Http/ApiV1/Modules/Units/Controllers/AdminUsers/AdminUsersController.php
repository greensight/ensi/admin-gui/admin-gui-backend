<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\AdminUsers;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\AdminUsers\AddRolesToAdminUserAction;
use App\Domain\Units\Actions\AdminUsers\CreateAdminUserAction;
use App\Domain\Units\Actions\AdminUsers\DeleteRoleFromAdminUserAction;
use App\Domain\Units\Actions\AdminUsers\MassChangeActiveAction;
use App\Domain\Units\Actions\AdminUsers\PatchAdminUserAction;
use App\Domain\Units\Actions\AdminUsers\RefreshPasswordTokenAction;
use App\Domain\Units\Actions\AdminUsers\SetPasswordForAdminUserAction;
use App\Http\ApiV1\Modules\Units\Queries\AdminUsers\AdminUsersQuery;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\AddRolesToAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\CreateAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\DeleteRoleFromAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\MassChangeActiveRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\PatchAdminUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\AdminUsers\SetAdminUserPasswordRequest;
use App\Http\ApiV1\Modules\Units\Resources\AdminUsers\AdminUserEnumValueResource;
use App\Http\ApiV1\Modules\Units\Resources\AdminUsers\AdminUsersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AdminUsersController
{
    public function search(AdminUsersQuery $query): AnonymousResourceCollection
    {
        return AdminUsersResource::collectPage($query->get());
    }

    public function searchEnumValues(AdminUsersQuery $query): AnonymousResourceCollection
    {
        return AdminUserEnumValueResource::collection($query->searchEnums());
    }

    public function create(CreateAdminUserRequest $request, CreateAdminUserAction $action): AdminUsersResource
    {
        return new AdminUsersResource($action->execute($request->validated()));
    }

    public function get(int $id, AdminUsersQuery $query): AdminUsersResource
    {
        return AdminUsersResource::make($query->find($id));
    }

    public function patch(int $id, PatchAdminUserRequest $request, PatchAdminUserAction $action): AdminUsersResource
    {
        return AdminUsersResource::make($action->execute($id, $request->validated()));
    }

    public function addRoles(int $id, AddRolesToAdminUserRequest $request, AddRolesToAdminUserAction $action): EmptyResource
    {
        $validated = $request->validated();
        $action->execute($id, $validated);

        return new EmptyResource();
    }

    public function deleteRole(int $id, DeleteRoleFromAdminUserRequest $request, DeleteRoleFromAdminUserAction $action): EmptyResource
    {
        $validated = $request->validated();
        $action->execute($id, $validated);

        return new EmptyResource();
    }

    public function setPassword(SetAdminUserPasswordRequest $request, SetPasswordForAdminUserAction $action): EmptyResource
    {
        $action->execute($request->getUserPassword(), $request->getPasswordToken());

        return new EmptyResource();
    }

    public function refreshPasswordToken(int $id, RefreshPasswordTokenAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function massChangeActive(MassChangeActiveRequest $request, MassChangeActiveAction $action): Responsable
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault(),
            Field::text('last_name', 'Фамилия')->listDefault()->filterDefault(),
            Field::text('first_name', 'Имя')->listDefault()->filterDefault(),
            Field::text('middle_name', 'Отчество')->listDefault()->filterDefault(),
            Field::text('phone', 'Телефон')->listDefault()->filterDefault(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
