<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\SellerUsers;

use App\Http\ApiV1\Modules\Units\Queries\SellerUsers\SellerUserRolesQuery;
use App\Http\ApiV1\Modules\Units\Resources\SellerUsers\SellerUserRolesResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SellerUserRolesController
{
    public function search(SellerUserRolesQuery $query): AnonymousResourceCollection
    {
        return SellerUserRolesResource::collectPage($query->get());
    }
}
