<?php

namespace App\Http\ApiV1\Modules\Units\Controllers\SellerUsers;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Units\Actions\SellerUsers\AddRolesToSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\CreateSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\DeleteRoleFromSellerUserAction;
use App\Domain\Units\Actions\SellerUsers\PatchSellerUserAction;
use App\Http\ApiV1\Modules\Units\Queries\SellerUsers\SellerUsersQuery;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\AddRolesToSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\CreateSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\DeleteRoleFromSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Requests\SellerUsers\PatchSellerUserRequest;
use App\Http\ApiV1\Modules\Units\Resources\SellerUsers\SellerUsersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class SellerUsersController
{
    public function create(CreateSellerUserRequest $request, CreateSellerUserAction $action): SellerUsersResource
    {
        return new SellerUsersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchSellerUserRequest $request, PatchSellerUserAction $action): SellerUsersResource
    {
        return new SellerUsersResource($action->execute($id, $request->validated()));
    }

    public function get(int $id, SellerUsersQuery $query): SellerUsersResource
    {
        return new SellerUsersResource($query->find($id));
    }

    public function search(SellerUsersQuery $query): AnonymousResourceCollection
    {
        return SellerUsersResource::collectPage($query->get());
    }

    public function meta(): ModelMetaResource
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->sortDefault(direction: 'desc')->detailLink(),
            Field::boolean('active', 'Активность')->listDefault()->filterDefault(),
            Field::text('last_name', 'Фамилия')->listDefault()->filterDefault(),
            Field::text('first_name', 'Имя')->listDefault()->filterDefault(),
            Field::text('middle_name', 'Отчество')->listDefault()->filterDefault(),
            Field::text('phone', 'Телефон')->listDefault()->filterDefault(),
            Field::text('email', 'Почта')->listDefault()->filterDefault(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }

    public function addRoles(
        int $id,
        AddRolesToSellerUserRequest $request,
        AddRolesToSellerUserAction $action
    ): EmptyResource {
        $action->execute($id, $request->validated());

        return new EmptyResource();
    }

    public function deleteRole(
        int $id,
        DeleteRoleFromSellerUserRequest $request,
        DeleteRoleFromSellerUserAction $action
    ): EmptyResource {
        $action->execute($id, $request->validated());

        return new EmptyResource();
    }
}
