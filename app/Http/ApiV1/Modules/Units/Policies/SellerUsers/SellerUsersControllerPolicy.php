<?php

namespace App\Http\ApiV1\Modules\Units\Policies\SellerUsers;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SellerUsersControllerPolicy
{
    use HandlesRules;
    use HandlesAuthorization;

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_USER_LIST_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_USER_LIST_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_USER_CREATE,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_USER_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::SELLER_USER_DETAIL_EDIT],
            permissionsFields: [
                RightsAccessEnum::SELLER_USER_ACTIVITY_EDIT => ['active'],
                RightsAccessEnum::SELLER_USER_PASSWORD_EDIT => ['password'],
            ],
        );
    }
}
