<?php

namespace App\Http\ApiV1\Modules\Units\Policies\Stores;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class StoresControllerPolicy
{
    use HandlesAuthorization;
    use HandlesRules;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::STORE_CREATE,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::STORE_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::STORE_LIST_READ,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::STORE_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::STORE_MAIN_DATA_EDIT],
            permissionsFields: [
                RightsAccessEnum::STORE_ACTIVITY_EDIT => ['active'],
            ],
        );
    }

    public function searchEnumValues(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::STORE_LIST_READ,
            RightsAccessEnum::OFFER_DETAIL_READ,
        ]);
    }
}
