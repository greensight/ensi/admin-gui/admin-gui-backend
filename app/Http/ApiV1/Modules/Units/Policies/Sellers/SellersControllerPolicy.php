<?php

namespace App\Http\ApiV1\Modules\Units\Policies\Sellers;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SellersControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function searchEnumValues(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_LIST_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SELLER_DETAIL_EDIT,
        ]);
    }
}
