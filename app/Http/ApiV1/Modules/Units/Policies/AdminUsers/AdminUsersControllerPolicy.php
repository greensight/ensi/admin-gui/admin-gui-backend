<?php

namespace App\Http\ApiV1\Modules\Units\Policies\AdminUsers;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class AdminUsersControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_READ,
            RightsAccessEnum::REFUND_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_ROLE_EDIT,
            RightsAccessEnum::USER_DETAIL_EDIT,
            RightsAccessEnum::USER_DETAIL_ACTIVE_EDIT,
            RightsAccessEnum::USER_DETAIL_PASSWORD_EDIT,
        ]);
    }

    public function massChangeActive(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_EDIT,
            RightsAccessEnum::USER_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function setPassword(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_EDIT,
            RightsAccessEnum::USER_DETAIL_PASSWORD_EDIT,
        ]);
    }

    public function addRoles(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_EDIT,
            RightsAccessEnum::USER_DETAIL_ROLE_EDIT,
        ]);
    }

    public function deleteRole(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_EDIT,
            RightsAccessEnum::USER_DETAIL_ROLE_EDIT,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_CREATE,
        ]);
    }

    public function refreshPasswordToken(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::USER_DETAIL_EDIT,
            RightsAccessEnum::USER_DETAIL_PASSWORD_EDIT,
        ]);
    }
}
