<?php

namespace App\Http\ApiV1\Modules\Units\Policies\AdminUsers;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class AdminUserRolesControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ROLE_CREATE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ROLE_DETAIL_EDIT,
            RightsAccessEnum::ROLE_DETAIL_ACTIVE_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ROLE_DELETE,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ROLE_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ROLE_LIST_READ,
            RightsAccessEnum::USER_DETAIL_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function getRightsAccess(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ROLE_LIST_READ,
            RightsAccessEnum::USER_DETAIL_READ,
        ]);
    }
}
