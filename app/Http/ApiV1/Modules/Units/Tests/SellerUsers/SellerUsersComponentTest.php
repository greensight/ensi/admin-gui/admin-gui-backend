<?php

use App\Domain\Units\Tests\Factories\SellerUserFactory;
use App\Http\ApiV1\Modules\Units\Tests\SellerUsers\Factories\SellerUserRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\PaginationFactory;
use Ensi\SellerAuthClient\Dto\RoleEnum;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component');

test('POST /api/v1/units/seller-users:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_USER_LIST_READ]);

    $sellerUsers = SellerUserFactory::new()->makeResponseSearch();

    $this->mockSellerAuthUsersApi()
        ->shouldReceive('searchUsers')
        ->once()
        ->andReturn($sellerUsers);

    postJson("/api/v1/units/seller-users:search", ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', current($sellerUsers->getData())->getId());
});

test('GET /api/v1/units/seller-users:meta success', function () {
    $this->setRights([RightsAccessEnum::SELLER_USER_LIST_READ]);

    getJson('/api/v1/units/seller-users:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/units/seller-users 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_USER_CREATE]);

    $this->mockSellerAuthUsersApi()
        ->shouldReceive('createUser')
        ->once()
        ->andReturn(SellerUserFactory::new()->makeResponse());

    $request = SellerUserRequestFactory::new()->make();

    postJson("/api/v1/units/seller-users", $request)
        ->assertStatus(200);
});

test('GET /api/v1/units/seller-users/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_USER_DETAIL_READ]);

    $sellerUserId = 1;
    $this->mockSellerAuthUsersApi()
        ->shouldReceive('getUser')
        ->once()
        ->andReturn(SellerUserFactory::new()->makeResponse(['id' => $sellerUserId]));

    getJson("/api/v1/units/seller-users/{$sellerUserId}")
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'login', 'email']]);
});

test('PATCH /api/v1/units/seller-users/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_USER_DETAIL_EDIT]);

    $sellerUserId = 1;
    $this->mockSellerAuthUsersApi()
        ->shouldReceive('patchUser')
        ->once()
        ->andReturn(SellerUserFactory::new()->makeResponse(['id' => $sellerUserId]));
    $request = SellerUserRequestFactory::new()->make();

    patchJson("/api/v1/units/seller-users/{$sellerUserId}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $sellerUserId)
        ->assertJsonStructure(['data' => ['id', 'login', 'email']]);
});

test('POST /api/v1/units/seller-users/{id}:add-roles 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->mockSellerAuthUsersApi()->shouldReceive('addRolesToUser')->once();

    $addRolesData = [
        'roles' => [RoleEnum::SELLER_OPERATOR],
        'expires' => null,
    ];

    postJson("/api/v1/units/seller-users/1:add-roles", $addRolesData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/units/seller-users/{id}:delete-role 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->mockSellerAuthUsersApi()->shouldReceive('deleteRoleFromUser')->once();

    $deleteRoleData = [
        'role_id' => RoleEnum::SELLER_OPERATOR,
    ];

    postJson("/api/v1/units/seller-users/1:delete-role", $deleteRoleData)
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});
