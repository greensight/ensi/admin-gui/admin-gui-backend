<?php

namespace App\Http\ApiV1\Modules\Units\Tests\SellerUsers\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class SellerUserRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'seller_id' => $this->faker->modelId(),
            'login' => $this->faker->unique()->userName(),
            'active' => $this->faker->boolean(),
            'password' => $this->faker->password(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->firstName(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'email' => $this->faker->unique()->safeEmail(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
