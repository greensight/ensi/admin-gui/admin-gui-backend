<?php

use App\Domain\Units\Tests\Factories\StorePickupTimeFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StorePickupTimeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\EmptyDataResponse;
use Ensi\BuClient\Dto\PatchStorePickupTimeRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'stores');

test('POST /api/v1/units/stores-pickup-times 200', function () {
    $this->setRights([RightsAccessEnum::STORE_PICKUP_TIME_EDIT]);

    $request = StorePickupTimeRequestFactory::new()->make();

    $this->mockBusinessUnitsStorePickupTimesApi()->allows([
        'createStorePickupTime' => StorePickupTimeFactory::new()->makeResponseOne(),
    ]);

    postJson('/api/v1/units/stores-pickup-times', $request)
        ->assertStatus(200);
});

test('POST /api/v1/units/stores-pickup-times 400', function () {
    $this->setRights([RightsAccessEnum::STORE_PICKUP_TIME_EDIT]);

    $request = StorePickupTimeRequestFactory::new()->make(['store_id' => null]);

    $this->mockBusinessUnitsStorePickupTimesApi()->shouldNotReceive('createStorePickupTime');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores-pickup-times', $request)
        ->assertStatus(400);
});

test('POST /api/v1/units/stores-pickup-times 401', function () {
    actingAsUnathenticated();

    $request = StorePickupTimeRequestFactory::new()->make();

    $this->mockBusinessUnitsStorePickupTimesApi()->shouldNotReceive('createStorePickupTime');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores-pickup-times', $request)
        ->assertStatus(401);
});

test('POST /api/v1/units/stores-pickup-times 403', function () {
    $this->setRights([]);

    $request = StorePickupTimeRequestFactory::new()->make();
    postJson('/api/v1/units/stores-pickup-times', $request)
        ->assertStatus(403);
});

test('DELETE /api/v1/units/stores-pickup-times/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */

    $this->setRights([RightsAccessEnum::STORE_PICKUP_TIME_EDIT]);

    $this->mockBusinessUnitsStorePickupTimesApi()->allows([
        'deleteStorePickupTime' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/units/stores-pickup-times/1')
        ->assertStatus(200);
});

test('DELETE /api/v1/units/stores-pickup-times/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStorePickupTimesApi()->shouldNotReceive('deleteStorePickupTime');

    $this->skipNextOpenApiRequestValidation();
    deleteJson('/api/v1/units/stores-pickup-times/1')
        ->assertStatus(401);
});

test('DELETE /api/v1/units/stores-pickup-times/{id} 403', function () {
    $this->setRights([]);

    $request = StorePickupTimeRequestFactory::new()->make();
    deleteJson('/api/v1/units/stores-pickup-times/1', $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores-pickup-times/{id} 200', function () {
    $this->setRights([RightsAccessEnum::STORE_PICKUP_TIME_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStorePickupTimesApi()
        ->shouldReceive('patchStorePickupTime')
        ->withArgs(fn (int $storePickupTimeId, PatchStorePickupTimeRequest $patchRequest) => $storePickupTimeId === $id)
        ->andReturn(StorePickupTimeFactory::new()->makeResponseOne(['id' => $id]));

    $request = StorePickupTimeRequestFactory::new()->make();

    patchJson("/api/v1/units/stores-pickup-times/$id", $request)
        ->assertStatus(200);
});

test('PATCH /api/v1/units/stores-pickup-times/{id} 400', function () {
    $this->setRights([RightsAccessEnum::STORE_PICKUP_TIME_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStorePickupTimesApi()->shouldNotReceive('patchStorePickupTime');

    $request = StorePickupTimeRequestFactory::new()->make(['store_id' => null]);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/units/stores-pickup-times/$id", $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/units/stores-pickup-times/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStorePickupTimesApi()->shouldNotReceive('patchStorePickupTime');

    $request = StorePickupTimeRequestFactory::new()->make();

    $this->skipNextOpenApiRequestValidation();
    patchJson('/api/v1/units/stores-pickup-times/1', $request)
        ->assertStatus(401);
});

test('PATCH /api/v1/units/stores-pickup-times/{id} 403', function () {
    $this->setRights([]);

    $request = StorePickupTimeRequestFactory::new()->make();
    patchJson('/api/v1/units/stores-pickup-times/1', $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores-pickup-times/{id} 404', function () {
    $this->setRights([RightsAccessEnum::STORE_PICKUP_TIME_EDIT]);

    $request = StorePickupTimeRequestFactory::new()->make();

    $this->mockBusinessUnitsStorePickupTimesApi()
        ->shouldReceive('patchStorePickupTime')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    patchJson('/api/v1/units/stores-pickup-times/1', $request)
        ->assertStatus(404);
});
