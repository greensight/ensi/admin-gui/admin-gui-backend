<?php

namespace App\Http\ApiV1\Modules\Units\Tests\Stores\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class StoreContactRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'store_id' => $this->faker->modelId(),
            'name' => $this->faker->word(),
            'phone' => $this->faker->numerify('+7##########'),
            'email' => $this->faker->email(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
