<?php

use App\Domain\Units\Tests\Factories\StoreContactFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StoreContactRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\EmptyDataResponse;
use Ensi\BuClient\Dto\PatchStoreContactRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'stores');

test('POST /api/v1/units/stores-contacts 200', function () {
    $this->setRights([RightsAccessEnum::STORE_CONTACTS_EDIT]);

    $request = StoreContactRequestFactory::new()->make();

    $this->mockBusinessUnitsStoreContactsApi()->allows([
        'createStoreContact' => StoreContactFactory::new()->makeResponseOne(),
    ]);

    postJson('/api/v1/units/stores-contacts', $request)
        ->assertStatus(200);
});

test('POST /api/v1/units/stores-contacts 400', function () {
    $this->setRights([RightsAccessEnum::STORE_CONTACTS_EDIT]);

    $request = StoreContactRequestFactory::new()->make(['store_id' => null]);

    $this->mockBusinessUnitsStoreContactsApi()->shouldNotReceive('createStoreContact');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores-contacts', $request)
        ->assertStatus(400);
});

test('POST /api/v1/units/stores-contacts 401', function () {
    actingAsUnathenticated();

    $request = StoreContactRequestFactory::new()->make();

    $this->mockBusinessUnitsStoreContactsApi()->shouldNotReceive('createStoreContact');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores-contacts', $request)
        ->assertStatus(401);
});

test('POST /api/v1/units/stores-contacts 403', function () {
    $this->setRights([]);

    $request = StoreContactRequestFactory::new()->make();
    postJson('/api/v1/units/stores-contacts', $request)
        ->assertStatus(403);
});

test('DELETE /api/v1/units/stores-contacts/{id} 200', function () {
    $this->setRights([RightsAccessEnum::STORE_CONTACTS_EDIT]);

    $this->mockBusinessUnitsStoreContactsApi()->allows([
        'deleteStoreContact' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/units/stores-contacts/1')
        ->assertStatus(200);
});

test('DELETE /api/v1/units/stores-contacts/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoreContactsApi()->shouldNotReceive('deleteStoreContact');

    $this->skipNextOpenApiRequestValidation();
    deleteJson('/api/v1/units/stores-contacts/1')
        ->assertStatus(401);
});

test('DELETE /api/v1/units/stores-contacts/{id} 403', function () {
    $this->setRights([]);

    $request = StoreContactRequestFactory::new()->make();
    deleteJson('/api/v1/units/stores-contacts/1', $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores-contacts/{id} 200', function () {
    $this->setRights([RightsAccessEnum::STORE_CONTACTS_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStoreContactsApi()
        ->shouldReceive('patchStoreContact')
        ->withArgs(fn (int $storeContactId, PatchStoreContactRequest $patchRequest) => $storeContactId === $id)
        ->andReturn(StoreContactFactory::new()->makeResponseOne(['id' => $id]));

    $request = StoreContactRequestFactory::new()->make();

    patchJson("/api/v1/units/stores-contacts/$id", $request)
        ->assertStatus(200);
});

test('PATCH /api/v1/units/stores-contacts/{id} 400', function () {
    $this->setRights([RightsAccessEnum::STORE_CONTACTS_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStoreContactsApi()
        ->shouldReceive('patchStoreContact')
        ->withArgs(fn (int $storeContactId, PatchStoreContactRequest $patchRequest) => $storeContactId === $id)
        ->andReturn(StoreContactFactory::new()->makeResponseOne(['id' => $id]));

    $request = StoreContactRequestFactory::new()->make(['store_id' => null]);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/units/stores-contacts/$id", $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/units/stores-contacts/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoreContactsApi()->shouldNotReceive('patchStoreContact');

    $request = StoreContactRequestFactory::new()->make();

    $this->skipNextOpenApiRequestValidation();
    patchJson('/api/v1/units/stores-contacts/1', $request)
        ->assertStatus(401);
});

test('PATCH /api/v1/units/stores-contacts/{id} 403', function () {
    $this->setRights([]);

    $request = StoreContactRequestFactory::new()->make();
    patchJson('/api/v1/units/stores-contacts/1', $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores-contacts/{id} 404', function () {
    $this->setRights([RightsAccessEnum::STORE_CONTACTS_EDIT]);

    $request = StoreContactRequestFactory::new()->make();

    $this->mockBusinessUnitsStoreContactsApi()
        ->shouldReceive('patchStoreContact')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    patchJson('/api/v1/units/stores-contacts/1', $request)
        ->assertStatus(404);
});
