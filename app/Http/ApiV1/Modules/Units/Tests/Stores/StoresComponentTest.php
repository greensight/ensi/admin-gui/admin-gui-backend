<?php

use App\Domain\Units\Tests\Factories\StoreFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StoreRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\PatchStoreRequest;
use Ensi\BuClient\Dto\SearchStoresRequest;
use Ensi\BuClient\Dto\StoreResponse;
use Ensi\LaravelTestFactories\FakerProvider;
use Mockery\Matcher\AndAnyOtherArgs;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'stores');

test('GET /api/v1/units/stores:meta 200', function () {
    $this->setRights([RightsAccessEnum::STORE_LIST_READ]);

    getJson('/api/v1/units/stores:meta')
        ->assertStatus(200);
});

test('GET /api/v1/units/stores:meta 401', function () {
    actingAsUnathenticated();

    $this->skipNextOpenApiRequestValidation();
    getJson('/api/v1/units/stores:meta')
        ->assertStatus(401);
});

test('POST /api/v1/units/stores:meta 403', function () {
    $this->setRights([]);

    getJson('/api/v1/units/stores:meta')
        ->assertStatus(403);
});

test('POST /api/v1/units/stores:search 200', function (bool $includeAllRelations) {
    $this->setRights([RightsAccessEnum::STORE_LIST_READ]);

    $count = 3;
    $this->mockBusinessUnitsStoresApi()->allows([
        'searchStores' => StoreFactory::new()
            ->withAllRelations($includeAllRelations)
            ->makeResponseSearch(count: $count),
    ]);

    $request = [
        'include' => $includeAllRelations ? ['workings', 'contacts', 'contact', 'pickup_times'] : [],
    ];
    postJson('/api/v1/units/stores:search', $request)
        ->assertStatus(200)
        ->assertJsonCount($count, 'data');
})->with([
    'without includes' => [false],
    'with all includes' => [true],
]);

test('POST /api/v1/units/stores:search 400', function () {
    $this->setRights([RightsAccessEnum::STORE_LIST_READ]);

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('searchStores')
        ->andThrowExceptions([new ApiException(
            "Requested filter(s) `wrong_filter` are not allowed. Allowed filter(s) are `stub_filter_1, stub_filter_2`.",
            400
        )]);

    $request = ['filter' => ['wrong_filter' => 'some value']];
    postJson('/api/v1/units/stores:search', $request)
        ->assertStatus(400);
});

test('POST /api/v1/units/stores:search 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('searchStores');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores:search')
        ->assertStatus(401);
});

test('POST /api/v1/units/stores:search 403', function () {
    $this->setRights([]);

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('searchStores');

    postJson('/api/v1/units/stores:search')
        ->assertStatus(403);
});

test('POST /api/v1/units/stores 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->setRights([RightsAccessEnum::STORE_CREATE]);

    $request = StoreRequestFactory::new()->make();

    $this->mockBusinessUnitsStoresApi()->allows([
        'createStore' => StoreFactory::new()->makeResponseOne(),
    ]);

    postJson('/api/v1/units/stores', $request)
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/units/stores 401', function () {
    actingAsUnathenticated();

    $request = StoreRequestFactory::new()->make();

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('createStore');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores', $request)
        ->assertStatus(401);
});

test('POST /api/v1/units/stores 403', function () {
    $this->setRights([]);

    $request = StoreRequestFactory::new()->make();

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('createStore');

    postJson('/api/v1/units/stores', $request)
        ->assertStatus(403);
});

test('GET /api/v1/units/stores/{id} 200', function () {
    $this->setRights([RightsAccessEnum::STORE_DETAIL_READ]);

    $id = 1;
    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('getStore')
        ->with($id, new AndAnyOtherArgs())
        ->andReturn(StoreFactory::new()->makeResponseOne(['id' => $id]));

    getJson("/api/v1/units/stores/$id")
        ->assertStatus(200);
});

test('GET /api/v1/units/stores/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('getStore');

    $this->skipNextOpenApiRequestValidation();
    getJson('/api/v1/units/stores/1')
        ->assertStatus(401);
});

test('GET /api/v1/units/stores/{id} 403', function () {
    $this->setRights([]);

    $request = StoreRequestFactory::new()->make();

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('getStore');

    getJson('/api/v1/units/stores/1', $request)
        ->assertStatus(403);
});

test('GET /api/v1/units/stores/{id} 404', function () {
    $this->setRights([RightsAccessEnum::STORE_DETAIL_READ]);

    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('getStore')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    getJson('/api/v1/units/stores/1')
        ->assertStatus(404);
});

test('PATCH /api/v1/units/stores/{id} 200', function (array $accessRights, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->setRights($accessRights);

    $id = 1;
    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('patchStore')
        ->withArgs(fn (int $storeId, PatchStoreRequest $patchRequest) => $storeId === $id)
        ->andReturn(StoreFactory::new()->makeResponseOne(['id' => $id]));

    $request = StoreRequestFactory::new()->make();

    patchJson("/api/v1/units/stores/$id", $request)
        ->assertStatus(200);
})->with([
    'only main data edit access right' => [[RightsAccessEnum::STORE_MAIN_DATA_EDIT]],
    'both main data edit and activity flag access right' => [[RightsAccessEnum::STORE_MAIN_DATA_EDIT, RightsAccessEnum::STORE_ACTIVITY_EDIT]],
], FakerProvider::$optionalDataset);

test('PATCH /api/v1/units/stores/{id} 200 only activity flag access right', function () {
    $this->setRights([RightsAccessEnum::STORE_ACTIVITY_EDIT]);

    $id = 1;
    $model = StoreFactory::new()->make(['id' => $id]);
    $newModel = (clone $model)->setActive(!$model->getActive());
    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('patchStore')
        ->withArgs(fn (int $storeId, PatchStoreRequest $patchRequest) => $storeId === $id)
        ->andReturn(new StoreResponse(['data' => $newModel]));

    $request = StoreRequestFactory::new()->only(['active'])->make(['active' => !$model->getActive()]);

    patchJson("/api/v1/units/stores/$id", $request)
        ->assertStatus(200);
});

test('PATCH /api/v1/units/stores/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('patchStore');

    $request = StoreRequestFactory::new()->make();

    $this->skipNextOpenApiRequestValidation();
    patchJson('/api/v1/units/stores/1', $request)
        ->assertStatus(401);
});

test('PATCH /api/v1/units/stores/{id} 403', function () {
    $this->setRights([]);

    $request = StoreRequestFactory::new()->make();

    $id = 1;
    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('patchStore');

    patchJson("/api/v1/units/stores/$id", $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores/{id} 403 only activity flag access right', function () {
    $this->setRights([RightsAccessEnum::STORE_ACTIVITY_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStoresApi()->shouldNotReceive('patchStore');

    $request = StoreRequestFactory::new()->make();

    patchJson("/api/v1/units/stores/$id", $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores/{id} 404', function () {
    $this->setRights([RightsAccessEnum::STORE_MAIN_DATA_EDIT, RightsAccessEnum::STORE_ACTIVITY_EDIT]);

    $request = StoreRequestFactory::new()->make();

    $id = 1;
    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('getStore')
        ->andReturn(StoreFactory::new()->makeResponseOne(['id' => $id]))
        ->shouldReceive('patchStore')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    patchJson('/api/v1/units/stores/1', $request)
        ->assertStatus(404);
});

test('POST /api/v1/units/store-enum-values:search 200', function ($filterCount, $request, $filterKey = null, $filterValue = null) {
    $this->setRights([RightsAccessEnum::STORE_LIST_READ]);

    $apiRequest = null;
    $this->mockBusinessUnitsStoresApi()
        ->shouldReceive('searchStores')
        ->once()
        ->withArgs(function (SearchStoresRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(StoreFactory::new()->makeResponseSearch());

    postJson('/api/v1/units/store-enum-values:search', $request)
        ->assertOk();

    if ($filterKey) {
        assertEquals($filterValue, $apiRequest->getFilter()->{$filterKey});
    }

    assertCount($filterCount, get_object_vars($apiRequest->getFilter()));
})->with([
    [1, ['filter' => ['id' => [1, 2]]], 'id', [1, 2]],
    [1, ['filter' => ['query' => 'foo']], 'name_like', 'foo'],
    [0, []],
]);
