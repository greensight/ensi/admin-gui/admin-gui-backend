<?php

use App\Domain\Units\Tests\Factories\StoreWorkingFactory;
use App\Http\ApiV1\Modules\Units\Tests\Stores\Factories\StoreWorkingRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\BuClient\ApiException;
use Ensi\BuClient\Dto\EmptyDataResponse;
use Ensi\BuClient\Dto\PatchStoreWorkingRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'stores');

test('POST /api/v1/units/stores-workings 200', function () {
    $this->setRights([RightsAccessEnum::STORE_WORKING_EDIT]);

    $request = StoreWorkingRequestFactory::new()->make();

    $this->mockBusinessUnitsStoreWorkingsApi()->allows([
        'createStoreWorking' => StoreWorkingFactory::new()->makeResponseOne(),
    ]);

    postJson('/api/v1/units/stores-workings', $request)
        ->assertStatus(200);
});

test('POST /api/v1/units/stores-workings 400', function () {
    $this->setRights([RightsAccessEnum::STORE_WORKING_EDIT]);

    $request = StoreWorkingRequestFactory::new()->make(['store_id' => null]);

    $this->mockBusinessUnitsStoreWorkingsApi()->shouldNotReceive('createStoreWorking');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores-workings', $request)
        ->assertStatus(400);
});

test('POST /api/v1/units/stores-workings 401', function () {
    actingAsUnathenticated();

    $request = StoreWorkingRequestFactory::new()->make();

    $this->mockBusinessUnitsStoreWorkingsApi()->shouldNotReceive('createStoreWorking');

    $this->skipNextOpenApiRequestValidation();
    postJson('/api/v1/units/stores-workings', $request)
        ->assertStatus(401);
});

test('POST /api/v1/units/stores-workings 403', function () {
    $this->setRights([]);

    $request = StoreWorkingRequestFactory::new()->make();
    postJson('/api/v1/units/stores-workings', $request)
        ->assertStatus(403);
});

test('DELETE /api/v1/units/stores-workings/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */

    $this->setRights([RightsAccessEnum::STORE_WORKING_EDIT]);

    $this->mockBusinessUnitsStoreWorkingsApi()->allows([
        'deleteStoreWorking' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/units/stores-workings/1')
        ->assertStatus(200);
});

test('DELETE /api/v1/units/stores-workings/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoreWorkingsApi()->shouldNotReceive('deleteStoreWorking');

    $this->skipNextOpenApiRequestValidation();
    deleteJson('/api/v1/units/stores-workings/1')
        ->assertStatus(401);
});

test('DELETE /api/v1/units/stores-workings/{id} 403', function () {
    $this->setRights([]);

    $request = StoreWorkingRequestFactory::new()->make();
    deleteJson('/api/v1/units/stores-workings/1', $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores-workings/{id} 200', function () {
    $this->setRights([RightsAccessEnum::STORE_WORKING_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStoreWorkingsApi()
        ->shouldReceive('patchStoreWorking')
        ->withArgs(fn (int $storeWorkingId, PatchStoreWorkingRequest $patchRequest) => $storeWorkingId === $id)
        ->andReturn(StoreWorkingFactory::new()->makeResponseOne(['id' => $id]));

    $request = StoreWorkingRequestFactory::new()->make();

    patchJson("/api/v1/units/stores-workings/$id", $request)
        ->assertStatus(200);
});

test('PATCH /api/v1/units/stores-workings/{id} 400', function () {
    $this->setRights([RightsAccessEnum::STORE_WORKING_EDIT]);

    $id = 1;
    $this->mockBusinessUnitsStoreWorkingsApi()->shouldNotReceive('patchStoreWorking');

    $request = StoreWorkingRequestFactory::new()->make(['store_id' => null]);

    $this->skipNextOpenApiRequestValidation();
    patchJson("/api/v1/units/stores-workings/$id", $request)
        ->assertStatus(400);
});

test('PATCH /api/v1/units/stores-workings/{id} 401', function () {
    actingAsUnathenticated();

    $this->mockBusinessUnitsStoreWorkingsApi()->shouldNotReceive('patchStoreWorking');

    $request = StoreWorkingRequestFactory::new()->make();

    $this->skipNextOpenApiRequestValidation();
    patchJson('/api/v1/units/stores-workings/1', $request)
        ->assertStatus(401);
});

test('PATCH /api/v1/units/stores-workings/{id} 403', function () {
    $this->setRights([]);

    $request = StoreWorkingRequestFactory::new()->make();
    patchJson('/api/v1/units/stores-workings/1', $request)
        ->assertStatus(403);
});

test('PATCH /api/v1/units/stores-workings/{id} 404', function () {
    $this->setRights([RightsAccessEnum::STORE_WORKING_EDIT]);

    $request = StoreWorkingRequestFactory::new()->make();

    $this->mockBusinessUnitsStoreWorkingsApi()
        ->shouldReceive('patchStoreWorking')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    patchJson('/api/v1/units/stores-workings/1', $request)
        ->assertStatus(404);
});
