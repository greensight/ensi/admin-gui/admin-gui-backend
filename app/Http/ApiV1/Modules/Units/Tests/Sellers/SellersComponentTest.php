<?php

use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Domain\Units\Tests\Factories\SellerFactory;
use App\Http\ApiV1\Modules\Units\Tests\Sellers\Factories\SellerRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\BuClient\Dto\PatchSellerRequest;
use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'units', 'sellers');


test('GET /api/v1/units/sellers/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_DETAIL_READ]);

    $sellerId = 1;

    $this->mockBusinessUnitsSellersApi()
        ->shouldReceive('getSeller')
        ->andReturn(SellerFactory::new()->makeResponse(['id' => $sellerId]));

    getJson("/api/v1/units/sellers/{$sellerId}")
        ->assertOk()
        ->assertJsonPath('data.id', $sellerId)
        ->assertJsonStructure(['data' => ['id', 'legal_name', 'status']]);
});

test('POST /api/v1/units/sellers:search 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_LIST_READ]);

    $sellerId = 1;
    $count = 2;

    $this->mockBusinessUnitsSellersApi()->allows([
        'searchSellers' => SellerFactory::new()
            ->makeResponseSearch([
                ['id' => $sellerId],
                ['id' => $sellerId + 1],
            ], $count),
    ]);

    $request = [
        'filter' => ['id' => [$sellerId]],
    ];

    postJson("/api/v1/units/sellers:search", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'legal_name', 'status']]])
        ->assertJsonPath('data.0.id', $sellerId);
});

test('POST /api/v1/units/sellers:search include all 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_LIST_READ]);

    $sellerId = 1;
    $managerId = 1;
    $storesCount = 2;

    $this->mockBusinessUnitsSellersApi()->allows([
        'searchSellers' => SellerFactory::new()
            ->withStores($storesCount)
            ->makeResponseSearch([['id' => $sellerId, 'manager_id' => $managerId]]),
    ]);

    $this->mockAdminAuthUsersApi()->allows([
        'searchUsersAsync' => PromiseFactory::make(
            AdminUserFactory::new()->makeResponseSearch([['id' => $managerId]])
        ),
    ]);

    $request = [
        'filter' => ['id' => [$sellerId]],
        'include' => ['manager', 'manager.roles', 'stores'],
    ];

    postJson("/api/v1/units/sellers:search", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => [['id', 'legal_name', 'status', 'manager', 'stores']]])
        ->assertJsonPath('data.0.id', $sellerId)
        ->assertJsonPath('data.0.manager.id', $managerId)
        ->assertJsonCount($storesCount, 'data.0.stores');

});

test('POST /api/v1/units/seller-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_LIST_READ]);

    $this->mockBusinessUnitsSellersApi()->allows([
        'searchSellers' => SellerFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/units/seller-enum-values:search", ['filter' => [$key => $value]])->assertOk();
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);

test('GET /api/v1/units/sellers:meta 200', function () {
    $this->setRights([RightsAccessEnum::SELLER_LIST_READ]);

    getJson('/api/v1/units/sellers:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('POST /api/v1/units/sellers 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_CREATE]);

    $this->mockBusinessUnitsSellersApi()
        ->shouldReceive('createSeller')
        ->andReturn(SellerFactory::new()->makeResponse());

    $request = SellerRequestFactory::new()->make();

    postJson('/api/v1/units/sellers', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'legal_name', 'status']]);
});

test('PATCH /api/v1/units/sellers/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::SELLER_DETAIL_EDIT]);

    $sellerId = 1;
    $this->mockBusinessUnitsSellersApi()
        ->shouldReceive('patchSeller')
        ->withArgs(fn (int $id, PatchSellerRequest $seller) => $id == $sellerId)
        ->andReturn(SellerFactory::new()->makeResponse(['id' => $sellerId]));

    $request = SellerRequestFactory::new()->make();

    patchJson("/api/v1/units/sellers/{$sellerId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $sellerId)
        ->assertJsonStructure(['data' => ['id', 'legal_name', 'status']]);
});
