<?php

namespace App\Http\ApiV1\Modules\Units\Tests\Sellers\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseAddressFactory;
use Ensi\BuClient\Dto\SellerStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class SellerRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'manager_id' => $this->faker->nullable()->modelId(),
            'legal_name' => $this->faker->unique()->company(),
            'legal_address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'fact_address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'inn' => $this->faker->nullable()->numerify('##########'),
            'kpp' => $this->faker->nullable()->numerify('#########'),
            'payment_account' => $this->faker->nullable()->numerify('####################'),
            'correspondent_account' => $this->faker->nullable()->numerify('####################'),
            'bank' => $this->faker->nullable()->company(),
            'bank_address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'bank_bik' => $this->faker->nullable()->numerify('#########'),
            'status' => $this->faker->randomElement(SellerStatusEnum::getAllowableEnumValues()),
            'site' => $this->faker->nullable()->url(),
            'info' => $this->faker->nullable()->text(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
