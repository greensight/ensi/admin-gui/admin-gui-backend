<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteRoleFromAdminUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'role_id' => ['required', 'integer'],
        ];
    }
}
