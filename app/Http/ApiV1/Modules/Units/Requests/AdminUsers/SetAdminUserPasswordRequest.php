<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SetAdminUserPasswordRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'token' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }

    public function getUserPassword(): string
    {
        return $this->input('password');
    }

    public function getPasswordToken(): string
    {
        return $this->input('token');
    }
}
