<?php

namespace App\Http\ApiV1\Modules\Units\Requests\AdminUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateAdminUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['required'],
            'active' => ['boolean'],
            'password' => ['nullable'],
            'first_name' => ['nullable'],
            'last_name' => ['nullable'],
            'middle_name' => ['nullable'],
            'timezone' => ['required', 'timezone'],
            'phone' => ['required', 'regex:/^\+7\d{10}$/'],
            'email' => ['required', 'email'],
        ];
    }
}
