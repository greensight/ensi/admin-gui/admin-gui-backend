<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateSellerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            "seller_id" => ['required', 'int'],
            "login" => ['required', 'string'],
            "active" => ['required', 'bool'],
            "password" => ['required', 'string'],
            "first_name" => ['required', 'string'],
            "last_name" => ['required', 'string'],
            "middle_name" => ['required', 'string'],
            "phone" => ['required', 'regex:/^\+7\d{10}$/'],
            "email" => ['required', 'email'],
        ];
    }
}
