<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchSellerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            "seller_id" => ['sometimes', 'int'],
            "login" => ['sometimes', 'string'],
            "active" => ['sometimes', 'bool'],
            "password" => ['sometimes', 'string'],
            "first_name" => ['sometimes', 'string'],
            "last_name" => ['sometimes', 'string'],
            "middle_name" => ['sometimes', 'string'],
            "phone" => ['sometimes', 'regex:/^\+7\d{10}$/'],
            "email" => ['sometimes', 'email'],
        ];
    }
}
