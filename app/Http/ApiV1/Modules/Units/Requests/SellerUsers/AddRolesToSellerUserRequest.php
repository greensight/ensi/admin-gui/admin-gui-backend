<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class AddRolesToSellerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'roles' => ['required', 'array'],
            'roles.*' => ['integer'],
            'expires' => ['nullable', 'date'],
        ];
    }
}
