<?php

namespace App\Http\ApiV1\Modules\Units\Requests\SellerUsers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteRoleFromSellerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'role_id' => ['required', 'integer'],
        ];
    }
}
