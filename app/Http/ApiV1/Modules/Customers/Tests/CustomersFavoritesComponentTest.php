<?php

use App\Domain\Catalog\Tests\Factories\Products\ProductFactory;
use App\Domain\Customers\Tests\Factories\CustomerFavoriteFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Favorites\ClearFavoriteRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Favorites\CreateFavoriteRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Favorites\DeleteFavoriteRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\CrmClient\Dto\EmptyDataResponse;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/favorites 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->setRights([RightsAccessEnum::CUSTOMER_DETAIL_EDIT]);

    $this->mockCrmCustomerFavoritesApi()->allows([
        'createCustomerFavorites' => new EmptyDataResponse(),
    ]);
    $request = CreateFavoriteRequestFactory::new()->make();

    postJson('/api/v1/customers/favorites', $request)
        ->assertOk();
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/customers/favorites 403', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = CreateFavoriteRequestFactory::new()->make();

    postJson('/api/v1/customers/favorites', $request)
        ->assertForbidden();
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/customers/favorites:delete-product 200', function () {
    $this->setRights([RightsAccessEnum::CUSTOMER_DETAIL_EDIT]);

    $this->mockCrmCustomerFavoritesApi()->allows([
        'deleteCustomerFavorites' => new EmptyDataResponse(),
    ]);
    $request = DeleteFavoriteRequestFactory::new()->make();

    postJson('/api/v1/customers/favorites:delete-product', $request)
        ->assertOk();
});

test('POST /api/v1/customers/favorites:delete-product 403', function () {
    $request = DeleteFavoriteRequestFactory::new()->make();

    postJson('/api/v1/customers/favorites:delete-product', $request)
        ->assertForbidden();
});

test('POST /api/v1/customers/favorites:clear 200', function () {
    $this->setRights([RightsAccessEnum::CUSTOMER_DETAIL_EDIT]);

    $this->mockCrmCustomerFavoritesApi()->allows([
        'clearCustomerFavorites' => new EmptyDataResponse(),
    ]);
    $request = ClearFavoriteRequestFactory::new()->make();

    postJson('/api/v1/customers/favorites:clear', $request)
        ->assertOk();
});

test('POST /api/v1/customers/favorites:clear 403', function () {
    $request = ClearFavoriteRequestFactory::new()->make();

    postJson('/api/v1/customers/favorites:clear', $request)
        ->assertForbidden();
});

test('POST /api/v1/customers/favorites:search 200', function () {
    $this->setRights([RightsAccessEnum::CUSTOMER_DETAIL_READ]);

    $productId = 1;
    $this->mockPimProductsApi()->allows([
        'searchProductsAsync' => PromiseFactory::make(ProductFactory::new()->makeResponseSearch([['id' => $productId]])),
    ]);
    $this->mockCrmCustomerFavoritesApi()->allows([
        'searchCustomerFavorite' => CustomerFavoriteFactory::new()->makeResponseSearch([
            ['id' => 2, 'product_id' => $productId],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
        'include' => ['products'],
    ];

    postJson('/api/v1/customers/favorites:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'product']]])
        ->assertJsonPath('data.0.id', 2)
        ->assertJsonPath('data.0.product.id', $productId);
});

test('POST /api/v1/customers/favorites:search 403', function () {
    $request = [
        'filter' => ['name' => 'foo'],
        'include' => ['products'],
    ];

    postJson('/api/v1/customers/favorites:search', $request)
        ->assertForbidden();
});
