<?php

use App\Domain\Customers\Tests\Factories\CustomerSubscribeFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Subscribes\ClearSubscribeRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Subscribes\CreateSubscribeRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Subscribes\DeleteSubscribeRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CrmClient\Dto\EmptyDataResponse;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/product-subscribes 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmProductSubscribesApi()->allows([
        'createProductSubscribe' => CustomerSubscribeFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = CreateSubscribeRequestFactory::new()->make();

    postJson('/api/v1/customers/product-subscribes', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'product_id', 'customer_id']]);
});

test('POST /api/v1/customers/product-subscribes:delete-product 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmProductSubscribesApi()->allows([
        'deleteProductFromProductSubscribes' => new EmptyDataResponse(),
    ]);
    $request = DeleteSubscribeRequestFactory::new()->make();

    postJson('/api/v1/customers/product-subscribes:delete-product', $request)
        ->assertOk();
});

test('POST /api/v1/customers/product-subscribes:clear 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmProductSubscribesApi()->allows([
        'clearProductSubscribes' => new EmptyDataResponse(),
    ]);
    $request = ClearSubscribeRequestFactory::new()->make();

    postJson('/api/v1/customers/product-subscribes:clear', $request)
        ->assertOk();
});

test('POST /api/v1/customers/product-subscribes:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmProductSubscribesApi()->allows([
        'searchProductSubscribes' => CustomerSubscribeFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/product-subscribes:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'product_id', 'customer_id']]])
        ->assertJsonPath('data.0.id', 2);
});
