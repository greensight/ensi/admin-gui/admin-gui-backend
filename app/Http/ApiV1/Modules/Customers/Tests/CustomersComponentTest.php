<?php

use App\Domain\Customers\Tests\Factories\CustomerFactory;
use App\Domain\Customers\Tests\Factories\CustomerUserFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers\CreateCustomerRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers\PatchCustomerRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\EmptyDataResponse;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customers 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;
    $userId = 1;

    $this->mockCustomersCustomersApi()->allows([
        'createCustomer' => CustomerFactory::new()->makeResponse(['id' => $id, 'user_id' => $userId]),
    ]);
    $this->mockCustomersAuthUsersApi()->allows([
        'getUser' => CustomerUserFactory::new()->makeResponse(['user_id' => $userId]),
    ]);
    $request = CreateCustomerRequestFactory::new()->make();

    postJson('/api/v1/customers/customers', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id);
});

test('GET /api/v1/customers/customers/{id} 200', function () {
    $id = 1;
    $userId = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersCustomersApi()->allows([
        'getCustomer' => CustomerFactory::new()->makeResponse(['id' => $id, 'user_id' => $userId]),
    ]);
    $this->mockCustomersAuthUsersApi()->allows([
        'searchUsers' => CustomerUserFactory::new()->makeResponseSearch(['user_id' => $userId]),
    ]);

    getJson("/api/v1/customers/customers/$id")
        ->assertOk()
        ->assertJsonPath('data.id', $id);
});

test('GET /api/v1/customers/customers/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersCustomersApi()
        ->shouldReceive('getCustomer')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/customers/customers/404')
        ->assertNotFound();
});

test('PATCH /api/v1/customers/customers/{id} 200', function () {
    $id = 1;
    $userId = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersCustomersApi()->allows([
        'patchCustomer' => CustomerFactory::new()->makeResponse(['id' => $id, 'user_id' => $userId]),
    ]);
    $this->mockCustomersAuthUsersApi()->allows([
        'getUser' => CustomerUserFactory::new()->makeResponse(['user_id' => $userId]),
    ]);

    $request = PatchCustomerRequestFactory::new()->make();

    patchJson("/api/v1/customers/customers/$id", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/customers/customer-enum-values:search 200 check nullable', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $user_id = 1;
    $email = 'test@mail.com';
    $this->mockCustomersCustomersApi()->allows([
        'searchCustomers' => CustomerFactory::new()->makeResponseSearch([['user_id' => $user_id, 'email' => $email]]),
    ]);

    postJson("/api/v1/customers/customer-enum-values:search", ['filter' => ['query' => 'test']])->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/customers/customer-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentTestCase $this */
    $user_id = 1;
    $email = 'test@mail.com';
    $this->mockCustomersCustomersApi()->allows([
        'searchCustomers' => CustomerFactory::new()->makeResponseSearch([['user_id' => $user_id, 'email' => $email]]),
    ]);

    postJson("/api/v1/customers/customer-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2]],
    ['query', 'test'],
]);


test('POST /api/v1/customers/customer-enum-values:search 200 email nullable', function () {
    /** @var ApiV1ComponentTestCase $this */
    $user_id = 1;
    $email = null;
    $this->mockCustomersCustomersApi()->allows([
        'searchCustomers' => CustomerFactory::new()->makeResponseSearch([['user_id' => $user_id, 'email' => $email, 'is_deleted' => false]]),
    ]);

    postJson("/api/v1/customers/customer-enum-values:search", ['filter' => ["id" => [$user_id]]])
        ->assertStatus(200)
        ->assertJsonPath('data.0.title', "Email не указан");
});

test('POST /api/v1/customers/customers/{id}:delete-personal-data 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersCustomersApi()
        ->expects('deletePersonalData')
        ->andReturn(new EmptyDataResponse());

    postJson('/api/v1/customers/customers/1:delete-personal-data')
        ->assertOk();
});
