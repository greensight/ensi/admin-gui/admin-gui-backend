<?php

use App\Domain\Customers\Tests\Factories\CustomerPreferenceFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/preferences:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmPreferencesApi()->allows([
        'searchPreferences' => CustomerPreferenceFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/preferences:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'customer_id', 'attribute_name', 'attribute_value', 'product_count', 'product_sum']]])
        ->assertJsonPath('data.0.id', 2);
});
