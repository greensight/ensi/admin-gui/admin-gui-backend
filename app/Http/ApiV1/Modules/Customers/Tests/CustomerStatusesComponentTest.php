<?php

use App\Domain\Customers\Tests\Factories\CustomerStatusFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Statuses\CreateStatusRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Statuses\PatchStatusRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\EmptyDataResponse;
use Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/statuses 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersStatusesApi()->allows([
        'createCustomerStatus' => CustomerStatusFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = CreateStatusRequestFactory::new()->make();

    postJson('/api/v1/customers/statuses', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'name']]);
});

test('GET /api/v1/customers/statuses/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersStatusesApi()->allows([
        'getCustomerStatus' => CustomerStatusFactory::new()->makeResponse(['id' => $id]),
    ]);

    getJson("/api/v1/customers/statuses/$id")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'name']]);
});

test('GET /api/v1/customers/statuses/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersStatusesApi()
        ->shouldReceive('getCustomerStatus')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/customers/statuses/404')
        ->assertNotFound();
});

test('DELETE /api/v1/customers/statuses/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersStatusesApi()->allows([
        'deleteCustomerStatus' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/customers/statuses/404')
        ->assertOk();
});

test('PATCH /api/v1/customers/statuses/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersStatusesApi()->allows([
        'patchCustomerStatus' => CustomerStatusFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchStatusRequestFactory::new()->make();

    patchJson("/api/v1/customers/statuses/$id", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'name']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/customers/statuses:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersStatusesApi()->allows([
        'searchCustomerStatuses' => CustomerStatusFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/statuses:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'name']]])
        ->assertJsonPath('data.0.id', 2);
});

test('POST /api/v1/customers/statuses-enum-values:search 200', function ($filterCount, $request, $filterKey = null, $filterValue = null) {
    /** @var ApiV1ComponentTestCase $this */

    $apiRequest = null;
    $this->mockCustomersStatusesApi()
        ->shouldReceive('searchCustomerStatuses')
        ->once()
        ->withArgs(function (SearchCustomerStatusesRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(CustomerStatusFactory::new()->makeResponseSearch());

    postJson('/api/v1/customers/statuses-enum-values:search', $request)
        ->assertStatus(200);

    if ($filterKey) {
        assertEquals($filterValue, $apiRequest->getFilter()->{$filterKey});
    }

    assertCount($filterCount, get_object_vars($apiRequest->getFilter()));
})->with([
    [1, ['filter' => ['id' => [1, 2]]], 'id', [1, 2]],
    [1, ['filter' => ['query' => 'foo']], 'name_like', 'foo'],
    [0, []],
]);
