<?php

use App\Domain\Customers\Tests\Factories\CustomerInfoFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Info\CreateInfoRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Info\PatchInfoRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CrmClient\ApiException;
use Ensi\CrmClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customers-info 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmCustomersInfoApi()->allows([
        'createCustomerInfo' => CustomerInfoFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = CreateInfoRequestFactory::new()->make();

    postJson('/api/v1/customers/customers-info', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'customer_id']]);
});

test('GET /api/v1/customers/customers-info/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmCustomersInfoApi()->allows([
        'getCustomerInfo' => CustomerInfoFactory::new()->makeResponse(['id' => $id]),
    ]);

    getJson("/api/v1/customers/customers-info/$id")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'customer_id']]);
});

test('GET /api/v1/customers/customers-info/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmCustomersInfoApi()
        ->shouldReceive('getCustomerInfo')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/customers/customers-info/404')
        ->assertNotFound();
});

test('DELETE /api/v1/customers/customers-info/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmCustomersInfoApi()->allows([
        'deleteCustomerInfo' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/customers/customers-info/404')
        ->assertOk();
});

test('PATCH /api/v1/customers/customers-info/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmCustomersInfoApi()->allows([
        'patchCustomerInfo' => CustomerInfoFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchInfoRequestFactory::new()->make();

    patchJson("/api/v1/customers/customers-info/$id", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'customer_id']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/customers/customers-info:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmCustomersInfoApi()->allows([
        'searchCustomersInfo' => CustomerInfoFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/customers-info:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'customer_id']]])
        ->assertJsonPath('data.0.id', 2);
});
