<?php

use App\Domain\Customers\Tests\Factories\CustomerBonusOperationFactory;

use App\Http\ApiV1\Modules\Customers\Tests\Factories\BonusOperations\CreateBonusOperationRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\BonusOperations\PatchBonusOperationRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\CrmClient\ApiException;
use Ensi\CrmClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/bonus-operations 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;

    $this->mockCrmBonusOperationsApi()->allows([
        'createBonusOperation' => CustomerBonusOperationFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = CreateBonusOperationRequestFactory::new()->make();

    postJson('/api/v1/customers/bonus-operations', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'customer_id', 'order_number', 'bonus_amount', 'comment', 'activation_date', 'expiration_date']]);
});

test('GET /api/v1/customers/bonus-operations/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmBonusOperationsApi()->allows([
        'getBonusOperation' => CustomerBonusOperationFactory::new()->makeResponse(['id' => $id]),
    ]);

    getJson("/api/v1/customers/bonus-operations/$id")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'customer_id', 'order_number', 'bonus_amount', 'comment', 'activation_date', 'expiration_date']]);
});

test('GET /api/v1/customers/bonus-operations/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmBonusOperationsApi()
        ->shouldReceive('getBonusOperation')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/customers/bonus-operations/404')
        ->assertNotFound();
});

test('DELETE /api/v1/customers/bonus-operations/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmBonusOperationsApi()->allows([
        'deleteBonusOperation' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/customers/bonus-operations/404')
        ->assertOk();
});

test('PATCH /api/v1/customers/bonus-operations/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmBonusOperationsApi()->allows([
        'patchBonusOperation' => CustomerBonusOperationFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchBonusOperationRequestFactory::new()->make();

    patchJson("/api/v1/customers/bonus-operations/$id", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'customer_id', 'order_number', 'bonus_amount', 'comment', 'activation_date', 'expiration_date']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/customers/bonus-operations:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCrmBonusOperationsApi()->allows([
        'searchBonusOperations' => CustomerBonusOperationFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/bonus-operations:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'customer_id', 'order_number', 'bonus_amount', 'comment', 'activation_date', 'expiration_date']]])
        ->assertJsonPath('data.0.id', 2);
});
