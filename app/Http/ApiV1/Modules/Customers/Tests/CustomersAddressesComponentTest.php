<?php

use App\Domain\Customers\Tests\Factories\CustomerAddressFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Addresses\CreateAddressRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Addresses\PatchAddressRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CustomersClient\ApiException;
use Ensi\CustomersClient\Dto\EmptyDataResponse;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/addresses 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $id = 1;

    $this->mockCustomersAddressesApi()->allows([
        'createCustomerAddress' => CustomerAddressFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = CreateAddressRequestFactory::new()->make();

    postJson('/api/v1/customers/addresses', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'customer_id', 'default', 'address']]);
});

test('GET /api/v1/customers/addresses/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAddressesApi()->allows([
        'getCustomerAddress' => CustomerAddressFactory::new()->makeResponse(['id' => $id]),
    ]);

    getJson("/api/v1/customers/addresses/$id")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'customer_id', 'default', 'address']]);
});

test('GET /api/v1/customers/addresses/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAddressesApi()
        ->shouldReceive('getCustomerAddress')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/customers/addresses/404')
        ->assertNotFound();
});

test('DELETE /api/v1/customers/addresses/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAddressesApi()->allows([
        'deleteCustomerAddress' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/customers/addresses/404')
        ->assertOk();
});

test('PUT /api/v1/customers/addresses/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAddressesApi()->allows([
        'replaceCustomerAddress' => CustomerAddressFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchAddressRequestFactory::new()->make();

    putJson("/api/v1/customers/addresses/$id", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'customer_id', 'default', 'address']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/customers/addresses:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAddressesApi()->allows([
        'searchCustomerAddresses' => CustomerAddressFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/addresses:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'customer_id', 'default', 'address']]])
        ->assertJsonPath('data.0.id', 2);
});

test('POST /api/v1/customers/addresses/{id}:set-as-default 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAddressesApi()->allows([
        'setCustomerAddressesAsDefault' => new EmptyDataResponse(),
    ]);

    postJson("/api/v1/customers/addresses/$id:set-as-default")
        ->assertOk();
});
