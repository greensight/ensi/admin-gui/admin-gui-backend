<?php

use App\Domain\Customers\Tests\Factories\CustomerUserFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Users\CreateUserRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Users\PatchUserRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\CustomerAuthClient\ApiException;
use Ensi\CustomerAuthClient\Dto\EmptyDataResponse;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/users 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()->allows([
        'createUser' => CustomerUserFactory::new()->makeResponse(['id' => $id]),
    ]);
    $request = CreateUserRequestFactory::new()->make();

    postJson('/api/v1/customers/users', $request)
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'login', 'active']]);
});

test('GET /api/v1/customers/users/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()->allows([
        'getUser' => CustomerUserFactory::new()->makeResponse(['id' => $id]),
    ]);

    getJson("/api/v1/customers/users/$id")
        ->assertOk()
        ->assertJsonPath('data.id', $id)
        ->assertJsonStructure(['data' => ['id', 'login', 'active']]);
});

test('GET /api/v1/customers/users/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()
        ->shouldReceive('getUser')
        ->andThrowExceptions([new ApiException('NotFound', 404)]);

    getJson('/api/v1/customers/users/404')
        ->assertNotFound();
});

test('DELETE /api/v1/customers/users/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()->allows([
        'deleteUser' => new EmptyDataResponse(),
    ]);

    deleteJson('/api/v1/customers/users/404')
        ->assertOk();
});

test('PATCH /api/v1/customers/users/{id} 200', function () {
    $id = 1;

    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()->allows([
        'patchUser' => CustomerUserFactory::new()->makeResponse(['id' => $id]),
    ]);

    $request = PatchUserRequestFactory::new()->make();

    patchJson("/api/v1/customers/users/$id", $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'login', 'active']])
        ->assertJsonPath('data.id', $id);
});

test('POST /api/v1/customers/users:search 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()->allows([
        'searchUsers' => CustomerUserFactory::new()->makeResponseSearch([
            ['id' => 2],
            ['id' => 3],
        ], 2),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/users:search', $request)
        ->assertOk()
        ->assertJsonCount(2, 'data')
        ->assertJsonStructure(['data' => [['id', 'login', 'active']]])
        ->assertJsonPath('data.0.id', 2);
});

test('POST /api/v1/customers/users:search-one 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockCustomersAuthUsersApi()->allows([
        'searchUser' => CustomerUserFactory::new()->makeResponse(),
    ]);

    $request = [
        'filter' => ['name' => 'foo'],
    ];

    postJson('/api/v1/customers/users:search-one', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'login', 'active']]);
});

test('POST /api/v1/customers/users-enum-values:search 200', function ($filterCount, $request, $filterKey = null, $filterValue = null) {
    /** @var ApiV1ComponentTestCase $this */

    $apiRequest = null;
    $this->mockCustomersAuthUsersApi()
        ->shouldReceive('searchUsers')
        ->once()
        ->withArgs(function (SearchUsersRequest $request) use (&$apiRequest) {
            $apiRequest = $request;

            return true;
        })
        ->andReturn(CustomerUserFactory::new()->makeResponseSearch());

    postJson('/api/v1/customers/users-enum-values:search', $request)
        ->assertStatus(200);

    if ($filterKey) {
        assertEquals($filterValue, $apiRequest->getFilter()->{$filterKey});
    }

    assertCount($filterCount, get_object_vars($apiRequest->getFilter()));
})->with([
    [1, ['filter' => ['id' => [1, 2]]], 'id', [1, 2]],
    [1, ['filter' => ['query' => 'foo']], 'login_like', 'foo'],
    [0, []],
]);
