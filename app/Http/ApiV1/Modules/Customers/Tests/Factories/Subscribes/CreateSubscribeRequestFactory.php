<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Subscribes;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateSubscribeRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
