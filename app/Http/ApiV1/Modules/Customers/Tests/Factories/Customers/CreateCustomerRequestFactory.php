<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers;

use Ensi\CustomersClient\Dto\CustomerGenderEnum;
use Ensi\CustomersClient\Dto\File;
use Ensi\LaravelEnsiFilesystem\Models\Tests\Factories\EnsiFileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateCustomerRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->nullable()->modelId(),
            'status_id' => $this->faker->modelId(),
            'manager_id' => $this->faker->nullable()->modelId(),
            'yandex_metric_id' => $this->faker->nullable()->text(10),
            'google_analytics_id' => $this->faker->nullable()->text(10),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->nullable()->text(),
            'gender' => $this->faker->nullable()->randomElement(CustomerGenderEnum::getAllowableEnumValues()),
            'create_by_admin' => $this->faker->boolean(),
            'last_visit_date' => $this->faker->nullable()->carbon(),
            'city' => $this->faker->nullable()->city(),
            'birthday' => $this->faker->nullable()->date(),
            'comment_status' => $this->faker->nullable()->text(),
            'timezone' => $this->faker->timezone(),
            'avatar' => $this->faker->boolean() ? new File(EnsiFileFactory::new()->make()) : null,
            'new_email' => $this->faker->unique()->email(),
            'email_token' => $this->faker->unique()->text(15),
            'is_deleted' => false,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
