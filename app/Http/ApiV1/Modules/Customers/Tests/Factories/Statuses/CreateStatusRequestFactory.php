<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Statuses;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateStatusRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->text(20),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
