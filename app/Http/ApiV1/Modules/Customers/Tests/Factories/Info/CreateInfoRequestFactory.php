<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Info;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateInfoRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'kpi_sku_count' => $this->faker->nullable()->randomNumber(),
            'kpi_sku_price_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_order_count' => $this->faker->nullable()->randomNumber(),
            'kpi_shipment_count' => $this->faker->nullable()->randomNumber(),
            'kpi_delivered_count' => $this->faker->nullable()->randomNumber(),
            'kpi_delivered_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_refunded_count' => $this->faker->nullable()->randomNumber(),
            'kpi_refunded_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_canceled_count' => $this->faker->nullable()->randomNumber(),
            'kpi_canceled_sum' => $this->faker->nullable()->randomNumber(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
