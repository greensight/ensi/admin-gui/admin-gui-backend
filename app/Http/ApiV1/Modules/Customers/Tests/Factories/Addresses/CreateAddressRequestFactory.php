<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Addresses;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateAddressRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'address' => [
                'address_string' => $this->faker->address(),
                'post_index' => $this->faker->postcode(),
                'country_code' => $this->faker->countryCode(),
                'region' => $this->faker->nullable()->text(5),
                'region_guid' => $this->faker->nullable()->uuid(),
                'area' => $this->faker->nullable()->text(),
                'area_guid' => $this->faker->nullable()->uuid(),
                'city' => $this->faker->nullable()->city(),
                'city_guid' => $this->faker->uuid(),
                'street' => $this->faker->nullable()->streetAddress(),
                'house' => $this->faker->nullable()->buildingNumber(),
                'block' => $this->faker->nullable()->numerify('##'),
                'porch' => $this->faker->nullable()->numerify('##'),
                'intercom' => $this->faker->nullable()->numerify('##'),
                'floor' => $this->faker->nullable()->numerify('##'),
                'flat' => $this->faker->nullable()->numerify('###'),
                'comment' => $this->faker->nullable()->text(),
                'geo_lat' => $this->faker->latitude(),
                'geo_lon' => $this->faker->longitude(),
            ],
            'default' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
