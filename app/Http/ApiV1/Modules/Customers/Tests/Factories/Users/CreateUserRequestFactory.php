<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Users;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateUserRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'active' => $this->faker->boolean(),
            'login' => $this->faker->email(),
            'password' => $this->faker->password(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
