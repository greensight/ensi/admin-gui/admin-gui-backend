<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Favorites;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateFavoriteRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'product_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), 1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
