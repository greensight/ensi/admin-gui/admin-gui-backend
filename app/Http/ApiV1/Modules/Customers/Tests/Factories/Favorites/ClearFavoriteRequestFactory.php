<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Favorites;

use Ensi\LaravelTestFactories\BaseApiFactory;

class ClearFavoriteRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
