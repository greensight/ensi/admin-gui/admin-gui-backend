<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\BonusOperations;

use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateBonusOperationRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'order_number' => $this->faker->nullable()->randomNumber(),
            'bonus_amount' => $this->faker->randomNumber(),
            'comment' => $this->faker->nullable()->text(),
            'activation_date' => $this->faker->nullable()->date(),
            'expiration_date' => $this->faker->nullable()->date(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
