<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\ProductSubscribes;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteCustomerProductSubscribeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'product_id' => ['required', 'integer'],
        ];
    }
}
