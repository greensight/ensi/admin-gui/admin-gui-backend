<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Users;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchCustomerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['nullable'],
            'active' => ['nullable', 'boolean'],
            'password' => ['nullable'],
        ];
    }
}
