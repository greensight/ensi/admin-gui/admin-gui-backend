<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Users;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateCustomerUserRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'login' => ['required'],
            'active' => ['boolean'],
            'password' => ['required'],
        ];
    }
}
