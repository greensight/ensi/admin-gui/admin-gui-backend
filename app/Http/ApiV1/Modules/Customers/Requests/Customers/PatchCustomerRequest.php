<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Customers;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchCustomerRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'user_id' => ['nullable', 'integer'],
            'manager_id' => ['nullable', 'integer'],
            'yandex_metric_id' => ['nullable', 'string'],
            'google_analytics_id' => ['nullable', 'string'],
            'email' => ['nullable', 'string', 'email'],
            'phone' => ['nullable', 'regex:/^\+7\d{10}$/'],
            'first_name' => ['nullable', 'string'],
            'last_name' => ['nullable', 'string'],
            'middle_name' => ['nullable', 'string'],
            'gender' => ['nullable', 'integer'],
            'active' => ['nullable', 'boolean'],
            'create_by_admin' => ['nullable', 'boolean'],
            'avatar' => ['nullable', 'array'],
            'birthday' => ['nullable', 'string', 'date'],
            'comment_status' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'last_visit_date' => ['nullable', 'string', 'date'],
            'timezone' => ['nullable', 'timezone'],

            'status_id' => ['nullable', 'integer'],
        ];
    }
}
