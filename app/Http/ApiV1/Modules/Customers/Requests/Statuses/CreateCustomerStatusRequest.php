<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Statuses;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateCustomerStatusRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
        ];
    }
}
