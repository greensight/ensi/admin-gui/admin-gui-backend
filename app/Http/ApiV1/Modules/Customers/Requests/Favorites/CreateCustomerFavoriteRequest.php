<?php

namespace App\Http\ApiV1\Modules\Customers\Requests\Favorites;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateCustomerFavoriteRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'product_ids' => ['required', 'array', 'min:1'],
            'product_ids.*' => ['required', 'integer', 'distinct'],
        ];
    }
}
