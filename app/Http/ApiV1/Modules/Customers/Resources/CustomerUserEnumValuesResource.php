<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\CustomerAuthClient\Dto\User;

/** @mixin User */
class CustomerUserEnumValuesResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getLogin() ?: 'ПД удалены';
    }
}
