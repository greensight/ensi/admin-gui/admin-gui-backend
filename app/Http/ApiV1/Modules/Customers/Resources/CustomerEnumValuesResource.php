<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Data\CustomerData;
use App\Http\ApiV1\Support\Resources\EnumValueResource;

/** @mixin CustomerData */
class CustomerEnumValuesResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->customer->getId();
    }

    protected function getEnumTitle(): string
    {
        if ($this->customer->getIsDeleted()) {
            return "ПД удалены";
        }

        return $this->customer->getEmail() ?? "Email не указан";
    }
}
