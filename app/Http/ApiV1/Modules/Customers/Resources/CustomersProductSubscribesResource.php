<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CrmClient\Dto\ProductSubscribe;

/** @mixin ProductSubscribe */
class CustomersProductSubscribesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->getId(),
            "product_id" => $this->getProductId(),
            "customer_id" => $this->getCustomerId(),
        ];
    }
}
