<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CrmClient\Dto\Preference;

/** @mixin Preference */
class CustomersPreferencesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'customer_id' => $this->getCustomerId(),
            'attribute_name' => $this->getAttributeName(),
            'attribute_value' => $this->getAttributeValue(),
            'product_count' => $this->getProductCount(),
            'product_sum' => $this->getProductSum(),
        ];
    }
}
