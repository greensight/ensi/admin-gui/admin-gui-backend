<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CrmClient\Dto\BonusOperation;

/** @mixin BonusOperation */
class CustomersBonusOperationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'customer_id' => $this->getCustomerId(),
            'order_number' => $this->getOrderNumber(),
            'bonus_amount' => $this->getBonusAmount(),
            'comment' => $this->getComment(),
            'activation_date' => $this->getActivationDate(),
            'expiration_date' => $this->getExpirationDate(),
        ];
    }
}
