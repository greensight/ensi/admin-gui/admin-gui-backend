<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\CustomersClient\Dto\CustomerStatus;

/** @mixin CustomerStatus */
class CustomerStatusEnumValuesResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
