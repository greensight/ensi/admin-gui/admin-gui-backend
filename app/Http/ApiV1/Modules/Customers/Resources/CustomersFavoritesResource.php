<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Data\CustomerFavoriteData;
use App\Http\ApiV1\Modules\Catalog\Resources\Products\ProductsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CustomerFavoriteData */
class CustomersFavoritesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->favorite->getId(),
            'product' => ProductsResource::make($this->whenNotNull($this->product)),
        ];
    }
}
