<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\CustomersClient\Dto\CustomerAddress;

/** @mixin CustomerAddress */
class CustomersAddressesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->getId(),
            "customer_id" => $this->getCustomerId(),
            "default" => $this->getDefault(),
            "address" => [
                "address_string" => $this->getAddress()->getAddressString(),
                "post_index" => $this->getAddress()->getPostIndex(),
                "country_code" => $this->getAddress()->getCountryCode(),
                "region" => $this->getAddress()->getRegion(),
                "region_guid" => $this->getAddress()->getRegionGuid(),
                "area" => $this->getAddress()->getArea(),
                "area_guid" => $this->getAddress()->getAreaGuid(),
                "city" => $this->getAddress()->getCity(),
                "city_guid" => $this->getAddress()->getCityGuid(),
                "street" => $this->getAddress()->getStreet(),
                "house" => $this->getAddress()->getHouse(),
                "block" => $this->getAddress()->getBlock(),
                "porch" => $this->getAddress()->getPorch(),
                "intercom" => $this->getAddress()->getIntercom(),
                "floor" => $this->getAddress()->getFloor(),
                "flat" => $this->getAddress()->getFlat(),
                "comment" => $this->getAddress()->getComment(),
                "geo_lat" => $this->getAddress()->getGeoLat(),
                "geo_lon" => $this->getAddress()->getGeoLon(),
            ],
        ];
    }
}
