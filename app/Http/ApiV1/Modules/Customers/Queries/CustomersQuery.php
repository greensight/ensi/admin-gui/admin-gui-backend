<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Data\CustomerData;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\Dto\PaginationTypeEnum as PaginationTypeEnumCustomerAuth;
use Ensi\CustomerAuthClient\Dto\RequestBodyPagination as RequestBodyPaginationCustomerAuth;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\CustomerResponse;
use Ensi\CustomersClient\Dto\PaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\CustomersClient\Dto\SearchCustomersResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CustomersQuery extends AbstractCustomersQuery
{
    use QueryBuilderFilterEnumTrait;
    use QueryBuilderFindTrait;

    protected bool $withoutUsers = false;

    public function __construct(
        protected Request $httpRequest,
        protected readonly CustomersApi $customersApi,
        protected readonly UsersApi $usersApi
    ) {
        parent::__construct($httpRequest, SearchCustomersRequest::class);
    }

    protected function search($request): SearchCustomersResponse
    {
        return $this->customersApi->searchCustomers($request);
    }

    public function searchById($id, string $include): CustomerResponse
    {
        return $this->customersApi->getCustomer($id, $include);
    }

    protected function convertFindToItem($response): CustomerData
    {
        return current($this->convertArray([$response->getData()]));
    }

    protected function convertGetToItems($response): array
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $array): array
    {
        /** @var Collection|Customer[] $orders */
        $customers = collect($array);
        $users = $this->loadUsers($customers->pluck('user_id')->unique()->filter()->all());

        $customersData = [];
        foreach ($customers as $customer) {
            $user = $users->get($customer->getUserId());
            $customerData = new CustomerData($customer, $user);

            $customersData[] = $customerData;
        }

        return $customersData;
    }

    protected function loadUsers(array $userIds): Collection
    {
        if (!$userIds || $this->withoutUsers) {
            return collect();
        }

        $request = new SearchUsersRequest();
        $request->setFilter((object)[
            'id' => $userIds,
        ]);
        $request->setPagination(
            (new RequestBodyPaginationCustomerAuth())
                ->setLimit(count($userIds))
                ->setType(PaginationTypeEnumCustomerAuth::CURSOR)
        );
        $users = $this->usersApi->searchUsers($request)->getData();

        return collect($users)->keyBy('id');
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $this->withoutUsers = true;
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['email_like'] = $query;
            $filter['is_deleted'] = false;
        }

        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
