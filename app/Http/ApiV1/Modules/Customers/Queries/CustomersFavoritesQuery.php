<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Data\CustomerFavoriteData;
use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Dto\CustomerFavorite;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesRequest;
use Ensi\CrmClient\Dto\SearchCustomerFavoritesResponse;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\PaginationTypeEnum as PimPaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestBodyPagination;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchProductsResponse;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CustomersFavoritesQuery extends AbstractCrmQuery
{
    protected Collection $products;

    protected bool $loadProducts = false;

    public function __construct(
        protected Request $httpRequest,
        protected readonly CustomerFavoritesApi $customerFavoritesApi,
        protected readonly ProductsApi $productsApi
    ) {
        parent::__construct($httpRequest, SearchCustomerFavoritesRequest::class);
    }

    protected function search($request): SearchCustomerFavoritesResponse
    {
        return $this->customerFavoritesApi->searchCustomerFavorite($request);
    }

    protected function convertGetToItems($response): array
    {
        return $this->convertArray($response->getData());
    }

    protected function convertArray(array $favorites): array
    {
        /** @var Collection|CustomerFavorite[] $favorites */
        $favorites = collect($favorites);
        $productIds = $favorites->pluck('product_id')->all();

        $this->products = collect();

        $promises = [];
        if ($this->loadProducts && !empty($productIds)) {
            $promises[] = $this->loadProducts($productIds);
        }

        Utils::all(array_filter($promises))->wait();

        $favoritesData = [];
        foreach ($favorites as $favorite) {
            $product = $this->products->get($favorite->getProductId());
            $favoritesData[] = new CustomerFavoriteData($favorite, $product);
        }

        return $favoritesData;
    }

    protected function loadProducts(array $ids): PromiseInterface
    {
        $request = new SearchProductsRequest();
        $request->setFilter((object)['id' => $ids]);
        $request->setPagination((new PimRequestBodyPagination())->setLimit(count($ids))->setType(PimPaginationTypeEnum::CURSOR));

        return $this->productsApi->searchProductsAsync($request)
            ->then(function (SearchProductsResponse $response) {
                $this->products = collect($response->getData())->keyBy('id');
            });
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case 'products':
                    $this->loadProducts = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }
}
