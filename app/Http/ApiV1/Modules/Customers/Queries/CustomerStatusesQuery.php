<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CustomersClient\Api\StatusesApi;
use Ensi\CustomersClient\Dto\CustomerStatusResponse;
use Ensi\CustomersClient\Dto\PaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomerStatusesRequest;
use Ensi\CustomersClient\Dto\SearchCustomerStatusesResponse;
use Illuminate\Http\Request;

class CustomerStatusesQuery extends AbstractCustomersQuery
{
    use QueryBuilderFindTrait;
    use QueryBuilderFilterEnumTrait;

    public function __construct(
        protected Request $httpRequest,
        protected readonly StatusesApi $statusesApi
    ) {
        parent::__construct($httpRequest, SearchCustomerStatusesRequest::class);
    }

    protected function search($request): SearchCustomerStatusesResponse
    {
        return $this->statusesApi->searchCustomerStatuses($request);
    }

    public function searchById($id, string $include): CustomerStatusResponse
    {
        return $this->statusesApi->getCustomerStatus($id);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }

        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
