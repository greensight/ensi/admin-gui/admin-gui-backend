<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\Dto\CustomerAddressResponse;
use Ensi\CustomersClient\Dto\SearchCustomerAddressesRequest;
use Ensi\CustomersClient\Dto\SearchCustomerAddressesResponse;
use Illuminate\Http\Request;

class CustomersAddressQuery extends AbstractCustomersQuery
{
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected readonly AddressesApi $addressesApi
    ) {
        parent::__construct($httpRequest, SearchCustomerAddressesRequest::class);
    }

    protected function search($request): SearchCustomerAddressesResponse
    {
        return $this->addressesApi->searchCustomerAddresses($request);
    }

    public function searchById($id, string $include): CustomerAddressResponse
    {
        return $this->addressesApi->getCustomerAddress($id);
    }
}
