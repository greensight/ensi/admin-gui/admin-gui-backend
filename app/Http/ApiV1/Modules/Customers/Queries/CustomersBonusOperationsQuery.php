<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CrmClient\Api\BonusOperationsApi;
use Ensi\CrmClient\Dto\BonusOperationResponse;
use Ensi\CrmClient\Dto\SearchBonusOperationsRequest;
use Ensi\CrmClient\Dto\SearchBonusOperationsResponse;
use Illuminate\Http\Request;

class CustomersBonusOperationsQuery extends AbstractCrmQuery
{
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected readonly BonusOperationsApi $bonusOperationsApi
    ) {
        parent::__construct($httpRequest, SearchBonusOperationsRequest::class);
    }

    protected function search($request): SearchBonusOperationsResponse
    {
        return $this->bonusOperationsApi->searchBonusOperations($request);
    }

    public function searchById($id, string $include): BonusOperationResponse
    {
        return $this->bonusOperationsApi->getBonusOperation($id);
    }
}
