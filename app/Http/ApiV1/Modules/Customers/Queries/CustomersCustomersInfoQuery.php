<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\CrmClient\Api\CustomersInfoApi;
use Ensi\CrmClient\Dto\CustomerInfoResponse;
use Ensi\CrmClient\Dto\SearchCustomersInfoRequest;
use Ensi\CrmClient\Dto\SearchCustomersInfoResponse;
use Illuminate\Http\Request;

class CustomersCustomersInfoQuery extends AbstractCrmQuery
{
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected readonly CustomersInfoApi $customersInfoApi
    ) {
        parent::__construct($httpRequest, SearchCustomersInfoRequest::class);
    }

    protected function search($request): SearchCustomersInfoResponse
    {
        return $this->customersInfoApi->searchCustomersInfo($request);
    }

    public function searchById($id, string $include): CustomerInfoResponse
    {
        return $this->customersInfoApi->getCustomerInfo($id);
    }
}
