<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use Ensi\CrmClient\Api\PreferencesApi;
use Ensi\CrmClient\Dto\SearchPreferencesRequest;
use Ensi\CrmClient\Dto\SearchPreferencesResponse;
use Illuminate\Http\Request;

class CustomersPreferencesQuery extends AbstractCrmQuery
{
    public function __construct(
        protected Request $httpRequest,
        protected readonly PreferencesApi $preferencesApi
    ) {
        parent::__construct($httpRequest, SearchPreferencesRequest::class);
    }

    protected function search($request): SearchPreferencesResponse
    {
        return $this->preferencesApi->searchPreferences($request);
    }
}
