<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomerAuthClient\Dto\PaginationTypeEnum;
use Ensi\CustomerAuthClient\Dto\RequestBodyPagination;
use Ensi\CustomerAuthClient\Dto\SearchUsersRequest;
use Ensi\CustomerAuthClient\Dto\SearchUsersResponse;
use Ensi\CustomerAuthClient\Dto\UserResponse;
use Illuminate\Http\Request;

class CustomersUserQuery extends AbstractCustomerAuthQuery
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderFilterEnumTrait;

    public function __construct(
        protected Request $httpRequest,
        protected readonly UsersApi $usersApi
    ) {
        parent::__construct($httpRequest, SearchUsersRequest::class);
    }

    protected function requestFirstClass(): string
    {
        return $this->requestGetClass;
    }

    protected function searchById($id, string $include): UserResponse
    {
        return $this->usersApi->getUser($id, $include);
    }

    protected function search($request): SearchUsersResponse
    {
        return $this->usersApi->searchUsers($request);
    }

    protected function searchOne($request): UserResponse
    {
        return $this->usersApi->searchUser($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['login_like'] = $query;
        }

        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
