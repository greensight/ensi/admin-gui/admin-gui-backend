<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Addresses\CreateCustomerAddressAction;
use App\Domain\Customers\Actions\Addresses\DeleteCustomerAddressAction;
use App\Domain\Customers\Actions\Addresses\ReplaceCustomerAddressAction;
use App\Domain\Customers\Actions\Addresses\SetDefaultCustomerAddressAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersAddressQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Addresses\CreateCustomerAddressRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Addresses\ReplaceCustomerAddressRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersAddressesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersAddressesController
{
    public function search(CustomersAddressQuery $query): AnonymousResourceCollection
    {
        return CustomersAddressesResource::collectPage($query->get());
    }

    public function create(
        CreateCustomerAddressRequest $request,
        CreateCustomerAddressAction $action
    ): CustomersAddressesResource {
        return new CustomersAddressesResource($action->execute($request->validated()));
    }

    public function get(int $id, CustomersAddressQuery $query): CustomersAddressesResource
    {
        return new CustomersAddressesResource($query->find($id));
    }

    public function replace(
        int $id,
        ReplaceCustomerAddressRequest $request,
        ReplaceCustomerAddressAction $action
    ): CustomersAddressesResource {
        return new CustomersAddressesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteCustomerAddressAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function setDefault(int $addressId, SetDefaultCustomerAddressAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }
}
