<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\BonusOperations\CreateBonusOperationAction;
use App\Domain\Customers\Actions\BonusOperations\DeleteBonusOperationAction;
use App\Domain\Customers\Actions\BonusOperations\PatchBonusOperationAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersBonusOperationsQuery;
use App\Http\ApiV1\Modules\Customers\Requests\BonusOperations\CreateBonusOperationRequest;
use App\Http\ApiV1\Modules\Customers\Requests\BonusOperations\PatchBonusOperationRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersBonusOperationsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class CustomersBonusOperationsController
{
    public function get(int $id, CustomersBonusOperationsQuery $query): Responsable
    {
        return new CustomersBonusOperationsResource($query->find($id));
    }

    public function search(CustomersBonusOperationsQuery $query): Responsable
    {
        return CustomersBonusOperationsResource::collectPage($query->get());
    }

    public function create(
        CreateBonusOperationRequest $request,
        CreateBonusOperationAction $action
    ): Responsable {
        return new CustomersBonusOperationsResource($action->execute($request->validated()));
    }

    public function patch(
        int $id,
        PatchBonusOperationRequest $request,
        PatchBonusOperationAction $action
    ): Responsable {
        return new CustomersBonusOperationsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteBonusOperationAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
