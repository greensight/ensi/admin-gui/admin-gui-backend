<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Users\CreateCustomerUserAction;
use App\Domain\Customers\Actions\Users\DeleteCustomerUserAction;
use App\Domain\Customers\Actions\Users\PatchCustomerUserAction;
use App\Domain\Customers\Actions\Users\RefreshPasswordTokenAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersUserQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Users\CreateCustomerUserRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Users\PatchCustomerUserRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerUserEnumValuesResource;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerUsersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class CustomerUsersController
{
    public function search(CustomersUserQuery $query): Responsable
    {
        return CustomerUsersResource::collectPage($query->get());
    }

    public function searchEnumValues(CustomersUserQuery $query): Responsable
    {
        return CustomerUserEnumValuesResource::collection($query->searchEnums());
    }

    public function searchOne(CustomersUserQuery $query): Responsable
    {
        return new CustomerUsersResource($query->first());
    }

    public function create(CreateCustomerUserRequest $request, CreateCustomerUserAction $action): Responsable
    {
        return new CustomerUsersResource($action->execute($request->validated()));
    }

    public function get(int $id, CustomersUserQuery $query): Responsable
    {
        return new CustomerUsersResource($query->find($id));
    }

    public function patch(int $id, PatchCustomerUserRequest $request, PatchCustomerUserAction $action): Responsable
    {
        return new CustomerUsersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $customerUserId, DeleteCustomerUserAction $action): Responsable
    {
        $action->execute($customerUserId);

        return new EmptyResource();
    }

    public function refreshPasswordToken(int $customerUserId, RefreshPasswordTokenAction $action): Responsable
    {
        $action->execute($customerUserId);

        return new EmptyResource();
    }
}
