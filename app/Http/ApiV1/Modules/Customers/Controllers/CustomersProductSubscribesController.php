<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\ProductSubscribes\ClearCustomerProductSubscribesAction;
use App\Domain\Customers\Actions\ProductSubscribes\CreateCustomerProductSubscribeAction;
use App\Domain\Customers\Actions\ProductSubscribes\DeleteCustomerProductSubscribeAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersProductSubscribesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\ProductSubscribes\ClearCustomerProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\ProductSubscribes\CreateCustomerProductSubscribeRequest;
use App\Http\ApiV1\Modules\Customers\Requests\ProductSubscribes\DeleteCustomerProductSubscribeRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersProductSubscribesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class CustomersProductSubscribesController
{
    public function search(CustomersProductSubscribesQuery $query): Responsable
    {
        return CustomersProductSubscribesResource::collectPage($query->get());
    }

    public function create(
        CreateCustomerProductSubscribeRequest $request,
        CreateCustomerProductSubscribeAction $action
    ): Responsable {
        return new CustomersProductSubscribesResource($action->execute($request->validated()));
    }

    public function delete(
        DeleteCustomerProductSubscribeRequest $request,
        DeleteCustomerProductSubscribeAction $action
    ): Responsable {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function clear(
        ClearCustomerProductSubscribesRequest $request,
        ClearCustomerProductSubscribesAction $action
    ): Responsable {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
