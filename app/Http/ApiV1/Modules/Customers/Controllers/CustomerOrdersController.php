<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\OrderStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;

class CustomerOrdersController
{
    public function meta(
        OrderStatusEnumInfo $orderStatus,
        PaymentStatusEnumInfo $orderPaymentStatus,
        DeliveryMethodEnumInfo $deliveryMethod,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::string('number', '№ заказа')->sort(),
            Field::integer('price', 'Сумма')->listDefault()->filterDefault() ,
            Field::string('client_comment', 'Комментарий клиента'),
            Field::enum('status', 'Статус', $orderStatus)->listDefault()->filterDefault(),
            Field::enum('payment_status', 'Статус оплаты', $orderPaymentStatus)->listDefault(),
            Field::enum('delivery_method', 'Метод доставки', $deliveryMethod),
            Field::integer('delivery_price', 'Цена доставки'),
            Field::string('delivery_comment', 'Комментарий к доставке')->listDefault(),
            Field::string('delivery_address.address_string', 'Адрес доставки')->object(),
        ]);
    }
}
