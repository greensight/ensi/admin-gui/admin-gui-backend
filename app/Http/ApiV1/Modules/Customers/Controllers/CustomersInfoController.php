<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CustomersInfo\CreateCustomerInfoAction;
use App\Domain\Customers\Actions\CustomersInfo\DeleteCustomerInfoAction;
use App\Domain\Customers\Actions\CustomersInfo\PatchCustomerInfoAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersCustomersInfoQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CustomersInfo\CreateCustomerInfoRequest;
use App\Http\ApiV1\Modules\Customers\Requests\CustomersInfo\PatchCustomerInfoRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersInfoResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class CustomersInfoController
{
    public function search(CustomersCustomersInfoQuery $query): Responsable
    {
        return CustomersInfoResource::collectPage($query->get());
    }

    public function create(CreateCustomerInfoRequest $request, CreateCustomerInfoAction $action): Responsable
    {
        return new CustomersInfoResource($action->execute($request->validated()));
    }

    public function get(int $id, CustomersCustomersInfoQuery $query): Responsable
    {
        return new CustomersInfoResource($query->find($id));
    }

    public function patch(int $id, PatchCustomerInfoRequest $request, PatchCustomerInfoAction $action): Responsable
    {
        return new CustomersInfoResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteCustomerInfoAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
