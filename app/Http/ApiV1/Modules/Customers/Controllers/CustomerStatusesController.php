<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Statuses\CreateCustomerStatusAction;
use App\Domain\Customers\Actions\Statuses\DeleteCustomerStatusAction;
use App\Domain\Customers\Actions\Statuses\PatchCustomerStatusAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomerStatusesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Statuses\CreateCustomerStatusRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Statuses\PatchCustomerStatusRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerStatusEnumValuesResource;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerStatusesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomerStatusesController
{
    public function search(CustomerStatusesQuery $query): AnonymousResourceCollection
    {
        return CustomerStatusesResource::collectPage($query->get());
    }

    public function searchEnumValues(CustomerStatusesQuery $query): AnonymousResourceCollection
    {
        return CustomerStatusEnumValuesResource::collection($query->searchEnums());
    }

    public function create(CreateCustomerStatusRequest $request, CreateCustomerStatusAction  $action): Responsable
    {
        return new CustomerStatusesResource($action->execute($request->validated()));
    }

    public function get(int $statusId, CustomerStatusesQuery $query): CustomerStatusesResource
    {
        return new CustomerStatusesResource($query->find($statusId));
    }

    public function patch(int $id, PatchCustomerStatusRequest $request, PatchCustomerStatusAction   $action): Responsable
    {
        return new CustomerStatusesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $statusId, DeleteCustomerStatusAction $action): EmptyResource
    {
        $action->execute($statusId);

        return new EmptyResource();
    }
}
