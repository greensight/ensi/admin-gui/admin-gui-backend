<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Common\Data\Meta\Enum\Customers\CustomerGenderEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Customers\CustomerStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Customers\CustomerUserEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Units\AdminUserEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Customers\Actions\Customers\CreateCustomerAction;
use App\Domain\Customers\Actions\Customers\DeleteAvatarAction;
use App\Domain\Customers\Actions\Customers\DeleteCustomerPersonalDataAction;
use App\Domain\Customers\Actions\Customers\PatchCustomerAction;
use App\Domain\Customers\Actions\Customers\UploadAvatarAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\CreateCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\PatchCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Customers\UploadCustomerAvatarRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomerEnumValuesResource;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersController
{
    public function searchEnumValues(CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomerEnumValuesResource::collection($query->searchEnums());
    }

    public function create(CreateCustomerRequest $request, CreateCustomerAction $action): CustomersResource
    {
        return CustomersResource::make($action->execute($request->validated()));
    }

    public function get(int $id, CustomersQuery $query): CustomersResource
    {
        return CustomersResource::make($query->find($id));
    }

    public function patch(int $id, PatchCustomerRequest $request, PatchCustomerAction $action): Responsable
    {
        return CustomersResource::make($action->execute($id, $request->validated()));
    }

    public function uploadAvatar(
        int $customerId,
        UploadCustomerAvatarRequest $request,
        UploadAvatarAction $action
    ): CustomersResource {
        return CustomersResource::make($action->execute($customerId, $request->getFile()));
    }

    public function deleteAvatar(int $customerId, DeleteAvatarAction $action): CustomersResource
    {
        return CustomersResource::make($action->execute($customerId));
    }

    public function deletePersonalData(int $customerId, DeleteCustomerPersonalDataAction $action): EmptyResource
    {
        $action->execute($customerId);

        return new EmptyResource();
    }

    public function search(CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomersResource::collectPage($query->get());
    }

    public function meta(
        CustomerUserEnumInfo $customerUserEnumInfo,
        CustomerStatusEnumInfo $customerStatusEnumInfo,
        AdminUserEnumInfo $adminUserEnumInfo,
        CustomerGenderEnumInfo $customerGenderEnumInfo,
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault(),
            Field::enum('user_id', 'Пользователь', $customerUserEnumInfo)->listDefault(),
            Field::enum('manager_id', 'Менеджер', $adminUserEnumInfo),
            Field::string('yandex_metric_id', 'ID пользователя в YandexMetric'),
            Field::string('google_analytics_id', 'ID пользователя в GoogleAnalytics'),
            Field::enum('status_id', 'Статус', $customerStatusEnumInfo),
            Field::boolean('active', 'Активность')->listDefault(),
            Field::boolean('is_deleted', 'Пользователь удален'),
            Field::text('email', 'Почта')->sort()->listDefault(),
            Field::phone('phone', 'Телефон')->sort()->listDefault()->filterDefault(),
            Field::text('first_name', 'Имя')->sort()->listDefault()->filterDefault(),
            Field::text('last_name', 'Фамилия')->sort()->listDefault()->filterDefault(),
            Field::text('middle_name', 'Отчество')->sort()->listDefault(),
            Field::enum('gender', 'Гендер', $customerGenderEnumInfo),
            Field::boolean('create_by_admin', 'Пользователь создан администратором')->resetSort(),
            Field::text('city', 'Город')->listDefault(),
            Field::datetime('birthday', 'Дата рождения'),
            Field::datetime('last_visit_date', 'Дата последней авторизации'),
            Field::string('comment_status', 'Комментарий к статусу'),
            Field::string('timezone', 'Временная зона'),
            Field::datetime('created_at', 'Дата создания')->listDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault(),
        ]);
    }
}
