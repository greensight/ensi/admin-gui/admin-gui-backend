<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Favorites\ClearCustomerFavoritesAction;
use App\Domain\Customers\Actions\Favorites\CreateCustomerFavoriteAction;
use App\Domain\Customers\Actions\Favorites\DeleteCustomerFavoriteAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersFavoritesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\Favorites\ClearCustomerFavoritesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Favorites\CreateCustomerFavoriteRequest;
use App\Http\ApiV1\Modules\Customers\Requests\Favorites\DeleteCustomerFavoriteRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersFavoritesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class CustomersFavoritesController
{
    public function search(CustomersFavoritesQuery $query): Responsable
    {
        return CustomersFavoritesResource::collectPage($query->get());
    }

    public function create(
        CreateCustomerFavoriteRequest $request,
        CreateCustomerFavoriteAction $action
    ): Responsable {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function delete(
        DeleteCustomerFavoriteRequest $request,
        DeleteCustomerFavoriteAction $action
    ): Responsable {
        $action->execute($request->validated());

        return new EmptyResource();
    }

    public function clear(ClearCustomerFavoritesRequest $request, ClearCustomerFavoritesAction $action): Responsable
    {
        $action->execute($request->validated());

        return new EmptyResource();
    }
}
