<?php

namespace App\Http\ApiV1\Modules\Customers\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CustomersControllerPolicy
{
    use HandlesAuthorization;

    public function deletePersonalData(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DELETE_PERSONAL_DATA,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DETAIL_EDIT,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DETAIL_READ,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_LIST_READ,
        ]);
    }
}
