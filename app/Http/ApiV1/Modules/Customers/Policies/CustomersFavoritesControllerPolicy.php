<?php

namespace App\Http\ApiV1\Modules\Customers\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CustomersFavoritesControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DETAIL_READ,
        ]);
    }

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DETAIL_EDIT,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DETAIL_EDIT,
        ]);
    }

    public function clear(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::CUSTOMER_DETAIL_EDIT,
        ]);
    }
}
