<?php

namespace App\Http\ApiV1\Modules\Customers\Policies;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CustomerOrdersControllerPolicy
{
    use HandlesAuthorization;

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_LIST_READ,
        ]);
    }
}
