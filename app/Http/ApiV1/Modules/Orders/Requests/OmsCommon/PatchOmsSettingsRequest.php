<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\OmsCommon;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchOmsSettingsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'settings' => ['required', 'min:1'],
            'settings.*.id' => ['required', 'integer'],
            'settings.*.name' => ['string'],
            'settings.*.value' => ['string'],
        ];
    }
}
