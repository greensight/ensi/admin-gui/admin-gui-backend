<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteOrderItemsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'offer_ids' => ['required', 'array', 'min:1'],
            'offer_ids.*' => ['integer'],
        ];
    }

    public function getOfferIds(): array
    {
        return $this->get('offer_ids');
    }
}
