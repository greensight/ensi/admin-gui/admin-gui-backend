<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class AddOrderItemsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'order_items' => ['required', 'array'],
            'order_items.*.offer_id' => ['required', 'integer', 'min:1'],
            'order_items.*.qty' => ['required', 'numeric', 'min:0'],
        ];
    }
}
