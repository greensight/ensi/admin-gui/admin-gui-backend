<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ChangeOrderItemQtyRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'order_items' => ['required', 'array'],
            'order_items.*.item_id' => ['required', 'integer'],
            'order_items.*.qty' => ['required', 'numeric', 'gt:0'],
        ];
    }
}
