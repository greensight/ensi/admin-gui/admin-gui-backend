<?php

namespace App\Http\ApiV1\Modules\Orders\Requests\Orders;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;
use Illuminate\Validation\Rule;

class PatchShipmentsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'status' => ['integer', Rule::in(ShipmentStatusEnum::getAllowableEnumValues())],
        ];
    }
}
