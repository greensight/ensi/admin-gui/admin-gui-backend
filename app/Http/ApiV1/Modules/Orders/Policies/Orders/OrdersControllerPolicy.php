<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Orders;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class OrdersControllerPolicy
{
    use HandlesAuthorization;
    use HandlesRules;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_READ,
            RightsAccessEnum::ORDER_DETAIL_MAIN_READ,
            RightsAccessEnum::ORDER_DETAIL_CLIENT_READ,
            RightsAccessEnum::ORDER_DETAIL_PRODUCTS_READ,
            RightsAccessEnum::ORDER_DETAIL_DELIVERY_READ,
            RightsAccessEnum::ORDER_DETAIL_COMMENTS_READ,
            RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::ORDER_DETAIL_EDIT],
            permissionsFields: [
                RightsAccessEnum::ORDER_DETAIL_COMMENTS_EDIT => ['client_comment'],
                RightsAccessEnum::ORDER_DETAIL_PROBLEMATIC_EDIT => ['is_problem', 'problem_comment'],
                RightsAccessEnum::ORDER_DETAIL_CLIENT_EDIT => ['receiver_name', 'receiver_phone', 'receiver_email'],
                RightsAccessEnum::ORDER_DETAIL_STATUS_EDIT => ['status'],

            ],
        );
    }

    public function attachFile(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_WRITE,
            RightsAccessEnum::ORDER_DETAIL_EDIT,
        ]);
    }

    public function deleteFiles(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_ATTACHMENTS_DELETE,
            RightsAccessEnum::ORDER_DETAIL_EDIT,
        ]);
    }

    public function changeDelivery(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [RightsAccessEnum::ORDER_DETAIL_EDIT],
            permissionsFields: [
                RightsAccessEnum::ORDER_DETAIL_DELIVERY_COMMENTS_EDIT => ['delivery_comment'],
                RightsAccessEnum::ORDER_DETAIL_DELIVERY_EDIT => [
                    'delivery_service',
                    'delivery_method',
                    'delivery_point_id',
                    'delivery_address',
                    'delivery_price',
                    'delivery_cost',
                ],

            ],
        );
    }

    public function addItem(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_PRODUCTS_EDIT,
        ]);
    }

    public function changeItemQty(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_PRODUCTS_EDIT,
        ]);
    }

    public function deleteItem(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_PRODUCTS_EDIT,
        ]);
    }
}
