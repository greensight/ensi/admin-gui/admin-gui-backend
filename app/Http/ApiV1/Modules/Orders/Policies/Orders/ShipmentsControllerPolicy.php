<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Orders;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ShipmentsControllerPolicy
{
    use HandlesAuthorization;

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::ORDER_DETAIL_SHIPMENT_EDIT,
        ]);
    }
}
