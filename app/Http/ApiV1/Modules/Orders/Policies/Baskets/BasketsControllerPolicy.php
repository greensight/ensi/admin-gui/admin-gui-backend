<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Baskets;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class BasketsControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::TECHNICAL_TABLES_READ,
            RightsAccessEnum::TECHNICAL_TABLES_ALL,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::TECHNICAL_TABLES_READ,
            RightsAccessEnum::TECHNICAL_TABLES_ALL,
        ]);
    }
}
