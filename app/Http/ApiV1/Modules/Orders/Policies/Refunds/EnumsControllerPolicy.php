<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\Refunds;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class EnumsControllerPolicy
{
    use HandlesAuthorization;

    public function refundReasons(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::REFUND_DETAIL_REASONS_READ,
            RightsAccessEnum::ORDER_DETAIL_REFUND_CREATE,
        ]);
    }
}
