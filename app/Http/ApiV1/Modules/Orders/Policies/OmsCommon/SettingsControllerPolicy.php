<?php

namespace App\Http\ApiV1\Modules\Orders\Policies\OmsCommon;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class SettingsControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SETTING_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $this->search($user);
    }

    public function patchSeveral(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::SETTING_PAYMENT_TIME_EDIT,
            RightsAccessEnum::SETTING_ORDER_CREATE_TIME_EDIT,
            RightsAccessEnum::SETTING_ORDER_PROCESS_TIME_EDIT,
        ]);
    }
}
