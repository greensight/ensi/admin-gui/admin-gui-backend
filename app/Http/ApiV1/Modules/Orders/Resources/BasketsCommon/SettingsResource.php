<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\BasketsCommon;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\Setting;

/** @mixin Setting */
class SettingsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),

            'code' => $this->getCode(),
            'name' => $this->getName(),
            'value' => $this->getValue(),

            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
