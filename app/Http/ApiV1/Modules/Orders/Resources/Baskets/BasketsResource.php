<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Baskets;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BasketsClient\Dto\Basket;

/**
 * @mixin Basket
 */
class BasketsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'customer_id' => $this->getCustomerId(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'promo_code' => $this->getPromoCode(),
            'items' => BasketItemsResource::collection($this->whenNotNull($this->getItems())),
        ];
    }
}
