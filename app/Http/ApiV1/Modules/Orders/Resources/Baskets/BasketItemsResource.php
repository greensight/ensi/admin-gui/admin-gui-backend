<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Baskets;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\BasketsClient\Dto\BasketItem;

/**
 * @mixin BasketItem
 */
class BasketItemsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'basket_id' => $this->getBasketId(),
            'offer_id' => $this->getOfferId(),
            'product_id' => $this->getProductId(),
            'qty' => $this->getQty(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
