<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Orders\Data;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OmsClient\Dto\Timeslot;

/** @mixin Timeslot */
class TimeslotResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'from' => $this->getFrom(),
            'to' => $this->getTo(),
        ];
    }
}
