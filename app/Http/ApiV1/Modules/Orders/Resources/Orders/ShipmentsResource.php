<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\Data\Orders\ShipmentData;
use App\Http\ApiV1\Modules\Units\Resources\Stores\StoresResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin ShipmentData */
class ShipmentsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->shipment->getId(),

            'number' => $this->shipment->getNumber(),
            'delivery_id' => $this->shipment->getDeliveryId(),
            'store_id' => $this->shipment->getStoreId(),

            'status' => $this->shipment->getStatus(),
            'status_at' => $this->dateTimeToIso($this->shipment->getStatusAt()),

            'cost' => $this->shipment->getCost(),
            'width' => $this->shipment->getWidth(),
            'height' => $this->shipment->getHeight(),
            'length' => $this->shipment->getLength(),
            'weight' => $this->shipment->getWeight(),

            'created_at' => $this->dateTimeToIso($this->shipment->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->shipment->getUpdatedAt()),

            'delivery' => new DeliveriesResource($this->whenNotNull($this->getDelivery())),
            'order_items' => OrderItemsResource::collection($this->whenNotNull($this->getOrderItems())),
            'store' => new StoresResource($this->whenNotNull($this->store)),
        ];
    }
}
