<?php

namespace App\Http\ApiV1\Modules\Orders\Resources\Orders;

use App\Domain\Orders\Data\Orders\OrderData;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\Data\DeliveryAddressResource;
use App\Http\ApiV1\Modules\Units\Resources\AdminUsers\AdminUsersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin OrderData */
class OrdersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->order->getId(),

            'number' => $this->order->getNumber(),
            'source' => $this->order->getSource(),

            'customer_id' => $this->order->getCustomerId(),
            'customer_email' => $this->order->getCustomerEmail(),

            'cost' => $this->order->getCost(),
            'price' => $this->order->getPrice(),
            'promo_code' => $this->order->getPromoCode(),

            'delivery_method' => $this->order->getDeliveryMethod(),
            'client_comment' => $this->order->getClientComment(),
            'delivery_address' => DeliveryAddressResource::make($this->order->getDeliveryAddress()),
            'delivery_comment' => $this->order->getDeliveryComment(),
            'receiver_name' => $this->order->getReceiverName(),
            'receiver_phone' => $this->order->getReceiverPhone(),
            'receiver_email' => $this->order->getReceiverEmail(),

            'status' => $this->order->getStatus(),
            'status_at' => $this->dateTimeToIso($this->order->getStatusAt()),

            'payment_status' => $this->order->getPaymentStatus(),
            'payment_status_at' => $this->dateTimeToIso($this->order->getPaymentStatusAt()),
            'payed_at' => $this->dateTimeToIso($this->order->getPayedAt()),
            'payment_expires_at' => $this->dateTimeToIso($this->order->getPaymentExpiresAt()),
            'payment_method' => $this->order->getPaymentMethod(),
            'payment_system' => $this->order->getPaymentSystem(),
            'payment_external_id' => $this->order->getPaymentExternalId(),

            'is_editable' => $this->order->getIsEditable(),
            'is_changed' => $this->order->getIsChanged(),

            'is_expired' => $this->order->getIsExpired(),
            'is_expired_at' => $this->dateTimeToIso($this->order->getIsExpiredAt()),

            'is_return' => $this->order->getIsReturn(),
            'is_return_at' => $this->dateTimeToIso($this->order->getIsPartialReturnAt()),

            'is_partial_return' => $this->order->getIsPartialReturn(),
            'is_partial_return_at' => $this->dateTimeToIso($this->order->getIsPartialReturnAt()),

            'is_problem' => $this->order->getIsProblem(),
            'is_problem_at' => $this->dateTimeToIso($this->order->getIsProblemAt()),
            'problem_comment' => $this->order->getProblemComment(),

            'created_at' => $this->dateTimeToIso($this->order->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->order->getUpdatedAt()),

            'deliveries' => DeliveriesResource::collection($this->whenNotNull($this->getDeliveries())),
            'files' => OrderFilesResource::collection($this->whenNotNull($this->order->getFiles())),
            'customer' => new CustomersResource($this->whenNotNull($this->customer)),
            'responsible' => new AdminUsersResource($this->whenNotNull($this->responsible)),
        ];
    }
}
