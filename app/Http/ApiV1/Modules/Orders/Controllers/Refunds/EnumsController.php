<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Refunds;

use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundReasonsResource;
use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\OmsClient\Api\EnumsApi;
use Ensi\OmsClient\Dto\RefundStatusEnum;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function refundStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(RefundStatusEnum::getDescriptions()));
    }

    public function refundReasons(EnumsApi $enumsApi): AnonymousResourceCollection
    {
        return RefundReasonsResource::collection($enumsApi->getRefundReasons()->getData());
    }
}
