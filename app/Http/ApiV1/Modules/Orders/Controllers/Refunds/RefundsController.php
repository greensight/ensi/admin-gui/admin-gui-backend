<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Refunds;

use App\Domain\Common\Data\Meta\Enum\Orders\OrderSourceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\RefundStatusEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Orders\Actions\Refunds\AttachRefundFileAction;
use App\Domain\Orders\Actions\Refunds\CreateRefundAction;
use App\Domain\Orders\Actions\Refunds\DeleteRefundFilesAction;
use App\Domain\Orders\Actions\Refunds\PatchRefundAction;
use App\Http\ApiV1\Modules\Orders\Queries\Refunds\RefundsQuery;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\AttachRefundFileRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\CreateRefundRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\DeleteRefundFilesRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Refunds\PatchRefundRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundFilesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Refunds\RefundsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class RefundsController
{
    public function create(CreateRefundRequest $request, CreateRefundAction $action): Responsable
    {
        return new RefundsResource($action->execute($request->validated()));
    }

    public function search(RefundsQuery $query): Responsable
    {
        return RefundsResource::collectPage($query->get());
    }

    public function get(int $id, RefundsQuery $query): Responsable
    {
        return new RefundsResource($query->find($id));
    }

    public function patch(int $id, PatchRefundRequest $request, PatchRefundAction $action): Responsable
    {
        return new RefundsResource($action->execute($id, $request->validated()));
    }

    public function attachFile(int $id, AttachRefundFileAction $action, AttachRefundFileRequest $request): Responsable
    {
        return new RefundFilesResource($action->execute($id, $request->getFile()));
    }

    public function deleteFiles(int $id, DeleteRefundFilesAction $action, DeleteRefundFilesRequest $request): Responsable
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }

    public function meta(
        OrderSourceEnumInfo $orderSource,
        RefundStatusEnumInfo $refundStatus
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('order_id', 'Номер заказа')->listDefault()->filterDefault()->filter(),
            Field::enum('source', 'Канал', $orderSource)->listDefault()->filterDefault()->sort(),
            Field::enum('status', 'Статус', $refundStatus)->listDefault()->filterDefault()->sort(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
        ]);
    }
}
