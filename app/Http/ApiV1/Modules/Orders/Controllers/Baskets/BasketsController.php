<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Baskets;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Orders\Queries\Baskets\BasketsBasketsQuery;
use App\Http\ApiV1\Modules\Orders\Resources\Baskets\BasketsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Contracts\Support\Responsable;

class BasketsController
{
    public function search(BasketsBasketsQuery $query): Responsable
    {
        return BasketsResource::collectPage($query->get());
    }

    /**
     * @throws Exception
     */
    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('customer_id', __('meta.orders.customer_id'))->listDefault()->filterDefault()->sort(),
            Field::text('promo_code', __('meta.orders.promo_code'))->listDefault()->filterDefault()->sort(),

            Field::datetime('created_at', __('meta.created_at'))->listDefault()->filterDefault(),
            Field::datetime('updated_at', __('meta.updated_at'))->sort(),
        ]);
    }
}
