<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Baskets;

use App\Domain\Common\Data\Meta\Field;
use App\Http\ApiV1\Modules\Orders\Queries\Baskets\BasketItemsQuery;
use App\Http\ApiV1\Modules\Orders\Resources\Baskets\BasketItemsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Exception;
use Illuminate\Contracts\Support\Responsable;

class BasketItemsController
{
    public function search(BasketItemsQuery $query): Responsable
    {
        return BasketItemsResource::collectPage($query->get());
    }

    /**
     * @throws Exception
     */
    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::integer('basket_id', __('meta.orders.basket_id'))->listDefault()->filterDefault(),
            Field::integer('offer_id', __('meta.orders.offer_id'))->listDefault()->filterDefault(),
            Field::integer('product_id', __('meta.orders.product_id'))->listDefault()->filterDefault(),
            Field::integer('qty', __('meta.orders.qty'))->filter()->sort(),

            Field::datetime('created_at', __('meta.created_at'))->listDefault()->filterDefault(),
            Field::datetime('updated_at', __('meta.updated_at'))->sort(),
        ]);
    }
}
