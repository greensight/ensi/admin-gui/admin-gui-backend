<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Domain\Common\Data\Meta\Enum\Customers\CustomerEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\OrderSourceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\OrderStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentStatusEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Orders\PaymentSystemEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Orders\Actions\Orders\AddOrderItemAction;
use App\Domain\Orders\Actions\Orders\AttachOrderFileAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderDeliveryAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderItemQtyAction;
use App\Domain\Orders\Actions\Orders\ChangeOrderPaymentSystemAction;
use App\Domain\Orders\Actions\Orders\DeleteOrderFilesAction;
use App\Domain\Orders\Actions\Orders\DeleteOrderItemAction;
use App\Domain\Orders\Actions\Orders\PatchOrderAction;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\OrdersQuery;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\AddOrderItemsRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\AttachOrderFileRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderItemQtyRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\ChangeOrderPaymentSystemRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\DeleteOrderFilesRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\DeleteOrderItemsRequest;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\PatchOrderRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrderFilesResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrderItemsResource;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\OrdersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\OmsClient\ApiException;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class OrdersController
{
    public function search(OrdersQuery $query): Responsable
    {
        return OrdersResource::collectPage($query->get());
    }

    public function meta(
        CustomerEnumInfo $customer,
        OrderSourceEnumInfo $orderSource,
        OrderStatusEnumInfo $orderStatus,
        PaymentStatusEnumInfo $orderPaymentStatus,
        PaymentMethodEnumInfo $orderPaymentMethod,
        PaymentSystemEnumInfo $orderPaymentSystem,
        DeliveryMethodEnumInfo $deliveryMethod,
    ): Responsable {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::text('number', '№ заказа')->sort()->filterDefault(),
            Field::enum('source', 'Источник', $orderSource)->filterDefault(),
            Field::enum('customer_id', 'Покупатель', $customer)->listDefault()->filterDefault(),
            Field::email('customer_email', 'Почта покупателя'),
            Field::price('cost', 'Сумма до скидок'),
            Field::price('price', 'Сумма')->listDefault()->filterDefault(),
            Field::text('promo_code', 'Промокод')->sort(),
            Field::enum('delivery_method', 'Метод доставки', $deliveryMethod),
            Field::string('delivery_address.address_string', 'Адрес доставки')->object(),
            Field::text('receiver_name', 'ФИО получателя')->sort(),
            Field::phone('receiver_phone', 'Телефон получателя')->sort(),
            Field::email('receiver_email', 'Почта получателя')->sort(),
            Field::enum('status', 'Статус', $orderStatus)->listDefault()->filterDefault(),
            Field::datetime('status_at', 'Дата изменения статуса'),
            Field::enum('payment_status', 'Статус оплаты', $orderPaymentStatus),
            Field::datetime('payment_status_at', 'Дата изменения статуса оплаты'),
            Field::datetime('payed_at', 'Дата оплаты'),
            Field::datetime('payment_expires_at', 'Дата просрочки оплаты'),
            Field::enum('payment_method', 'Метод оплаты', $orderPaymentMethod),
            Field::enum('payment_system', 'Система оплаты', $orderPaymentSystem),
            Field::text('payment_external_id', 'ID оплаты во внешней системе')->sort(),
            Field::boolean('is_changed', 'Заказ был изменён')->filterDefault(),
            Field::boolean('is_editable', 'Заказ может быть изменён')->filterDefault()->resetSort(),
            Field::boolean('is_expired', 'Заказ просрочен')->listDefault()->filterDefault(),
            Field::datetime('is_expired_at', 'Дата, когда заказ был просрочен'),
            Field::boolean('is_return', 'Заказ возвращен')->listDefault()->filterDefault(),
            Field::datetime('is_return_at', 'Дата изменения признака "Заказ возвращен"'),
            Field::boolean('is_partial_return', 'Заказ возвращен частично')->listDefault()->filterDefault(),
            Field::datetime('is_partial_return_at', 'Дата изменения признака "Заказ возвращен частично"'),
            Field::boolean('is_problem', 'Заказ проблемный')->listDefault()->filterDefault(),
            Field::datetime('is_problem_at', 'Дата изменения признака "Заказ проблемный"'),
            Field::text('problem_comment', 'Комментарий о проблеме'),
            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления'),
            Field::text('delivery_comment', 'Комментарий к доставке'),
            Field::text('client_comment', 'Комментарий клиента'),
        ]);
    }

    public function get(int $id, OrdersQuery $ordersQuery): Responsable
    {
        return new OrdersResource($ordersQuery->find($id));
    }

    public function patch(int $id, PatchOrderAction $action, PatchOrderRequest $request): Responsable
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function changePaymentSystem(int $id, ChangeOrderPaymentSystemRequest $request, ChangeOrderPaymentSystemAction $action): Responsable
    {
        return new OrdersResource($action->execute($id, $request->getPaymentSystem()));
    }

    public function changeDelivery(int $id, ChangeOrderDeliveryRequest $request, ChangeOrderDeliveryAction $action): Responsable
    {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    public function attachFile(int $id, AttachOrderFileAction $action, AttachOrderFileRequest $request): Responsable
    {
        return new OrderFilesResource($action->execute($id, $request->getFile()));
    }

    public function deleteFiles(int $id, DeleteOrderFilesAction $action, DeleteOrderFilesRequest $request): Responsable
    {
        $action->execute($id, $request->getFileIds());

        return new EmptyResource();
    }

    /**
     * @throws ApiException
     */
    public function addItem(
        int $id,
        AddOrderItemsRequest $request,
        AddOrderItemAction $action
    ): OrdersResource {
        return new OrdersResource($action->execute($id, $request->validated()));
    }

    /**
     * @throws ApiException
     */
    public function deleteItem(
        int $id,
        DeleteOrderItemsRequest $request,
        DeleteOrderItemAction $action
    ): Responsable {
        $action->execute($id, $request->validated());

        return new EmptyResource();
    }

    /**
     * @throws ApiException
     */
    public function changeItemQty(
        int $id,
        ChangeOrderItemQtyRequest $request,
        ChangeOrderItemQtyAction $action
    ): AnonymousResourceCollection {
        return OrderItemsResource::collection($action->execute($id, $request->validated()));
    }
}
