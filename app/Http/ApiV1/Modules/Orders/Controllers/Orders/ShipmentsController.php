<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Domain\Orders\Actions\Orders\PatchShipmentAction;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\PatchShipmentsRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\ShipmentsResource;
use Illuminate\Contracts\Support\Responsable;

class ShipmentsController
{
    public function patch(int $id, PatchShipmentAction $action, PatchShipmentsRequest $request): Responsable
    {
        return new ShipmentsResource($action->execute($id, $request->validated()));
    }
}
