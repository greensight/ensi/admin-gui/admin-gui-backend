<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\Orders;

use App\Domain\Orders\Actions\Orders\PatchDeliveryAction;
use App\Http\ApiV1\Modules\Orders\Requests\Orders\PatchDeliveryRequest;
use App\Http\ApiV1\Modules\Orders\Resources\Orders\DeliveriesResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveriesController
{
    public function patch(int $id, PatchDeliveryAction $action, PatchDeliveryRequest $request): Responsable
    {
        return new DeliveriesResource($action->execute($id, $request->validated()));
    }
}
