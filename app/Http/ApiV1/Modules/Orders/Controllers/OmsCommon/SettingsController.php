<?php

namespace App\Http\ApiV1\Modules\Orders\Controllers\OmsCommon;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Orders\Actions\OmsCommon\PatchSeveralSettingsAction;
use App\Http\ApiV1\Modules\Orders\Requests\OmsCommon\PatchOmsSettingsRequest;
use App\Http\ApiV1\Modules\Orders\Resources\OmsCommon\SettingsResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Ensi\OmsClient\Api\CommonApi;
use Illuminate\Contracts\Support\Responsable;

class SettingsController
{
    public function search(CommonApi $commonApi): Responsable
    {
        return SettingsResource::collection($commonApi->searchSettings()->getData());
    }

    public function patchSeveral(PatchSeveralSettingsAction $action, PatchOmsSettingsRequest $request): Responsable
    {
        return SettingsResource::collection($action->execute($request->validated()));
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::keyword('code', 'Символьный код')->listDefault()->filterDefault()->sort(),
            Field::text('name', 'Название')->listDefault()->filterDefault()->sort(),
            Field::keyword('value', 'Значение')->listDefault()->filterDefault()->sort(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->sort(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->sort(),
        ]);
    }
}
