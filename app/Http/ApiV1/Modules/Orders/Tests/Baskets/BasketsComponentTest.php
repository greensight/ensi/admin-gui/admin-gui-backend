<?php

use App\Domain\Orders\Tests\Factories\Baskets\BasketFactory;
use App\Domain\Orders\Tests\Factories\Baskets\BasketItemFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PaginationFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'orders', 'orders.baskets');

test('POST /api/v1/orders/baskets:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $basketId = 1;

    $this->mockBasketsApi()->allows([
        'searchBaskets' => BasketFactory::new()->makeResponseSearch([['id' => $basketId]]),
    ]);

    postJson('/api/v1/orders/baskets:search', ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $basketId);
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/orders/baskets:search include success', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $basketId = 1;
    $basketItems = BasketItemFactory::new()->makeSeveral(1)->toArray();

    $this->mockBasketsApi()->allows([
        'searchBaskets' => BasketFactory::new()->withItems($basketItems)->makeResponseSearch([['id' => $basketId]]),
    ]);

    postJson('/api/v1/orders/baskets:search',  ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $basketId)
        ->assertJsonPath('data.0.items.0.id', $basketItems[0]->getId());
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/orders/baskets:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/orders/baskets:search')
        ->assertStatus(403);
});

test('GET /api/v1/orders/baskets:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $meta = getJson('/api/v1/orders/baskets:meta')
        ->assertOk()
        ->json();

    assertMeta($meta);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);
;

test('GET /api/v1/orders/baskets:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/orders/baskets:meta')
        ->assertStatus(403);
});
