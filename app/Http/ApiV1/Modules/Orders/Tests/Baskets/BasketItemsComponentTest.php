<?php

use App\Domain\Orders\Tests\Factories\Baskets\BasketItemFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PaginationFactory;
use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'orders', 'orders.baskets');

test('POST /api/v1/orders/basket-items:search 200', function (?bool $always, array $accessRights) {
    FakerProvider::$optionalAlways = $always;
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $basketItemId = 1;

    $this->mockBasketItemsApi()->allows([
        'searchBasketItems' => BasketItemFactory::new()->makeResponseSearch([['id' => $basketItemId]]),
    ]);

    postJson('/api/v1/orders/basket-items:search', ['pagination' => PaginationFactory::new()->makeRequestOffset()])
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $basketItemId);
})->with(FakerProvider::$optionalDataset, [
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('POST /api/v1/orders/basket-items:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    postJson('/api/v1/orders/basket-items:search')
        ->assertStatus(403);
});

test('GET /api/v1/orders/basket-items:meta 200', function (array $accessRights) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights($accessRights);

    $this->mockBasketItemsApi()->allows([
        'searchBasketItemsAsync' => PromiseFactory::make(BasketItemFactory::new()->makeResponseSearch()),
    ]);

    getJson('/api/v1/orders/basket-items:meta')
        ->assertStatus(200);
})->with([
    [[RightsAccessEnum::TECHNICAL_TABLES_READ]],
    [[RightsAccessEnum::TECHNICAL_TABLES_ALL]],
]);

test('GET /api/v1/orders/basket-items:meta 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRoles([]);

    getJson('/api/v1/orders/basket-items:meta')
        ->assertStatus(403);
});
