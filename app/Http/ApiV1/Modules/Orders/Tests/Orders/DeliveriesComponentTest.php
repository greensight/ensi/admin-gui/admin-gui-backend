<?php

use App\Domain\Orders\Tests\Factories\Orders\DeliveryFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories\DeliveryRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.deliveries');

test('PATCH /api/v1/orders/deliveries/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ApiV1ComponentTestCase $this */
    $pathDeliveryData = DeliveryRequestFactory::new()->makePathDelivery();

    $this->mockOmsDeliveriesApi()->allows([
        'patchDelivery' => DeliveryFactory::new()->makeResponse(),
    ]);

    patchJson("/api/v1/orders/deliveries/1", $pathDeliveryData)->assertStatus(200);
})->with(FakerProvider::$optionalDataset);
