<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class AddOrderItemsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'order_items' => $this->faker->randomList(function () {
                return [
                    'offer_id' => $this->faker->modelId(),
                    'qty' => $this->faker->randomFloat(4) + 0.01,
                ];
            }, 1),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
