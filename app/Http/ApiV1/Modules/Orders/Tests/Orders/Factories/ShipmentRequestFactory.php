<?php

namespace App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;

class ShipmentRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'status' => $this->faker->randomElement(ShipmentStatusEnum::getAllowableEnumValues()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
