<?php

use App\Domain\Orders\Tests\Factories\Orders\ShipmentFactory;
use App\Http\ApiV1\Modules\Orders\Tests\Orders\Factories\ShipmentRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use Ensi\OmsClient\ApiException;

use function Pest\Laravel\patchJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'orders', 'orders.shipments');

test('PATCH /api/v1/orders/shipments/{id} 200', function () {
    /** @var ApiV1ComponentTestCase $this */
    $request = ShipmentRequestFactory::new()->make();

    $this->mockOmsShipmentsApi()->allows([
        'patchShipment' => ShipmentFactory::new()->makeResponse(),
    ]);

    patchJson("/api/v1/orders/shipments/1", $request)->assertStatus(200);
});

test('PATCH /api/v1/orders/shipments/{id} 400', function () {
    $request = ShipmentRequestFactory::new()->make(['status' => 0]);

    patchJson("/api/v1/orders/shipments/1", $request)->assertStatus(400);
});

test('PATCH /api/v1/orders/shipments/{id} 404', function () {
    /** @var ApiV1ComponentTestCase $this */
    $this->mockOmsShipmentsApi()
        ->shouldReceive('patchShipment')
        ->andThrowExceptions([new ApiException("NotFound", 404)]);

    patchJson("/api/v1/orders/shipments/1")->assertStatus(404);
});
