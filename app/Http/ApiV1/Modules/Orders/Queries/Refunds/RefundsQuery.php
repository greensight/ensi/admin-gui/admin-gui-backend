<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Refunds;

use App\Domain\Common\Actions\AsyncLoadAction;
use App\Domain\Orders\Data\Refunds\RefundData;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderOffersLoader;
use App\Http\ApiV1\Modules\Orders\Queries\Orders\IncludeLoaders\OrderProductsLoader;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Dto\Refund;
use Ensi\OmsClient\Dto\RefundResponse;
use Ensi\OmsClient\Dto\RequestBodyPagination as RequestBodyPaginationOms;
use Ensi\OmsClient\Dto\SearchRefundsRequest;
use Ensi\OmsClient\Dto\SearchRefundsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class RefundsQuery extends QueryBuilder
{
    use QueryBuilderGetTrait;
    use QueryBuilderFindTrait;

    public function __construct(
        protected Request $httpRequest,
        protected RefundsApi $refundsApi,
        protected AsyncLoadAction $asyncLoadAction,
        protected OrderOffersLoader $offersLoader,
        protected OrderProductsLoader $productsLoader,
    ) {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchRefundsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPaginationOms::class;
    }

    protected function convertGetToItems($response): array
    {
        return $this->convertArray($response->getData());
    }

    protected function convertFindToItem($response): RefundData
    {
        return current($this->convertArray([$response->getData()]));
    }

    /**
     * @param Refund[] $refunds
     */
    protected function convertArray(array $refunds): array
    {
        /** @var Collection<Refund> $refunds */
        $refunds = collect($refunds);
        $items = $refunds->pluck('items')->collapse();

        $this->offersLoader->offerIds = $this->prepareFilterValues($items->pluck('offer_id'));
        $this->asyncLoadAction->execute([$this->offersLoader]);

        $this->productsLoader->productIds = array_values(array_unique(Arr::pluck($this->offersLoader->offersData, 'offer.product_id')));
        $this->asyncLoadAction->execute([$this->productsLoader]);

        $refundsData = [];
        foreach ($refunds as $refund) {
            $refundData = new RefundData($refund);

            $refundData->offersData = $this->offersLoader->offersData;
            $refundData->products = $this->productsLoader->products;

            $refundsData[] = $refundData;
        }

        return $refundsData;
    }

    protected function search($request): SearchRefundsResponse
    {
        return $this->refundsApi->searchRefunds($request);
    }

    protected function searchById($id, string $include): RefundResponse
    {
        return $this->refundsApi->getRefund($id, $include);
    }

    protected function getHttpInclude(): array
    {
        $httpIncludes = parent::getHttpInclude();

        $includes = [];
        foreach ($httpIncludes as $include) {
            switch ($include) {
                case "items.product":
                    $this->productsLoader->load = true;
                    $this->offersLoader->load = true;

                    break;
                default:
                    $includes[] = $include;
            }
        }

        return array_values(array_unique($includes));
    }

    protected function prepareFilterValues(Collection $values): array
    {
        return $values->unique()->values()->filter()->all();
    }
}
