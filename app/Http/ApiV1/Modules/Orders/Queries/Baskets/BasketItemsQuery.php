<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Baskets;

use Ensi\BasketsClient\Api\BasketItemsApi;
use Ensi\BasketsClient\ApiException;
use Ensi\BasketsClient\Dto\SearchBasketItemsRequest;
use Ensi\BasketsClient\Dto\SearchBasketItemsResponse;
use Illuminate\Http\Request;

class BasketItemsQuery extends BasketsQuery
{
    public function __construct(Request $request, private BasketItemsApi $api)
    {
        parent::__construct($request, SearchBasketItemsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBasketItemsResponse
    {
        return $this->api->searchBasketItems($request);
    }
}
