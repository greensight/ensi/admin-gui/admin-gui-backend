<?php

namespace App\Http\ApiV1\Modules\Orders\Queries\Baskets;

use Ensi\BasketsClient\Api\BasketsApi;
use Ensi\BasketsClient\ApiException;
use Ensi\BasketsClient\Dto\SearchBasketsRequest;
use Ensi\BasketsClient\Dto\SearchBasketsResponse;
use Illuminate\Http\Request;

class BasketsBasketsQuery extends BasketsQuery
{
    public function __construct(Request $request, private BasketsApi $api)
    {
        parent::__construct($request, SearchBasketsRequest::class);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchBasketsResponse
    {
        return $this->api->searchBaskets($request);
    }
}
