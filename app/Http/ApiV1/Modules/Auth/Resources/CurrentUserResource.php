<?php

namespace App\Http\ApiV1\Modules\Auth\Resources;

use App\Domain\Auth\Models\User;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin User
 */
class CurrentUserResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "full_name" => $this->fullName,
            "short_name" => $this->shortName,
            "active" => $this->active,
            "login" => $this->login,
            "last_name" => $this->lastName,
            "first_name" => $this->firstName,
            "middle_name" => $this->middleName,
            "email" => $this->email,
            "phone" => $this->phone,
            "roles" => $this->roles,
            "rights_access" => $this->rightsAccess,
        ];
    }
}
