<?php

use App\Domain\Auth\Models\Tests\Factories\TokenFactory;
use App\Domain\Common\Tests\Factories\JWTFactory;
use App\Domain\Units\Tests\Factories\AdminUserFactory;
use App\Http\ApiV1\Support\Tests\ApiV1NoAuthComponentTestCase;
use Ensi\AdminAuthClient\ApiException;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1NoAuthComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/auth/login 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockAdminAuthOauthApi()->allows([
        'createToken' => $token = TokenFactory::new()->make(),
    ]);
    $requestData = [
        'login' => 'test_login',
        'password' => 'test_password',
    ];

    postJson("/api/v1/auth/login", $requestData)
        ->assertStatus(200)
        ->assertJsonPath('data.access_token', $token->getAccessToken());
});

test('POST /api/v1/auth/login 401', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockAdminAuthOauthApi()
        ->shouldReceive('createToken')
        ->andThrowExceptions([new ApiException("Unauthorized", 401)]);

    $requestData = [
        'login' => 'test_login',
        'password' => 'test_password',
    ];

    postJson("/api/v1/auth/login", $requestData)
        ->assertStatus(401);
});

test('POST /api/v1/auth/logout 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $this->mockAdminAuthUsersApi()
        ->shouldReceive('getCurrentUser')
        ->once()
        ->andReturn(AdminUserFactory::new()->makeResponse());

    $this->mockAdminAuthOauthApi()->shouldReceive('deleteToken');

    getJson("/api/v1/auth/logout", [config('auth.header_for_token') => JWTFactory::new()->make()])
        ->assertStatus(200)
        ->assertJsonPath('data', null);
});

test('POST /api/v1/auth/refresh 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockAdminAuthOauthApi()->allows([
        'createToken' => $newToken = TokenFactory::new()->make(),
    ]);

    $requestData = [
        'refresh_token' => JWTFactory::new()->make(),
    ];

    postJson("/api/v1/auth/refresh", $requestData)
        ->assertStatus(200)
        ->assertJsonPath('data.access_token', $newToken->getAccessToken());
});

test('POST /api/v1/auth/refresh 401', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */

    $this->mockAdminAuthOauthApi()
        ->shouldReceive('createToken')
        ->andThrowExceptions([new ApiException("Unauthorized", 401)]);

    $requestData = [
        'refresh_token' => 'test_token',
    ];

    postJson("/api/v1/auth/refresh", $requestData)
        ->assertStatus(401);
});

test('POST /api/v1/auth/current-user 200', function () {
    /** @var ApiV1NoAuthComponentTestCase $this */
    $userResponse = AdminUserFactory::new()->makeResponse();

    $this->mockAdminAuthUsersApi()
        ->shouldReceive('getCurrentUser')
        ->once()
        ->andReturn($userResponse);

    getJson("/api/v1/auth/current-user", [config('auth.header_for_token') => JWTFactory::new()->make()])
        ->assertStatus(200)
        ->assertJsonPath('data.id', $userResponse->getData()->getId())
        ->assertJsonCount(count($userResponse->getData()->getRoles()), 'data.roles');
});
