<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\Geos;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\FederalDistrictResponse;
use Ensi\LogisticClient\Dto\PaginationTypeEnum;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsRequest;
use Ensi\LogisticClient\Dto\SearchFederalDistrictsResponse;
use Illuminate\Http\Request;

class FederalDistrictsQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;
    use QueryBuilderFilterEnumTrait;

    public function __construct(protected Request $httpRequest, protected GeosApi $geosApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchFederalDistrictsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchFederalDistrictsRequest::class;
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): FederalDistrictResponse
    {
        return $this->geosApi->getFederalDistrict($id, $include);
    }

    /**
     * @param  SearchFederalDistrictsRequest  $request
     * @throws ApiException
     */
    protected function search($request): SearchFederalDistrictsResponse
    {
        return $this->geosApi->searchFederalDistricts($request);
    }

    /**
     * @param  SearchFederalDistrictsRequest  $request
     * @throws ApiException
     */
    protected function searchOne($request): FederalDistrictResponse
    {
        return $this->geosApi->searchFederalDistrict($request);
    }

    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }
        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
