<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis;

use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpi;

class DeliveryKpiQuery
{
    public function __construct(protected KpiApi $kpiApi)
    {
    }

    /**
     * @throws ApiException
     */
    public function get(): DeliveryKpi
    {
        return $this->kpiApi->getDeliveryKpi()->getData();
    }
}
