<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiPptResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiPptRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiPptResponse;
use Illuminate\Http\Request;

class DeliveryKpiPptQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    public function __construct(protected Request $httpRequest, protected KpiApi $kpiApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchDeliveryKpiPptRequest::class;
    }

    protected function requestGetClass(): string
    {
        return SearchDeliveryKpiPptRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @throws ApiException
     */
    public function searchById($id, string $include): DeliveryKpiPptResponse
    {
        return $this->kpiApi->getDeliveryKpiPpt($id);
    }

    /**
     * @throws ApiException
     */
    public function search($request): SearchDeliveryKpiPptResponse
    {
        return $this->kpiApi->searchDeliveryKpiPpts($request);
    }

    /**
     * @throws ApiException
     */
    public function searchOne($request): DeliveryKpiPptResponse
    {
        return $this->kpiApi->searchOneDeliveryKpiPpt($request);
    }
}
