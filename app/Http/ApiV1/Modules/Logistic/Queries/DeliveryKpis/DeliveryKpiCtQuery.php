<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryKpiCtResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiCtRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryKpiCtResponse;
use Illuminate\Http\Request;

class DeliveryKpiCtQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    public function __construct(protected Request $httpRequest, protected KpiApi $kpiApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchDeliveryKpiCtRequest::class;
    }

    protected function requestGetClass(): string
    {
        return SearchDeliveryKpiCtRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    /**
     * @throws ApiException
     */
    public function searchById($id, string $include): DeliveryKpiCtResponse
    {
        return $this->kpiApi->getDeliveryKpiCt($id);
    }

    /**
     * @throws ApiException
     */
    public function search($request): SearchDeliveryKpiCtResponse
    {
        return $this->kpiApi->searchDeliveryKpiCts($request);
    }

    /**
     * @throws ApiException
     */
    public function searchOne($request): DeliveryKpiCtResponse
    {
        return $this->kpiApi->searchOneDeliveryKpiCt($request);
    }
}
