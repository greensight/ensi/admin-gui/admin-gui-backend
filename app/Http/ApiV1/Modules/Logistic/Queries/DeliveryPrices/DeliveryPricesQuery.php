<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\ApiException;
use Ensi\LogisticClient\Dto\DeliveryPriceResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryPricesResponse;
use Illuminate\Http\Request;

class DeliveryPricesQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    public function __construct(protected Request $httpRequest, protected DeliveryPricesApi $deliveryPricesApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchDeliveryPricesRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    protected function requestGetClass(): string
    {
        return SearchDeliveryPricesRequest::class;
    }

    /**
     * @throws ApiException
     */
    protected function searchById($id, string $include): DeliveryPriceResponse
    {
        return $this->deliveryPricesApi->getDeliveryPrice($id, $include);
    }

    /**
     * @throws ApiException
     */
    protected function search($request): SearchDeliveryPricesResponse
    {
        return $this->deliveryPricesApi->searchDeliveryPrices($request);
    }

    /**
     * @throws ApiException
     */
    protected function searchOne($request): DeliveryPriceResponse
    {
        return $this->deliveryPricesApi->searchDeliveryPrice($request);
    }
}
