<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFilterEnumTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Dto\PaginationTypeEnum;
use Ensi\LogisticClient\Dto\PointResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchPointsRequest;
use Ensi\LogisticClient\Dto\SearchPointsResponse;
use Illuminate\Http\Request;

class PointsQuery extends QueryBuilder
{
    use QueryBuilderFilterEnumTrait;
    use QueryBuilderFindTrait;

    public function __construct(protected Request $httpRequest, protected readonly GeosApi $geosApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestGetClass(): string
    {
        return SearchPointsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    public function searchById($id, string $include): PointResponse
    {
        return $this->geosApi->getPoint($id, $include);
    }

    public function search($request): SearchPointsResponse
    {
        return $this->geosApi->searchPoints($request);
    }

    /**
     * @param SearchPointsRequest $request
     */
    protected function prepareEnumRequest($request, ?array $id, ?string $query): void
    {
        $filter = [];
        if ($id) {
            $filter['id'] = $id;
        }
        if ($query) {
            $filter['name_like'] = $query;
        }
        $request->setFilter((object)$filter);
        $request->setPagination(
            $id ?
                (new RequestBodyPagination())->setLimit(count($id))->setType(PaginationTypeEnum::CURSOR) :
                (new RequestBodyPagination())->setType(PaginationTypeEnum::CURSOR)
        );
    }
}
