<?php

namespace App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices;

use App\Domain\Logistic\Data\DeliveryServiceDocumentData;
use App\Http\ApiV1\Support\Queries\QueryBuilder;
use App\Http\ApiV1\Support\Queries\QueryBuilderFindTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderFirstTrait;
use App\Http\ApiV1\Support\Queries\QueryBuilderGetTrait;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Dto\DeliveryServiceDocument;
use Ensi\LogisticClient\Dto\DeliveryServiceDocumentResponse;
use Ensi\LogisticClient\Dto\RequestBodyPagination;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsRequest;
use Ensi\LogisticClient\Dto\SearchDeliveryServiceDocumentsResponse;
use Illuminate\Http\Request;

class DeliveryServiceDocumentsQuery extends QueryBuilder
{
    use QueryBuilderFindTrait;
    use QueryBuilderFirstTrait;
    use QueryBuilderGetTrait;

    public function __construct(protected Request $httpRequest, protected DeliveryServicesApi $deliveryServicesApi)
    {
        parent::__construct($httpRequest);
    }

    protected function requestFirstClass(): string
    {
        return SearchDeliveryServiceDocumentsRequest::class;
    }

    protected function requestGetClass(): string
    {
        return SearchDeliveryServiceDocumentsRequest::class;
    }

    protected function paginationClass(): string
    {
        return RequestBodyPagination::class;
    }

    public function searchById($id, string $include): DeliveryServiceDocumentResponse
    {
        return $this->deliveryServicesApi->getDeliveryServiceDocument($id, $include);
    }

    public function search($request): SearchDeliveryServiceDocumentsResponse
    {
        return $this->deliveryServicesApi->searchDeliveryServiceDocuments($request);
    }

    public function searchOne($request): DeliveryServiceDocumentResponse
    {
        return $this->deliveryServicesApi->searchDeliveryServiceDocument($request);
    }

    /**
     * @param DeliveryServiceDocumentResponse $response
     */
    protected function convertFirstToItem($response): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData($response->getData());
    }

    /**
     * @param DeliveryServiceDocumentResponse $response
     */
    protected function convertFindToItem($response): DeliveryServiceDocumentData
    {
        return new DeliveryServiceDocumentData($response->getData());
    }

    /**
     * @param SearchDeliveryServiceDocumentsResponse $response
     * @return DeliveryServiceDocumentData[]
     */
    protected function convertGetToItems($response): array
    {
        return array_map(function (DeliveryServiceDocument $deliveryServiceDocument) {
            return new DeliveryServiceDocumentData($deliveryServiceDocument);
        }, $response->getData());
    }
}
