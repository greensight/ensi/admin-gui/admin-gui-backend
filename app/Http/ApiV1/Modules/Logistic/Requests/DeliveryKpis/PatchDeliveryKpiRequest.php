<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchDeliveryKpiRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'rtg' => ['nullable', 'integer', 'min:0'],
            'ct' => ['nullable', 'integer', 'min:0'],
            'ppt' => ['nullable', 'integer', 'min:0'],
      ];
    }
}
