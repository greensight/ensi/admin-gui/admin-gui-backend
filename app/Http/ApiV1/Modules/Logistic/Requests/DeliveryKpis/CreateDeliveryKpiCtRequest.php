<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateDeliveryKpiCtRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'ct' => ['required', 'integer', 'min:0'],
        ];
    }
}
