<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\Geos;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateRegionRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'federal_district_id' => ['required', 'integer'],
            'name' => ['required', 'string'],
            'guid' => ['required', 'string'],
      ];
    }
}
