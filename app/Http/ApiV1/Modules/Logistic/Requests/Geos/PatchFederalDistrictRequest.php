<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\Geos;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchFederalDistrictRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['nullable', 'string'],
      ];
    }
}
