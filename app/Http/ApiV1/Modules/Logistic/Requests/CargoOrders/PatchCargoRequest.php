<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\CargoOrders;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticCargoStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchCargoRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'status' => [new Enum(LogisticCargoStatusEnum::class)],
            'is_problem' => ['sometimes', 'boolean', 'nullable'],
            'is_canceled' => ['sometimes', 'boolean', 'nullable'],
            'width' => ['nullable', 'numeric'],
            'height' => ['nullable', 'numeric'],
            'length' => ['nullable', 'numeric'],
            'weight' => ['nullable', 'numeric'],
            'shipping_problem_comment' => ['sometimes', 'string', 'nullable'],
        ];
    }
}
