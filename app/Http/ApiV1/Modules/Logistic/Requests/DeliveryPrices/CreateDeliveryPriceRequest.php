<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryMethodEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class CreateDeliveryPriceRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'federal_district_id' => ['required', 'integer'],
            'region_id' => ['nullable', 'integer'],
            'region_guid' => ['nullable', 'string'],
            'delivery_service' => ['nullable', new Enum(LogisticDeliveryServiceEnum::class)],
            'delivery_method' => ['required', new Enum(LogisticDeliveryMethodEnum::class)],
            'price' => ['required', 'integer', 'min:0'],
      ];
    }
}
