<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticDeliveryServiceEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class PatchDeliveryServiceDocumentRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service' => ['nullable', new Enum(LogisticDeliveryServiceEnum::class)],
            'name' => ['nullable', 'string'],
      ];
    }
}
