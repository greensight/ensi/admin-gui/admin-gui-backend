<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\Modules\Logistic\Requests\AddressRequest;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchPointRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'delivery_service_id' => ['nullable', 'integer'],
            ...self::nestedRules('address', AddressRequest::baseRules()),
            'is_active' => ['boolean'],
            'city_guid' => ['nullable', 'string'],
            'timezone' => ['nullable', 'string'],
            'geo_lat' => ['nullable', 'string'],
            'geo_lon' => ['nullable', 'string'],
        ];
    }
}
