<?php

namespace App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices;

use App\Http\ApiV1\OpenApiGenerated\Enums\OrdersPaymentMethodEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rules\Enum;

class DeletePaymentMethodFromDeliveryServiceRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'payment_method' => ['required', new Enum(OrdersPaymentMethodEnum::class)],
        ];
    }

    public function getPaymentMethod(): int
    {
        return $this->get('payment_method');
    }
}
