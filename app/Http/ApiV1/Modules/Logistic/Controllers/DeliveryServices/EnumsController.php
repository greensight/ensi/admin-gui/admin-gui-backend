<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Http\ApiV1\Support\Controllers\Data\EnumData;
use App\Http\ApiV1\Support\Resources\EnumResource;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceStatusEnum;
use Ensi\LogisticClient\Dto\ShipmentMethodEnum;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class EnumsController
{
    public function getDeliveryMethods(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(DeliveryMethodEnum::getDescriptions()));
    }

    public function getDeliveryServiceStatuses(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(DeliveryServiceStatusEnum::getDescriptions()));
    }

    public function getShipmentMethods(): AnonymousResourceCollection
    {
        return EnumResource::collection(EnumData::makeFromEnumDescriptions(ShipmentMethodEnum::getDescriptions()));
    }
}
