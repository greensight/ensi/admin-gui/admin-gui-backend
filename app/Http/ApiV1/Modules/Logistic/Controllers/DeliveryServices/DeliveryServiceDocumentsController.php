<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceDocuments\CreateDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocuments\DeleteDeliveryServiceDocumentAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocuments\PatchDeliveryServiceDocumentAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServiceDocumentsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\CreateDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceDocumentRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceDocumentsResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServiceDocumentsController
{
    public function create(CreateDeliveryServiceDocumentRequest $request, CreateDeliveryServiceDocumentAction $action): Responsable
    {
        return new DeliveryServiceDocumentsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryServiceDocumentRequest $request, PatchDeliveryServiceDocumentAction $action): Responsable
    {
        return new DeliveryServiceDocumentsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryServiceDocumentAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryServiceDocumentsQuery $query): Responsable
    {
        return new DeliveryServiceDocumentsResource($query->find($id));
    }

    public function search(DeliveryServiceDocumentsQuery $query): Responsable
    {
        return DeliveryServiceDocumentsResource::collectPage($query->get());
    }

    public function searchOne(DeliveryServiceDocumentsQuery $query): Responsable
    {
        return new DeliveryServiceDocumentsResource($query->first());
    }
}
