<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryServiceEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Logistic\Actions\DeliveryServices\PatchPointAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\PointsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchPointRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\PointEnumValueResource;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\PointsResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class PointsController
{
    public function get(int $id, PointsQuery $query): Responsable
    {
        return PointsResource::make($query->find($id));
    }

    public function patch(int $id, PatchPointRequest $request, PatchPointAction $action): Responsable
    {
        return PointsResource::make($action->execute($id, $request->validated()));
    }

    public function search(PointsQuery $query): Responsable
    {
        return PointsResource::collectPage($query->get());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, PointsQuery $query): Responsable
    {
        return PointEnumValueResource::collection($query->searchEnums());
    }

    public function meta(DeliveryServiceEnumInfo $deliveryServiceEnumInfo): Responsable
    {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::enum('delivery_service_id', 'Сервис доставки', $deliveryServiceEnumInfo)->listDefault()->filterDefault()->sort(),
            Field::text('name', 'Название')->sort()->listDefault()->filterDefault(),
            Field::text('external_id', 'Внешний идентификатор')->sort()->listDefault()->filterDefault(),
            Field::boolean('is_active', 'Активность')->listDefault()->filterDefault(),

            Field::boolean('only_online_payment', 'Выдача только полностью оплаченных посылок')->listDefault()->filterDefault(),
            Field::boolean('has_payment_card', 'Возможность оплаты банковской картой')->listDefault()->filterDefault(),
            Field::boolean('has_courier', 'Курьерская доставка')->listDefault()->filterDefault(),
            Field::boolean('is_postamat', 'Является постaматом')->listDefault()->filterDefault(),

            Field::text('max_value', 'Максимальный объем')->sort()->listDefault()->filterDefault(),
            Field::integer('max_weight', 'Максимальный вес')->listDefault()->filterDefault(),

            Field::phone('phone', 'Номер телефона')->sort()->listDefault()->filterDefault(),
            Field::text('timezone', 'Часовой пояс')->sort()->listDefault()->filterDefault(),
            Field::text('metro_station', 'Станция метро')->sort()->listDefault()->filterDefault(),
            Field::text('geo_lat', 'Широта')->sort()->listDefault()->filterDefault(),
            Field::text('geo_lon', 'Долгота')->sort()->listDefault()->filterDefault(),
            Field::text('city_guid', 'Идентификатор города')->sort()->listDefault()->filterDefault(),

            Field::datetime('created_at', 'Дата создания')->listDefault()->filterDefault(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->filterDefault(),
        ]);
    }
}
