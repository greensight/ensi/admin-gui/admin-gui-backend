<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServices\PatchDeliveryServiceAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServicesQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceEnumValueResource;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServicesResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServicesController
{
    public function patch(int $id, PatchDeliveryServiceRequest $request, PatchDeliveryServiceAction $action): Responsable
    {
        return new DeliveryServicesResource($action->execute($id, $request->validated()));
    }

    public function get(int $id, DeliveryServicesQuery $query): Responsable
    {
        return new DeliveryServicesResource($query->find($id));
    }

    public function search(DeliveryServicesQuery $query): Responsable
    {
        return DeliveryServicesResource::collectPage($query->get());
    }

    public function searchOne(DeliveryServicesQuery $query): Responsable
    {
        return new DeliveryServicesResource($query->first());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, DeliveryServicesQuery $query): Responsable
    {
        return DeliveryServiceEnumValueResource::collection($query->searchEnums());
    }
}
