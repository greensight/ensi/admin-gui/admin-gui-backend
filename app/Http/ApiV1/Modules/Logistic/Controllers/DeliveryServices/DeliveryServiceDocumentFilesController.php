<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceDocuments\DeleteDeliveryServiceDocumentFileAction;
use App\Domain\Logistic\Actions\DeliveryServiceDocuments\SaveDeliveryServiceDocumentFileAction;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\UploadDeliveryServiceDocumentFileRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceDocumentsResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServiceDocumentFilesController
{
    public function upload(int $id, UploadDeliveryServiceDocumentFileRequest $request, SaveDeliveryServiceDocumentFileAction $action): Responsable
    {
        return new DeliveryServiceDocumentsResource($action->execute($id, $request->getFile()));
    }

    public function delete(int $id, DeleteDeliveryServiceDocumentFileAction $action): Responsable
    {
        return new DeliveryServiceDocumentsResource($action->execute($id));
    }
}
