<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryServices;

use App\Domain\Logistic\Actions\DeliveryServiceManagers\CreateDeliveryServiceManagerAction;
use App\Domain\Logistic\Actions\DeliveryServiceManagers\DeleteDeliveryServiceManagerAction;
use App\Domain\Logistic\Actions\DeliveryServiceManagers\PatchDeliveryServiceManagerAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryServices\DeliveryServiceManagersQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\CreateDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryServices\PatchDeliveryServiceManagerRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices\DeliveryServiceManagersResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryServiceManagersController
{
    public function create(CreateDeliveryServiceManagerRequest $request, CreateDeliveryServiceManagerAction $action): Responsable
    {
        return new DeliveryServiceManagersResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryServiceManagerRequest $request, PatchDeliveryServiceManagerAction $action): Responsable
    {
        return new DeliveryServiceManagersResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryServiceManagerAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryServiceManagersQuery $query): Responsable
    {
        return new DeliveryServiceManagersResource($query->find($id));
    }

    public function search(DeliveryServiceManagersQuery $query): Responsable
    {
        return DeliveryServiceManagersResource::collectPage($query->get());
    }

    public function searchOne(DeliveryServiceManagersQuery $query): Responsable
    {
        return new DeliveryServiceManagersResource($query->first());
    }
}
