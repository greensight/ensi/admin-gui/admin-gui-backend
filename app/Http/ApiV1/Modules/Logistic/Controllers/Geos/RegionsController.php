<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\Geos;

use App\Domain\Logistic\Actions\Geos\CreateRegionAction;
use App\Domain\Logistic\Actions\Geos\DeleteRegionAction;
use App\Domain\Logistic\Actions\Geos\PatchRegionAction;
use App\Http\ApiV1\Modules\Logistic\Queries\Geos\RegionsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\CreateRegionRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\PatchRegionRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\RegionEnumValueResource;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\RegionsResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class RegionsController
{
    public function create(CreateRegionRequest $request, CreateRegionAction $action): Responsable
    {
        return new RegionsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchRegionRequest $request, PatchRegionAction $action): Responsable
    {
        return new RegionsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteRegionAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, RegionsQuery $query): Responsable
    {
        return new RegionsResource($query->find($id));
    }

    public function search(RegionsQuery $query): Responsable
    {
        return RegionsResource::collectPage($query->get());
    }

    public function searchOne(RegionsQuery $query): Responsable
    {
        return new RegionsResource($query->first());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, RegionsQuery $query): Responsable
    {
        return RegionEnumValueResource::collection($query->searchEnums());
    }
}
