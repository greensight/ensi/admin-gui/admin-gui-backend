<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\Geos;

use App\Domain\Logistic\Actions\Geos\CreateFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\DeleteFederalDistrictAction;
use App\Domain\Logistic\Actions\Geos\PatchFederalDistrictAction;
use App\Http\ApiV1\Modules\Logistic\Queries\Geos\FederalDistrictsQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\CreateFederalDistrictRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\Geos\PatchFederalDistrictRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\FederalDistrictEnumValueResource;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\FederalDistrictsResource;
use App\Http\ApiV1\Support\Requests\CommonFilterEnumValuesRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class FederalDistrictsController
{
    public function create(CreateFederalDistrictRequest $request, CreateFederalDistrictAction $action): Responsable
    {
        return new FederalDistrictsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchFederalDistrictRequest $request, PatchFederalDistrictAction $action): Responsable
    {
        return new FederalDistrictsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteFederalDistrictAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, FederalDistrictsQuery $query): Responsable
    {
        return new FederalDistrictsResource($query->find($id));
    }

    public function search(FederalDistrictsQuery $query): Responsable
    {
        return FederalDistrictsResource::collectPage($query->get());
    }

    public function searchOne(FederalDistrictsQuery $query): Responsable
    {
        return new FederalDistrictsResource($query->first());
    }

    public function searchEnumValues(CommonFilterEnumValuesRequest $request, FederalDistrictsQuery $query): Responsable
    {
        return FederalDistrictEnumValueResource::collection($query->searchEnums());
    }
}
