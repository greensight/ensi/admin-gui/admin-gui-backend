<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryPrices;

use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryMethodEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\DeliveryServiceEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\FederalDistrictEnumInfo;
use App\Domain\Common\Data\Meta\Enum\Logistic\RegionEnumInfo;
use App\Domain\Common\Data\Meta\Field;
use App\Domain\Logistic\Actions\DeliveryPrices\CreateDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\DeleteDeliveryPriceAction;
use App\Domain\Logistic\Actions\DeliveryPrices\PatchDeliveryPriceAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryPrices\DeliveryPricesQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices\CreateDeliveryPriceRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryPrices\PatchDeliveryPriceRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryPrices\DeliveryPricesResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryPricesController
{
    public function create(CreateDeliveryPriceRequest $request, CreateDeliveryPriceAction $action): Responsable
    {
        return new DeliveryPricesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchDeliveryPriceRequest $request, PatchDeliveryPriceAction $action): Responsable
    {
        return new DeliveryPricesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteDeliveryPriceAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, DeliveryPricesQuery $query): Responsable
    {
        return new DeliveryPricesResource($query->find($id));
    }

    public function search(DeliveryPricesQuery $query): Responsable
    {
        return DeliveryPricesResource::collectPage($query->get());
    }

    public function searchOne(DeliveryPricesQuery $query): Responsable
    {
        return new DeliveryPricesResource($query->first());
    }

    public function meta(
        DeliveryServiceEnumInfo $deliveryService,
        DeliveryMethodEnumInfo $deliveryMethod,
        FederalDistrictEnumInfo $federalDistrict,
        RegionEnumInfo $region
    ): ModelMetaResource {
        return new ModelMetaResource([
            Field::id()->listDefault()->filterDefault()->detailLink(),
            Field::enum('federal_district_id', 'Федеральный округ', $federalDistrict)->listDefault()->sort('federal_district.name'),
            Field::enum('region_id', 'Регион', $region)->listDefault()->sort('region.name'),
            Field::keyword('region_guid', 'ФИАС региона')->listDefault(),
            Field::enum('delivery_service', 'Служба доставки', $deliveryService)->listDefault(),
            Field::enum('delivery_method', 'Метод доставки', $deliveryMethod)->listDefault(),
            Field::price('price', 'Цена')->listDefault(),
        ]);
    }
}
