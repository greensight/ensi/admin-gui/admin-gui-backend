<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis;

use App\Domain\Logistic\Actions\DeliveryKpis\CreateDeliveryKpiCtAction;
use App\Domain\Logistic\Actions\DeliveryKpis\DeleteDeliveryKpiCtAction;
use App\Domain\Logistic\Actions\DeliveryKpis\PatchDeliveryKpiCtAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis\DeliveryKpiCtQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\CreateDeliveryKpiCtRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\PatchDeliveryKpiCtRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis\DeliveryKpiCtResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryKpiCtController
{
    public function search(DeliveryKpiCtQuery $query): Responsable
    {
        return DeliveryKpiCtResource::collectPage($query->get());
    }

    public function searchOne(DeliveryKpiCtQuery $query): Responsable
    {
        return new DeliveryKpiCtResource($query->first());
    }

    public function get(int $sellerId, DeliveryKpiCtQuery $query): Responsable
    {
        return new DeliveryKpiCtResource($query->find($sellerId));
    }

    public function create(int $sellerId, CreateDeliveryKpiCtRequest $request, CreateDeliveryKpiCtAction $action): Responsable
    {
        return new DeliveryKpiCtResource($action->execute($sellerId, $request->validated()));
    }

    public function patch(int $sellerId, PatchDeliveryKpiCtRequest $request, PatchDeliveryKpiCtAction $action): Responsable
    {
        return new DeliveryKpiCtResource($action->execute($sellerId, $request->validated()));
    }

    public function delete(int $sellerId, DeleteDeliveryKpiCtAction $action): Responsable
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
