<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis;

use App\Domain\Logistic\Actions\DeliveryKpis\CreateDeliveryKpiPptAction;
use App\Domain\Logistic\Actions\DeliveryKpis\DeleteDeliveryKpiPptAction;
use App\Domain\Logistic\Actions\DeliveryKpis\PatchDeliveryKpiPptAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis\DeliveryKpiPptQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\CreateDeliveryKpiPptRequest;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\PatchDeliveryKpiPptRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis\DeliveryKpiPptResource;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryKpiPptController
{
    public function search(DeliveryKpiPptQuery $query): Responsable
    {
        return DeliveryKpiPptResource::collectPage($query->get());
    }

    public function searchOne(DeliveryKpiPptQuery $query): Responsable
    {
        return new DeliveryKpiPptResource($query->first());
    }

    public function get(int $sellerId, DeliveryKpiPptQuery $query): Responsable
    {
        return new DeliveryKpiPptResource($query->find($sellerId));
    }

    public function create(int $sellerId, CreateDeliveryKpiPptRequest $request, CreateDeliveryKpiPptAction $action): Responsable
    {
        return new DeliveryKpiPptResource($action->execute($sellerId, $request->validated()));
    }

    public function patch(int $sellerId, PatchDeliveryKpiPptRequest $request, PatchDeliveryKpiPptAction $action): Responsable
    {
        return new DeliveryKpiPptResource($action->execute($sellerId, $request->validated()));
    }

    public function delete(int $sellerId, DeleteDeliveryKpiPptAction $action): Responsable
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }
}
