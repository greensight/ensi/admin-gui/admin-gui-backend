<?php

namespace App\Http\ApiV1\Modules\Logistic\Controllers\DeliveryKpis;

use App\Domain\Common\Data\Meta\Field;
use App\Domain\Logistic\Actions\DeliveryKpis\PatchDeliveryKpiAction;
use App\Http\ApiV1\Modules\Logistic\Queries\DeliveryKpis\DeliveryKpiQuery;
use App\Http\ApiV1\Modules\Logistic\Requests\DeliveryKpis\PatchDeliveryKpiRequest;
use App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis\DeliveryKpiResource;
use App\Http\ApiV1\Support\Resources\ModelMetaResource;
use Illuminate\Contracts\Support\Responsable;

class DeliveryKpiController
{
    public function get(DeliveryKpiQuery $query): Responsable
    {
        return new DeliveryKpiResource($query->get());
    }

    public function patch(PatchDeliveryKpiRequest $request, PatchDeliveryKpiAction $action): Responsable
    {
        return new DeliveryKpiResource($action->execute($request->validated()));
    }

    public function meta(): Responsable
    {
        return new ModelMetaResource([
            Field::id()->detailLink()->sortDefault(),
            Field::integer('rtg', 'Среднее время подтверждения заказа')->listDefault()->resetFilter(),
            Field::integer('cc', 'Среднее время ожидания начала сборки заказа')->listDefault()->resetFilter(),
            Field::integer('ppt', 'Среднее время сборки заказа')->listDefault()->resetFilter(),
            Field::datetime('created_at', 'Дата создания')->listDefault()->resetFilter(),
            Field::datetime('updated_at', 'Дата обновления')->listDefault()->resetFilter(),
        ]);
    }
}
