<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\PointWorking;
use Illuminate\Http\Request;

/**
 * @mixin PointWorking
 */
class PointWorkingsResource extends BaseJsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->getId(),
            'point_id' => $this->getPointId(),
            'is_active' => $this->getIsActive(),
            'day' => $this->getDay(),
            'working_start_time' => $this->getWorkingStartTime(),
            'working_end_time' => $this->getWorkingEndTime(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
