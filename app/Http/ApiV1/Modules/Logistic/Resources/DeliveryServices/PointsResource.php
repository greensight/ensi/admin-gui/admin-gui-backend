<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\AddressResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\Point;

/**
 * @mixin Point
 */
class PointsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'delivery_service_id' => $this->getDeliveryServiceId(),
            'external_id' => $this->getExternalId(),
            'is_active' => $this->getIsActive(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'phone' => $this->getPhone(),
            'timezone' => $this->getTimezone(),
            'address' => AddressResource::makeIfNotNull($this->getAddress()),
            'address_reduce' => $this->getAddressReduce(),
            'metro_station' => $this->getMetroStation(),
            'city_guid' => $this->getCityGuid(),
            'geo_lat' => $this->getGeoLat(),
            'geo_lon' => $this->getGeoLon(),
            'only_online_payment' => $this->getOnlyOnlinePayment(),
            'has_payment_card' => $this->getHasPaymentCard(),
            'has_courier' => $this->getHasCourier(),
            'is_postamat' => $this->getIsPostamat(),
            'max_value' => $this->getMaxValue(),
            'max_weight' => $this->getMaxWeight(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),

            'point_workings' => PointWorkingsResource::collection($this->whenNotNull($this->getPointWorkings())),
        ];
    }
}
