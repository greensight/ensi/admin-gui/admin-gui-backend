<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryServices;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\LogisticClient\Dto\DeliveryService;

/** @mixin DeliveryService */
class DeliveryServiceEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
