<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\CargoOrders;

use App\Domain\Logistic\Data\CargoData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\CargoOrder;
use Illuminate\Http\Resources\MissingValue;

/**
 * @mixin CargoOrder
 */
class CargoOrdersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        $cargo = is_null($this->getCargo()) ? new MissingValue() : new CargoData($this->getCargo());

        return [
            'id' => $this->getId(),
            'cargo_id' => $this->getCargoId(),
            'timeslot_id' => $this->getTimeslotId(),
            'timeslot_from' => $this->dateTimeToIso($this->getTimeslotFrom()),
            'timeslot_to' => $this->dateTimeToIso($this->getTimeslotTo()),
            'cdek_intake_number' => $this->getCdekIntakeNumber(),
            'external_id' => $this->getExternalId(),
            'error_external_id' => $this->getErrorExternalId(),
            'date' => $this->dateTimeToIso($this->getDate()),
            'status' => $this->getStatus(),
            'cargo' => CargoResource::make($cargo),
        ];
    }
}
