<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryKpis;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\DeliveryKpi;

/** @mixin DeliveryKpi */
class DeliveryKpiResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'rtg' => $this->getRtg(),
            'ct' => $this->getCt(),
            'ppt' => $this->getPpt(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
