<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\DeliveryPrices;

use App\Http\ApiV1\Modules\Logistic\Resources\Geos\FederalDistrictsResource;
use App\Http\ApiV1\Modules\Logistic\Resources\Geos\RegionsResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\DeliveryPrice;

/** @mixin DeliveryPrice */
class DeliveryPricesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'federal_district_id' => $this->getFederalDistrictId(),
            'region_id' => $this->getRegionId(),
            'region_guid' => $this->getRegionGuid(),
            'delivery_service' => $this->getDeliveryService(),
            'delivery_method' => $this->getDeliveryMethod(),
            'price' => $this->getPrice(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'federal_district' => new FederalDistrictsResource($this->whenNotNull($this->getFederalDistrict())),
            'region' => new RegionsResource($this->whenNotNull($this->getRegion())),
        ];
    }
}
