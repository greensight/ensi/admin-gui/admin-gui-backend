<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\Geos;

use App\Http\ApiV1\Support\Resources\EnumValueResource;
use Ensi\LogisticClient\Dto\Region;

/**
 * @mixin Region
 */
class RegionEnumValueResource extends EnumValueResource
{
    protected function getEnumId(): int
    {
        return $this->getId();
    }

    protected function getEnumTitle(): string
    {
        return $this->getName();
    }
}
