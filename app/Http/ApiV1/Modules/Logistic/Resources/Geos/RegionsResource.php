<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\Geos;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\Region;

/** @mixin Region */
class RegionsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'federal_district_id' => $this->getFederalDistrictId(),
            'name' => $this->getName(),
            'guid' => $this->getGuid(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
        ];
    }
}
