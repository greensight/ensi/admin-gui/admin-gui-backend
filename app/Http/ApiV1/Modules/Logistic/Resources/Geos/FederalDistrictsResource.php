<?php

namespace App\Http\ApiV1\Modules\Logistic\Resources\Geos;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LogisticClient\Dto\FederalDistrict;

/** @mixin FederalDistrict */
class FederalDistrictsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'created_at' => $this->dateTimeToIso($this->getCreatedAt()),
            'updated_at' => $this->dateTimeToIso($this->getUpdatedAt()),
            'regions' => RegionsResource::collection($this->whenNotNull($this->getRegions())),
        ];
    }
}
