<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\Geos\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class RegionsRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'federal_district_id' => $this->faker->modelId(),
            'name' => $this->faker->name(),
            'guid' => $this->faker->uuid(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
