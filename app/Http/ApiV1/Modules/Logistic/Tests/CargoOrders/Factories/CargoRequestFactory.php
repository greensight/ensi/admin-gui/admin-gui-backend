<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\CargoOrders\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\LogisticCargoStatusEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CargoRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'seller_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'delivery_service_id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(array_column(LogisticCargoStatusEnum::cases(), 'value')),
            'status_at' => $this->faker->dateTime(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->dateTime(),
            'shipping_problem_comment' => $this->faker->text(200),
            'is_canceled' => $this->faker->boolean(),
            'is_canceled_at' => $this->faker->dateTime(),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),

            'width' => $this->faker->randomFloat(),
            'height' => $this->faker->randomFloat(),
            'length' => $this->faker->randomFloat(),
            'weight' => $this->faker->randomFloat(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
