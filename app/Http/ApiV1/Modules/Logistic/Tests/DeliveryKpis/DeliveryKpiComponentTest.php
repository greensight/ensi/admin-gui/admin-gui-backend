<?php

use App\Domain\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryKpis\Factories\DeliveryKpiRequestFactory;

use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-kpi success", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_READ]);

    $this->mockLogisticKpiApi()->allows([
        'getDeliveryKpi' => DeliveryKpiFactory::new()->makeResponse(),
    ]);

    getJson("/api/v1/logistic/delivery-kpi")->assertStatus(200);
});

test("PATCH /api/v1/logistic/delivery-kpi success", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_EDIT]);

    $deliveryKpiData = DeliveryKpiRequestFactory::new()->make();

    $this->mockLogisticKpiApi()->allows([
        'patchDeliveryKpi' => DeliveryKpiFactory::new()->makeResponse(),
    ]);

    patchJson("/api/v1/logistic/delivery-kpi", $deliveryKpiData)->assertStatus(200);
});

test('GET /api/v1/logistic/delivery-kpi:meta success', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_READ]);
    getJson('/api/v1/logistic/delivery-kpi:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});
