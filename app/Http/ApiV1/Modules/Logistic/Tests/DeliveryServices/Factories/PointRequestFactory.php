<?php

namespace App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseAddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PointRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'delivery_service_id' => $this->faker->nullable()->modelId(),
            'address' => $this->faker->nullable()->exactly(BaseAddressFactory::new()->make()),
            'is_active' => $this->faker->boolean(),
            'city_guid' => $this->faker->nullable()->uuid(),
            'timezone' => $this->faker->nullable()->timezone(),
            'geo_lat' => $this->faker->nullable()->exactly((string)$this->faker->latitude()),
            'geo_lon' => $this->faker->nullable()->exactly((string)$this->faker->longitude()),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
