<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\DeliveryServiceRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'logistic');

test("GET /api/v1/logistic/delivery-services/{id} success", function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([$right]);

    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryService' => DeliveryServiceFactory::new()->makeResponse(['id' => $deliveryServiceId]),
    ]);

    getJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceId);
})->with(RightsAccessEnum::DELIVERY_SERVICES_DETAIL_READ);

test("GET /api/v1/logistic/delivery-services/{id} 403", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);

    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'getDeliveryService' => DeliveryServiceFactory::new()->makeResponse(['id' => $deliveryServiceId]),
    ]);

    getJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}")
        ->assertStatus(403);
});

test("PATCH /api/v1/logistic/delivery-services/{id} success", function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([$right]);

    $deliveryServiceId = 1;
    $deliveryServicesData = DeliveryServiceRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'patchDeliveryService' => DeliveryServiceFactory::new()->makeResponse(['id' => $deliveryServiceId]),
    ]);

    patchJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}", $deliveryServicesData)
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceId);
})->with(RightsAccessEnum::DELIVERY_SERVICES_DETAIL_EDIT);

test("PATCH /api/v1/logistic/delivery-services/{id} 403", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);

    $deliveryServiceId = 1;
    $deliveryServicesData = DeliveryServiceRequestFactory::new()->make();

    $this->mockLogisticDeliveryServicesApi()->allows([
        'patchDeliveryService' => DeliveryServiceFactory::new()->makeResponse(['id' => $deliveryServiceId]),
    ]);

    patchJson("/api/v1/logistic/delivery-services/{$deliveryServiceId}", $deliveryServicesData)
        ->assertStatus(403);
});

test("POST /api/v1/logistic/delivery-services:search success", function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([$right]);

    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch([['id' => $deliveryServiceId]]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search")
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $deliveryServiceId);
})->with(RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ);

test("POST /api/v1/logistic/delivery-services:search 403", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);

    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch([['id' => $deliveryServiceId]]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search")
        ->assertStatus(403);
});

test("POST /api/v1/logistic/delivery-services:search-one success", function (int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([$right]);

    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchOneDeliveryService' => DeliveryServiceFactory::new()->makeResponse(['id' => $deliveryServiceId]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search-one")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $deliveryServiceId);
})->with(RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ);

test("POST /api/v1/logistic/delivery-services:search-one 403", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);

    $deliveryServiceId = 1;

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchOneDeliveryService' => DeliveryServiceFactory::new()->makeResponse(['id' => $deliveryServiceId]),
    ]);

    postJson("/api/v1/logistic/delivery-services:search-one")
        ->assertStatus(403);
});

test('POST /api/v1/logistic/delivery-service-enum-values:search 200', function ($key, $value, int $right) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([$right]);

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/logistic/delivery-service-enum-values:search", ['filter' => [$key => $value]])->assertStatus(200);
})->with([
    ['id', [1, 2], RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ],
    ['query', 'name', RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ],
]);

test('POST /api/v1/logistic/delivery-service-enum-values:search 403', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([]);

    $this->mockLogisticDeliveryServicesApi()->allows([
        'searchDeliveryServices' => DeliveryServiceFactory::new()->makeResponseSearch(),
    ]);

    postJson("/api/v1/logistic/delivery-service-enum-values:search")->assertStatus(403);
});
