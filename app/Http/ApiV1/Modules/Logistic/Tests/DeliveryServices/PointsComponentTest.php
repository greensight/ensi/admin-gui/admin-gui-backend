<?php

use App\Domain\Logistic\Tests\DeliveryServices\Factories\PointFactory;
use App\Http\ApiV1\Modules\Logistic\Tests\DeliveryServices\Factories\PointRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentWithRightsTestCase;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Ensi\LogisticClient\Dto\SearchPointsRequest;

use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function PHPUnit\Framework\assertEquals;

uses(ApiV1ComponentWithRightsTestCase::class);
uses()->group('component', 'logistic');

test('POST /api/v1/logistic/point-enum-values:search 200', function ($key, $value) {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->mockLogisticGeosApi()
        ->shouldReceive('searchPoints')
        ->once()
        ->andReturn(PointFactory::new()->makeResponseSearch());

    postJson("/api/v1/logistic/point-enum-values:search", ['filter' => [$key => $value]])->assertOk();
})->with([
    ['id', [1, 2]],
    ['query', 'name'],
]);

test("POST /api/v1/logistic/points:search success", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::POINT_LIST_READ]);
    $pointId = 1;

    $this->mockLogisticGeosApi()
        ->shouldReceive('searchPoints')
        ->once()
        ->andReturn(PointFactory::new()->makeResponseSearch([['id' => $pointId]]));

    postJson("/api/v1/logistic/points:search")
        ->assertOk()
        ->assertJsonPath('data.0.id', $pointId);
});

test("POST /api/v1/logistic/points:search all includes 200", function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::POINT_LIST_READ]);

    $pointId = 1;
    $includes = ["point_workings"];

    $pointsSearchResponse = PointFactory::new()->withPointWorking()->makeResponseSearch([['id' => $pointId]]);

    $this->mockLogisticGeosApi()
        ->shouldReceive('searchPoints')
        ->once()
        ->withArgs(function (SearchPointsRequest $arg) use ($includes) {
            assertEquals($includes, $arg->getInclude());

            return true;
        })
        ->andReturn($pointsSearchResponse);

    postJson("/api/v1/logistic/points:search", [
        "include" => $includes,
    ])
        ->assertOk()
        ->assertJsonPath('data.0.id', $pointId)
        ->assertJsonCount(1, 'data.0.point_workings');
});

test('POST /api/v1/logistic/points:search 403', function () {
    postJson('/api/v1/logistic/points:search')
        ->assertForbidden();
});

test('GET /api/v1/logistic/points:meta 200', function () {
    $this->setRights([RightsAccessEnum::POINT_LIST_READ]);
    getJson('/api/v1/logistic/points:meta')
        ->assertOk()
        ->assertJsonStructure(['data' => ['fields', 'detail_link', 'default_sort', 'default_list']]);
});

test('GET /api/v1/logistic/points:meta 403', function () {
    getJson('/api/v1/logistic/points:meta')
        ->assertForbidden();
});

test('GET /api/v1/logistic/points/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::POINT_DETAIL_READ]);

    $pointId = 1;
    $requestInclude = "point_workings";

    $pointsSearchResponse = PointFactory::new()->withPointWorking()->makeResponse(['id' => $pointId]);

    $this->mockLogisticGeosApi()
        ->shouldReceive('getPoint')
        ->once()
        ->withArgs(function ($id, $include = null) use ($pointId, $requestInclude) {
            assertEquals($id, $pointId);
            assertEquals($include, $requestInclude);

            return true;
        })
        ->andReturn($pointsSearchResponse);

    getJson("/api/v1/logistic/points/{$pointId}?include={$requestInclude}")
        ->assertOk()
        ->assertJsonPath('data.id', $pointId)
        ->assertJsonCount(1, 'data.point_workings');
});

test('GET /api/v1/logistic/points/{id} 403', function () {
    getJson('/api/v1/logistic/points/403')
        ->assertStatus(403);
});

test('PATCH /api/v1/logistic/points/{id} 200', function () {
    /** @var ApiV1ComponentWithRightsTestCase $this */
    $this->setRights([RightsAccessEnum::POINT_DETAIL_MAIN_DATA_EDIT]);

    $pointId = 1;

    $this->mockLogisticGeosApi()
        ->shouldReceive('patchPoint')
        ->once()
        ->withArgs(function ($id, $request) use ($pointId) {
            assertEquals($id, $pointId);

            return true;
        })
        ->andReturn(PointFactory::new()->makeResponse(['id' => $pointId]));

    $request = PointRequestFactory::new()->make();
    patchJson("/api/v1/logistic/points/{$pointId}", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $pointId);
});

test('PATCH /api/v1/logistic/points/{id} 403', function () {
    $request = PointRequestFactory::new()->make();

    patchJson('/api/v1/logistic/points/403', $request)
        ->assertForbidden();
});
