<?php

namespace App\Http\ApiV1\Modules\Logistic\Policies\Geos;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class RegionsControllerPolicy
{
    use HandlesAuthorization;

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
        ]);
    }

    public function searchOne(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
        ]);
    }

    public function searchEnumValues(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
        ]);
    }
}
