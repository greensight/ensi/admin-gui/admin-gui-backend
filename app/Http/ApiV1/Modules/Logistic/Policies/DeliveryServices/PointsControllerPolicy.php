<?php

namespace App\Http\ApiV1\Modules\Logistic\Policies\DeliveryServices;

use App\Domain\Auth\Models\User;
use App\Domain\Auth\Traits\HandlesRules;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class PointsControllerPolicy
{
    use HandlesAuthorization;
    use HandlesRules;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::POINT_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $this->allowFieldsOf(
            data: request()->input(),
            userPermissions: $user->rightsAccess,
            mainPermissions: [
                RightsAccessEnum::POINT_DETAIL_MAIN_DATA_EDIT,
            ],
            permissionsFields: [
                RightsAccessEnum::POINT_DETAIL_ADDRESS_EDIT => ['address'],

            ],
        );
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::POINT_LIST_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::POINT_LIST_READ,
        ]);
    }
}
