<?php

namespace App\Http\ApiV1\Modules\Logistic\Policies\DeliveryServices;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DeliveryServicesControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_SERVICES_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_SERVICES_DETAIL_EDIT,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ,
        ]);
    }

    public function searchOne(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ,
        ]);
    }

    public function searchEnumValues(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_SERVICES_TABLES_READ,
        ]);
    }
}
