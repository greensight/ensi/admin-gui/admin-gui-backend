<?php

namespace App\Http\ApiV1\Modules\Logistic\Policies\DeliveryKpis;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DeliveryKpiControllerPolicy
{
    use HandlesAuthorization;

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_READ,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_EDIT,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_OPTIONS_SETTINGS_DETAIL_READ,
        ]);
    }
}
