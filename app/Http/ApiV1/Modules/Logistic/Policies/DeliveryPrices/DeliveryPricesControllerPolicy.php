<?php

namespace App\Http\ApiV1\Modules\Logistic\Policies\DeliveryPrices;

use App\Domain\Auth\Models\User;
use Ensi\AdminAuthClient\Dto\RightsAccessEnum;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class DeliveryPricesControllerPolicy
{
    use HandlesAuthorization;

    public function create(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_CREATE,
        ]);
    }

    public function get(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_DETAIL_READ,
        ]);
    }

    public function delete(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_DELETE,
        ]);
    }

    public function patch(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_DETAIL_EDIT,
        ]);
    }

    public function replace(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_DETAIL_EDIT,
        ]);
    }

    public function search(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
        ]);
    }

    public function searchOne(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
            RightsAccessEnum::DELIVERY_PRICE_DETAIL_READ,
        ]);
    }

    public function meta(User $user): Response
    {
        return $user->allowOneOf([
            RightsAccessEnum::DELIVERY_PRICE_LIST_READ,
        ]);
    }
}
