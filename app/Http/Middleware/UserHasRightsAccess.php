<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserHasRightsAccess
{
    /**
     * @throws HttpException
     */
    public function handle(Request $request, Closure $next): Response
    {
        $method = $request->route()->getActionMethod();
        $controller = $request->route()->getControllerClass();

        $response = Gate::inspect($method, [$controller]);

        return match (true) {
            $response->allowed(), !$response->status() => $next($request),
            default => throw new HttpException($response->status(), 'Access denied.'),
        };
    }
}
