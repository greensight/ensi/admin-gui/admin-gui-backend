<?php

namespace Tests;

use Ensi\AdminAuthClient\Api\EnumsApi as AdminAuthEnumsApi;
use Ensi\AdminAuthClient\Api\OauthApi;
use Ensi\AdminAuthClient\Api\RolesApi as AdminAuthUserRolesApi;
use Ensi\AdminAuthClient\Api\UsersApi as AdminAuthUsersApi;
use Ensi\AdminAuthClient\Configuration;
use Ensi\BasketsClient\Api\BasketItemsApi;
use Ensi\BasketsClient\Api\BasketsApi;
use Ensi\BasketsClient\Api\CommonApi as BasketsCommonApi;
use Ensi\BuClient\Api\SellersApi;
use Ensi\BuClient\Api\StoreContactsApi;
use Ensi\BuClient\Api\StorePickupTimesApi;
use Ensi\BuClient\Api\StoresApi;
use Ensi\BuClient\Api\StoreWorkingsApi;
use Ensi\CatalogCacheClient\Api\BrandsApi as CatalogCacheBrandsApi;
use Ensi\CatalogCacheClient\Api\CategoriesApi as CatalogCacheCategoriesApi;
use Ensi\CatalogCacheClient\Api\CommonApi as CatalogCacheCommonApi;
use Ensi\CatalogCacheClient\Api\DiscountsApi as CatalogCacheDiscountsApi;
use Ensi\CatalogCacheClient\Api\ElasticApi;
use Ensi\CatalogCacheClient\Api\NameplatesApi as CatalogCacheNameplatesApi;
use Ensi\CatalogCacheClient\Api\OffersApi as CatalogCacheOffersApi;
use Ensi\CatalogCacheClient\Api\ProductGroupsApi as CatalogCacheProductGroupsApi;
use Ensi\CatalogCacheClient\Api\ProductsApi as CatalogCacheProductsApi;
use Ensi\CatalogCacheClient\Api\PropertiesApi as CatalogCachePropertiesApi;
use Ensi\CatalogCacheClient\Api\PropertyDirectoryValuesApi;
use Ensi\CmsClient\Api\BannersApi;
use Ensi\CmsClient\Api\NameplateProductsApi;
use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Api\PagesApi;
use Ensi\CmsClient\Api\ProductsApi as CmsProductsApi;
use Ensi\CmsClient\Api\SeoTemplateProductsApi;
use Ensi\CmsClient\Api\SeoTemplatesApi;
use Ensi\CommunicationManagerClient\Api\NotificationsApi;
use Ensi\CrmClient\Api\BonusOperationsApi;
use Ensi\CrmClient\Api\CustomerFavoritesApi;
use Ensi\CrmClient\Api\CustomersInfoApi;
use Ensi\CrmClient\Api\PreferencesApi;
use Ensi\CrmClient\Api\ProductSubscribesApi;
use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\CustomersClient\Api\AddressesApi;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Api\StatusesApi;
use Ensi\FeedClient\Api\CloudIntegrationsApi as FeedCloudIntegrationsApi;
use Ensi\FeedClient\Api\CommonApi as FeedCommonApi;
use Ensi\FeedClient\Api\FeedsApi;
use Ensi\FeedClient\Api\FeedSettingsApi;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\MarketingClient\Api\DiscountProductsApi;
use Ensi\MarketingClient\Api\DiscountRelationsApi;
use Ensi\MarketingClient\Api\DiscountsApi;
use Ensi\MarketingClient\Api\PromoCodesApi;
use Ensi\OffersClient\Api\CommonApi as OffersCommonApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Api\StocksApi;
use Ensi\OmsClient\Api\CommonApi as OmsCommonApi;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Api\EnumsApi as OmsEnumsApi;
use Ensi\OmsClient\Api\OrderItemsApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Api\RefundsApi;
use Ensi\OmsClient\Api\ShipmentsApi;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Api\CommonApi;
use Ensi\PimClient\Api\ImportsApi;
use Ensi\PimClient\Api\ProductEventsApi;
use Ensi\PimClient\Api\ProductFieldsApi;
use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Api\ProductStatusesApi;
use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Api\TempFilesApi;
use Ensi\ReviewsClient\Api\ReviewsApi;
use Ensi\SellerAuthClient\Api\UsersApi as SellerAuthUsersApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Catalog ===============

    // region service Offers
    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }

    protected function mockOffersStocksApi(): MockInterface|StocksApi
    {
        return $this->mock(StocksApi::class);
    }

    public function mockOffersCommonApi(): MockInterface|OffersCommonApi
    {
        return $this->mock(OffersCommonApi::class);
    }
    // endregion

    // region service Pim
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }

    protected function mockPimPropertiesApi(): MockInterface|PropertiesApi
    {
        return $this->mock(PropertiesApi::class);
    }

    protected function mockPimCategoriesApi(): MockInterface|CategoriesApi
    {
        return $this->mock(CategoriesApi::class);
    }

    protected function mockPimBrandsApi(): MockInterface|BrandsApi
    {
        return $this->mock(BrandsApi::class);
    }

    protected function mockPimProductFieldsApi(): MockInterface|ProductFieldsApi
    {
        return $this->mock(ProductFieldsApi::class);
    }

    protected function mockPimProductGroupsApi(): MockInterface|ProductGroupsApi
    {
        return $this->mock(ProductGroupsApi::class);
    }

    protected function mockPimImportsApi(): MockInterface|ImportsApi
    {
        return $this->mock(ImportsApi::class);
    }

    protected function mockPimCommonApi(): MockInterface|CommonApi
    {
        return $this->mock(CommonApi::class);
    }

    protected function mockPimTempFilesApi(): MockInterface|TempFilesApi
    {
        return $this->mock(TempFilesApi::class);
    }

    protected function mockProductEventsApi(): MockInterface|ProductEventsApi
    {
        return $this->mock(ProductEventsApi::class);
    }

    protected function mockPimProductStatusesApi(): MockInterface|ProductStatusesApi
    {
        return $this->mock(ProductStatusesApi::class);
    }
    // endregion

    // region service Feed
    public function mockFeedsApi(): MockInterface|FeedsApi
    {
        return $this->mock(FeedsApi::class);
    }

    public function mockFeedSettingsApi(): MockInterface|FeedSettingsApi
    {
        return $this->mock(FeedSettingsApi::class);
    }

    public function mockFeedCloudIntegrationApi(): MockInterface|FeedCloudIntegrationsApi
    {
        return $this->mock(FeedCloudIntegrationsApi::class);
    }

    public function mockFeedCommonApi(): MockInterface|FeedCommonApi
    {
        return $this->mock(FeedCommonApi::class);
    }

    // region service Catalog Cache
    public function mockCatalogCacheCommonApi(): MockInterface|CatalogCacheCommonApi
    {
        return $this->mock(CatalogCacheCommonApi::class);
    }

    protected function mockCatalogCacheElasticApi(): MockInterface|ElasticApi
    {
        return $this->mock(ElasticApi::class);
    }

    protected function mockCatalogCacheCategoriesApi(): MockInterface|CatalogCacheCategoriesApi
    {
        return $this->mock(CatalogCacheCategoriesApi::class);
    }

    protected function mockCatalogCachePropertiesApi(): MockInterface|CatalogCachePropertiesApi
    {
        return $this->mock(CatalogCachePropertiesApi::class);
    }

    protected function mockCatalogCacheNameplatesApi(): MockInterface|CatalogCacheNameplatesApi
    {
        return $this->mock(CatalogCacheNameplatesApi::class);
    }

    protected function mockCatalogCacheBrandsApi(): MockInterface|CatalogCacheBrandsApi
    {
        return $this->mock(CatalogCacheBrandsApi::class);
    }

    protected function mockCatalogCachePropertyDirectoryValuesApi(): MockInterface|PropertyDirectoryValuesApi
    {
        return $this->mock(PropertyDirectoryValuesApi::class);
    }

    protected function mockCatalogCacheProductGroupsApi(): MockInterface|CatalogCacheProductGroupsApi
    {
        return $this->mock(CatalogCacheProductGroupsApi::class);
    }

    protected function mockCatalogCacheProductsApi(): MockInterface|CatalogCacheProductsApi
    {
        return $this->mock(CatalogCacheProductsApi::class);
    }

    protected function mockCatalogCacheOffersApi(): MockInterface|CatalogCacheOffersApi
    {
        return $this->mock(CatalogCacheOffersApi::class);
    }

    protected function mockCatalogCacheDiscountsApi(): MockInterface|CatalogCacheDiscountsApi
    {
        return $this->mock(CatalogCacheDiscountsApi::class);
    }

    // endregion

    // =============== Reviews ===============

    // region service Reviews
    public function mockReviewsReviewsApi(): MockInterface|ReviewsApi
    {
        return $this->mock(ReviewsApi::class);
    }

    // endregion


    // =============== CMS ===============

    // region service CMS
    protected function mockCmsPagesApi(): MockInterface|PagesApi
    {
        return $this->mock(PagesApi::class);
    }

    protected function mockCmsNameplatesApi(): MockInterface|NameplatesApi
    {
        return $this->mock(NameplatesApi::class);
    }

    protected function mockCmsNameplateProductsApi(): MockInterface|NameplateProductsApi
    {
        return $this->mock(NameplateProductsApi::class);
    }

    protected function mockCmsProductsApi(): MockInterface|CmsProductsApi
    {
        return $this->mock(CmsProductsApi::class);
    }

    protected function mockCmsSeoTemplatesApi(): MockInterface|SeoTemplatesApi
    {
        return $this->mock(SeoTemplatesApi::class);
    }

    protected function mockCmsSeoTemplateProductsApi(): MockInterface|SeoTemplateProductsApi
    {
        return $this->mock(SeoTemplateProductsApi::class);
    }

    protected function mockCmsBannersApi(): MockInterface|BannersApi
    {
        return $this->mock(BannersApi::class);
    }
    // endregion

    // =============== CRM ===============

    // region service CRM
    protected function mockCrmBonusOperationsApi(): MockInterface|BonusOperationsApi
    {
        return $this->mock(BonusOperationsApi::class);
    }

    protected function mockCrmCustomersInfoApi(): MockInterface|CustomersInfoApi
    {
        return $this->mock(CustomersInfoApi::class);
    }

    protected function mockCrmPreferencesApi(): MockInterface|PreferencesApi
    {
        return $this->mock(PreferencesApi::class);
    }

    protected function mockCrmCustomerFavoritesApi(): MockInterface|CustomerFavoritesApi
    {
        return $this->mock(CustomerFavoritesApi::class);
    }

    protected function mockCrmProductSubscribesApi(): MockInterface|ProductSubscribesApi
    {
        return $this->mock(ProductSubscribesApi::class);
    }
    // endregion

    // =============== Customers ===============

    // region service Customers
    public function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }

    public function mockCustomersStatusesApi(): MockInterface|StatusesApi
    {
        return $this->mock(StatusesApi::class);
    }

    public function mockCustomersAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class);
    }

    public function mockCustomersAddressesApi(): MockInterface|AddressesApi
    {
        return $this->mock(AddressesApi::class);
    }
    // endregion


    // =============== Logistic ===============

    // region service Logistic
    protected function mockLogisticCargoOrdersApi(): MockInterface|CargoOrdersApi
    {
        return $this->mock(CargoOrdersApi::class);
    }

    protected function mockLogisticKpiApi(): MockInterface|KpiApi
    {
        return $this->mock(KpiApi::class);
    }

    public function mockLogisticDeliveryServicesApi(): MockInterface|DeliveryServicesApi
    {
        return $this->mock(DeliveryServicesApi::class);
    }

    public function mockLogisticGeosApi(): MockInterface|GeosApi
    {
        return $this->mock(GeosApi::class);
    }

    public function mockLogisticDeliveryPricesApi(): MockInterface|DeliveryPricesApi
    {
        return $this->mock(DeliveryPricesApi::class);
    }
    // endregion


    // =============== Marketing ===============

    // region service Marketing
    protected function mockMarketingDiscountsApi(): MockInterface|DiscountsApi
    {
        return $this->mock(DiscountsApi::class);
    }

    protected function mockMarketingDiscountProductsApi(): MockInterface|DiscountProductsApi
    {
        return $this->mock(DiscountProductsApi::class);
    }

    protected function mockMarketingDiscountRelationsApi(): MockInterface|DiscountRelationsApi
    {
        return $this->mock(DiscountRelationsApi::class);
    }

    protected function mockMarketingPromoCodesApi(): MockInterface|PromoCodesApi
    {
        return $this->mock(PromoCodesApi::class);
    }
    // endregion


    // =============== Orders ===============

    // region service OMS
    public function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    public function mockOmsOrderItemsApi(): MockInterface|OrderItemsApi
    {
        return $this->mock(OrderItemsApi::class);
    }

    public function mockOmsShipmentsApi(): MockInterface|ShipmentsApi
    {
        return $this->mock(ShipmentsApi::class);
    }

    public function mockOmsDeliveriesApi(): MockInterface|DeliveriesApi
    {
        return $this->mock(DeliveriesApi::class);
    }

    public function mockOmsRefundsApi(): MockInterface|RefundsApi
    {
        return $this->mock(RefundsApi::class);
    }

    public function mockOmsEnumsApi(): MockInterface|OmsEnumsApi
    {
        return $this->mock(OmsEnumsApi::class);
    }

    public function mockOmsCommonApi(): MockInterface|OmsCommonApi
    {
        return $this->mock(OmsCommonApi::class);
    }
    // endregion

    // region service Baskets
    public function mockBasketsCommonApi(): MockInterface|BasketsCommonApi
    {
        return $this->mock(BasketsCommonApi::class);
    }

    public function mockBasketsApi(): MockInterface|BasketsApi
    {
        return $this->mock(BasketsApi::class);
    }

    public function mockBasketItemsApi(): MockInterface|BasketItemsApi
    {
        return $this->mock(BasketItemsApi::class);
    }
    // endregion


    // =============== Units ===============

    // region service Admin Auth
    public function mockAdminAuthOauthApi(): MockInterface|OauthApi
    {
        return $this->mock(OauthApi::class)->allows([
            'getConfig' => resolve(Configuration::class),
        ]);
    }

    public function mockAdminAuthUsersApi(): MockInterface|AdminAuthUsersApi
    {
        return $this->mock(AdminAuthUsersApi::class)->allows([
            'getConfig' => resolve(Configuration::class),
        ]);
    }

    public function mockAdminAuthEnumsApi(): MockInterface|AdminAuthEnumsApi
    {
        return $this->mock(AdminAuthEnumsApi::class);
    }

    public function mockAdminAuthUserRolesApi(): MockInterface|AdminAuthUserRolesApi
    {
        return $this->mock(AdminAuthUserRolesApi::class);
    }
    // endregion

    // region service Seller Auth

    public function mockSellerAuthUsersApi(): MockInterface|SellerAuthUsersApi
    {
        return $this->mock(SellerAuthUsersApi::class);
    }

    // endregion

    // region service Business Units
    protected function mockBusinessUnitsSellersApi(): MockInterface|SellersApi
    {
        return $this->mock(SellersApi::class);
    }

    protected function mockBusinessUnitsStoresApi(): MockInterface|StoresApi
    {
        return $this->mock(StoresApi::class);
    }

    protected function mockBusinessUnitsStoreContactsApi(): MockInterface|StoreContactsApi
    {
        return $this->mock(StoreContactsApi::class);
    }

    protected function mockBusinessUnitsStoreWorkingsApi(): MockInterface|StoreWorkingsApi
    {
        return $this->mock(StoreWorkingsApi::class);
    }

    protected function mockBusinessUnitsStorePickupTimesApi(): MockInterface|StorePickupTimesApi
    {
        return $this->mock(StorePickupTimesApi::class);
    }
    // endregion

    // =============== Communication ===============

    // region service Communication
    protected function mockCommunicationNotificationsApi(): MockInterface|NotificationsApi
    {
        return $this->mock(NotificationsApi::class);
    }
    // endregion
}
